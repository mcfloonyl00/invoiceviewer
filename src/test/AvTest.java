/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import by.avest.crypto.pkcs11.provider.universal.AvPersonalKeyListKeyStore;
import by.avest.crypto.pkcs11.provider.universal.AvPersonalKeyStore;
import by.avest.crypto.pkcs11.provider.universal.KeyListCallbackHandler;
import by.avest.crypto.pkcs11.provider.universal.KeyListLoadParameter;
import java.io.IOException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Date;
import java.util.Enumeration;
import sun.security.x509.X509CertImpl;

/**
 *
 * @author avolkov
 */
public class AvTest {
    
    static {
        System.setProperty("by.avest.loader.shared","true");
        System.setProperty("java.library.path", "C:\\Program Files\\Avest\\AvJCEProv\\win64;");
        System.setProperty("classpath", ".\\lib\\*;C:\\Program Files\\Avest\\AvJCEProv\\*");
    }
    
    public final static void main(String[] args) {
        try {
            AvPersonalKeyListKeyStore keyList = getKeyList();
            AvPersonalKeyStore keyStore = getKeyStore();
            Enumeration<String> aliasKeyList = keyList.engineAliases();
            while(aliasKeyList.hasMoreElements()) {
                String value = aliasKeyList.nextElement();
                if(keyStore.engineContainsAlias(value)) {
                    X509CertImpl cert = (X509CertImpl) keyStore.engineGetCertificate(value);
                    if(cert.getNotAfter().after(new Date())) {
                        System.out.println("certificate "+value+" is invalid");
                    }
                }
            }
            System.out.println("-------");
            
            Enumeration<String> aliasKeyStore = keyStore.engineAliases();
            while(aliasKeyStore.hasMoreElements()) {
                String value = aliasKeyStore.nextElement();
                X509CertImpl cert = (X509CertImpl) keyStore.engineGetCertificate(value);
                System.out.println("certificate "+value+" : serial number "+cert.getSerialNumber());
            }
            
        } catch (IOException | NoSuchAlgorithmException | CertificateException ex) {
            System.err.println(ex.getLocalizedMessage());
        }
    }
    
    private static AvPersonalKeyListKeyStore getKeyList() throws IOException, NoSuchAlgorithmException, CertificateException, CertificateException {
        AvPersonalKeyListKeyStore keyStore = new AvPersonalKeyListKeyStore();
        keyStore.engineLoad(new KeyListLoadParameter(new KeyStore.CallbackHandlerProtection(new KeyListCallbackHandler())));
        return keyStore;
    }
    
    private static AvPersonalKeyStore getKeyStore() throws IOException, NoSuchAlgorithmException, CertificateException, CertificateException {
        AvPersonalKeyStore keyStore = new AvPersonalKeyStore();
        keyStore.engineLoad(null);
        return keyStore;
    }
}
