
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author avolkov
 */
public class CreateXmlOfCsv {
    
    public static void main(String[] args) {
        try {
            createBranch("Filial.csv");
            createOcei("Okei.csv");
            createOced("Oked.csv");
            createCountry("Stran.csv");
            createImns("Imns.csv");
            createTnved("Tnved.csv");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException | ParserConfigurationException | TransformerConfigurationException ex) {
            ex.printStackTrace();
        } catch (TransformerException ex) {
            ex.printStackTrace();
        }
    }
    
    public static void createBranch(String filename) throws FileNotFoundException, IOException, ParserConfigurationException, TransformerConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.newDocument();
        
        Iterable<CSVRecord> records = CSVFormat.RFC4180
                .withDelimiter(';')
                .withHeader("UNP_G", "NOM_SF", "VNAIMP_F", "VNAIMK_F", "INDEX", "ADRES", "CKODSOST_F", "DREG", "DLIKV", "DOP")
                .parse(new InputStreamReader(CreateXmlOfCsv.class.getResourceAsStream(filename), "windows-1251"));
        
        Element element = document.createElement("branch");
        for(CSVRecord record : records) {
            Element xmlRecord = document.createElement("record");
            if(!record.get("UNP_G").equals("UNP_G")) {
                xmlRecord.setAttribute("unp", record.get("UNP_G"));
            }
            
            if(!record.get("NOM_SF").equals("NOM_SF")) {
                xmlRecord.setAttribute("num", record.get("NOM_SF"));
            }
            
            if(!record.get("VNAIMP_F").equals("VNAIMP_F")) {
                xmlRecord.setAttribute("fullName", record.get("VNAIMP_F"));
            }
            
            if(!record.get("VNAIMK_F").equals("VNAIMK_F")) {
                xmlRecord.setAttribute("shortName", record.get("VNAIMK_F"));
            }
            
            if(!record.get("INDEX").equals("INDEX")) {
                xmlRecord.setAttribute("postIndex", record.get("INDEX"));
            }
            
            if(!record.get("ADRES").equals("ADRES")) {
                xmlRecord.setAttribute("address", record.get("ADRES"));
            }
            
            if(!record.get("CKODSOST_F").equals("CKODSOST_F")) {
                xmlRecord.setAttribute("status", record.get("CKODSOST_F"));
            }
            
            if(!record.get("DREG").equals("DREG")) {
                xmlRecord.setAttribute("dateReg", record.get("DREG"));
            }
            
            if(!record.get("DLIKV").equals("DLIKV")) {
                xmlRecord.setAttribute("dateClose", record.get("DLIKV"));
            }
            
            if(!record.get("DOP").equals("DOP")) {
                xmlRecord.setAttribute("dateEdit", record.get("DOP"));
            }
            
            element.appendChild(xmlRecord);
        }
        
        document.appendChild(element);
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        FileOutputStream stream = new FileOutputStream("C:\\Branch.xml");
        StreamResult result = new StreamResult(stream);
        transformer.transform(new DOMSource(document), result);
    }
    
    public static void createOcei(String filename) throws FileNotFoundException, IOException, ParserConfigurationException, TransformerConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.newDocument();
        
        Iterable<CSVRecord> records = CSVFormat.RFC4180
                .withDelimiter(';')
                .withHeader("id", "fullName", "shortName")
                .parse(new InputStreamReader(CreateXmlOfCsv.class.getResourceAsStream(filename), "windows-1251"));
        
        Element element = document.createElement("ocei");
        for(CSVRecord record : records) {
            Element xmlRecord = document.createElement("record");
            if(!record.get("id").equals("id")) {
                xmlRecord.setAttribute("id", record.get("id"));
            }
            
            if(!record.get("fullName").equals("fullName")) {
                xmlRecord.setAttribute("fullName", record.get("fullName"));
            }
            
            if(!record.get("shortName").equals("shortName")) {
                xmlRecord.setAttribute("shortName", record.get("shortName"));
            }
           
            element.appendChild(xmlRecord);
        }
        
        document.appendChild(element);
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        FileOutputStream stream = new FileOutputStream("C:\\Ocei.xml");
        StreamResult result = new StreamResult(stream);
        transformer.transform(new DOMSource(document), result);
    }

    public static void createOced(String filename) throws FileNotFoundException, IOException, ParserConfigurationException, TransformerConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.newDocument();
        
        Iterable<CSVRecord> records = CSVFormat.RFC4180
                .withDelimiter(';')
                .withHeader("id", "name", "dateFrom", "dateTo")
                .withIgnoreSurroundingSpaces(false)
                .parse(new InputStreamReader(CreateXmlOfCsv.class.getResourceAsStream(filename), "windows-1251"));
        
        Element element = document.createElement("oced");
        for(CSVRecord record : records) {
            Element xmlRecord = document.createElement("record");
            if(!record.get("id").equals("id")) {
                xmlRecord.setAttribute("id", record.get("id"));
            }
            
            if(!record.get("name").equals("name")) {
                xmlRecord.setAttribute("name", record.get("name"));
            }
            
            if(!record.get("dateFrom").equals("dateFrom")) {
                xmlRecord.setAttribute("dateFrom", record.get("dateFrom"));
            }
           
            if(!record.get("dateTo").equals("dateTo")) {
                xmlRecord.setAttribute("dateTo", record.get("dateTo"));
            }
            
            element.appendChild(xmlRecord);
        }
        
        document.appendChild(element);
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        FileOutputStream stream = new FileOutputStream("C:\\Oced.xml");
        StreamResult result = new StreamResult(stream);
        transformer.transform(new DOMSource(document), result);
    }
    
    public static void createCountry(String filename) throws FileNotFoundException, IOException, ParserConfigurationException, TransformerConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.newDocument();
        
        Iterable<CSVRecord> records = CSVFormat.RFC4180
                .withDelimiter(';')
                .withHeader("id", "name")
                .withIgnoreSurroundingSpaces(false)
                .parse(new InputStreamReader(CreateXmlOfCsv.class.getResourceAsStream(filename), "windows-1251"));
        
        Element element = document.createElement("country");
        for(CSVRecord record : records) {
            Element xmlRecord = document.createElement("record");
            if(!record.get("id").equals("id")) {
                xmlRecord.setAttribute("id", record.get("id"));
            }
            
            if(!record.get("name").equals("name")) {
                xmlRecord.setAttribute("name", record.get("name"));
            }
            
            element.appendChild(xmlRecord);
        }
        
        document.appendChild(element);
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        FileOutputStream stream = new FileOutputStream("C:\\Country.xml");
        StreamResult result = new StreamResult(stream);
        transformer.transform(new DOMSource(document), result);
    }
    
    public static void createImns(String filename) throws FileNotFoundException, IOException, ParserConfigurationException, TransformerConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.newDocument();
        
        Iterable<CSVRecord> records = CSVFormat.RFC4180
                .withDelimiter(';')
                .withHeader("id", "shortName", "fullName", "unp")
                .withIgnoreSurroundingSpaces(false)
                .parse(new InputStreamReader(CreateXmlOfCsv.class.getResourceAsStream(filename), "windows-1251"));
        
        Element element = document.createElement("inspection");
        for(CSVRecord record : records) {
            Element xmlRecord = document.createElement("record");
            if(!record.get("id").equals("id")) {
                xmlRecord.setAttribute("id", record.get("id"));
            }
            
            if(!record.get("shortName").equals("shortName")) {
                xmlRecord.setAttribute("shortName", record.get("shortName"));
            }
            
            if(!record.get("fullName").equals("fullName")) {
                xmlRecord.setAttribute("fullName", record.get("fullName"));
            }
            
            if(!record.get("unp").equals("unp")) {
                xmlRecord.setAttribute("unp", record.get("unp"));
            }
            
            element.appendChild(xmlRecord);
        }
        
        document.appendChild(element);
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        FileOutputStream stream = new FileOutputStream("C:\\Inspection.xml");
        StreamResult result = new StreamResult(stream);
        transformer.transform(new DOMSource(document), result);
    }
    
    public static void createTnved(String filename) throws FileNotFoundException, IOException, ParserConfigurationException, TransformerConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.newDocument();
        
        Iterable<CSVRecord> records = CSVFormat.RFC4180
                .withDelimiter(';')
                .withHeader("id", "name", "dateFrom", "dateTo")
                .withIgnoreSurroundingSpaces(false)
                .parse(new InputStreamReader(CreateXmlOfCsv.class.getResourceAsStream(filename), "windows-1251"));
        
        Element element = document.createElement("nomenclature");
        for(CSVRecord record : records) {
            Element xmlRecord = document.createElement("record");
            if(!record.get("id").equals("id")) {
                xmlRecord.setAttribute("id", record.get("id"));
            }
            
            if(!record.get("name").equals("name")) {
                xmlRecord.setAttribute("name", record.get("name"));
            }
            
            if(!record.get("dateFrom").equals("dateFrom")) {
                xmlRecord.setAttribute("dateFrom", record.get("dateFrom"));
            }
            
            if(!record.get("dateTo").equals("dateTo")) {
                xmlRecord.setAttribute("dateTo", record.get("dateTo"));
            }
            
            element.appendChild(xmlRecord);
        }
        
        document.appendChild(element);
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        FileOutputStream stream = new FileOutputStream("C:\\CommondityNomenclature.xml");
        StreamResult result = new StreamResult(stream);
        transformer.transform(new DOMSource(document), result);
    }
}
