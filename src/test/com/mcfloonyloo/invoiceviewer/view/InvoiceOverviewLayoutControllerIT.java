/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.view;

import com.mcfloonyloo.invoiceviewer.MainApp;
import com.mcfloonyloo.invoiceviewer.model.invoice.Invoice;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.junit.Assert;
import org.junit.Test;
import org.testfx.framework.junit.ApplicationTest;

/**
 *
 * @author avolkov
 */
public class InvoiceOverviewLayoutControllerIT extends ApplicationTest {

    private FXMLLoader loader;
    private InvoiceOverviewLayoutController controller;
    private AnchorPane pane;
    
    @Override
    public void start(Stage stage) throws Exception {
        this.loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("view/InvoiceOverviewLayout.fxml"));
        this.pane = (AnchorPane) this.loader.load();
    }
    
    @Test
    public void testInvoice() {
        System.out.print("Тест заполнения списка секций при наличии ЭСЧФ: ");
        this.controller = this.loader.getController();
        Invoice invoice = new Invoice();
        this.controller.init(invoice);
        Assert.assertTrue(this.controller.getListPanes().size() > 0);
        System.out.println(this.controller.getListPanes().size()+" секций прочитано");
    }
    
    @Test(expected = IllegalStateException.class)
    public void testNoInvoice() {
        System.out.print("Тест заполнения списка секций при отсутствии ЭСЧФ : ");
        this.controller = this.loader.getController();
        this.controller.init(null);
        Assert.assertTrue(this.controller.getListPanes().isEmpty());
        System.out.println(this.controller.getListPanes().size()+" секций прочитано");
    }   
    
    
}
