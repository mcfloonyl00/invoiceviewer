/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author avolkov
 */
public class LocalDateTimeTest {
    
    public static void main(String[] args) {
        String datetime = "2018-06-04T16:35:25";
        LocalDateTime localDateTime = LocalDateTime.parse(datetime);
        String formatted = localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        System.out.println(formatted);
        LocalDateTime formattedLdt = LocalDateTime.parse(formatted, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }
    
}
