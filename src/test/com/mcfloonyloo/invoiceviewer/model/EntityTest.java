/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model;

import java.sql.Types;
import org.apache.cayenne.map.DataMap;
import org.apache.cayenne.map.DbAttribute;
import org.apache.cayenne.map.DbEntity;

/**
 *
 * @author avolkov
 */
public class EntityTest {
    
    private DataMap map;
    
    public static void main(String args[]) {
        DataMap map = new DataMap("OrgMap");
        map.setDefaultPackage("com.mcfloonyloo.invoiceviewer.model.certificate.db");
       
        
        DbAttribute alias = new DbAttribute("alias");
        alias.setMandatory(true);
        alias.setType(Types.NVARCHAR);
        alias.setMaxLength(255);
        
        
        DbEntity entity = new DbEntity("KeyPreloader");
        entity.addAttribute(alias);
        
    }
    
}
