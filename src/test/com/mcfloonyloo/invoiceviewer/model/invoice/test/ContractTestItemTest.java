/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.test;

import com.mcfloonyloo.invoiceviewer.model.invoice.Invoice;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author avolkov
 */
public class ContractTestItemTest {
    
    public ContractTestItemTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testTest() {
        Invoice invoice = new Invoice();
        ContractTestItem test = new ContractTestItem(invoice);
        test.test();
        ErrorList errors = test.getErrors();
        ErrorTestItem item = new ErrorTestItem();
        item.setErrors(errors.getList());
        System.out.println(item.toError());
    }
}
