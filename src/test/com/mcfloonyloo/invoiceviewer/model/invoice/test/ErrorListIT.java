/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.test;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author avolkov
 */
public class ErrorListIT {
    
    public ErrorListIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        System.out.println("Старт теста");
    }
    
    @After
    public void tearDown() {
        System.out.println("Конец теста\n---");
    }

    @Test
    public void testErrorList() {
        ErrorList list = new ErrorList();
        ErrorItem error = null;
        System.out.println("Тест 1 : ввод пустого элемента - "+list.add(error)+", количество элементов в списке - "+list.size());
        error = new ErrorItem(null).setFieldName("Тестовое поле");
        System.out.println("Тест 2 : "+error.getFieldName()+" - ввод элемента с пустым значением - "+list.add(error)+", количество элементов в списке - "+list.size());
        ErrorItem fullError = new ErrorItem("Ошибка").setFieldName("Тестовое поле с текстом");
        System.out.println("Тест 3 : "+fullError.getFieldName()+" - ввод элемента с пустым значением - "+list.add(fullError)+", количество элементов в списке - "+list.size());
        
    }
    
}
