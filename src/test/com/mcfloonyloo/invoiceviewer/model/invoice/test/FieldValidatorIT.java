/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.test;

import com.mcfloonyloo.invoiceviewer.model.invoice.validator.Comparison;
import com.mcfloonyloo.invoiceviewer.model.invoice.DefaultInvoiceSection;
import com.mcfloonyloo.invoiceviewer.model.invoice.DefaultInvoiceSubSection;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.FieldValidator;
import com.mcfloonyloo.invoiceviewer.model.invoice.Invoice;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Optional;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author avolkov
 */
public class FieldValidatorIT {
    
    FieldValidator test;
    Invoice invoice;
    
    public FieldValidatorIT() {
        this.invoice = new Invoice();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        this.test = new FieldValidator();
        System.out.print("Старт теста");
    }
    
    @After
    public void tearDown() {
        System.out.println("Конец теста\n---\n");
    }
    
    private String testPrint(ErrorItem error) {
        if(Optional.ofNullable(error.getMessage()).isPresent()) {
            StringBuilder builder = new StringBuilder(error.getFieldName()).append(". ");
            return builder.append(error.getMessage()).toString(); 
        } else {
            StringBuilder builder = new StringBuilder(error.getFieldName()).append(". ");
            return builder.append("ошибок нет").toString();
        }
    }
    

    @Test
    public void testValidateList() {
        System.out.println(" validate list");
        ArrayList<String> list = null;
        System.out.println("Тест 1 : " + testPrint(this.test.validateList(list).setFieldName("list")));
        list = new ArrayList<>();
        System.out.println("Тест 2 : " + testPrint(this.test.validateList(list).setFieldName("list")));
        list.add(" ");
        System.out.println("Тест 3 : " + testPrint(this.test.validateList(list).setFieldName("list")));
    }
    
    @Test
    public void testValidateValue() {
        System.out.println(" validate value");
        String field = null;
        System.out.println("Тест 1 : " + testPrint(this.test.validateField(field).setFieldName("field")));
        field = "";
        System.out.println("Тест 2 : " + testPrint(this.test.validateField(field).setFieldName("field")));
        field = "1";
        System.out.println("Тест 3 : " + testPrint(this.test.validateField(field).setFieldName("field")));
    }
    
    @Test
    public void testValidateFieldValue() {
        System.out.println(" validate field value");
        String field = null;
        System.out.println("Тест 1 : "+testPrint(this.test.validateFieldValue(field, "test").setFieldName("field")));
        field = "";
        System.out.println("Тест 2 : " + testPrint(this.test.validateFieldValue(field, "test").setFieldName("field")));
        field = "1";
        System.out.println("Тест 3 : " + testPrint(this.test.validateFieldValue(field, "test").setFieldName("field")));
        field = "test";
        System.out.println("Тест 4 : " + testPrint(this.test.validateFieldValue(field, "test").setFieldName("field")));
    }
    
    @Test
    public void testValidateSection() {
        System.out.println(" validate section");
        DefaultInvoiceSection section = invoice.getGeneral();
        DefaultInvoiceSubSection subsection = invoice.getDeliveryCondition().getContract();
        System.out.println("Тест 1 : "+testPrint(this.test.validateSection(null).setFieldName("пустая секция")));
        System.out.println("Тест 2 : "+testPrint(this.test.validateSection(section).setFieldName("DefaultInvoiceSection")));
        System.out.println("Тест 3 : "+testPrint(this.test.validateSection(subsection).setFieldName("DefaultInvoiceSubSection")));
    }
    
    @Test
    public void testValidateCompareValues() {
        System.out.println(" validate compare values");
        BigDecimal field = null;
        BigDecimal valueMore = new BigDecimal(10.3);
        BigDecimal valueLess = new BigDecimal(10.1);
        BigDecimal valueEqual = new BigDecimal(10.2);
        System.out.println("Тест 1 : "+testPrint(this.test.validateCompareValues(field, valueMore, Comparison.MORE).setFieldName("field")));
        field = new BigDecimal(10.2);
        System.out.println("-");
        System.out.println("Тест 2 : "+testPrint(this.test.validateCompareValues(field, null, Comparison.MORE).setFieldName("field")));
        System.out.println("-");
        System.out.println("Тест 31 : "+testPrint(this.test.validateCompareValues(field, valueMore, Comparison.MORE).setFieldName("field")));
        System.out.println("Тест 3.2 : "+testPrint(this.test.validateCompareValues(field, valueMore, Comparison.LESS).setFieldName("field")));
        System.out.println("Тест 3.3 : "+testPrint(this.test.validateCompareValues(field, valueMore, Comparison.EQUAL).setFieldName("field")));
        System.out.println("-");
        System.out.println("Тест 4.1 : "+testPrint(this.test.validateCompareValues(field, valueLess, Comparison.MORE).setFieldName("field")));
        System.out.println("Тест 4.2 : "+testPrint(this.test.validateCompareValues(field, valueLess, Comparison.LESS).setFieldName("field")));
        System.out.println("Тест 4.3 : "+testPrint(this.test.validateCompareValues(field, valueLess, Comparison.EQUAL).setFieldName("field")));
        System.out.println("-");
        System.out.println("Тест 5.1 : "+testPrint(this.test.validateCompareValues(field, valueEqual, Comparison.MORE).setFieldName("field")));
        System.out.println("Тест 5.2 : "+testPrint(this.test.validateCompareValues(field, valueEqual, Comparison.LESS).setFieldName("field")));
        System.out.println("Тест 5.3 : "+testPrint(this.test.validateCompareValues(field, valueEqual, Comparison.EQUAL).setFieldName("field")));
    }
    
    @Test
    public void testValidateCompareLenghts() {
        System.out.println(" validate compare lenghts");
        String field = null;
        int lenghtMore = 5;
        int lenghtLess = 3;
        int lenghtEqual = 4;
        System.out.println("Тест 1 : "+testPrint(this.test.validateCompareLenghts(field, lenghtMore, Comparison.MORE).setFieldName("field")));
        field = "test";
        System.out.println("-");
        System.out.println("Тест 2.1 : "+testPrint(this.test.validateCompareLenghts(field, 0, Comparison.MORE).setFieldName("field")));
        System.out.println("Тест 2.2 : "+testPrint(this.test.validateCompareLenghts(field, -1, Comparison.MORE).setFieldName("field")));
        System.out.println("-");
        System.out.println("Тест 3.1 : "+testPrint(this.test.validateCompareLenghts(field, lenghtMore, Comparison.MORE).setFieldName("field")));
        System.out.println("Тест 3.2 : "+testPrint(this.test.validateCompareLenghts(field, lenghtMore, Comparison.LESS).setFieldName("field")));
        System.out.println("Тест 3.3 : "+testPrint(this.test.validateCompareLenghts(field, lenghtMore, Comparison.EQUAL).setFieldName("field")));
        System.out.println("-");
        System.out.println("Тест 4.1 : "+testPrint(this.test.validateCompareLenghts(field, lenghtLess, Comparison.MORE).setFieldName("field")));
        System.out.println("Тест 4.2 : "+testPrint(this.test.validateCompareLenghts(field, lenghtLess, Comparison.LESS).setFieldName("field")));
        System.out.println("Тест 4.3 : "+testPrint(this.test.validateCompareLenghts(field, lenghtLess, Comparison.EQUAL).setFieldName("field")));
        System.out.println("-");
        System.out.println("Тест 5.1 : "+testPrint(this.test.validateCompareLenghts(field, lenghtEqual, Comparison.MORE).setFieldName("field")));
        System.out.println("Тест 5.2 : "+testPrint(this.test.validateCompareLenghts(field, lenghtEqual, Comparison.LESS).setFieldName("field")));
        System.out.println("Тест 5.3 : "+testPrint(this.test.validateCompareLenghts(field, lenghtEqual, Comparison.EQUAL).setFieldName("field")));
    }
    
    @Test
    public void testValidateRange() {
        System.out.println(" validate range");
        String field = null;
        int left = 5;
        int right = 3;
        System.out.println("Тест 1 : "+testPrint(this.test.validateRange(field, left, right).setFieldName("field")));
        right = 5;
        System.out.println("Тест 2 : "+testPrint(this.test.validateRange(field, left, right).setFieldName("field")));
        right = 0;
        System.out.println("Тест 3 : "+testPrint(this.test.validateRange(field, left, right).setFieldName("field")));
        right = 7;
        System.out.println("Тест 4 : "+testPrint(this.test.validateRange(field, left, right).setFieldName("field")));
        field = "test";
        System.out.println("Тест 5 : "+testPrint(this.test.validateRange(field, left, right).setFieldName("field")));
        field = "test01";
        System.out.println("Тест 6 : "+testPrint(this.test.validateRange(field, left, right).setFieldName("field")));
        field = "test0102";
        System.out.println("Тест 7 : "+testPrint(this.test.validateRange(field, left, right).setFieldName("field")));
    }
    
}
