/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.properties;

import com.mcfloonyloo.invoiceviewer.dialog.ExceptionDialog;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import javafx.scene.control.Alert;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author avolkov
 */
public enum AppProperties {
    INSTANCE;
    
    private AppProperties() {
        load();
    }
    
    private static final String PATH_FILE = ".\\resources\\app.properties";
    
    private Stage stage;
    
    private String libraryPath;
    
    private String classPath;
    
    private String serviceUrl;
    
    private String xsdPath;
    
    private String xmldbPath;
    
    private String databasesPath;
    
    private String reportsPath;
    
    public String getLibraryPath() {
        return this.libraryPath;
    }
    
    public String getClassPath() {
        return this.classPath;
    }
    
    public String getServiceUrl() {
        return this.serviceUrl;
    }
    
    public String getXsdPath() {
        return this.xsdPath;
    }
    
    public String getXmldbPath() {
        return this.xmldbPath;
    }
    
    public String getDatabasesPath() {
        return this.databasesPath;
    }
    
    public String getReportsPath() {
        return this.reportsPath;
    }
    
    public void setStage(Stage stage) {
        this.stage = stage;
    }
    
    public void setLibraryPath(String libraryPath) {
        this.libraryPath = libraryPath;
    }
    
    public void setClassPath(String classPath) {
        this.classPath = classPath;
    }
    
    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }
    
    public void setXsdPath(String xsdPath) {
        this.xsdPath = xsdPath;
    }
    
    public void setXmldbPath(String xmldbPath) {
        this.xmldbPath = xmldbPath;
    }
    
    public void setDatabasesPath(String databasePath) {
        this.databasesPath = databasePath;
    }
    
    public void setReportsPath(String reportsPath) {
        this.reportsPath = reportsPath;
    }
    
    public AppProperties load() {
        Properties properties = new Properties();
        File file = new File(PATH_FILE);
        if(!file.exists()) {
            setDefault("Файл "+PATH_FILE+" не обнаружен.\nФайл будет создан со стандартными настройками");
            save();
            return this;
        }
        try(FileInputStream inputStream = new FileInputStream(file)) {
            properties.load(inputStream);
            this.libraryPath = properties.getProperty("path.library");
            this.classPath = properties.getProperty("path.classpath");
            this.serviceUrl = properties.getProperty("url.service");
            this.xsdPath = properties.getProperty("path.xsd");
            this.xmldbPath = properties.getProperty("path.xmldb");
            this.databasesPath = properties.getProperty("path.databases");
            this.reportsPath = properties.getProperty("path.reports");
        } catch (FileNotFoundException ex) {
            createExceptionDialog(ex).showAndWait();
        } catch (IOException ex) {
            createExceptionDialog(ex).showAndWait();
        } finally {
            return this;
        }        
    }
    
    public void save() {
        Properties properties = new Properties();
        properties.setProperty("path.library", this.libraryPath);
        properties.setProperty("path.classpath", this.classPath);
        properties.setProperty("url.service", this.serviceUrl);
        properties.setProperty("path.xsd", this.xsdPath);
        properties.setProperty("path.xmldb", this.xmldbPath);
        properties.setProperty("path.databases", this.databasesPath);
        properties.setProperty("path.reports", this.reportsPath);
        
        File file = new File(PATH_FILE);
        if(!file.getParentFile().exists()) {
            file.getParentFile().mkdir();
            save();
        }
        
        if(!file.exists()) {
            try {
                file.createNewFile();
                save();
            } catch (IOException ex) {
                createExceptionDialog(ex, "Невозможно создать файл настроек.\n").showAndWait();
            }
        } else {
            try(FileOutputStream outputStream = new FileOutputStream(file)) {
                properties.store(outputStream, "");
            } catch (FileNotFoundException ex) {
                createExceptionDialog(ex).showAndWait();
            } catch (IOException ex) {
                createExceptionDialog(ex).showAndWait();
            }
        }
    }
    
    private void setDefault(String content){
        this.libraryPath = "C:\\Program Files\\Avest\\AvJCEProv\\win32;";
        this.classPath = ".\\lib\\*;C:\\Program Files\\Avest\\AvJCEProv\\*;";
        this.serviceUrl = "https://ws.vat.gov.by:443/InvoicesWS/services/InvoicesPort?wsdl";
        this.xsdPath = ".\\xsd\\";
        this.xmldbPath = ".\\resources\\xmldb\\";
        this.databasesPath = ".\\resources\\databases\\";
        this.reportsPath = ".\\reports\\";
        
        String message = "Внимание! Установлены настройки по умолчанию";

        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("АРМ оператора ЭСЧФ");
        alert.setHeaderText(content);
        alert.setContentText(message);
        alert.initOwner(stage);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.show();
    }
    
    private ExceptionDialog createExceptionDialog(Exception ex) {
        String message = ex.getLocalizedMessage();
        ExceptionDialog dialog = new ExceptionDialog(message, ex)
                .setTitle("АРМ оператора ЭСЧФ")
                .setHeaderText(ex.getClass().getName());
        return dialog;
    }
    
    private ExceptionDialog createExceptionDialog(Exception ex, String message) {
        ExceptionDialog dialog = new ExceptionDialog(message, ex)
                .setTitle("АРМ оператора ЭСЧФ")
                .setHeaderText(ex.getClass().getName());
        return dialog;
    }
}


