/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.service;

import by.avest.certstore.AvCertStoreProvider;
import by.avest.crypto.pkcs11.provider.AvestProvider;
import by.avest.crypto.pkcs11.provider.ProviderFactory;
import by.avest.edoc.client.AvDocException;
import by.avest.edoc.client.AvLoginException;
import by.avest.edoc.client.EVatService;
import by.avest.net.tls.AvTLSProvider;
import com.mcfloonyloo.invoiceviewer.dialog.ExceptionDialog;
import com.mcfloonyloo.invoiceviewer.model.preloader.db.OrgPreloaderItem;
import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.ProviderException;
import java.security.Security;
import java.security.cert.CertificateException;
import java.util.Optional;

/**
 *
 * @author avolkov
 */
public class InvoiceServiceAuthorizer {
   
    private String alias;
    
    private AvestProvider provider;
    
    private EVatService service;
    
    private InvoiceKeySelector keySelector;
        
    private boolean isProvider() {
        return Optional.ofNullable(this.provider).isPresent();
    }
    
    private void clearProvider() {
        this.provider = null;
    }
    
    public EVatService getService() {
        return this.service;
    }
    
    public boolean isService() {
        return Optional.ofNullable(this.service).isPresent();
    }
    
    public void clearService() {
        this.service = null;
    }
    
    public boolean isKeySelectorClosed() {
        return this.keySelector.isClosed();
    }
    
    public boolean isKeySelector() {
        return Optional.ofNullable(this.keySelector).isPresent();
    }
    
    public void clearKeySelector() {
        this.keySelector = null;
    }

    public InvoiceServiceAuthorizer(String alias) {
        this.alias = alias;
        try {
            this.provider = ProviderFactory.addAvUniversalProvider();
            Security.addProvider(new AvTLSProvider());
            Security.addProvider(new AvCertStoreProvider());
        } catch (ProviderException ex) {
            clearProvider();
            String message = "Отсутствует провайдер ключа Avest";
            ExceptionDialog dialog = new ExceptionDialog(message, ex)
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText(ex.getClass().toString());
            dialog.showAndWait();
        }
    }
    
    public OrgPreloaderItem authorize(String url) {
        String message = "";
        try {
            this.keySelector = new InvoiceKeySelector(this.alias);            
            if(isProvider()) {
                if(isService()) {
                    message += "Авторизация пользователя уже проведена";
                    close();
                    return null;
                } else {
                    if(isKeySelectorClosed()) {
                        message += "Хранилище ключей закрыто";
                        close();
                        return null;
                    } else {
                        message += "Ошибка авторизации пользователя";
                        this.service = new EVatService(url, this.keySelector);
                        char[] answer = this.keySelector.promptPassword(alias);
                        if(Optional.ofNullable(answer).isPresent()) {
                            this.service.login();
                            String unp2 = this.service.getMyCertProperty("1.2.112.1.2.1.1.1.1.2");
                            String unp101 = this.service.getMyCertProperty("1.3.6.1.4.1.12656.106.101");
                            String orgname = this.service.getMyCertProperty("2.5.4.10");
                            close();
                            if(unp2.length() > 0) {
                                return new OrgPreloaderItem(unp2, orgname);
                            }
                            if(unp101.length() > 0) {
                                return new OrgPreloaderItem(unp101, orgname);
                            }
                            return null;
                        } else {
                            close();
                            return null;
                        }
                    }
                }
            } else {
                message += "Отсутствует провайдер";
                return null;
            }
        } catch (CertificateException | 
                IOException | 
                KeyStoreException | 
                NoSuchAlgorithmException |
                AvDocException | 
                IllegalArgumentException | 
                AvLoginException ex) {
            ExceptionDialog dialog = new ExceptionDialog(message, ex)
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText(ex.getClass().toString());
            dialog.showAndWait();
            //досрочное закрытие сессии
            close();
            return null;
        }
    }    
   
    private void close() {
        if (isProvider()) {
            this.service.logout();
            clearService();
        } //досрочное обнуление подключения
        if(isKeySelector()) {
            clearKeySelector();
        }
    }
    
}
