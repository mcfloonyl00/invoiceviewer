/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import javafx.scene.control.ChoiceDialog;
import javafx.stage.Modality;

/**
 * 
 * @author avolkov
 */
public class InvoiceKeySelectorDialog {
    
    private final ChoiceDialog<String> dialog;
    
    private final ArrayList<String> choices = new ArrayList<>();
    
    public InvoiceKeySelectorDialog setTitle(String title) {
        this.dialog.setTitle(title);
        return this;
    }
    
    public InvoiceKeySelectorDialog setHeaderText(String headerText) {
        this.dialog.setHeaderText(headerText);
        return this;
    }
    
    public Optional<String> getResult() {
        return this.dialog.showAndWait();
    }
    
    public InvoiceKeySelectorDialog(String[] choices) {
        this.choices.clear();
        this.choices.addAll(Arrays.asList(choices));
        
        this.dialog = new ChoiceDialog<>("Выберите ключ из списка...", choices);
        this.dialog.initModality(Modality.APPLICATION_MODAL);
    }
    
}
