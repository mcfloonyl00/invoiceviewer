/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.service;

import by.avest.edoc.client.PersonalKeyManager;
import by.avest.edoc.tool.ToolException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.stage.Modality;

/**
 *
 * @author avolkov
 */
public class InvoiceKeySelector extends PersonalKeyManager {
    
    private char[] password;

    private boolean closed = false;
    
    private String alias;
    
    public boolean isClosed() {
        return this.closed;        
    }
    
    public void open() {
        this.closed = false;
    }
    
    public void close() {
        this.closed = true;
    }
    
    public InvoiceKeySelector(KeyStore ks) {
        super(ks);
    }
        
    public InvoiceKeySelector(String alias) throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException {
        super(getDefaultKS());
        this.alias = alias;
    }
    
    public InvoiceKeySelector() throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException {
        super(getDefaultKS());
    }

    private static KeyStore getDefaultKS() throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException {
        KeyStore keyStore = KeyStore.getInstance("AvPersonal");
        keyStore.load(null, null);
        return keyStore;
    }
    
    @Override
    public String chooseAlias(String[] aliases) throws IOException {
        if(Optional.ofNullable(this.alias).isPresent()) {
            int index = Arrays.asList(aliases).indexOf(this.alias);
            return aliases[index];
        }
        InvoiceKeySelectorDialog dialog = new InvoiceKeySelectorDialog(aliases)
                .setTitle("АРМ оператора ЭСЧФ")
                .setHeaderText("Выберите ключ для авторизации");
        Optional<String> result = dialog.getResult();
        if(result.isPresent()) {
            int index = Arrays.asList(aliases).indexOf(result.get());
            return aliases[index];
        } else {
            close();
            String message = "Авторизация отменена";
            try {
                throw new ToolException(message);
            } catch (ToolException ex) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("АРМ оператора ЭСЧФ");
                alert.setHeaderText("Внимание!");
                alert.setContentText(ex.getLocalizedMessage());
                alert.showAndWait();
                return null;
            }
        }
    }

    @Override
    public char[] promptPassword(String alias) throws IOException {
        if(Optional.ofNullable(this.password).isPresent()) {
            char[] innerPassword = this.password;
            this.password = null;
            return innerPassword;
        }
            
        this.password = promptPasswordInternal(alias);
        return this.password;
    }
    
    //проверяет введенный пароль на корректность
    private char[] promptPasswordInternal(String request) {
        char[] answer = promptForPassword(request);
        
        if(!Optional.ofNullable(answer).isPresent()) {
            return null;
        }
                
        if(answer.length < 8) {
            String message = "Минимальная длина пароля 8 символов, повторите ввод пароля";
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("АРМ оператора ЭСЧФ");
            alert.setHeaderText("Внимание!");
            alert.setContentText(message);
            alert.initModality(Modality.APPLICATION_MODAL);
            alert.showAndWait();
            return promptPasswordInternal(request);
        }
        
        return answer;        
    }
    
    private char[] promptForPassword(String request) {
        String message = new StringBuilder("Запрос пароля для ключа [")
                .append(request)
                .append("]").toString();
        
        PasswordDialog dialog = new PasswordDialog()
                .setTitle("АРМ оператора ЭСЧФ")
                .setHeaderText(message)
                .setContentText("Пароль: ");       
        Optional<String> result = dialog.getResult();
        if(result.isPresent() && result.get().length() > 0) {
            return result.get().toCharArray();
        } else {
            return null;
        }
    }
       
}
