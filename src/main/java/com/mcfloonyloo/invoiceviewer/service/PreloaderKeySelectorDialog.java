/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.service;

import com.mcfloonyloo.invoiceviewer.model.preloader.db.KeyPreloaderDTO;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import javafx.scene.control.ChoiceDialog;
import javafx.stage.Modality;

/**
 *
 * @author avolkov
 */
public class PreloaderKeySelectorDialog {
    
    private ChoiceDialog<String> dialog;
    
    private ArrayList<String> choices;

    public Optional<String> getResult() {
        return this.dialog.showAndWait();
    }
    
    public PreloaderKeySelectorDialog(String orgName, String[] choices) {
        this.choices = new ArrayList<>();
        this.choices.clear();
        this.choices.addAll(Arrays.asList(choices));
        
        this.dialog = createDialog(orgName);
        this.dialog.getItems().addAll(choices);
    }
    
    public PreloaderKeySelectorDialog(String orgName, List<KeyPreloaderDTO> keys) {
        this.choices = new ArrayList<>();
        this.choices.clear();
        this.choices.addAll(getChoicesOfKeys(keys));
        
        this.dialog = createDialog(orgName);
        this.dialog.getItems().addAll(getChoicesOfKeys(keys));
    }
    
    private ChoiceDialog<String> createDialog(String orgName) {
        ChoiceDialog<String> choiseDialog = new ChoiceDialog<>();
        choiseDialog.setTitle("АРМ оператора ЭСЧФ");
        choiseDialog.setHeaderText("Выберите ключ из списка ключей, зарегистрированных в\n"
                + ""+orgName);
        choiseDialog.initModality(Modality.APPLICATION_MODAL);
        return choiseDialog;
    }
    
    private List<String> getChoicesOfKeys(List<KeyPreloaderDTO> keys) {
        List<String> choiseList = new ArrayList<>();
        
        for(int index = 0; index < keys.size(); index++) {
            choiseList.add(keys.get(index).getAlias());
        }
        
        return choiseList;
    }
    
}
