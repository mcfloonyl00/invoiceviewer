/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.service;

import by.avest.certstore.AvCertStoreProvider;
import by.avest.crypto.pkcs11.provider.AvestProvider;
import by.avest.crypto.pkcs11.provider.ProviderFactory;
import by.avest.edoc.client.AvDocException;
import by.avest.edoc.client.AvLoginException;
import by.avest.edoc.client.EVatService;
import by.avest.net.tls.AvTLSProvider;
import com.mcfloonyloo.invoiceviewer.dialog.ExceptionDialog;
import com.mcfloonyloo.invoiceviewer.properties.AppProperties;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.ProviderException;
import java.security.Security;
import java.security.cert.CertificateException;
import java.util.Optional;

/**
 *
 * @author avolkov
 */
public enum InvoiceService {
    INSTANCE;
    
    private InvoiceService() {
        setAuth(false);
        disconnection();
        
        try {
            this.provider = ProviderFactory.addAvUniversalProvider();
            Security.addProvider(new AvTLSProvider());
            Security.addProvider(new AvCertStoreProvider());
        } catch (ProviderException ex) {
            clearProvider();
            String message = "Отсутствует провайдер ключа Avest";
            ExceptionDialog dialog = new ExceptionDialog(message, ex)
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText(ex.getClass().toString());
            dialog.showAndWait();
        }
    }
    
    private AvestProvider provider;
    
    private EVatService service;
    
    private InvoiceKeySelector keySelector;
        
    private boolean auth = false;
    
    private boolean connection = false;
    
    
    private boolean isProvider() {
        return Optional.ofNullable(this.provider).isPresent();
    }
    
    private void clearProvider() {
        this.provider = null;
    }
    
    public EVatService getService() {
        return this.service;
    }
    
    public boolean isService() {
        return Optional.ofNullable(this.service).isPresent();
    }
    
    public void clearService() {
        this.service = null;
    }
    
    public boolean isKeySelectorClosed() {
        return this.keySelector.isClosed();
    }
    
    public boolean isKeySelector() {
        return Optional.ofNullable(this.keySelector).isPresent();
    }
    
    public void clearKeySelector() {
        this.keySelector = null;
    }

    public boolean isAuth() {
        return this.auth;
    }
    
    public void setAuth(boolean auth) {
        this.auth = auth;
    }
    
    public boolean isConnected() {
        return this.connection;
    }
    
    public void connection() {
        this.connection = true;
    }
    
    public void disconnection() {
        this.connection = false;
    }
    
    public boolean authorize(String alias) {
        String message = "";
        try {
            this.keySelector = new InvoiceKeySelector(alias);
            if(isProvider()) {
                if(isService()) {
                    message += "Авторизация пользователя уже проведена";
                    close();
                    return false;
                } else {
                    if(isKeySelectorClosed()) {
                        message += "Хранилище ключей закрыто";
                        close();
                        return false;
                    } else {
                        message += "Ошибка авторизации пользователя";
                        this.service = new EVatService(AppProperties.INSTANCE.getServiceUrl().trim(), this.keySelector);
                        char[] answer = this.keySelector.promptPassword(alias);
                        if(Optional.ofNullable(answer).isPresent()) {
                            if(answer.length > 0) {
                                this.service.login();
                                setAuth(true);
                            } else {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    }
                }
            } else {
                message += "Отсутствует провайдер";
                return false;
            }
        } catch (CertificateException | 
                IOException | 
                KeyStoreException | 
                NoSuchAlgorithmException |
                AvDocException | 
                IllegalArgumentException | 
                AvLoginException ex) {
            ExceptionDialog dialog = new ExceptionDialog(message, ex)
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText(ex.getClass().toString());
            dialog.showAndWait();
            //досрочное закрытие сессии
            close();
            return false;
        }
        return true;
    }    
    
    public boolean connect() {
        String message = "";
        try {
            if(isService() && isAuth()) {
                if(isConnected()) {
                    message += "Подключение уже выполнено";
                } else {
                    this.service.connect();
                    connection();
                }
            } else {
                message += "Пользователь не авторизован";
            }
        } catch (KeyManagementException | 
                InvalidAlgorithmParameterException | 
                NoSuchAlgorithmException | 
                KeyStoreException | 
                CertificateException | 
                IOException | 
                AvDocException ex) {
            ExceptionDialog dialog = new ExceptionDialog(message, ex)
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText(ex.getClass().toString() + " : " + ex.getLocalizedMessage());
            dialog.showAndWait();
            //досрочное закрытие сессии
            close();
            return false;
        } 
        return true;
    }
    
    public boolean disconnect() {
        String message = "";
        try {
            if(isService() && isAuth()) {
                if(isConnected()) {
                    this.service.disconnect();
                    disconnection();
                } else {
                    message += "Отсутствует подключение к сервису";
                }
            } else {
                message += "Пользователь не авторизован";
            }
        } catch (IOException ex) {
            ExceptionDialog dialog = new ExceptionDialog(message, ex)
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText(ex.getClass().toString());
            dialog.showAndWait();
            //досрочное закрытие сессии                    
            close();
            return false;
        }
        return true;
    }
    
    private int indexConnection = 0;
   
    public boolean close() {
        try {
            if(isProvider()) {
                if(isConnected()) {
                    this.service.disconnect();
                    disconnection();
                }
                if(isAuth()) {
                    this.service.logout();
                    setAuth(false);
                }
                clearService();
            }
            
            if(isKeySelector()) {
                clearKeySelector();
            }
        } catch (IOException ex) {
            ExceptionDialog dialog = new ExceptionDialog(ex.getLocalizedMessage(), ex)
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText(ex.getClass().toString());
            dialog.showAndWait();
            //досрочное обнуление подключения
            disconnection();
            while(indexConnection < 100) {
                indexConnection++;
                boolean close = close();
                if(close) {
                    return true;
                }
            }
            indexConnection = 0;
            return false;
        }
        return true;
    }
}
