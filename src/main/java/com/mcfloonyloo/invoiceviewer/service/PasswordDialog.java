/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.service;

import java.util.Optional;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Modality;

/**
 * 
 * @author avolkov
 */
public class PasswordDialog {
    
   private ButtonType ok;
   
   private ButtonType cancel;
   
   private final Dialog<String> dialog;
   
   private PasswordField password;
   
   protected ButtonType getOk() {
       return this.ok;
   }
   
   protected ButtonType getCancel() {
       return this.cancel;
   }
   
   public PasswordDialog setTitle(String title) {
       this.dialog.setTitle(title);
       return this;
   }
   
   public PasswordDialog setHeaderText(String headerText) {
       this.dialog.setHeaderText(headerText);
       return this;
   }
   
   public PasswordDialog setContentText(String contentText) {
       this.dialog.setContentText(contentText);
       return this;
   }
    
   public Optional<String> getResult() {
       return this.dialog.showAndWait();
   }
   
   public String getPassword() {
       return this.password.getText();
   }
   
   public PasswordDialog() {
       this.password = new PasswordField(); 
       this.password.setPromptText("");
       
       this.ok = new ButtonType("ОК", ButtonBar.ButtonData.OK_DONE);
       this.cancel = new ButtonType("Отмена", ButtonBar.ButtonData.CANCEL_CLOSE);
       
       this.dialog = new Dialog<>();
       this.dialog.getDialogPane().getButtonTypes().addAll(this.ok, this.cancel);
       
       Button btnOk = (Button) this.dialog.getDialogPane().lookupButton(this.ok);
       this.password.lengthProperty().addListener((observable) -> {
           btnOk.setDisable(!(this.password.getText().length() > 0));
       });
       
       this.dialog.getDialogPane().setContent(getBox());
       this.dialog.initModality(Modality.APPLICATION_MODAL);
       this.dialog.setResultConverter((ButtonType buttonType) -> {
           if(buttonType == this.ok) {
               return this.password.getText();
           } else {
               return null;
           }
       });
       
       Platform.runLater(() -> {
           this.password.requestFocus();
           this.password.clear();
       });
   }
   
   private HBox getBox() {
       HBox box = new HBox();
       box.getChildren().add(this.password);
       box.setPadding(new Insets(10));
       HBox.setHgrow(this.password, Priority.ALWAYS);
       return box;
   }
   
}
