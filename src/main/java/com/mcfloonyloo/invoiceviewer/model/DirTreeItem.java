/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model;

import java.io.File;
import java.nio.file.Path;
import java.util.Optional;

/**
 *
 * @author avolkov
 */
public class DirTreeItem {
    
    private final Path dirPath;
    
    public boolean isNullable() {
        return !Optional.ofNullable(this.dirPath).isPresent();
    }
        
    public String getName() {
        return this.dirPath.toFile().getName();
    }
    
    public File getFile() {
        return this.dirPath.toFile();
    }
    
    public DirTreeItem(Path dirPath) {
        this.dirPath = dirPath;
    }
    
}
