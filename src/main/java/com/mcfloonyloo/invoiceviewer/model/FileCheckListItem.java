/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model;

import java.nio.file.Path;

/**
 *
 * @author avolkov
 */
public class FileCheckListItem {
    
    private final Path filePath;
    
    private String messageError;
    
    private StatusFileCheckListItem status;
    
    public Path getPath() {
        return this.filePath;
    }
    
    public String getFileName() {
        return this.filePath.getFileName().toString();
    }
    
    public String getAbsoluteFileName() {
        return this.filePath.getFileName().toAbsolutePath().toString();
    }
    
    public String getMessageError() {
        return this.messageError;
    }
    
    public StatusFileCheckListItem getStatus() {
        return this.status;
    }
    
    public void setMessageError(String messageError) {
        this.messageError = messageError;
    }
    
    public void setStatus(StatusFileCheckListItem status) {
        this.status = status;
    }
    
    public FileCheckListItem(Path filePath, StatusFileCheckListItem status) {
        this.filePath = filePath;
        this.status = status;
    }
    
    public FileCheckListItem(Path filePath, StatusFileCheckListItem status, String messageError) {
        this.filePath = filePath;
        this.status = status;
        this.messageError = messageError;
    }
    
}
