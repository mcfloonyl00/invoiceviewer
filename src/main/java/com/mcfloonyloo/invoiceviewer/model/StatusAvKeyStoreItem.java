/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model;

/**
 *
 * @author avolkov
 */
public enum StatusAvKeyStoreItem {
    NO_INFORMATION_CARRIER {
        @Override
        public String get() {
            return "black";
        }

        @Override
        public boolean isDisabled() {
            return true;
        }
    },
    AVAILABLE_FOR_UPLOAD {
        @Override
        public String get() {
            return "red";
        }

        @Override
        public boolean isDisabled() {
            return true;
        }
    }, 
    ADDED_TO_DB {
        @Override
        public String get() {
            return "green";
        }

        @Override
        public boolean isDisabled() {
            return false;
        }
    },
    CERTIFICATE_EXPIRED {
        @Override
        public String get() {
            return "grey";
        }

        @Override
        public boolean isDisabled() {
            return false;
        }
        
    };
    
    public abstract String get();
    public abstract boolean isDisabled();
}
