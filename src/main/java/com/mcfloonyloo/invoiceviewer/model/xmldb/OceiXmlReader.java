/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.xmldb;

import com.mcfloonyloo.invoiceviewer.dialog.ExceptionDialog;
import com.mcfloonyloo.invoiceviewer.properties.AppProperties;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author avolkov
 */
public class OceiXmlReader extends AbstractXmldbReader<OceiXmlItem> implements DefaultXmldbReader<OceiXmlItem> {

    @Override
    protected ArrayList<OceiXmlItem> readElements(NodeList nodes) {
        ArrayList<OceiXmlItem> list = new ArrayList<>();
        
        for(int index = 0; index < nodes.getLength(); index++){
            Node node = nodes.item(index);
            if(node.getNodeType() == Node.ELEMENT_NODE){
                Element element = (Element) node;
                
                OceiXmlItem ocei = new OceiXmlItem();
                ocei.setCode(element.getAttribute("id"));
                ocei.setFullName(element.getAttribute("fullName"));
                ocei.setShortName(element.getAttribute("shortName"));
                
                list.add(ocei);
            }
        }
        
        return list;
    }

    @Override
    public void read() {
        try {
            File file = new File(AppProperties.INSTANCE.getXmldbPath()+"Ocei.xml");
            Document document = createDocument(file);
            setList(readElements(document.getElementsByTagName("record")));
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            String message = ex.getLocalizedMessage();
            ExceptionDialog dialog = new ExceptionDialog(message, ex)
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText(ex.getClass().getName());
            dialog.showAndWait();
            getList().clear();
        }
    }

    @Override
    public OceiXmlItem get(String code) {
        if(getList().isEmpty()) {
            return null;
        } else {
            for(OceiXmlItem item : getList()) {
                if(item.getCode().equals(code)) {
                    return item;
                }
            }
            return null;
        }
    }

    @Override
    public boolean isRecord(String code) {
        if(getList().isEmpty()) {
            return false;
        } else {
            return getList().stream()
                    .anyMatch(item -> item.getCode().equals(code));
        }
    }
    
}
