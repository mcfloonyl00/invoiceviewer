/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.xmldb;

/**
 *
 * @author avolkov
 */
public class InvoiceStatusXmlItem implements DefaultXmldbItem {
    
    private final String nameru;
    
    private final String nameen;
    
    public String getNameru() {
        return this.nameru;
    }
    
    public String getNameen() {
        return this.nameen;
    }
    
    public InvoiceStatusXmlItem(String nameru, String nameen) {
        this.nameru = nameru;
        this.nameen = nameen;
    }
    
}
