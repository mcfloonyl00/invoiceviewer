/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.xmldb;

import com.mcfloonyloo.invoiceviewer.dialog.ExceptionDialog;
import com.mcfloonyloo.invoiceviewer.properties.AppProperties;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author avolkov
 */
public class OcedXmlReader extends AbstractXmldbReader<OcedXmlItem> implements DefaultXmldbReader<OcedXmlItem> {

    @Override
    protected ArrayList<OcedXmlItem> readElements(NodeList nodes) {
        ArrayList<OcedXmlItem> list = new ArrayList<>();
        
        for(int index = 0; index < nodes.getLength(); index++){
            Node node = nodes.item(index);
            if(node.getNodeType() == Node.ELEMENT_NODE){
                Element element = (Element) node;
                
                OcedXmlItem oced = new OcedXmlItem();
                oced.setCode(element.getAttribute("id"));
                oced.setName(element.getAttribute("name"));
                oced.setDateFrom(element.getAttribute("dateFrom"));
                oced.setDateBy(element.getAttribute("dateTo"));
                
                list.add(oced);
            }
        }
        
        return list;
    }

    @Override
    public void read() {
        try {
            File file = new File(AppProperties.INSTANCE.getXmldbPath()+"Oced.xml");
            Document document = createDocument(file);
            setList(readElements(document.getElementsByTagName("record")));
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            String message = ex.getLocalizedMessage();
            ExceptionDialog dialog = new ExceptionDialog(message, ex)
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText(ex.getClass().getName());
            dialog.showAndWait();
            getList().clear();
        }
    }

    @Override
    public OcedXmlItem get(String code) {
        if(getList().isEmpty()) {
            return null;
        } else {
            for(OcedXmlItem item : getList()) {
                if(item.getCode().equals(code)) {
                    return item;
                }
            }
            return null;
        }
    }

    @Override
    public boolean isRecord(String code) {
        if(getList().isEmpty()) {
            return false;
        } else {
            return getList().stream()
                    .anyMatch(item -> item.getCode().equals(code));
        }
    }
    
}
