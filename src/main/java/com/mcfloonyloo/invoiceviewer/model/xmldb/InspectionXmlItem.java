/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.xmldb;

/**
 *
 * @author avolkov
 */
public class InspectionXmlItem implements DefaultXmldbItem {
    
    private String code;
    
    private String fullName;
    
    private String breifName;
    
    private String unp;
    
    public String getCode() {
        return this.code;
    }
    
    protected void setCode(String code) {
        this.code = code;
    }
    
    public String getFullName() {
        return this.fullName;
    }
    
    protected void setFullName(String fullName) {
        this.fullName = fullName;
    }
    
    public String getBreifName() {
        return this.breifName;
    }
    
    protected void setBreifName(String breifName) {
        this.breifName = breifName;
    }
    
    public String getUnp() {
        return this.unp;
    }
    
    protected void setUnp(String unp) {
        this.unp = unp;
    }
    
    public InspectionXmlItem() {
        defaultValues();
    }
    
    private void defaultValues() {
        this.code = "";
        this.fullName = "";
        this.breifName = "";
        this.unp = "";
    }
    
}
