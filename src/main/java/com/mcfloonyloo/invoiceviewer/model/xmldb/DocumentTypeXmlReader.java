/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.xmldb;

import com.mcfloonyloo.invoiceviewer.dialog.ExceptionDialog;
import com.mcfloonyloo.invoiceviewer.properties.AppProperties;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author avolkov
 */
public class DocumentTypeXmlReader extends AbstractXmldbReader<DocumentTypeXmlItem> implements DefaultXmldbReader<DocumentTypeXmlItem> {   

    @Override
    public void read() {
        getList().clear();
        try { 
            File file = new File(AppProperties.INSTANCE.getXmldbPath()+"DocumentType.xml");
            Document document = createDocument(file);
            setList(readElements(document.getElementsByTagName("type")));
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            String message = ex.getLocalizedMessage();
            ExceptionDialog dialog = new ExceptionDialog(message, ex)
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText(ex.getClass().getName());
            dialog.showAndWait();
            getList().clear();
        }
    }
    
    @Override
    protected ArrayList<DocumentTypeXmlItem> readElements(NodeList nodes) {
        ArrayList<DocumentTypeXmlItem> list = new ArrayList<>();
        for(int index = 0; index < nodes.getLength(); index++){
            Node node = nodes.item(index);
            if(node.getNodeType() == Node.ELEMENT_NODE){
                Element element = (Element) node;
                String code = element.getAttribute("id");
                String name = element.getAttribute("name");
                list.add(new DocumentTypeXmlItem(code, name));
            }
        }
        return list;
    }
    
    @Override
    public DocumentTypeXmlItem get(String code) {
        if(getList().isEmpty()) {
            return null;
        } else {
            for(DocumentTypeXmlItem item : getList()) { 
                if(item.getCode().equals(code)) {
                    return item;
                }
            }
            return null;
        }
    }

    @Override
    public boolean isRecord(String code) {
        if(getList().isEmpty()) {
            return false;
        } else {
            return getList().stream()
                    .anyMatch(item -> item.getCode().equals(code));
        }
    } 
    
}
