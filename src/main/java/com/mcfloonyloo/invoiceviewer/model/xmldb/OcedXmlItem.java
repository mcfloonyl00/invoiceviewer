/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.xmldb;

/**
 *
 * @author avolkov
 */
public class OcedXmlItem implements DefaultXmldbItem {
    
    private String code;
    
    private String name;
    
    private String dateFrom;
    
    private String dateBy;
    
    public String getCode() {
        return this.code;
    }
    
    protected void setCode(String code) {
        this.code = code;
    }
    
    public String getName() {
        return this.name;
    }
    
    protected void setName(String name) {
        this.name = name;
    }
    
    public String getDateFrom() {
        return this.dateFrom;
    }
    
    protected void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }
    
    public String getDateBy() {
        return this.dateBy;
    }
    
    protected void setDateBy(String dateBy) {
        this.dateBy = dateBy;
    }
    
    public OcedXmlItem() {
        defaultValues();
    }
    
    private void defaultValues() {
        this.code = "";
        this.name = "";
        this.dateFrom = "";
        this.dateBy = "";
    }
}
