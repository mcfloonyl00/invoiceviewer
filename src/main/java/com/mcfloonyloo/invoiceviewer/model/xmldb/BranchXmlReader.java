/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.xmldb;

import com.mcfloonyloo.invoiceviewer.dialog.ExceptionDialog;
import com.mcfloonyloo.invoiceviewer.properties.AppProperties;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author avolkov
 */
public class BranchXmlReader extends AbstractXmldbReader<BranchXmlItem> {

    @Override
    protected ArrayList<BranchXmlItem> readElements(NodeList nodes) {
        ArrayList<BranchXmlItem> list = new ArrayList<>();
        
        for(int index = 0; index < nodes.getLength(); index++){
            Node node = nodes.item(index);
            if(node.getNodeType() == Node.ELEMENT_NODE){
                Element element = (Element) node;
                
                BranchXmlItem branch = new BranchXmlItem();
                branch.setUnp(element.getAttribute("unp"));
                branch.setNumberBranch(element.getAttribute("num"));
                branch.setFullName(element.getAttribute("fullName"));
                branch.setShortName(element.getAttribute("shortName"));
                branch.setIndex(element.getAttribute("postIndex"));
                branch.setAddress(element.getAttribute("address"));
                branch.setStatusBranch(element.getAttribute("status"));
                branch.setDateRegistration(element.getAttribute("dateReg"));
                branch.setDateLiquidation(element.getAttribute("dateClose"));
                branch.setDateLastChanges(element.getAttribute("dateEdit"));
                
                list.add(branch);
            }
        }
        
        return list;
    }

    @Override
    public void read() {
        try {
            File file = new File(AppProperties.INSTANCE.getXmldbPath()+"Branch.xml");
            Document document = createDocument(file);
            setList(readElements(document.getElementsByTagName("record")));
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            String message = ex.getLocalizedMessage();
            ExceptionDialog dialog = new ExceptionDialog(message, ex)
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText(ex.getClass().getName());
            dialog.showAndWait();
            getList().clear();
        }
    }
    
    public BranchXmlItem get(String unp, String numberBranch) {
        if(getList().isEmpty()) {
            return null;
        } else {
            for(BranchXmlItem item : getList()) {
                if(item.getUnp().equals(unp) && item.getNumberBranch().equals(numberBranch)) {
                    return item;
                }
            }
            return null;
        }
    }
    
    public boolean isRecord(String unp, String numberBranch) {
        if(getList().isEmpty()) {
            return false;
        } else {
            return getList().stream()
                    .anyMatch(item -> item.getUnp().equals(unp) && item.getNumberBranch().equals(numberBranch));
        }
    }
}
