/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.xmldb;

import com.mcfloonyloo.invoiceviewer.dialog.ExceptionDialog;
import com.mcfloonyloo.invoiceviewer.properties.AppProperties;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author avolkov
 */
public class VatTypeXmlReader extends AbstractXmldbReader<VatTypeXmlItem> implements DefaultXmldbReader<VatTypeXmlItem> {

    @Override
    protected ArrayList<VatTypeXmlItem> readElements(NodeList nodes) {
        ArrayList<VatTypeXmlItem> list = new ArrayList<>();
        
        for(int index = 0; index < nodes.getLength(); index++){
            Node node = nodes.item(index);
            if(node.getNodeType() == Node.ELEMENT_NODE){
                Element element = (Element) node;
                String nameru = element.getAttribute("nameru");
                String nameen = element.getAttribute("nameen");
                list.add(new VatTypeXmlItem(nameru, nameen));
            }
        }
        
        return list;
    }

    @Override
    public void read() {
        getList().clear();
        try {
            File file = new File(AppProperties.INSTANCE.getXmldbPath()+"VatType.xml");
            Document document = createDocument(file);
            setList(readElements(document.getElementsByTagName("type")));
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            String message = ex.getLocalizedMessage();
            ExceptionDialog dialog = new ExceptionDialog(message, ex)
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText(ex.getClass().getName());
            dialog.showAndWait();
            getList().clear();
        }
    }

    @Override
    public VatTypeXmlItem get(String nameen) {
        if(getList().isEmpty()) {
            return null;
        } else {
            for(VatTypeXmlItem item : getList()) {
                if(item.getNameen().equals(nameen)) {
                    return item;
                }
            }
            return null;
        }
    }

    @Override
    public boolean isRecord(String nameen) {
        if(getList().isEmpty()) {
            return false;
        } else {
            return getList().stream()
                    .anyMatch(item -> item.getNameen().equals(nameen));
        }
    }
    
}
