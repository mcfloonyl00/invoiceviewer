/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.xmldb;

/**
 *
 * @author avolkov
 */
public class VatRateXmlItem implements DefaultXmldbItem {
    
    private final String nameru;
    
    private final String nameen;
    
    private final String value;
    
    public String getNameru() {
        return this.nameru;
    }
    
    public String getNameen() {
        return this.nameen;
    }
    
    public String getValue() {
        return this.value;
    }
    
    public VatRateXmlItem(String nameru, String nameen, String value) {
        this.nameru = nameru;
        this.nameen = nameen;
        this.value = value;
    }
    
}
