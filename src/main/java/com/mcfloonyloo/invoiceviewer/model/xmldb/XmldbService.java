/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.xmldb;

/**
 *
 * @author avolkov
 */
public enum XmldbService {
    INSTANCE;
    
    private BranchXmlReader branchReader;
    
    private CommodityNomenclatureXmlReader nomenclatureReader;
    
    private CountryXmlReader countryReader;
    
    private DocumentTypeXmlReader documentTypeReader;
    
    private InspectionXmlReader inspectionReader;
    
    private InvoiceStatusXmlReader invoiceStatusReader;
    
    private InvoiceTypeXmlReader invoiceTypeReader;
    
    private OceiXmlReader oceiReader;
    
    private OcedXmlReader ocedReader;
    
    private ProviderStatusXmlReader providerReader;
    
    private RecipientStatusXmlReader recipientReader;
    
    private VatRateXmlReader vatRateReader;
    
    private VatTypeXmlReader vatTypeReader;
    
    public BranchXmlReader getBranch() {
        return this.branchReader;
    }
    
    public CommodityNomenclatureXmlReader getNomenclature() {
        return this.nomenclatureReader;
    }
    
    public CountryXmlReader getCountryCode() {
        return this.countryReader;
    }
    
    public DocumentTypeXmlReader getDocumentType() {
        return this.documentTypeReader;
    }
    
    public InspectionXmlReader getInspection() {
        return this.inspectionReader;
    };
    
    public InvoiceStatusXmlReader getInvoiceStatus() {
        return this.invoiceStatusReader;
    }
    
    public InvoiceTypeXmlReader getInvoiceType() {
        return this.invoiceTypeReader;
    }
    
    public OceiXmlReader getOcei() {
        return this.oceiReader;
    }
    
    public OcedXmlReader getOced() {
        return this.ocedReader;
    }
    
    public ProviderStatusXmlReader getProviderStatus() {
        return this.providerReader;
    }
    
    public RecipientStatusXmlReader getRecipientStatus() {
        return this.recipientReader;
    }
    
    public VatRateXmlReader getVatRate() {
        return this.vatRateReader;
    }
    
    public VatTypeXmlReader getVatType() {
        return this.vatTypeReader;
    }
    
    private XmldbService() {
        init();
        read();
    }
    
    private void init() {
        this.branchReader = new BranchXmlReader();
        this.nomenclatureReader = new CommodityNomenclatureXmlReader();
        this.countryReader = new CountryXmlReader();
        this.documentTypeReader = new DocumentTypeXmlReader();
        this.inspectionReader = new InspectionXmlReader();
        this.invoiceStatusReader = new InvoiceStatusXmlReader();
        this.invoiceTypeReader = new InvoiceTypeXmlReader();
        this.oceiReader = new OceiXmlReader();
        this.ocedReader = new OcedXmlReader();
        this.providerReader = new ProviderStatusXmlReader();
        this.recipientReader = new RecipientStatusXmlReader();
        this.vatRateReader = new VatRateXmlReader();
        this.vatTypeReader =  new VatTypeXmlReader();
    }
    
    private void read() {
        this.branchReader.read();
        //this.nomenclatureReader.read();
        this.countryReader.read();
        this.documentTypeReader.read();
        this.inspectionReader.read();
        this.invoiceStatusReader.read();
        this.invoiceTypeReader.read();
        this.oceiReader.read();
        this.ocedReader.read();
        this.providerReader.read();
        this.recipientReader.read();
        this.vatRateReader.read();
        this.vatTypeReader.read();
    }
    
}
