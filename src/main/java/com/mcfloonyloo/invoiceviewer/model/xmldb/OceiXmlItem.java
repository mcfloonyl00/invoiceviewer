/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.xmldb;

/**
 *
 * @author avolkov
 */
public class OceiXmlItem implements DefaultXmldbItem {
    
    private String code;
    
    private String fullName;
    
    private String shortName;
    
    public String getCode() {
        return this.code;
    }
    
    protected void setCode(String code) {
        this.code = code;
    }
    
    public String getFullName() {
        return this.fullName;
    }
    
    protected void setFullName(String fullName) {
        this.fullName = fullName;
    }
    
    public String getShortName() {
        return this.shortName;
    }
    
    protected void setShortName(String shortName) {
        this.shortName = shortName;
    }
    
    public OceiXmlItem() {
        defaultValues();
    }
    
    private void defaultValues() {
        this.code = "";
        this.fullName = "";
        this.shortName = "";
    }
    
    
}
