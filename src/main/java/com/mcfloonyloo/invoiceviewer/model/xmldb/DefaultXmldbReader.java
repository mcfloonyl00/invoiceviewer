/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.xmldb;

/**
 *
 * @author avolkov
 */
public interface DefaultXmldbReader<E extends DefaultXmldbItem> {
    
    public E get(String field);
    
    public boolean isRecord(String field);  
    
}
