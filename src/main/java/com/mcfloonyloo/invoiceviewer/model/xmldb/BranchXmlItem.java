/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.xmldb;

/**
 *
 * @author avolkov
 */
public class BranchXmlItem implements DefaultXmldbItem {
    
    private String unp;
    
    private String numberBranch;
    
    private String fullName;
    
    private String shortName;
    
    private String index;
    
    private String address;
    
    private String statusBranch;
    
    private String dateRegistration;
    
    private String dateLiquidation;
    
    private String dateLastChanges;
    
    public String getUnp() {
        return this.unp;
    }
    
    protected void setUnp(String unp) {
        this.unp = unp;
    }
    
    public String getNumberBranch() {
        return this.numberBranch;
    }
    
    protected void setNumberBranch(String numberBranch) {
        this.numberBranch = numberBranch;
    }
    
    public String getFullName() {
        return this.fullName;
    }
    
    protected void setFullName(String fullName) {
        this.fullName = fullName;
    }
    
    public String getShortName() {
        return this.shortName;
    }
    
    protected void setShortName(String shortName) {
        this.shortName = shortName;
    }
    
    public String getIndex() {
        return this.index;
    }
    
    protected void setIndex(String index) {
        this.index = index;
    }
    
    public String getAddress() {
        return this.address;
    }
    
    protected void setAddress(String address) {
        this.address = address;
    }
    
    public String getStatusBranch() {
        return this.statusBranch;
    }
    
    protected void setStatusBranch(String statusBranch) {
        this.statusBranch = statusBranch;
    }
    
    public String getDateRegistration() {
        return this.dateRegistration;
    }
    
    protected void setDateRegistration(String dateRegistration) {
        this.dateRegistration = dateRegistration;
    }
    
    public String getDateLiquidation() {
        return this.dateLiquidation;
    }
    
    protected void setDateLiquidation(String dateLiquidation) {
        this.dateLiquidation = dateLiquidation;
    }
    
    public String getDateLastChanges() {
        return this.dateLastChanges;
    }
    
    protected void setDateLastChanges(String dateLastChanges) {
        this.dateLastChanges = dateLastChanges;
    }
    
    public BranchXmlItem() {
        defaultValues();
    }
    
    private void defaultValues() {
        this.unp = "";
        this.numberBranch = "";
        this.fullName = "";
        this.shortName = "";
        this.index = "";
        this.address = "";
        this.statusBranch = "";
        this.dateRegistration = "";
        this.dateLiquidation = "";
        this.dateLastChanges = "";
    }
    
}
