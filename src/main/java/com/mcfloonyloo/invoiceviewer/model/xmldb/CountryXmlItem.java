/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.xmldb;

/**
 *
 * @author avolkov
 */
public class CountryXmlItem implements DefaultXmldbItem {
    
    private final String code;
    
    private final String name;
    
    public String getCode() {
        return this.code;
    }
    
    public String getName() {
        return this.name;
    }
    
    public CountryXmlItem(String code, String name) {
        this.code = code;
        this.name = name;
    }
    
}
