/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.xmldb;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author avolkov
 */
public abstract class AbstractXmldbReader<E extends DefaultXmldbItem> {

    private ArrayList<E> list;
    
    public ArrayList<E> getList() {
        return this.list;
    }
    
    protected void setList(ArrayList<E> list) {
        this.list = list;
    }
    
    public AbstractXmldbReader() {
        this.list = new ArrayList<>();
        this.list.clear();
    }
    
    protected Document createDocument(File file) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(file);
        document.normalizeDocument();
        return document;
    }
    
    protected abstract ArrayList<E> readElements(NodeList nodes);
    
    protected abstract void read();
    
}
