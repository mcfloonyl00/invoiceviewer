/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model;

/**
 *
 * @author avolkov
 */
public enum StatusFileCheckListItem {
    EMPTY{
        @Override
        public String get() {
            return "blackfile";
        }

        @Override
        public boolean isDisabled() {
            return true;
        }
    },
    IN_DATABASE{
        @Override
        public String get() {
            return "redfile";
        }

        @Override
        public boolean isDisabled() {
            return true;
        }
    },
    ERROR_XML_STRUCTURE{
        @Override
        public String get() {
            return "purplefile";
        }

        @Override
        public boolean isDisabled() {
            return true;
        }
    },
    ERROR_BASE_TEST{
        @Override
        public String get() {
            return "orangefile";
        }

        @Override
        public boolean isDisabled() {
            return true;
        }
    },
    ERROR_POSITIVE_TEST{
        @Override
        public String get() {
            return "bluefile";
        }

        @Override
        public boolean isDisabled() {
            return true;
        }
    },
    FOR_SENDING{
        @Override
        public String get() {
            return "greenfile";
        }

        @Override
        public boolean isDisabled() {
            return false;
        }
    };
    
    public abstract String get();
    public abstract boolean isDisabled();
}
