/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.cayenne;

import org.apache.cayenne.map.ObjEntity;

/**
 *
 * @author avolkov
 */
public abstract class AbstractObjEntity {
    
    protected static final String PACKAGE = "com.mcfloonyloo.invoiceviewer.model.invoice.db.";
    
    private final ObjEntity entity;
    
    public ObjEntity getObjEntity() {
        return this.entity;
    }
    
    public AbstractObjEntity(String name) {
        this.entity = new ObjEntity(name);        
    }
    
}
