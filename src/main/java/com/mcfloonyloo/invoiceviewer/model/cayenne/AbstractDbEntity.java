/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.cayenne;

import org.apache.cayenne.map.DbEntity;

/**
 *
 * @author avolkov
 */
public abstract class AbstractDbEntity {
    
    private final DbEntity entity;
    
    public DbEntity getDbEntity() {
        return this.entity;
    }
    
    public AbstractDbEntity(String name) {
        this.entity = new DbEntity(name);
    }
}
