/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.cayenne;

import org.apache.cayenne.map.DataMap;

/**
 *
 * @author avolkov
 */
public abstract class AbstractDataMap {
    
    private final DataMap map;
    
    public DataMap getDataMap() {
        return this.map;
    }
    
    public AbstractDataMap(String name) {
        this.map = new DataMap(name);
    }
    
}
