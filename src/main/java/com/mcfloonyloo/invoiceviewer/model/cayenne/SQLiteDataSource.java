/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.cayenne;

import javax.sql.DataSource;
import org.apache.cayenne.datasource.DataSourceBuilder;

/**
 *
 * @author avolkov
 */
public class SQLiteDataSource {
    private final DataSource dataSource;
    
    public DataSource getDataSource() {
        return this.dataSource;
    }
    
    public SQLiteDataSource(String url) {
        this.dataSource = DataSourceBuilder.url(url)
                .driver("org.sqlite.JDBC")
                .pool(1, 1)
                .build();
    }
}
