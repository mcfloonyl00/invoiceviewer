/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model;

/**
 *
 * @author avolkov
 */
public class AvKeyStoreItem {
    
    private final String alias;
    
    private final String dateAfter;
    
    private StatusAvKeyStoreItem status;
    
    public String getAlias() {
        return this.alias;
    }
    
    public String getDateAfter() {
        return this.dateAfter;
    }
    
    public StatusAvKeyStoreItem getStatus() {
        return this.status;
    }
    
    public void setStatus(StatusAvKeyStoreItem status) {
        this.status = status;
    }
    
    public AvKeyStoreItem(String alias, String dateAfter) {
        this.alias = alias;
        this.dateAfter = dateAfter;
    }
    
}
