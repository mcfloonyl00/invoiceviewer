/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model;

import java.time.LocalDateTime;

/**
 *
 * @author avolkov
 */
public class UpdateInvoiceItem {
    
    private String number;
    
    private String statusRu;
    
    private String statusEn;
    
    private LocalDateTime date;
    
    public String getNumber() {
        return this.number;
    }
    
    public String getStatusEn() {
        return this.statusEn;
    }
    
    public String getStatusRu() {
        return this.statusRu;
    }
    
    public LocalDateTime getDate() {
        return this.date;
    }
    
    public void setNumber(String number) {
        this.number = number;
    }
    
    public void setStatusEn(String statusEn) {
        this.statusEn = statusEn;
    }
    
    public void setStatusRu(String statusRu) {
        this.statusRu = statusRu;
    }
    
    public void setDate(LocalDateTime date) {
        this.date = date;
    }
    
    public UpdateInvoiceItem() {
        defaultValues();
    }
    
    private void defaultValues() {
        this.number = "";
        this.statusEn = "";
        this.statusRu = "";
        this.date = LocalDateTime.now();
    }
    
}
