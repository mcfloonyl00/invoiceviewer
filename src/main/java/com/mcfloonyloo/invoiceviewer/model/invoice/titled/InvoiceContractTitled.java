/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.titled;

import com.mcfloonyloo.invoiceviewer.model.Title;
import com.mcfloonyloo.invoiceviewer.model.invoice.DefaultInvoiceSubSection;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceContract;

/**
 *
 * @author avolkov
 */
public class InvoiceContractTitled implements DefaultInvoiceSubSection {

    private final Title<String> number;
    
    private final Title<String> date;
    
    private final Title<InvoiceDocumentsTitled> documents;
    
    public InvoiceContractTitled(InvoiceContract contract) {
        this.number = new Title<>("  Номер контракта", true);
        this.date = new Title<>("  Дата", true);
        this.documents = new Title<>("", true);
        
        this.number.setValue(contract.getNumber());
        this.date.setValue(contract.getDate());
        this.documents.setValue(new InvoiceDocumentsTitled(contract.getDocuments()));
    }
    
}
