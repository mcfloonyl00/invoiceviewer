/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.parser;

import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceDocumentItem;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author avolkov
 */
class InvoiceDocumentItemParser extends AbstractInvoiceSectionParser<InvoiceDocumentItem> {

    public InvoiceDocumentItemParser(Node node) {
        super(node);
    }

    @Override
    public InvoiceDocumentItem parse() {
        InvoiceDocumentItem section = new InvoiceDocumentItem();
        section.setNullValues();
        
        NodeList list = getNode().getChildNodes();
        for(int index = 0; index < list.getLength(); index++) {
            Node element = list.item(index);
            if(element.getNodeType() != Node.TEXT_NODE) {
                switch(element.getNodeName()) {
                    case "docType" : {
                        section.setDocType(new InvoiceDocumentTypeParser(element).parse());
                        break;
                    }
                    case "date" : {
                        section.setDate(element.getTextContent());
                        break;
                    }
                    case "blankCode" : {
                        section.setBlankCode(element.getTextContent());
                        break;
                    }
                    case "seria" : {
                        section.setSeria(element.getTextContent());
                        break;
                    }
                    case "number" : {
                        section.setNumber(element.getTextContent());
                        break;
                    }
                }
            }
        }
        
        return section;
    }
    
}
