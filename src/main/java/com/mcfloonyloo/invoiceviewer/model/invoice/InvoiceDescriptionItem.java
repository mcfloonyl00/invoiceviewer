/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice;

/**
 *
 * @author avolkov
 */
public class InvoiceDescriptionItem implements DefaultInvoiceSubSectionItem, DefaultInvoiceNullable {
    
    private String description;
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public InvoiceDescriptionItem() {
        defaultValues();
    }
    
    private void defaultValues() {
        this.description = "";
    }

    @Override
    public void setNullValues() {
        this.description = null;
    }
    
}
