/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.test;

import com.mcfloonyloo.invoiceviewer.model.invoice.Invoice;
import com.mcfloonyloo.invoiceviewer.model.invoice.XsdInvoiceLoader;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.XsdValidator;
import com.mcfloonyloo.invoiceviewer.properties.AppProperties;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author avolkov
 */
public class XmlStructureTestItem extends AbstractTestItem {

    public XmlStructureTestItem(Invoice invoice) {
        super(invoice);
        setTitle("[XML TEST] Корректность структуры файла ЭСЧФ - обнаружены проблемы : ");
    }

    @Override
    public void test() {
        
        XsdInvoiceLoader loader = new XsdInvoiceLoader(AppProperties.INSTANCE.getXsdPath(), getInvoice().getGeneral());
        try{
            List<Exception> exceptions = new XsdValidator(loader.getFile()).validate(getInvoice().getContent());
            if(Optional.ofNullable(exceptions).isPresent()) {
                exceptions.stream().forEach((exception) -> {
                    this.getErrors().add(new ErrorItem("внимание - "+exception.getLocalizedMessage()).setFieldName("[Структура файла ЭСЧФ]"));
                });
            }
        } catch (Exception ex) {
            this.getErrors().add(new ErrorItem("ошибка - "+ex.getLocalizedMessage()).setFieldName("[Структура файла ЭСЧФ]"));
        }
        
        if(this.getErrors().isEmpty()) {
            setTitle("");
        } else {
            setTitle("[XML TEST] Корректность структуры файла ЭСЧФ - обнаружены проблемы : ");
        }
    }
    
}
