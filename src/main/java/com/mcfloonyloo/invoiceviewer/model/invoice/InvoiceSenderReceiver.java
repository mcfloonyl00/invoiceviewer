/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice;

import java.util.ArrayList;

/**
 *
 * @author avolkov
 */
public class InvoiceSenderReceiver implements DefaultInvoiceSection, DefaultInvoiceNullable {

    private ArrayList<InvoiceSenderItem> consignors;
    
    private ArrayList<InvoiceSenderItem> consignees;
    
    public ArrayList<InvoiceSenderItem> getConsignors() {
        return this.consignors;
    }
    
    public ArrayList<InvoiceSenderItem> getConsignees() {
        return this.consignees;
    }
    
    public void setConsingors(ArrayList<InvoiceSenderItem> consignors) {
        this.consignors = consignors;
    }
    
    public void setConsignees(ArrayList<InvoiceSenderItem> consignees) {
        this.consignees = consignees;
    }
    
    public InvoiceSenderReceiver() {
        defaultValues();
    }
    
    private void defaultValues() {
        this.consignors = new ArrayList<>();
        this.consignees = new ArrayList<>();
    }

    @Override
    public void setNullValues() {
        defaultValues();
    }
    
}
