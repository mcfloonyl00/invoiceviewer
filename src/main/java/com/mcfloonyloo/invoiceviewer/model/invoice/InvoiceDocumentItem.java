/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice;

/**
 *
 * @author avolkov
 */
public class InvoiceDocumentItem implements DefaultInvoiceSubSectionItem, DefaultInvoiceNullable {
    
    private InvoiceDocumentType docType;
    
    private String date;
    
    private String blankCode;
    
    private String seria;
    
    private String number;
    
    public InvoiceDocumentType getDocType() {
        return this.docType;
    }
    
    public String getDate() {
        return this.date;
    }
    
    public String getBlankCode() {
        return this.blankCode;
    }
    
    public String getSeria() {
        return this.seria;
    }
    
    public String getNumber() {
        return this.number;
    }
    
    public void setDocType(InvoiceDocumentType docType) {
        this.docType = docType;
    }
    
    public void setDate(String date) {
        this.date = date;
    }
    
    public void setBlankCode(String blankCode) {
        this.blankCode = blankCode;
    }
    
    public void setSeria(String seria) {
        this.seria = seria;
    }
    
    public void setNumber(String number) {
        this.number = number;
    }
    
    public InvoiceDocumentItem() {
        defaultValues();
    }
    
    private void defaultValues() {
        this.docType = new InvoiceDocumentType();
        this.date = "";
        this.blankCode = "";
        this.seria = "";
        this.number = "";
    }

    @Override
    public void setNullValues() {
        this.docType = null;
        this.date = null;
        this.blankCode = null;
        this.seria = null;
        this.number = null;
    }
    
}
