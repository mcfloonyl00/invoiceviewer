/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.db;

import by.avest.edoc.client.AvEStatus;
import com.mcfloonyloo.invoiceviewer.model.UpdateInvoiceItem;
import static com.mcfloonyloo.invoiceviewer.model.invoice.db.DefaultInvoiceStatusDAO.getStatusRu;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.query.ObjectSelect;

/**
 *
 * @author avolkov
 */
public class InvoiceInStatusDAO extends AbstractInvoiceDAO 
        implements DefaultInvoiceStatusDAO<InvoiceInStatusDTO> {

    public InvoiceInStatusDAO(String config) {
        super(config);
    }

    @Override
    public void addStatus(String number, AvEStatus status) {
        ObjectContext context = createContext();
        InvoiceInDTO invoiceIn = ObjectSelect.query(InvoiceInDTO.class).where(InvoiceInDTO.NUMBER.like(number)).selectOne(context);
        InvoiceInStatusDTO invoiceInStatus = context.newObject(InvoiceInStatusDTO.class);
        invoiceInStatus.setStatusen(status.getStatus());
        invoiceInStatus.setStatusru(getStatusRu(status.getStatus()));
        
        String since = LocalDateTime.parse(status.getSince()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));;        
        invoiceInStatus.setDatetimeupdate(LocalDateTime.parse(since, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        
        invoiceIn.addToStatus(invoiceInStatus);
       
        context.commitChanges();
        shutdownRuntime();
    }

    public boolean isStatus(String number, AvEStatus status) {
        ObjectContext context = createContext();
        InvoiceInDTO invoiceIn = ObjectSelect.query(InvoiceInDTO.class).where(InvoiceInDTO.NUMBER.like(number)).selectOne(context);
        List<InvoiceInStatusDTO> list = invoiceIn.getStatuses();
        
        shutdownRuntime();
        for(int index = 0; index < list.size(); index++) {
            if(list.get(index).getStatusen().equals(status.getStatus())) {
                return true;
            }
        }
        
        return false;
    }

    @Override
    public InvoiceInStatusDTO getStatus(String number) {
        ObjectContext context = createContext();
        InvoiceInDTO invoiceIn = ObjectSelect.query(InvoiceInDTO.class).where(InvoiceInDTO.NUMBER.like(number)).selectOne(context);
        List<InvoiceInStatusDTO> list = invoiceIn.getStatuses();
        Collections.sort(list, (InvoiceInStatusDTO left, InvoiceInStatusDTO right) -> right.getDatetimeupdate().compareTo(left.getDatetimeupdate()));
        
        shutdownRuntime();
        if(Optional.ofNullable(list).isPresent() && !list.isEmpty()) {
            return list.get(0);
        } else {
            return null;
        }
    }

    @Override
    public ArrayList<UpdateInvoiceItem> getInvoicesAndStatuses(LocalDate from, LocalDate to) {
        ArrayList<UpdateInvoiceItem> updateInvoices = new ArrayList<>();
        
        ObjectContext context = createContext();
        List<InvoiceInDTO> invoices = ObjectSelect.query(InvoiceInDTO.class).where(InvoiceInDTO.DATETRANS.between(from, to)).select(context);
        Collections.sort(invoices, (InvoiceInDTO left, InvoiceInDTO right) -> left.getDatetrans().compareTo(right.getDatetrans()));
        
        for(InvoiceInDTO invoice : invoices) {
            InvoiceInStatusDTO status = getStatus(invoice.getNumber());
            
            UpdateInvoiceItem updateItem = new UpdateInvoiceItem();
            updateItem.setNumber(invoice.getNumber());
            updateItem.setStatusEn(status.getStatusen());
            updateItem.setStatusRu(status.getStatusru());
            updateItem.setDate(status.getDatetimeupdate());
            
            updateInvoices.add(updateItem);
            
        }
        
        return updateInvoices;
    }
}
