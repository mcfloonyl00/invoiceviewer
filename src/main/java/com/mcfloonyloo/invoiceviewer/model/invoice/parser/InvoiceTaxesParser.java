/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.parser;

import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceTaxes;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author avolkov
 */
class InvoiceTaxesParser extends AbstractInvoiceSectionParser<InvoiceTaxes>{

    public InvoiceTaxesParser(Node node) {
        super(node);
    }

    @Override
    public InvoiceTaxes parse() {
        InvoiceTaxes section = new InvoiceTaxes();
        section.setNullValues();
        
        NodeList list = getNode().getChildNodes();
        for(int index = 0; index < list.getLength(); index++) {
            Node element = list.item(index);
            if(element.getNodeType() != Node.TEXT_NODE) {
                switch(element.getNodeName()) {
                    case "number" : {
                        section.setNumber(element.getTextContent());
                        break;
                    }
                    case "date" : {
                        section.setDate(element.getTextContent());
                        break;
                    }
                }
            }
        }
        
        return section;
    }
    
}
