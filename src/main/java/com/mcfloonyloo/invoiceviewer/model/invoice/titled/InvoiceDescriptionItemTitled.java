/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.titled;

import com.mcfloonyloo.invoiceviewer.model.Title;
import com.mcfloonyloo.invoiceviewer.model.invoice.DefaultInvoiceSubSectionItem;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceDescriptionItem;

/**
 *
 * @author avolkov
 */
public class InvoiceDescriptionItemTitled implements DefaultInvoiceSubSectionItem {
    
    private final Title<String> description;
    
    public InvoiceDescriptionItemTitled(InvoiceDescriptionItem description) {
        this.description = new Title<>("    Содержание", true);
        
        this.description.setValue(description.getDescription());
    }
    
}
