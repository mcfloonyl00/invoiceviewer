/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.test;

import com.mcfloonyloo.invoiceviewer.model.invoice.Invoice;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceDocumentItem;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceContractValidator;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceDocumentItemValidator;
import java.util.Optional;

/**
 *
 * @author avolkov
 */
public class ContractTestItem extends AbstractTestItem {
    
    public ContractTestItem(Invoice invoice) {
        super(invoice);
        setTitle("[CONTRACT TEST] Условия поставки - обнаружены проблемы : ");
    }

    @Override
    public void test() {
        
        InvoiceContractValidator contractValidator = new InvoiceContractValidator(getInvoice().getDeliveryCondition().getContract());
        
        ErrorItem number = contractValidator.validateNumber();
        if(Optional.ofNullable(number).isPresent()) {
            this.getErrors().add(number);
        }
        ErrorItem date = contractValidator.validateDate();
        if(Optional.ofNullable(date).isPresent()) {
            this.getErrors().add(date);
        }
        ErrorItem documents = contractValidator.validateDocuments();
        if(Optional.ofNullable(documents).isPresent()) {
            this.getErrors().add(documents);
        } else {
            int index = 0;
            for(InvoiceDocumentItem item : getInvoice().getDeliveryCondition().getContract().getDocuments()) {
                index++;
                InvoiceDocumentItemValidator documentItemValidator = new InvoiceDocumentItemValidator(index, item);
                ErrorItem documentItem = documentItemValidator.validateDocumentTypeCode();
                if(Optional.ofNullable(documentItem).isPresent()) {
                    this.getErrors().add(documentItem);
                } else {
                    switch(item.getDocType().getCode()) {
                        case "600" : {
                            break;
                        }
                        case "602" : 
                        case "603" : {
                            this.getErrors().addAll(getDocItemWaybill(documentItemValidator).getList());
                            break;
                        }
                        case "601" :
                        case "604" :
                        case "605" :
                        case "606" :
                        case "607" :
                        case "608" :
                        case "609" :
                        case "610" :
                        case "611" :
                        case "612" :
                        case "613" :
                        case "614" :
                        case "615" : {
                            this.getErrors().addAll(getDocItemCert(documentItemValidator).getList());
                            break;
                        }
                        default : {
                            ErrorItem defaultItem = new ErrorItem("внимание - отсутствует значение поля в списке доступных знчений");
                            defaultItem.setFieldName("[Условия поставки].[Список документов].[Документ №"+index+"].[Код документа]");
                            this.getErrors().add(defaultItem);
                            break;
                        }
                    }
                }
            }
        }
    }
    
    //Проверяет корректность полей документа, если тип документа - ТН
    private ErrorList getDocItemWaybill(InvoiceDocumentItemValidator documentItemValidator) {
        ErrorList errors = new ErrorList();
        
        ErrorItem blankCode = documentItemValidator.validateBlankCode();
        if(Optional.ofNullable(blankCode).isPresent()) {
            errors.add(blankCode);
        }
        
        ErrorItem seria = documentItemValidator.validateSeria();
        if(Optional.ofNullable(seria).isPresent()) {
            errors.add(seria);
        }
        
        ErrorItem seriaLenght = documentItemValidator.validateSeriaLenght();
        if(Optional.ofNullable(seriaLenght).isPresent()) {
            errors.add(seriaLenght);
        }
        
        ErrorItem number = documentItemValidator.validateNumber();
        if(Optional.ofNullable(number).isPresent()) {
            errors.add(number);
        }
        
        ErrorItem date = documentItemValidator.validateDate();
        if(Optional.ofNullable(date).isPresent()) {
            errors.add(date);
        }
        
        return errors;
    }
    
    //Проверяет корректность полей документа, если тип документа - акт
    private ErrorList getDocItemCert(InvoiceDocumentItemValidator documentItemValidator) {
        ErrorList errors = new ErrorList();
        
        ErrorItem number = documentItemValidator.validateNumber();
        if(Optional.ofNullable(number).isPresent()) {
            errors.add(number);
        }
        
        ErrorItem date = documentItemValidator.validateDate();
        if(Optional.ofNullable(date).isPresent()) {
            errors.add(date);
        }
        
        return errors;
    }
    
}
