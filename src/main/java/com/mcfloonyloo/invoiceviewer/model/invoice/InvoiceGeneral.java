/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice;

/**
 *
 * @author avolkov
 */
public class InvoiceGeneral implements DefaultInvoiceSection, DefaultInvoiceNullable {
    
    private String number;
    
    private String dateIssuance;
    
    private String dateTransaction;
    
    private String documentType;
    
    private String byInvoice;
    
    private String sendToRecipient;
    
    private String dateCancelled;
    
    public String getNumber() {
        return this.number;
    }
    
    public String getDateIssuance() {
        return this.dateIssuance;
    }
    
    public String getDateTransaction() {
        return this.dateTransaction;
    }
    
    public String getDocumentType() {
        return this.documentType;
    }
    
    public String getByInvoice() {
        return this.byInvoice;
    }
    
    public String getSendToRecipient() {
        return this.sendToRecipient;
    }
    
    public String getDateCancelled() {
        return this.dateCancelled;
    }
    
    public void setNumber(String number) {
        this.number = number;
    }
    
    public void setDateIssuance(String dateIssuance) {
        this.dateIssuance = dateIssuance;
    }
    
    public void setDateTransaction(String dateTransaction) {
        this.dateTransaction = dateTransaction;
    }
    
    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }
    
    public void setByInvoice(String byInvoice) {
        this.byInvoice = byInvoice;
    }
    
    public void setSendToRecipient(String sendToRecipient) {
        this.sendToRecipient = sendToRecipient;
    }
    
    public void setDateCancelled(String dateCancelled) {
        this.dateCancelled = dateCancelled;
    }
    
    public InvoiceGeneral() {
        defaultValues();
    }
    
    private void defaultValues() {
        this.number = "";
        this.dateIssuance = "";
        this.dateTransaction = "";
        this.documentType = "";
        this.byInvoice = "";
        this.sendToRecipient = "";
        this.dateCancelled = "";
    }

    @Override
    public void setNullValues() {
        this.number = null;
        this.dateIssuance = null;
        this.dateTransaction = null;
        this.documentType = null;
        this.byInvoice = null;
        this.sendToRecipient = null;
        this.dateCancelled = null;
    }
    
}
