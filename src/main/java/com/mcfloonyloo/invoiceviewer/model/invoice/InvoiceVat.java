/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice;

/**
 *
 * @author avolkov
 */
public class InvoiceVat implements DefaultInvoiceSubSection, DefaultInvoiceNullable {
    
    private String rate;
    
    private String rateType;
    
    private String summaVat;
    
    public String getRate() {
        return this.rate;
    }
    
    public String getRateType() {
        return this.rateType;
    }
    
    public String getSummaVat() {
        return this.summaVat;
    }
    
    public void setRate(String rate) {
        this.rate = rate;
    }
    
    public void setRateType(String rateType) {
        this.rateType = rateType;
    }
    
    public void setSummaVat(String summaVat) {
        this.summaVat = summaVat;
    }
    
    public InvoiceVat() {
        defaultValues();
    }
    
    private void defaultValues() {
        this.rate = "";
        this.rateType = "";
        this.summaVat = "";
    }

    @Override
    public void setNullValues() {
        this.rate = null;
        this.rateType = null;
        this.summaVat = null;
    }
    
}
