/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice;

import com.mcfloonyloo.invoiceviewer.model.invoice.parser.InvoiceParser;
import com.mcfloonyloo.invoiceviewer.service.InvoiceService;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 *
 * @author avolkov
 */
public class ServiceInvoiceLoader implements DefaultInvoiceLoader {

    private final String number;
    
    public ServiceInvoiceLoader(String number) {
        this.number = number;
    }
    
    @Override
    public Invoice load() {
        try {
            byte[] doc = InvoiceService.INSTANCE.getService().getEDoc(this.number).getDocument().getEncoded();
            try(ByteArrayInputStream stream = new ByteArrayInputStream(doc)) {
                Invoice invoice = new InvoiceParser(getDocument(stream)).parse();
                invoice.setContent(doc);
                return invoice;
            } catch (Exception ex) {
                ex.printStackTrace();
                return null;
            } 
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    private Document getDocument(ByteArrayInputStream stream) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        return factory.newDocumentBuilder().parse(stream);
    }
    
}
