/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.parser;

import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceRoster;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author avolkov
 */
class InvoiceRosterParser extends AbstractInvoiceSectionParser<InvoiceRoster> {

    public InvoiceRosterParser(Node node) {
        super(node);
    }

    @Override
    public InvoiceRoster parse() {
        InvoiceRoster section = new InvoiceRoster();
        section.setNullValues();
        
        section.setTotalCostVat(getNode().getAttributes().getNamedItem("totalCostVat").getNodeValue());
        section.setTotalExcise(getNode().getAttributes().getNamedItem("totalExcise").getNodeValue());
        section.setTotalVat(getNode().getAttributes().getNamedItem("totalVat").getNodeValue());
        section.setTotalCost(getNode().getAttributes().getNamedItem("totalCost").getNodeValue());
        
        NodeList list = getNode().getChildNodes();
        for(int index = 0; index < list.getLength(); index++) {
            Node element = list.item(index);
            if(element.getNodeType() != Node.TEXT_NODE) {
                switch(element.getNodeName()) {
                    case "rosterItem" : {
                        section.getRosters().add(new InvoiceRosterItemParser(element).parse());
                        break;                        
                    }
                }
            }
        }
        
        return section;
    }
    
}
