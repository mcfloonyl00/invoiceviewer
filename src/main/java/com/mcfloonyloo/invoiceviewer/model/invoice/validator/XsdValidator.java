/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.validator;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.xml.sax.SAXException;

/**
 *
 * @author avolkov
 */
public class XsdValidator {
    
    private static final String SCHEMA_LANGUAGE = "http://www.w3.org/2001/XMLSchema";
    
    private Validator validator = null;
    
    public XsdValidator(String xsd){
        try {
            this.validator = SchemaFactory.newInstance(SCHEMA_LANGUAGE)
                    .newSchema(new File(xsd))
                    .newValidator();
        } catch (SAXException ex) {
            this.validator = null;
        }
    }    
    
    public XsdValidator(File xsd){
        try {
            this.validator = SchemaFactory.newInstance(SCHEMA_LANGUAGE)
                    .newSchema(xsd)
                    .newValidator();
        } catch (SAXException ex) {
            this.validator = null;
        }
    }
    
    public XsdValidator(byte[] xsd){
        try {
            this.validator = SchemaFactory.newInstance(SCHEMA_LANGUAGE)
                    .newSchema(new StreamSource(new ByteArrayInputStream(xsd)))
                    .newValidator();
        } catch (SAXException ex) {
            this.validator = null;
        }
    }
    
    public boolean isValid(String xml){
        if(Optional.ofNullable(this.validator).isPresent()) {
            try{
                this.validator.validate(new StreamSource(new StringReader(xml)));
                return true;
            }catch(IOException | SAXException e){
                return false;
            }
        } else {
            return false;
        }
    }
    
    public boolean isValid(File xml){
        if(Optional.ofNullable(this.validator).isPresent()) {
            try{
                this.validator.validate(new StreamSource(xml));
                return true;
            }catch(IOException | SAXException e){
                return false;
            }
        } else {
            return false;
        }
    }
    
    public boolean isValid(byte[] xml){
        if(Optional.ofNullable(this.validator).isPresent()) {
            try{
                this.validator.validate(new StreamSource(new ByteArrayInputStream(xml)));
                return true;
            }catch(IOException | SAXException e){
                return false;
            }
        } else {
            return false;
        }
    }
    
    public List<Exception> validate(String xml){
        List<Exception> exceptions = new ArrayList<>();
        
        if(Optional.ofNullable(this.validator).isPresent()) {
            try{
                this.validator.setErrorHandler(new XsdValidationErrorHandler(exceptions));
                this.validator.validate(new StreamSource(new StringReader(xml)));
            }catch(IOException | SAXException e){
                return null;
            }
        }
        
        return exceptions;
    }
    
    public List<Exception> validate(File xml){
        List<Exception> exceptions = new ArrayList<>();
        
        if(Optional.ofNullable(this.validator).isPresent()) {
            try{
                this.validator.setErrorHandler(new XsdValidationErrorHandler(exceptions));
                this.validator.validate(new StreamSource(xml));
            }catch(IOException | SAXException e){
                return null;
            }
        }
        
        return exceptions;
    }
    
    public List<Exception> validate(byte[] xml){
        List<Exception> exceptions = new ArrayList<>();
        
        if(Optional.ofNullable(this.validator).isPresent()) {
            try{
                this.validator.setErrorHandler(new XsdValidationErrorHandler(exceptions));
                this.validator.validate(new StreamSource(new ByteArrayInputStream(xml)));
            }catch(IOException | SAXException e){
                return null;
            }
        }
        
        return exceptions;
    }
    
}
