/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.db.cayenne;

import com.mcfloonyloo.invoiceviewer.model.cayenne.AbstractDbEntity;
import java.sql.Types;
import org.apache.cayenne.map.DbAttribute;

/**
 *
 * @author avolkov
 */
abstract class AbstractInvoiceDocDbEntity extends AbstractDbEntity {

    public AbstractInvoiceDocDbEntity(String name) {
        super(name);
        init();
    }    
    
    private void init() {
        this.getDbEntity().addAttribute(createDateDoc());
        this.getDbEntity().addAttribute(createId());
        this.getDbEntity().addAttribute(createInvoiceId());
    }
   
    private DbAttribute createDateDoc() {
        DbAttribute dateDoc = new DbAttribute("datedoc");
        dateDoc.setType(Types.DATE);
        dateDoc.setMandatory(true);
        return dateDoc;
    }
    
    private DbAttribute createId() {
        DbAttribute id = new DbAttribute("id");
        id.setType(Types.NUMERIC);
        id.setPrimaryKey(true);
        id.setMandatory(true);
        return id;
    }
    
    private DbAttribute createInvoiceId() {
        DbAttribute invoiceId = new DbAttribute("invoiceid");
        invoiceId.setType(Types.NUMERIC);
        invoiceId.setMandatory(true);
        return invoiceId;
    }   
}
