/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.db.cayenne;

import com.mcfloonyloo.invoiceviewer.model.cayenne.AbstractObjEntity;
import org.apache.cayenne.map.ObjAttribute;

/**
 *
 * @author avolkov
 */
public class AbstractInvoiceDocObjEntity  extends AbstractObjEntity {
    
    private final String name;
    
    public AbstractInvoiceDocObjEntity(String name) {
        super(name+"DTO");
        this.name = name;
        init();
    }
    
    private void init() {
        this.getObjEntity().setClassName(PACKAGE+this.name+"DTO");
        this.getObjEntity().setDbEntityName(name);
        this.getObjEntity().addAttribute(createDateDoc());
    }    
    
    private ObjAttribute createDateDoc() {
        ObjAttribute dateDoc = new ObjAttribute("datedoc");
        dateDoc.setType("java.time.LocalDate");
        dateDoc.setDbAttributePath("datedoc");
        return dateDoc;
    }
}