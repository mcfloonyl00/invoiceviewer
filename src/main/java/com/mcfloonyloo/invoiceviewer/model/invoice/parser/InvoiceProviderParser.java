/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.parser;

import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceProvider;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author avolkov
 */
class InvoiceProviderParser extends AbstractInvoiceSectionParser<InvoiceProvider> {

    public InvoiceProviderParser(Node node) {
        super(node);
    }
   
    @Override
    public InvoiceProvider parse() {
        InvoiceProvider section = new InvoiceProvider();
        section.setNullValues();
        
        NodeList list = getNode().getChildNodes();
        for(int index = 0; index < list.getLength(); index++) {
            Node element = list.item(index);
            if(element.getNodeType() != Node.TEXT_NODE) {
                switch(element.getNodeName()) {
                    case "providerStatus" : {
                        section.setProviderStatus(element.getTextContent());
                        break;
                    }
                    case "dependentPerson" : {
                        section.setDependentPerson(element.getTextContent());
                        break;
                    }
                    case "residentsOfOffshore" : {
                        section.setResidentsOfOffshore(element.getTextContent());
                        break;
                    }
                    case "specialDealGoods" : {
                        section.setSpecialDealGoods(element.getTextContent());
                        break;
                    }
                    case "bigCompany" : {
                        section.setBigCompany(element.getTextContent());
                        break;
                    }
                    case "countryCode" : {
                        section.setCountryCode(element.getTextContent());
                        break;
                    }
                    case "unp" : {
                        section.setUnp(element.getTextContent());
                        break;
                    }
                    case "branchCode" : {
                        section.setBranchCode(element.getTextContent());
                        break;
                    }
                    case "name" : {
                        section.setName(element.getTextContent());
                        break;
                    }
                    case "address" : {
                        section.setAddress(element.getTextContent());
                        break;
                    }
                    case "principal" : {
                        section.setPrincipal(new InvoiceForInvoiceParser(element).parse());
                        break;
                    }
                    case "vendor" : {
                        section.setPrincipal(new InvoiceForInvoiceParser(element).parse());
                        break;
                    }
                    case "declaration" : {
                        section.setDeclaration(element.getTextContent());
                        break;
                    }
                    case "dateRelease" : {
                        section.setDateRelease(element.getTextContent());
                        break;
                    }
                    case "dateActualExport" : {
                        section.setDateActualExport(element.getTextContent());
                        break;
                    }
                    case "taxes" : {
                        section.setTaxes(new InvoiceTaxesParser(element).parse());
                        break;
                    }   
                }
            }
        }
        
        return section;
    }
    
}
