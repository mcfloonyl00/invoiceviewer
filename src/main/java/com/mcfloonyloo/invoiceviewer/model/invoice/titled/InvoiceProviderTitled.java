/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.titled;

import com.mcfloonyloo.invoiceviewer.model.Title;
import com.mcfloonyloo.invoiceviewer.model.invoice.DefaultInvoiceSection;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceProvider;
import java.util.Optional;

/**
 *
 * @author avolkov
 */
public class InvoiceProviderTitled implements DefaultInvoiceSection {
    
    private final Title<String> providerStatus;
    
    private final Title<String> dependentPerson;
    
    private final Title<String> residentsOfOffshore;
    
    private final Title<String> specialDealGoods;
    
    private final Title<String> bigCompany;
    
    private final Title<String> countryCode;
    
    private final Title<String> unp;
    
    private final Title<String> branchCode;
    
    private final Title<String> name;
    
    private final Title<String> address;
    
    private final Title<InvoiceForInvoiceTitled> principal;
    
    private final Title<InvoiceForInvoiceTitled> vendor;
    
    private final Title<String> declaration;
    
    private final Title<String> dateRelease;
    
    private final Title<String> dateActualExport;
    
    private final Title<InvoiceTaxesTitled> taxes;
    
    public InvoiceProviderTitled(InvoiceProvider provider) {
        this.providerStatus = new Title<>("6. Статус поставщика", true);
        this.dependentPerson = new Title<>("6.1. Взаимозависимое лицо", true);
        this.residentsOfOffshore = new Title<>("6.2. Сделка с резидентом оффшорной зоны", true);
        this.specialDealGoods = new Title<>("6.3. Сделка с товарами по перечню", true);
        this.bigCompany = new Title<>("6.4. Организация, включенная в перечень крупных плательщиков", true);
        this.countryCode = new Title<>("7. Код страны постащика", true);
        this.unp = new Title<>("8. УНП", true);
        this.branchCode = new Title<>("8.1. Код филиала (обособленного подразделения)", true);
        this.name = new Title<>("9. Поставщик", true);
        this.address = new Title<>("10. Юридический адрес", true);
        this.principal = new Title<>("11. Комитент", true);
        this.vendor = new Title<>("12. Продавец", true);
        this.declaration = new Title<>("13. Регистрационный номер выпуска товаров", true);
        this.dateRelease = new Title<>("13.1. Дата выпуска товаров", true);
        this.dateActualExport = new Title<>("13.2. Дата разрешения на убытие товаров", true);
        this.taxes = new Title<>("14. Реквизиты заявления о ввозе товаров", true);
        
        this.providerStatus.setValue(provider.getProviderStatus());
        this.dependentPerson.setValue(provider.getDependentPerson());
        this.residentsOfOffshore.setValue(provider.getResidentsOfOffshore());
        this.specialDealGoods.setValue(provider.getSpecialDealGoods());
        this.bigCompany.setValue(provider.getBigCompany());
        this.countryCode.setValue(provider.getCountryCode());
        this.unp.setValue(provider.getUnp());
        this.branchCode.setValue(provider.getBranchCode());
        this.name.setValue(provider.getName());
        this.address.setValue(provider.getAddress());
        if(Optional.ofNullable(provider.getPrincipal()).isPresent()) {
            this.principal.setValue(new InvoiceForInvoiceTitled(provider.getPrincipal()));
        }
        if(Optional.ofNullable(provider.getVendor()).isPresent()) {
            this.vendor.setValue(new InvoiceForInvoiceTitled(provider.getVendor()));
        }
        this.declaration.setValue(provider.getDeclaration());
        this.dateRelease.setValue(provider.getDateRelease());
        this.dateActualExport.setValue(provider.getDateActualExport());
        if(Optional.ofNullable(provider.getTaxes()).isPresent()) {
            this.taxes.setValue(new InvoiceTaxesTitled(provider.getTaxes()));
        }
    }
    
}
