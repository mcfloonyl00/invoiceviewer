 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.db;

import com.mcfloonyloo.invoiceviewer.model.invoice.Invoice;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceDocumentItem;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.query.ObjectSelect;

/**
 *
 * @author avolkov
 */
public class InvoiceInDAO extends AbstractInvoiceDAO 
        implements DefaultInvoiceDAO<InvoiceInDTO, InvoiceInDocDTO, InvoiceInStatusDTO> {
        

    public InvoiceInDAO(String config) {
        super(config);
    }
    
    @Override
    public void addInvoice(Invoice invoice) {
        ObjectContext context = createContext();
        
        InvoiceInDTO invoiceIn = context.newObject(InvoiceInDTO.class);
        invoiceIn.setNumber(invoice.getGeneral().getNumber());
        if(Optional.ofNullable(invoice.getProvider()).isPresent()) {
            invoiceIn.setUnp(invoice.getProvider().getUnp());
        }
        invoiceIn.setDatetrans(LocalDate.parse(invoice.getGeneral().getDateTransaction(), DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        if(Optional.ofNullable(invoice.getRoster()).isPresent()) {
            invoiceIn.setTotalcost(new BigDecimal(invoice.getRoster().getTotalCost()));
            invoiceIn.setTotalvat(new BigDecimal(invoice.getRoster().getTotalVat()));
            invoiceIn.setTotalcostvat(new BigDecimal(invoice.getRoster().getTotalCostVat()));
        }
        
        if(Optional.ofNullable(invoice.getDeliveryCondition()).isPresent() 
                && Optional.ofNullable(invoice.getDeliveryCondition().getContract()).isPresent()
                && Optional.ofNullable(invoice.getDeliveryCondition().getContract().getDocuments()).isPresent()) {
            ArrayList<InvoiceDocumentItem> documents = invoice.getDeliveryCondition().getContract().getDocuments();
            for(InvoiceDocumentItem item : documents) {
                try {
                    InvoiceInDocDTO docIn = context.newObject(InvoiceInDocDTO.class);
                    docIn.setDatedoc(LocalDate.parse(item.getDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                    invoiceIn.addToDoc(docIn);
                } catch (Exception ex) {
                    System.out.println(ex.getLocalizedMessage());
                }
            }
        }
        
        context.commitChanges();
        
        shutdownRuntime();
    }
    
    @Override
    public boolean isInvoice(String number) {
        boolean is;
        
        ObjectContext context = createContext();
        is = !ObjectSelect.query(InvoiceInDTO.class).where(InvoiceInDTO.NUMBER.like(number)).select(context).isEmpty();
        shutdownRuntime();
        
        return is;
    }
    
    
    @Override
    public InvoiceInDTO getInvoice(String number) {
        ObjectContext context = createContext();
        InvoiceInDTO invoiceIn = ObjectSelect.query(InvoiceInDTO.class).where(InvoiceInDTO.NUMBER.like(number)).selectOne(context);
        shutdownRuntime();
        
        return invoiceIn;
    }


    @Override
    public ArrayList<InvoiceInStatusDTO> getStatuses(String number) {
        return (ArrayList<InvoiceInStatusDTO>) getInvoice(number).getStatuses();
    }

    @Override
    public ArrayList<InvoiceInDocDTO> getDocs(String number) {
        return (ArrayList<InvoiceInDocDTO>) getInvoice(number).getDocs();
    }

    public ArrayList<InvoiceInDTO> getAll() {
        ArrayList<InvoiceInDTO> invoices = new ArrayList<>();
        
        ObjectContext context = createContext();
        invoices.addAll(ObjectSelect.query(InvoiceInDTO.class).select(context));
        shutdownRuntime();
        
        return invoices;
    }

    @Override
    public ArrayList<InvoiceInDTO> getInvoices(LocalDate from, LocalDate to) {
        ArrayList<InvoiceInDTO> invoices = new ArrayList<>();
        
        ObjectContext context = createContext();
        List<InvoiceInDTO> invoicesDb = ObjectSelect.query(InvoiceInDTO.class).where(InvoiceInDTO.DATETRANS.between(from, to)).select(context);
        if(Optional.ofNullable(invoicesDb).isPresent()) {
            invoices.addAll(invoicesDb);
        }
        shutdownRuntime();
        
        return invoices;
    }

}
