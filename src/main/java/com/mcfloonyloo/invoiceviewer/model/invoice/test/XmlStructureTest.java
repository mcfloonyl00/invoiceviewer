/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.test;

import com.mcfloonyloo.invoiceviewer.model.invoice.Invoice;

/**
 *
 * @author avolkov
 */
public class XmlStructureTest extends AbstractTest {
    
    public XmlStructureTest(Invoice invoice) {
        super(invoice);
        test();
    }
    
    private void test() {
        XmlStructureTestItem item = new XmlStructureTestItem(getInvoice());
        item.test();
        if(!item.getErrors().isEmpty()) {
            this.getErrors().add(item);
        }
    }
    
}
