/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.validator;

import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceSenderReceiver;
import com.mcfloonyloo.invoiceviewer.model.invoice.test.ErrorItem;

/**
 *
 * @author avolkov
 */
public class InvoiceSenderReceiverValidator extends AbstractSectionValidator {
    
    private final static String SECTION_NAME = "[Реквизиты].";
    
    private final InvoiceSenderReceiver senderReceiver;
    
    public InvoiceSenderReceiverValidator(InvoiceSenderReceiver senderReceiver) {
        this.senderReceiver = senderReceiver;
    }
    
    public ErrorItem validateConsignors() {
        ErrorItem item = getValidator().validateList(this.senderReceiver.getConsignors());
        item.setFieldName(SECTION_NAME+"[Список грузоотправителей]");
        return item;
    }
    
    public ErrorItem validateConsignees() {
        ErrorItem item = getValidator().validateList(this.senderReceiver.getConsignees());
        item.setFieldName(SECTION_NAME+"[Список грузополучателей]");
        return item;
    }
    
}
