/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.titled;

import com.mcfloonyloo.invoiceviewer.model.Title;
import com.mcfloonyloo.invoiceviewer.model.invoice.DefaultInvoiceSubSection;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceDocumentType;

/**
 *
 * @author avolkov
 */
public class InvoiceDocumentTypeTitled implements DefaultInvoiceSubSection {
    
    private final Title<String> code;
    
    private final Title<String> value;
    
    public InvoiceDocumentTypeTitled(InvoiceDocumentType documentType) {
        this.code = new Title<>("        Код вида документа", true);
        this.value = new Title<>("        Название вида документа", true);
        
        this.code.setValue(documentType.getCode());
        this.value.setValue(documentType.getValue());
    }
    
}
