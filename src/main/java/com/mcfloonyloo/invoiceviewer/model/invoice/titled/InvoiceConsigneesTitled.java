/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.titled;

import com.mcfloonyloo.invoiceviewer.model.Title;
import com.mcfloonyloo.invoiceviewer.model.invoice.DefaultInvoiceSubSection;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceSenderItem;
import java.util.ArrayList;

/**
 *
 * @author avolkov
 */
public class InvoiceConsigneesTitled implements DefaultInvoiceSubSection {
    
    private final Title<ArrayList<Title<InvoiceSenderItemTitled>>> consignees;
    
    public InvoiceConsigneesTitled(ArrayList<InvoiceSenderItem> consignees) {
        this.consignees = new Title<>("26-29. Реквизиты грузополучателя", true);
        this.consignees.setValue(new ArrayList<>());
        int index = 0;
        for(InvoiceSenderItem item : consignees) {
            index++;
            Title<InvoiceSenderItemTitled> itemTitle = new Title<>("  Реквизит №"+index, true);
            itemTitle.setValue(new InvoiceSenderItemTitled(item));
            this.consignees.getValue().add(itemTitle);
        }
    }
    
}
