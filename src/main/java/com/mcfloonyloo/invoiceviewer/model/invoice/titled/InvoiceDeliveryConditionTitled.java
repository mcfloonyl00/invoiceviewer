/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.titled;

import com.mcfloonyloo.invoiceviewer.model.Title;
import com.mcfloonyloo.invoiceviewer.model.invoice.DefaultInvoiceSection;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceDeliveryCondition;

/**
 *
 * @author avolkov
 */
public class InvoiceDeliveryConditionTitled implements DefaultInvoiceSection {
    
    private final Title<InvoiceContractTitled> contract;
    
    private final Title<String> description;
        
    public InvoiceDeliveryConditionTitled(InvoiceDeliveryCondition deliveryCondition) {
        this.contract = new Title<>("30. Договор (контракт) на поставку товара", true);
        this.description = new Title<>("31. Дополнительные сведения", true);
        
        this.description.setValue(deliveryCondition.getDescription());
        this.contract.setValue(new InvoiceContractTitled(deliveryCondition.getContract()));
    }
    
}
