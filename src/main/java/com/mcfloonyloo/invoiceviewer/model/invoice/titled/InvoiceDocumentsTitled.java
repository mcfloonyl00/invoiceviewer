/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.titled;

import com.mcfloonyloo.invoiceviewer.model.Title;
import com.mcfloonyloo.invoiceviewer.model.invoice.DefaultInvoiceSubSection;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceDocumentItem;
import java.util.ArrayList;

/**
 *
 * @author avolkov
 */
public class InvoiceDocumentsTitled implements DefaultInvoiceSubSection {
    
    private final Title<ArrayList<Title<InvoiceDocumentItemTitled>>> documents;
    
    public InvoiceDocumentsTitled(ArrayList<InvoiceDocumentItem> documents) {
        this.documents = new Title<>("  Список сопроводительных документов",true);
        this.documents.setValue(new ArrayList<>());
        int index = 0;
        for(InvoiceDocumentItem item : documents) {
            index++;
            Title<InvoiceDocumentItemTitled> itemTitle = new Title<>("    Документ №"+index, true);
            itemTitle.setValue(new InvoiceDocumentItemTitled(item));
            this.documents.getValue().add(itemTitle);
        }
    }
    
}
