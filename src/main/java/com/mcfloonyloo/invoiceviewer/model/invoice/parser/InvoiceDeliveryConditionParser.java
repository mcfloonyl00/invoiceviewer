/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.parser;

import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceDeliveryCondition;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author avolkov
 */
class InvoiceDeliveryConditionParser extends AbstractInvoiceSectionParser<InvoiceDeliveryCondition> {

    public InvoiceDeliveryConditionParser(Node node) {
        super(node);
    }

    @Override
    public InvoiceDeliveryCondition parse() {
        InvoiceDeliveryCondition section = new InvoiceDeliveryCondition();
        section.setNullValues();
        
        NodeList list = getNode().getChildNodes();
        for(int index = 0; index < list.getLength(); index++) {
            Node element = list.item(index);
            if(element.getNodeType() != Node.TEXT_NODE) {
                switch(element.getNodeName()) {
                    case "description" : { 
                        section.setDescription(element.getTextContent());
                        break;
                    }
                    case "contract" : { 
                        section.setContract(new InvoiceContractParser(element).parse());
                        break;
                    }
                }
            }
        }
        
        return section;
    }
    
}
