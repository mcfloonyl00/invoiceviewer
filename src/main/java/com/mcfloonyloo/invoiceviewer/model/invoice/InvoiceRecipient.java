/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice;

/**
 *
 * @author avolkov
 */
public class InvoiceRecipient implements DefaultInvoiceSection, DefaultInvoiceNullable {
    
    private String recipientStatus;
    
    private String dependentPerson;
    
    private String residentsOfOffshore;
    
    private String specialDealGoods;
    
    private String bigCompany;
    
    private String countryCode;
    
    private String unp;
    
    private String branchCode;
    
    private String name;
    
    private String address;
    
    private String declaration;
    
    private InvoiceTaxes taxes;
    
    private String dateImport;
    
    public String getRecipientStatus() {
        return this.recipientStatus;
    }
    
    public String getDependentPerson() {
        return this.dependentPerson;
    }
    
    public String getResidentsOfOffshore() {
        return this.residentsOfOffshore;
    }
    
    public String getSpecialDealGoods() {
        return this.specialDealGoods;
    }
    
    public String getBigCompany() {
        return this.bigCompany;
    }
    
    public String getCountryCode() {
        return this.countryCode;
    }
    
    public String getUnp() {
        return this.unp;
    }
    
    public String getBranchCode() {
        return this.branchCode;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getAddress() {
        return this.address;
    }
    
    public String getDeclaration() {
        return this.declaration;
    }
    
    public InvoiceTaxes getTaxes() {
        return this.taxes;
    }
    
    public String getDateImport() {
        return this.dateImport;
    }
    
    public void setRecipientStatus(String recipientStatus) {
        this.recipientStatus = recipientStatus;
    }
    
    public void setDependentPerson(String dependentPerson) {
        this.dependentPerson = dependentPerson;
    }
    
    public void setResidentsOfOffshore(String residentsOfOffshore) {
        this.residentsOfOffshore = residentsOfOffshore;
    }
    
    public void setSpecialDealGoods(String specialDealGoods) {
        this.specialDealGoods = specialDealGoods;
    }
    
    public void setBigCompany(String bigCompany) {
        this.bigCompany = bigCompany;
    }
    
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
    
    public void setUnp(String unp) {
        this.unp = unp;
    }
    
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }
    
    public void setDeclaration(String declaration) {
        this.declaration = declaration;
    }
    
    public void setTaxes(InvoiceTaxes taxes) {
        this.taxes = taxes;
    }
    
    public void setDateImport(String dateImport) {
        this.dateImport = dateImport;
    }
    
    public InvoiceRecipient() {
        defaultValues();
    }
    
    private void defaultValues() {
        this.recipientStatus = "";
        this.dependentPerson = "";
        this.residentsOfOffshore = "";
        this.specialDealGoods = "";
        this.bigCompany = "";
        this.countryCode = "";
        this.unp = "";
        this.branchCode = "";
        this.name = "";
        this.address = "";
        this.declaration = "";
        this.taxes = new InvoiceTaxes();
        this.dateImport = "";
    }

    @Override
    public void setNullValues() {
        this.recipientStatus = null;
        this.dependentPerson = null;
        this.residentsOfOffshore = null;
        this.specialDealGoods = null;
        this.bigCompany = null;
        this.countryCode = null;
        this.unp = null;
        this.branchCode = null;
        this.name = null;
        this.address = null;
        this.declaration = null;
        this.taxes = null;
        this.dateImport = null;
    }
    
}
