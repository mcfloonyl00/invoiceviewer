/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice;

import java.io.File;
import java.util.Optional;

/**
 * 
 * @author avolkov
 */
public class XsdInvoiceLoader {
    
    private final String folder;
    
    private final InvoiceGeneral general;
    
    public byte[] getByte() throws Exception {
        return new FileXmlReader(load()).read();
    }
    
    public File getFile() throws Exception {
        return load();
    }
    
    public XsdInvoiceLoader(String folder, InvoiceGeneral general) {
        this.folder = folder;
        this.general = general;
    }
    
    protected File load() throws Exception {
        File file = null;
        
        switch(this.general.getDocumentType().toUpperCase()) {
            case "ORIGINAL" : 
            case "ADD_NO_REFERENCE" : {
                file = new File(this.folder, "MNSATI_original.xsd");
                break;
            }
            case "FIXED":{
                file = new File(this.folder, "MNSATI_fixed.xsd");
                break;
            }
            case "ADDITIONAL":{
                file = new File(this.folder, "MNSATI_additional.xsd");
                break;
            }
            default : {
                String message = new StringBuilder("Неизвестный тип ЭСЧФ '")
                        .append(this.folder)
                        .append("' - ")
                        .append(this.general.getDocumentType().toUpperCase())
                        .toString();
                throw new Exception(message);
            }
        }
        
        String message;
        if(Optional.ofNullable(file).isPresent()) {
            if(file.exists()) {
                if(file.isFile()) {
                    return file;
                } else {
                    message = new StringBuilder("Объект '")
                            .append(file.getAbsolutePath())
                            .append("' не является файлом XSD")
                            .toString();
                }
            } else {
                message = new StringBuilder("Файл XSD '")
                        .append(file.getAbsolutePath())
                        .append("' не существует")
                        .toString();
            }
        } else {
            message = new StringBuilder("Ошибка формирования объекта файла XSD '")
                    .toString();
        }
        throw new Exception(message);
    }
    
}
