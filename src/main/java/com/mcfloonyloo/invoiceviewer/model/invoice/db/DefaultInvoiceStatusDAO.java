/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.db;

import by.avest.edoc.client.AvEStatus;
import com.mcfloonyloo.invoiceviewer.model.UpdateInvoiceItem;
import com.mcfloonyloo.invoiceviewer.model.xmldb.XmldbService;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author avolkov
 */
public interface DefaultInvoiceStatusDAO<E extends DefaultInvoiceStatusDTO> {
    
    public void addStatus(String number, AvEStatus status);
        
    public boolean isStatus(String number, AvEStatus status);
    
    public E getStatus(String number);
    
    public static String getStatusRu(String statusEn) {
        String statusRu = XmldbService.INSTANCE.getInvoiceStatus().get(statusEn).getNameru();
        return statusRu;
    }
    
    public ArrayList<UpdateInvoiceItem> getInvoicesAndStatuses(LocalDate from, LocalDate to);
    
}
