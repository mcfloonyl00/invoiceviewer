/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.parser;

import com.mcfloonyloo.invoiceviewer.model.invoice.Invoice;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author avolkov
 */
public class InvoiceParser {
    
    private final Document document;
    
    public InvoiceParser(Document document) {
        this.document = document;
    }
    
    public Invoice parse() {
        Invoice invoice = new Invoice();
        invoice.setNullValues();
        
        Node root = this.document.getDocumentElement();
        invoice.setSender(root.getAttributes().getNamedItem("sender").getNodeValue());
        NodeList list = root.getChildNodes();
        for(int index = 0; index < list.getLength(); index++) {
            Node element = list.item(index);
            if(element.getNodeType() != Node.TEXT_NODE) {
                switch(element.getNodeName()) {
                    case "general" : {
                        invoice.setGeneral(new InvoiceGeneralParser(element).parse());
                        break;
                    }
                    case "provider" : {
                        invoice.setProvider(new InvoiceProviderParser(element).parse());
                        break;
                    }
                    case "recipient" : {
                        invoice.setRecipient(new InvoiceRecipientParser(element).parse());
                        break;
                    }
                    case "senderReceiver" : {
                        invoice.setSenderReceiver(new InvoiceSenderReceiverParser(element).parse());
                        break;
                    }
                    case "deliveryCondition" : {
                        invoice.setDeliveryCondition(new InvoiceDeliveryConditionParser(element).parse());
                        break;
                    }
                    case "roster" : {
                        invoice.setRoster(new InvoiceRosterParser(element).parse());
                        break;
                    }
                }
            }
        }
        
        return invoice;
    }
    
}
