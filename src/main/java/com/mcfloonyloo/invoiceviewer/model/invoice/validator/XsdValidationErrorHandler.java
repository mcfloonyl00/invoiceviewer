/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.validator;

import java.util.List;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 *
 * @author avolkov
 */
public class XsdValidationErrorHandler implements ErrorHandler {

    private final List<Exception> exceptions;
    
    public XsdValidationErrorHandler(List<Exception> exceptions) {
        this.exceptions = exceptions;
    }
    
    @Override
    public void warning(SAXParseException exception) throws SAXException {
        this.exceptions.add(exception);
    }

    @Override
    public void error(SAXParseException exception) throws SAXException {
        this.exceptions.add(exception);
    }

    @Override
    public void fatalError(SAXParseException exception) throws SAXException {
        this.exceptions.add(exception);
    }
    
}
