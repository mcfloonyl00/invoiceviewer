/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.titled;

import com.mcfloonyloo.invoiceviewer.model.Title;
import com.mcfloonyloo.invoiceviewer.model.invoice.DefaultInvoiceSubSectionItem;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceSenderItem;

/**
 *
 * @author avolkov
 */
public class InvoiceSenderItemTitled implements DefaultInvoiceSubSectionItem {

    private final Title<String> countryCode;
    
    private final Title<String> unp;
    
    private final Title<String> name;
    
    private final Title<String> address;
    
    public InvoiceSenderItemTitled(InvoiceSenderItem senderItem) {
        this.countryCode = new Title<>("    Код страны грузоотправителя", true);
        this.unp = new Title<>("    УНП", true);
        this.name = new Title<>("    Наименование", true);
        this.address = new Title<>("    Адрес", true);
        
        this.countryCode.setValue(senderItem.getCountryCode());
        this.unp.setValue(senderItem.getUnp());
        this.name.setValue(senderItem.getName());
        this.address.setValue(senderItem.getAddress());
    }
    
}
