/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.titled;

import com.mcfloonyloo.invoiceviewer.model.Title;
import com.mcfloonyloo.invoiceviewer.model.invoice.DefaultInvoiceSubSection;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceVat;

/**
 *
 * @author avolkov
 */
class InvoiceVatTitled implements DefaultInvoiceSubSection {
    
    private final Title<String> rate;
    
    private final Title<String> rateType; 
    
    private final Title<String> summaVat;
    
    public InvoiceVatTitled(InvoiceVat vat) {
        this.rate = new Title<>("      9.1. Ставка НДС", true);
        this.rateType = new Title<>("      9.2. Ставка НДС (тип)", true);
        this.summaVat = new Title<>("      9.3. Сумма НДС", true);
        
        this.rate.setValue(vat.getRate());
        this.rateType.setValue(vat.getRateType());
        this.summaVat.setValue(vat.getSummaVat());
    }
    
}
