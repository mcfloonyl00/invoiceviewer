/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.validator;

import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceGeneral;
import com.mcfloonyloo.invoiceviewer.model.invoice.test.ErrorItem;
import com.mcfloonyloo.invoiceviewer.model.xmldb.XmldbService;
import java.util.Optional;

/**
 *
 * @author avolkov
 */
public class InvoiceGeneralValidator extends AbstractSectionValidator {
    
    private final static String SECTION_NAME = "[Основная информация].";
    
    private final InvoiceGeneral general;
    
    public InvoiceGeneralValidator(InvoiceGeneral general) {
        this.general = general;
    }
    
    public ErrorItem validateDateTransaction() {
        ErrorItem item = getValidator().validateField(this.general.getDateTransaction());
        item.setFieldName(SECTION_NAME+"[Дата совершения транзакции]");
        return item;
    }
    
    public ErrorItem validateDocumentType() {
        ErrorItem item = getValidator().validateField(this.general.getDocumentType());
        item.setFieldName(SECTION_NAME+"[Тип ЭСЧФ]");
        return item;
    }
    
    public ErrorItem validateDocumentTypeDatabase() {
        ErrorItem item = getValidator().validateField(this.general.getDocumentType());
        if(Optional.ofNullable(item.getMessage()).isPresent()) {
            item.setFieldName(SECTION_NAME+"[Тип ЭСЧФ]");
            return item;
        } else {
            if(XmldbService.INSTANCE.getDocumentType().isRecord(this.general.getDocumentType())) {
                return new ErrorItem(null);
            } else {
                ErrorItem documentType = new ErrorItem("значение '"+this.general.getDocumentType()+"' не найдено в таблице 'DocumentType'");
                documentType.setFieldName(SECTION_NAME+"[Тип ЭСЧФ]");
                return documentType;
            }
        }
    }
    
    public ErrorItem validateByInvoice(ErrorItem compareItem) {
        ErrorItem item = getValidator().validateField(this.general.getByInvoice());
        item.setFieldName(compareItem+" - "+SECTION_NAME+"[К ЭСЧФ]");
        return item;
    }
    
    public ErrorItem checkTypeInvoice(ErrorItem compareItem) { 
        ErrorItem item = validateDocumentType();
        
        if(Optional.ofNullable(item).isPresent()) {
            return item;
        } else {
            switch(this.general.getDocumentType()) {
                case "ADDITIONAL" : {
                    ErrorItem byInvoice = validateByInvoice(compareItem);
                    if(Optional.ofNullable(byInvoice).isPresent()) {
                        return byInvoice;
                    } else {
                        return new ErrorItem(null);
                    }
                }
                case "ADD_NO_REFERENCE" : {
                    return new ErrorItem(null);
                }
                default : {
                    ErrorItem defaultItem = new ErrorItem("внимание - получено отрицательное значение");
                    defaultItem.setFieldName(compareItem.getFieldName()+" - "+SECTION_NAME+"[Тип ЭСЧФ]");
                    return defaultItem;
                }
            }
        }
    }
    
}
