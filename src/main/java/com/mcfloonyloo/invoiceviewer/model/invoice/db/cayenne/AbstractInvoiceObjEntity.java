/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.db.cayenne;

import com.mcfloonyloo.invoiceviewer.model.cayenne.AbstractObjEntity;
import org.apache.cayenne.map.ObjAttribute;

/**
 *
 * @author avolkov
 */
abstract class AbstractInvoiceObjEntity extends AbstractObjEntity {
    
    private final String name;
    
    public AbstractInvoiceObjEntity(String name) {
        super(name+"DTO");
        this.name = name;
        init();
    }
    
    private void init() {
        this.getObjEntity().setClassName(PACKAGE+this.name+"DTO");
        this.getObjEntity().setDbEntityName(this.name);
        this.getObjEntity().addAttribute(createDateTrans());
        this.getObjEntity().addAttribute(createNumber());
        this.getObjEntity().addAttribute(createTotalCost());
        this.getObjEntity().addAttribute(createTotalCostVat());
        this.getObjEntity().addAttribute(createTotalVat());
        this.getObjEntity().addAttribute(createUnp());
    }
    
    private ObjAttribute createDateTrans() {
        ObjAttribute dateTrans = new ObjAttribute("datetrans");
        dateTrans.setType("java.time.LocalDate");
        dateTrans.setDbAttributePath("datetrans");
        return dateTrans;
    }
    
    private ObjAttribute createNumber() {
        ObjAttribute number = new ObjAttribute("number");
        number.setType("java.lang.String");
        number.setDbAttributePath("number");
        return number;
    }
    
    private ObjAttribute createTotalCost() {
        ObjAttribute totalCost = new ObjAttribute("totalcost");
        totalCost.setType("java.math.BigDecimal");
        totalCost.setDbAttributePath("totalcost");
        return totalCost;
    }
    
    private ObjAttribute createTotalCostVat() {
        ObjAttribute totalCostVat = new ObjAttribute("totalcostvat");
        totalCostVat.setType("java.math.BigDecimal");
        totalCostVat.setDbAttributePath("totalcostvat");
        return totalCostVat;
    }
    
    private ObjAttribute createTotalVat() {
        ObjAttribute totalVat = new ObjAttribute("totalvat");
        totalVat.setType("java.math.BigDecimal");
        totalVat.setDbAttributePath("totalvat");
        return totalVat;
    }
    
    private ObjAttribute createUnp() {
        ObjAttribute unp = new ObjAttribute("unp");
        unp.setType("java.lang.String");
        unp.setDbAttributePath("unp");
        return unp;
    }
    
}
