/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.parser;

import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceRosterItem;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author avolkov
 */
class InvoiceRosterItemParser extends AbstractInvoiceSectionParser<InvoiceRosterItem> {

    public InvoiceRosterItemParser(Node node) {
        super(node);
    }

    @Override
    public InvoiceRosterItem parse() {
        InvoiceRosterItem section = new InvoiceRosterItem();
        
        NodeList list = getNode().getChildNodes();
        for(int index = 0; index < list.getLength(); index++) {
            Node element = list.item(index);
            if(element.getNodeType() != Node.TEXT_NODE) {
                switch(element.getNodeName()) {
                    case "number" : {
                        section.setNumber(element.getTextContent());
                        break;
                    }
                    case "name" : {
                        section.setName(element.getTextContent());
                        break;
                    }
                    case "code" : {
                        section.setCode(element.getTextContent());
                        break;
                    }
                    case "code_oced" : {
                        section.setCodeOced(element.getTextContent());
                        break;
                    }
                    case "units" : {
                        section.setUnits(element.getTextContent());
                        break;
                    }
                    case "count" : {
                        section.setCount(element.getTextContent());
                        break;
                    }
                    case "price" : {
                        section.setPrice(element.getTextContent());
                        break;
                    }
                    case "cost" : {
                        section.setCost(element.getTextContent());
                        break;
                    }
                    case "summaExcise" : {
                        section.setSummaExcise(element.getTextContent());
                        break;
                    }
                    case "vat" : {
                        section.setVat(new InvoiceVatParser(element).parse()); 
                        break;}
                    case "costVat" : {
                        section.setCostVat(element.getTextContent());
                        break;
                    }
                    case "descriptions" : {
                        section.setDescriptions(new InvoiceDescriptionItemParser(element).parse());
                        break;
                    }
                    case "skipDeduction" : {
                        section.setSkipDeduction(element.getTextContent());
                        break;
                    }
                }
            }
        }
        
        return section;
    }
    
}
