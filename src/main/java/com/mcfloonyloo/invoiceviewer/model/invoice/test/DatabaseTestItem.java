/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.test;

import com.mcfloonyloo.invoiceviewer.model.invoice.Invoice;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceDocumentItem;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceRosterItem;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceSenderItem;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceContractValidator;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceDeliveryConditionValidator;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceDescriptionsValidator;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceDocumentItemValidator;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceGeneralValidator;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceProviderValidator;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceRecipientValidator;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceRosterItemValidator;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceRosterValidator;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceSenderItemValidator;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceSenderReceiverValidator;
import java.util.Optional;

/**
 *
 * @author avolkov
 */
public class DatabaseTestItem extends AbstractTestItem {

    public DatabaseTestItem(Invoice invoice) {
        super(invoice);
        setTitle("[DATABASE TEST] Проверка значений на их наличие в базе данных - обнаружены проблемы : ");
    }

    @Override
    public void test() {

        InvoiceGeneralValidator generalValidator = new InvoiceGeneralValidator(getInvoice().getGeneral());
        validateGeneral(generalValidator, this.getErrors());
        
        InvoiceProviderValidator providerValidator = new InvoiceProviderValidator(getInvoice().getProvider());
        validateProvider(providerValidator, this.getErrors());
        
        InvoiceRecipientValidator recipientValidator = new InvoiceRecipientValidator(getInvoice().getRecipient());
        validateRecipient(recipientValidator, this.getErrors());
        
        InvoiceDeliveryConditionValidator deliveryValidator = new InvoiceDeliveryConditionValidator(getInvoice().getDeliveryCondition());
        validateDeliveryCondition(deliveryValidator, this.getErrors());
        
        InvoiceSenderReceiverValidator senderValidator = new InvoiceSenderReceiverValidator(getInvoice().getSenderReceiver());
        validateSenderReceiver(senderValidator, this.getErrors());
        
        InvoiceRosterValidator rosterValidator = new InvoiceRosterValidator(getInvoice().getRoster());
        validateRoster(rosterValidator, this.getErrors());
        
    }
    
    private void validateGeneral(InvoiceGeneralValidator generalValidator, ErrorList errors) {
        ErrorItem generalError = generalValidator.validateDocumentTypeDatabase();
        if(Optional.ofNullable(generalError.getMessage()).isPresent()) {
            errors.add(generalError);
        }
    }
    
    private void validateProvider(InvoiceProviderValidator providerValidator, ErrorList errors) {
        ErrorItem providerCountryError = providerValidator.validateCountryCodeDatabase();
        if(Optional.ofNullable(providerCountryError.getMessage()).isPresent()) {
            errors.add(providerCountryError);
        }
        ErrorItem providerUnpBranchError = providerValidator.validateUnpBranchCodeDatabase();
        if(Optional.ofNullable(providerUnpBranchError.getMessage()).isPresent()) {
            errors.add(providerUnpBranchError);
        }
        ErrorItem providerStatusError = providerValidator.validateStatusDatabase();
        if(Optional.ofNullable(providerStatusError.getMessage()).isPresent()) {
            errors.add(providerStatusError);
        }
    }
    
    private void validateRecipient(InvoiceRecipientValidator recipientValidator, ErrorList errors) {
        ErrorItem recipientCountryError = recipientValidator.validateCountryCodeDatabase();
        if(Optional.ofNullable(recipientCountryError.getMessage()).isPresent()) {
            errors.add(recipientCountryError);
        }
        ErrorItem recipientUnpBranchError = recipientValidator.validateUnpBranchCodeDatabase();
        if(Optional.ofNullable(recipientUnpBranchError.getMessage()).isPresent()) {
            errors.add(recipientUnpBranchError);
        }
        ErrorItem recipientStatusError = recipientValidator.validateStatusDatabase();
        if(Optional.ofNullable(recipientStatusError.getMessage()).isPresent()) {
            errors.add(recipientStatusError);
        }
    }
    
    private void validateDeliveryCondition(InvoiceDeliveryConditionValidator deliveryValidator, ErrorList errors) {
        ErrorItem deliveryError = deliveryValidator.validateContract();
        if(Optional.ofNullable(deliveryError.getMessage()).isPresent()) {
            errors.add(deliveryError);
        } else {
            InvoiceContractValidator contractValidator = new InvoiceContractValidator(getInvoice().getDeliveryCondition().getContract());
            ErrorItem contractError = contractValidator.validateDocuments();
            if(Optional.ofNullable(contractError.getMessage()).isPresent()) {
                errors.add(contractError);
            } else {
                int documentIndex = 0;
                for(InvoiceDocumentItem documentItem : getInvoice().getDeliveryCondition().getContract().getDocuments()) {
                    documentIndex++;
                    InvoiceDocumentItemValidator documentItemValidator = new InvoiceDocumentItemValidator(documentIndex, documentItem);
                    validateDocumentItem(documentItemValidator, errors);
                }
            }
        }
    }
    
    private void validateDocumentItem(InvoiceDocumentItemValidator documentItemValidator, ErrorList errors) {
        ErrorItem documentItemError = documentItemValidator.validateDocumentTypeCodeDatabase();
        if(Optional.ofNullable(documentItemError.getMessage()).isPresent()) {
            errors.add(documentItemError);
        }
    }
    
    private void validateSenderReceiver(InvoiceSenderReceiverValidator senderValidator, ErrorList errors) {
        validateConsignors(senderValidator, errors);
        validateConsignees(senderValidator, errors);
    }
    
    private void validateConsignors(InvoiceSenderReceiverValidator senderValidator, ErrorList errors) {
        ErrorItem consignorsError = senderValidator.validateConsignors();
        if(Optional.ofNullable(consignorsError.getMessage()).isPresent()) {
            errors.add(consignorsError);
        } else {
            int senderIndex = 0;
            for(InvoiceSenderItem senderItem : getInvoice().getSenderReceiver().getConsignors()) {
                senderIndex++;
                InvoiceSenderItemValidator senderItemValidator = new InvoiceSenderItemValidator(senderIndex, senderItem);
                validateSenderItem(senderItemValidator, consignorsError, errors);
            }
        }
    }
    
    private void validateConsignees(InvoiceSenderReceiverValidator senderValidator, ErrorList errors) {
        ErrorItem consigneesError = senderValidator.validateConsignees();
        if(Optional.ofNullable(consigneesError.getMessage()).isPresent()) {
            errors.add(consigneesError);
        } else {
            int senderIndex = 0;
            for(InvoiceSenderItem senderItem : getInvoice().getSenderReceiver().getConsignees()) {
                senderIndex++;
                InvoiceSenderItemValidator senderItemValidator = new InvoiceSenderItemValidator(senderIndex, senderItem);
                validateSenderItem(senderItemValidator, consigneesError, errors);
            }
        }
    }
    
    private void validateSenderItem(InvoiceSenderItemValidator senderItemValidator, ErrorItem senderErrorItem, ErrorList errors) {
        ErrorItem countryError = senderItemValidator.validateCountryCodeDatabase(senderErrorItem);
        if(Optional.ofNullable(countryError.getMessage()).isPresent()) {
            errors.add(countryError);
        }
    }
    
    private void validateRoster(InvoiceRosterValidator rosterValidator, ErrorList errors) {
        ErrorItem rosterError = rosterValidator.validateRosters();
        if(Optional.ofNullable(rosterError.getMessage()).isPresent()) {
            errors.add(rosterError);
        } else {
            int rosterIndex = 0;
            for(InvoiceRosterItem rosterItem : getInvoice().getRoster().getRosters()) {
                rosterIndex++;
                
                InvoiceRosterItemValidator rosterItemValidator = new InvoiceRosterItemValidator(rosterIndex, rosterItem);
                validateUnit(rosterItemValidator, errors);
                validateVatRate(rosterItemValidator, errors);
                
                InvoiceDescriptionsValidator descriptionsValidator = new InvoiceDescriptionsValidator(rosterIndex, rosterItem.getDescriptions());
                validateDescriptions(descriptionsValidator, errors);
            }
        }
    }
    
    private void validateUnit(InvoiceRosterItemValidator rosterItemValidator, ErrorList errors) {
        ErrorItem unitError = rosterItemValidator.validateUnitDatabase();
        if(Optional.ofNullable(unitError.getMessage()).isPresent()) {
            errors.add(unitError);
        }
    }
    
    private void validateVatRate(InvoiceRosterItemValidator rosterItemValidator, ErrorList errors) {
        ErrorItem vatRateError = rosterItemValidator.validateVatRateDatabase();
        if(Optional.ofNullable(vatRateError.getMessage()).isPresent()) {
            errors.add(vatRateError);
        }
    }
    
    private void validateDescriptions(InvoiceDescriptionsValidator descriptionsValidator, ErrorList errors) {
        ErrorItem descriptionError = descriptionsValidator.validateDescriptionsDatabase();
        if(Optional.ofNullable(descriptionError.getMessage()).isPresent()) {
            errors.add(descriptionError);
        }
    }
    
}
