/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.test;

import com.mcfloonyloo.invoiceviewer.model.invoice.Invoice;

/**
 *
 * @author avolkov
 */
public class BaseTest extends AbstractTest {
    
    public BaseTest(Invoice invoice) {
        super(invoice);
        test();
    }
    
    private void test() {
        ContractTestItem contractTestItem = new ContractTestItem(this.getInvoice());
        contractTestItem.test();
        if(contractTestItem.getErrors().size() > 0) {
            this.getErrors().add(contractTestItem);
        }
        
        DateIssuanceTestItem dateIssuanceTestItem = new DateIssuanceTestItem(getInvoice());
        dateIssuanceTestItem.test();
        if(dateIssuanceTestItem.getErrors().size() > 0) {
            this.getErrors().add(dateIssuanceTestItem);
        }
        
        RequisiteTestItem requisiteTestItem = new RequisiteTestItem(getInvoice());
        requisiteTestItem.test();
        if(requisiteTestItem.getErrors().size() > 0) {
            this.getErrors().add(requisiteTestItem); 
        }
        
        /*DatabaseTestItem databaseTestItem = new DatabaseTestItem(getInvoice());
        databaseTestItem.test();
        if(databaseTestItem.getErrors().size() > 0) {
            this.getErrors().add(databaseTestItem);
        }*/
        
        RosterTestItem rosterTestItem = new RosterTestItem(getInvoice());
        rosterTestItem.test();
        if(rosterTestItem.getErrors().size() > 0) {
            this.getErrors().add(rosterTestItem);
        }
    }
    
}
