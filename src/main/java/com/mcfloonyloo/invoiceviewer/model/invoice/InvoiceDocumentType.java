/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice;

/**
 *
 * @author avolkov
 */
public class InvoiceDocumentType implements DefaultInvoiceSubSection, DefaultInvoiceNullable {
    
    private String code;
    
    private String value;
    
    public String getCode() {
        return this.code;
    }
    
    public String getValue() {
        return this.value;
    }
    
    public void setCode(String code) {
        this.code = code;
    }
    
    public void setValue(String value) {
        this.value = value;
    }
    
    public InvoiceDocumentType() {
        defaultValues();
    }
    
    private void defaultValues() {
        this.code = "";
        this.value = "";
    }

    @Override
    public void setNullValues() {
        this.code = null;
        this.value = null;
    }
    
}
