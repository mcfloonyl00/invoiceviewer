/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice;

/**
 *
 * @author avolkov
 */
public class InvoiceProvider implements DefaultInvoiceSection, DefaultInvoiceNullable {
    
    private String providerStatus;
    
    private String dependentPerson;
    
    private String residentsOfOffshore;
    
    private String specialDealGoods;
    
    private String bigCompany;
    
    private String countryCode;
    
    private String unp;
    
    private String branchCode;
    
    private String name;
    
    private String address;
    
    private InvoiceForInvoice principal;
    
    private InvoiceForInvoice vendor;
    
    private String declaration;
    
    private String dateRelease;
    
    private String dateActualExport;
    
    private InvoiceTaxes taxes;
    
    public String getProviderStatus() {
        return this.providerStatus;
    }
    
    public String getDependentPerson() {
        return this.dependentPerson;
    }
    
    public String getResidentsOfOffshore() {
        return this.residentsOfOffshore;
    }
    
    public String getSpecialDealGoods() {
        return this.specialDealGoods;
    }
    
    public String getBigCompany() {
        return this.bigCompany;
    }
    
    public String getCountryCode() {
        return this.countryCode;
    }
    
    public String getUnp() {
        return this.unp;
    }
    
    public String getBranchCode() {
        return this.branchCode;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getAddress() {
        return this.address;
    }
    
    public InvoiceForInvoice getPrincipal() {
        return this.principal;
    }
    
    public InvoiceForInvoice getVendor() {
        return this.vendor;
    }
    
    public String getDeclaration() {
        return this.declaration;
    }
    
    public String getDateRelease() {
        return this.dateRelease;
    }
    
    public String getDateActualExport() {
        return this.dateActualExport;
    }
    
    public InvoiceTaxes getTaxes() {
        return this.taxes;
    }
    
    public void setProviderStatus(String providerStatus) {
        this.providerStatus = providerStatus;
    }
    
    public void setDependentPerson(String dependentPerson) {
        this.dependentPerson = dependentPerson;
    }
    
    public void setResidentsOfOffshore(String residentsOfOffshore) {
        this.residentsOfOffshore = residentsOfOffshore;
    }
    
    public void setSpecialDealGoods(String specialDealGoods) {
        this.specialDealGoods = specialDealGoods;
    }
    
    public void setBigCompany(String bigCompany) {
        this.bigCompany = bigCompany;
    }
    
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
    
    public void setUnp(String unp) {
        this.unp = unp;
    }
    
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }
    
    public void setPrincipal(InvoiceForInvoice principal) {
        this.principal = principal;
    }
    
    public void setVendor(InvoiceForInvoice vendor) {
        this.vendor = vendor;
    }
    
    public void setDeclaration(String declaration) {
        this.declaration = declaration;
    }
    
    public void setDateRelease(String dateRelease) {
        this.dateRelease = dateRelease;
    }
    
    public void setDateActualExport(String dateActualExport) {
        this.dateActualExport = dateActualExport;
    }
    
    public void setTaxes(InvoiceTaxes taxes) {
        this.taxes = taxes;
    }
    
    public InvoiceProvider() {
        defaultValues();
    }
    
    private void defaultValues() {
        this.providerStatus = "";
        this.dependentPerson = "";
        this.residentsOfOffshore = "";
        this.specialDealGoods = "";
        this.bigCompany = "";
        this.countryCode = "";
        this.unp = "";
        this.branchCode = "";
        this.name = "";
        this.address = "";
        this.principal = new InvoiceForInvoice();
        this.vendor = new InvoiceForInvoice();
        this.declaration = "";
        this.dateRelease = "";
        this.dateActualExport = "";
        this.taxes = new InvoiceTaxes();
    }

    @Override
    public void setNullValues() {
        this.providerStatus = null;
        this.dependentPerson = null;
        this.residentsOfOffshore = null;
        this.specialDealGoods = null;
        this.bigCompany = null;
        this.countryCode = null;
        this.unp = null;
        this.branchCode = null;
        this.name = null;
        this.address = null;
        this.principal = null;
        this.vendor = null;
        this.declaration = null;
        this.dateRelease = null;
        this.dateActualExport = null;
        this.taxes = null;
    }
    
}
