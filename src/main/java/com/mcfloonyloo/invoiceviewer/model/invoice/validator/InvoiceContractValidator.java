/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.validator;

import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceContract;
import com.mcfloonyloo.invoiceviewer.model.invoice.test.ErrorItem;

/**
 *
 * @author avolkov
 */
public class InvoiceContractValidator extends AbstractSectionValidator {
    
    private final static String SECTION_NAME = "[Условия поставки].";
    
    private final InvoiceContract contract;
    
    public InvoiceContractValidator(InvoiceContract contract) {
        this.contract = contract;
    }
    
    public ErrorItem validateNumber() {
        ErrorItem item = getValidator().validateField(this.contract.getNumber());
        item.setFieldName(SECTION_NAME+"[Номер контракта]");
        return item;
    }
    
    public ErrorItem validateDate() {
        ErrorItem item = getValidator().validateField(this.contract.getDate());
        item.setFieldName(SECTION_NAME+"[Дата контракта]");
        return item;
    }
    
    public ErrorItem validateDocuments() {
        ErrorItem item = getValidator().validateList(this.contract.getDocuments());
        item.setFieldName(SECTION_NAME+"[Список документов]");
        return item;
    }
}
