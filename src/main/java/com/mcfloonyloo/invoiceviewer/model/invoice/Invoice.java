/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice;

import com.mcfloonyloo.invoiceviewer.dialog.ExceptionDialog;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

/**
 *
 * @author avolkov
 */
public class Invoice implements DefaultInvoiceNullable {
    
    private String sender;
    
    private InvoiceGeneral general;
    
    private InvoiceProvider provider;
    
    private InvoiceRecipient recipient;
    
    private InvoiceSenderReceiver senderReceiver;
    
    private InvoiceDeliveryCondition deliveryCondition;
    
    private InvoiceRoster roster;
    
    private byte[] content;
    
    private byte[] xsdSchema;
    
    public String getSender() {
        return this.sender;
    }
    
    public InvoiceGeneral getGeneral() {
        return this.general;
    }
    
    public InvoiceProvider getProvider() {
        return this.provider;
    }
    
    public InvoiceRecipient getRecipient() {
        return this.recipient;
    }
    
    public InvoiceSenderReceiver getSenderReceiver() {
        return this.senderReceiver;
    }
    
    public InvoiceDeliveryCondition getDeliveryCondition() {
        return this.deliveryCondition;
    }
    
    public InvoiceRoster getRoster() {
        return this.roster;
    }
    
    public byte[] getContent() {
        return this.content;
    }
    
    public byte[] getXsdSchema() {
        return this.xsdSchema;
    }
    
    public void setSender(String sender) {
        this.sender = sender;
    }
    
    public void setGeneral(InvoiceGeneral general) {
        this.general = general;
    }
    
    public void setProvider(InvoiceProvider provider) {
        this.provider = provider;
    }
    
    public void setRecipient(InvoiceRecipient recipient) {
        this.recipient = recipient;
    }
    
    public void setSenderReceiver(InvoiceSenderReceiver senderReceiver) {
        this.senderReceiver = senderReceiver;
    }
    
    public void setDeliveryCondition(InvoiceDeliveryCondition deliveryCondition) {
        this.deliveryCondition = deliveryCondition;
    }
    
    public void setRoster(InvoiceRoster roster) {
        this.roster = roster;
    }
    
    public void setContent(byte[] content) {
        this.content = content;
    }
    
    public void setConvertedContent(byte[] content) {
        String strData;
        try {
            strData = new String(content, "windows-1251");
            this.content = strData.getBytes(Charset.forName("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            ExceptionDialog dialog = new ExceptionDialog(ex.getLocalizedMessage(), ex)
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText(ex.getClass().toString());
            this.content = content;
            dialog.showAndWait();
        }
    }
    
    public void setXsdSchema(byte[] xsdSchema) {
        this.xsdSchema = xsdSchema;
    }
    
    public Invoice() {
        defaultValues();
    }
    
    private void defaultValues() {
        this.sender = "";
        this.general = new InvoiceGeneral();
        this.provider = new InvoiceProvider();
        this.recipient = new InvoiceRecipient();
        this.senderReceiver = new InvoiceSenderReceiver();
        this.deliveryCondition = new InvoiceDeliveryCondition();
        this.roster = new InvoiceRoster();
        this.content = null;
        this.xsdSchema = null;
    }

    @Override
    public void setNullValues() {
        this.sender = null;
        this.general = null;
        this.provider = null;
        this.recipient = null;
        this.senderReceiver = null;
        this.deliveryCondition = null;
        this.roster = null;
        this.content = null;
        this.xsdSchema = null;
    }
    
}
