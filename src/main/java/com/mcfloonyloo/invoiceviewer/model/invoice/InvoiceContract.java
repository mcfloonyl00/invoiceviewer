/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice;

import java.util.ArrayList;

/**
 *
 * @author avolkov
 */
public class InvoiceContract implements DefaultInvoiceSubSection, DefaultInvoiceNullable {
    
    private String number;
    
    private String date;
    
    private ArrayList<InvoiceDocumentItem> documents;
    
    public String getNumber() {
        return this.number;
    }
    
    public String getDate() {
        return this.date;
    }
    
    public ArrayList<InvoiceDocumentItem> getDocuments() {
        return this.documents;
    }
    
    public void setNumber(String number) {
        this.number = number;
    }
    
    public void setDate(String date) {
        this.date = date;
    }
    
    public void setDocuments(ArrayList<InvoiceDocumentItem> documents) {
        this.documents = documents;
    }
    
    public InvoiceContract() {
        defaultValues();
    }
    
    private void defaultValues() {
        this.number = "";
        this.date = "";
        this.documents = new ArrayList<>();
    }

    @Override
    public void setNullValues() {
        this.number = null;
        this.date = null;
        this.documents = null;
    }
    
}
