/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.validator;

/**
 *
 * @author avolkov
 */
public class AbstractSectionValidator {
    
    private final FieldValidator validator;
    
    public FieldValidator getValidator() {
        return this.validator;
    }
    
    public AbstractSectionValidator() {
        this.validator = new FieldValidator();
    }
    
}
