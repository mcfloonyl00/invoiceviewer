/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice;

import java.io.IOException;
import java.nio.file.Path;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author avolkov
 */
abstract class AbstractPathInvoiceLoader implements DefaultInvoiceLoader {
    
    private final Path path;
    
    public Path getPath() {
        return this.path;
    }
    
    public AbstractPathInvoiceLoader(Path path) {
        this.path = path;
    }

    @Override
    public abstract Invoice load();
    
    protected Document getDocument(String encoder) throws ParserConfigurationException, SAXException, IOException, XMLStreamException {
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        InputSource source = new InputSource("file:///"+this.path.toString());

        source.setEncoding(encoder);
        return builder.parse(source);
    }
    
}
