/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice;

/**
 *
 * @author avolkov
 */
public class InvoiceForInvoice implements DefaultInvoiceSubSection, DefaultInvoiceNullable {
    
    private String number;
    
    private String date;
    
    public String getNumber() {
        return this.number;
    }
    
    public String getDate() {
        return this.date;
    }
    
    public void setNumber(String number) { 
        this.number = number;
    }
    
    public void setDate(String date) {
        this.date = date;
    }
    
    public InvoiceForInvoice() { 
        defaultValues();
    }
    
    public InvoiceForInvoice(String number, String date) {
        this.number = number;
        this.date = date;
    }
    
    private void defaultValues() {
        this.number = "";
        this.date = "";
    }

    @Override
    public void setNullValues() {
        this.number = null;
        this.date = null;
    }
}
