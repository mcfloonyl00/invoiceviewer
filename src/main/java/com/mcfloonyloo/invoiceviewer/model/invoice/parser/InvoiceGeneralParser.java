/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.parser;

import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceGeneral;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author avolkov
 */
class InvoiceGeneralParser extends AbstractInvoiceSectionParser<InvoiceGeneral> {

    public InvoiceGeneralParser(Node node) {
        super(node);
    }

    @Override
    public InvoiceGeneral parse() {
        InvoiceGeneral section = new InvoiceGeneral();
        section.setNullValues();
        
        NodeList list = getNode().getChildNodes();
        for(int index = 0; index < list.getLength(); index++) {
            Node element = list.item(index);
            if(element.getNodeType() != Node.TEXT_NODE) {
                switch(element.getNodeName()) {
                    case "number" : {
                        section.setNumber(element.getTextContent());
                        break;
                    }
                    case "dateIssuance" : {
                        section.setDateIssuance(element.getTextContent());
                        break;
                    }
                    case "dateTransaction" : {
                        section.setDateTransaction(element.getTextContent());
                        break;
                    }
                    case "documentType" : {
                        section.setDocumentType(element.getTextContent());
                        break;
                    }
                    case "invoice" : {
                        section.setByInvoice(element.getTextContent());
                        break;
                    }
                    case "dateCancelled" : {
                        section.setDateCancelled(element.getTextContent());
                        break;
                    }
                    case "sendToRecipient" : {
                        section.setSendToRecipient(element.getTextContent());
                        break;
                    }
                }
            }
        }
        
        return section;
    }
    
}
