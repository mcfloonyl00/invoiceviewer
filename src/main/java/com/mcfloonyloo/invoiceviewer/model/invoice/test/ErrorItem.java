/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.test;

/**
 * Класс для хранения теста
 * @author avolkov
 */
public class ErrorItem implements DefaultErrorItem {
    
    private String fieldName;
    
    private final String message;
    
    public String getFieldName() {
        return this.fieldName;
    }
    
    public ErrorItem setFieldName(String fieldName) {
        this.fieldName = fieldName;
        return this;
    }
    
    @Override
    public String getMessage() {
        return this.message;
    }
    
    public ErrorItem(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.fieldName)
                .append(" : ")
                .append(this.message);
        return builder.toString();
    }
    
}
