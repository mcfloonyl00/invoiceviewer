/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.db;

import com.mcfloonyloo.invoiceviewer.model.cayenne.SQLiteDataSource;
import com.mcfloonyloo.invoiceviewer.model.invoice.db.cayenne.InvoiceDataMap;
import com.mcfloonyloo.invoiceviewer.properties.AppProperties;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.dbsync.CreateIfNoSchemaStrategy;
import org.apache.cayenne.configuration.server.ServerRuntime;

/**
 *
 * @author avolkov
 */
abstract class AbstractInvoiceDAO {
    
    private final String databaseName;
    
    public String getDatabaseName() {
        return this.databaseName;
    }
    
    private ServerRuntime runtime;
        
    public AbstractInvoiceDAO(String databaseName) {
        this.databaseName = databaseName;
    }
    
    protected ObjectContext createContext() {
        String url = AppProperties.INSTANCE.getDatabasesPath()+this.databaseName;
        SQLiteDataSource sqliteDataSource = new SQLiteDataSource("jdbc:sqlite:"+url);
        this.runtime = ServerRuntime.builder().dataSource(sqliteDataSource.getDataSource()).build();
        
        InvoiceDataMap invoiceDataMap = new InvoiceDataMap();
        this.runtime.getDataDomain().addDataMap(invoiceDataMap.getDataMap());
        
        this.runtime.getDataDomain().getDefaultNode().setSchemaUpdateStrategy(new CreateIfNoSchemaStrategy());
        
        return runtime.newContext();
    }
    
    protected void shutdownRuntime() {
        this.runtime.shutdown();
    }
}
