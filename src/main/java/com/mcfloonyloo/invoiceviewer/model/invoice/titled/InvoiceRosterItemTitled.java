/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.titled;

import com.mcfloonyloo.invoiceviewer.model.Title;
import com.mcfloonyloo.invoiceviewer.model.invoice.DefaultInvoiceSubSectionItem;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceDescriptionItem;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceRosterItem;
import java.util.ArrayList;
import java.util.Optional;

/**
 *
 * @author avolkov
 */
public class InvoiceRosterItemTitled implements DefaultInvoiceSubSectionItem {
    
    private final Title<String> number;
    
    private final Title<String> name;
    
    private final Title<String> code;
    
    private final Title<String> codeOced;
    
    private final Title<String> units;
    
    private final Title<String> count;
    
    private final Title<String> price;
    
    private final Title<String> cost;
    
    private final Title<String> summaExcise;
    
    private final Title<InvoiceVatTitled> vat;
    
    private final Title<String> costVat;
    
    private final Title<ArrayList<Title<InvoiceDescriptionItemTitled>>> descriptions;
    
    private final Title<String> skipDeduction;
    
    public InvoiceRosterItemTitled(InvoiceRosterItem rosterItem) {
        this.number = new Title<>("    1. № п/п", true);
        this.name = new Title<>("    2. Наименование товаров (работ, услуг)", true);
        this.code = new Title<>("    3.1. Код товара (ТН ВЭД ЕАЭС)", true);
        this.codeOced = new Title<>("    3.2. Код вида деятельности (ОКЭД)", true);
        this.units = new Title<>("    4. Единица измерения", true);
        this.count = new Title<>("    5. Количество (объём)", true);
        this.price = new Title<>("    6. Цена (тариф) за единицу товара (работы, услуги) без учета НДС", true);
        this.cost = new Title<>("    7. Стоимость товаров (работ, услуг) без НДС", true);
        this.summaExcise = new Title<>("    8. В том числе сумма акциза", true);
        this.vat = new Title<>("    9. НДС", true);
        this.costVat = new Title<>("    11. Стоимость товаров (работ, услуг) с учетом НДС", true);
        this.descriptions = new Title<>("    12. Дополнительные данные", true);
        this.skipDeduction = new Title<>("    Пропустить вычет", true);
        
        this.number.setValue(rosterItem.getNumber());
        this.name.setValue(rosterItem.getName());
        this.code.setValue(rosterItem.getCode());
        this.codeOced.setValue(rosterItem.getCodeOced());
        this.units.setValue(rosterItem.getUnits());
        this.count.setValue(rosterItem.getCount());
        this.price.setValue(rosterItem.getPrice());
        this.cost.setValue(rosterItem.getCost());
        this.summaExcise.setValue(rosterItem.getSummaExcise());
        if(Optional.ofNullable(rosterItem.getVat()).isPresent()) {
            this.vat.setValue(new InvoiceVatTitled(rosterItem.getVat()));
        }
        this.costVat.setValue(rosterItem.getCostVat());
        
        ArrayList<Title<InvoiceDescriptionItemTitled>> descriptionList = new ArrayList<>();
        for(InvoiceDescriptionItem item : rosterItem.getDescriptions()) {
            Title<InvoiceDescriptionItemTitled> itemTitle = new Title<>("", true);
            itemTitle.setValue(new InvoiceDescriptionItemTitled(item));
            descriptionList.add(itemTitle);
        }
        this.descriptions.setValue(descriptionList);
        
        this.skipDeduction.setValue(rosterItem.getSkipDeduction());
    }
    
}
