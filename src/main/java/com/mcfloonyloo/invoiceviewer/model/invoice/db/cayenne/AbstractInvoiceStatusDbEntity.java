/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.db.cayenne;

import com.mcfloonyloo.invoiceviewer.model.cayenne.AbstractDbEntity;
import java.sql.Types;
import org.apache.cayenne.map.DbAttribute;

/**
 *
 * @author avolkov
 */
abstract class AbstractInvoiceStatusDbEntity extends AbstractDbEntity {
    
    public AbstractInvoiceStatusDbEntity(String name) {
        super(name);
        init();
    }
    
    private void init() {
        this.getDbEntity().addAttribute(createDateTimeUpdate());
        this.getDbEntity().addAttribute(createId());
        this.getDbEntity().addAttribute(createInvoiceId());
        this.getDbEntity().addAttribute(createStatusEn());
        this.getDbEntity().addAttribute(createStatusRu());
    }
    
    private DbAttribute createDateTimeUpdate() {
        DbAttribute dateTimeUpdate = new DbAttribute("datetimeupdate");
        dateTimeUpdate.setType(Types.TIMESTAMP);
        dateTimeUpdate.setMandatory(true);
        return dateTimeUpdate;
    }
    
    private DbAttribute createId() {
        DbAttribute id = new DbAttribute("id");
        id.setType(Types.NUMERIC);
        id.setPrimaryKey(true);
        id.setMandatory(true);
        return id;
    }
    
    private DbAttribute createInvoiceId() {
        DbAttribute invoiceId = new DbAttribute("invoiceid");
        invoiceId.setType(Types.NUMERIC);
        invoiceId.setMandatory(true);
        return invoiceId;
    }   
    
    private DbAttribute createStatusEn() {
        DbAttribute statusEn = new DbAttribute("statusen");
        statusEn.setType(Types.NVARCHAR);
        statusEn.setMandatory(true);
        statusEn.setMaxLength(19);
        return statusEn;
    }
    
    private DbAttribute createStatusRu() {
        DbAttribute statusRu = new DbAttribute("statusru");
        statusRu.setType(Types.NVARCHAR);
        statusRu.setMandatory(true);
        statusRu.setMaxLength(35);
        return statusRu;
    }
    
}
