/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice;

import java.util.ArrayList;

/**
 *
 * @author avolkov
 */
public class InvoiceRosterItem implements DefaultInvoiceSubSectionItem, DefaultInvoiceNullable {
    
    private String number;
    
    private String name;
    
    private String code;
    
    private String codeOced;
    
    private String units;
    
    private String count;
    
    private String price;
    
    private String cost;
    
    private String summaExcise;
    
    private InvoiceVat vat;
    
    private String costVat;
    
    private ArrayList<InvoiceDescriptionItem> descriptions;
    
    private String skipDeduction;
    
    public String getNumber() {
        return this.number;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getCode() {
        return this.code;
    }
    
    public String getCodeOced() {
        return this.codeOced;
    }
    
    public String getUnits() {
        return this.units;
    }
    
    public String getCount() {
        return this.count;
    }
    
    public String getPrice() {
        return this.price;
    }
    
    public String getCost() {
        return this.cost;
    }
    
    public String getSummaExcise() {
        return this.summaExcise;
    }
    
    public InvoiceVat getVat() {
        return this.vat;
    }
    
    public String getCostVat() {
        return this.costVat;
    }
    
    public ArrayList<InvoiceDescriptionItem> getDescriptions() {
        return this.descriptions;
    }
    
    public String getSkipDeduction() {
        return this.skipDeduction;
    }
    
    public void setNumber(String number) {
        this.number = number;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setCode(String code) {
        this.code = code;
    }
    
    public void setCodeOced(String codeOced) {
        this.codeOced = codeOced;
    }
    
    public void setUnits(String units) {
        this.units = units;
    }
    
    public void setCount(String count) {
        this.count = count;
    }
    
    public void setPrice(String price) {
        this.price = price;
    }
    
    public void setCost(String cost) {
        this.cost = cost;
    }
    
    public void setSummaExcise(String summaExcise) {
        this.summaExcise = summaExcise;
    }
    
    public void setVat(InvoiceVat vat) {
        this.vat = vat;
    }
    
    public void setCostVat(String costVat) {
        this.costVat = costVat;
    }
    
    public void setDescriptions(ArrayList<InvoiceDescriptionItem> descriptions) {
        this.descriptions.clear();
        this.descriptions.addAll(descriptions);
    }
    
    public void setSkipDeduction(String skipDeduction) {
        this.skipDeduction = skipDeduction;
    }
    
    public InvoiceRosterItem() {
        defaultValues();
    }
    
    private void defaultValues() {
        this.number = "";
        this.name = "";
        this.code = "";
        this.codeOced = "";
        this.units = "";
        this.count = "";
        this.price = "";
        this.cost = "";
        this.summaExcise = "";
        this.vat = new InvoiceVat();
        this.costVat = "";
        this.descriptions = new ArrayList<>();
        this.skipDeduction = "";
    }

    @Override
    public void setNullValues() {
        this.number = null;
        this.name = null;
        this.code = null;
        this.codeOced = null;
        this.units = null;
        this.count = null;
        this.price = null;
        this.cost = null;
        this.summaExcise = null;
        this.vat = null;
        this.costVat = null;
        this.descriptions = null;
        this.skipDeduction = null;
    }
    
}
