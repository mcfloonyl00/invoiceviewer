/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.parser;

import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceSenderItem;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceSenderReceiver;
import java.util.ArrayList;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author avolkov
 */
class InvoiceSenderReceiverParser extends AbstractInvoiceSectionParser<InvoiceSenderReceiver> {

    public InvoiceSenderReceiverParser(Node node) {
        super(node);
    }

    @Override
    public InvoiceSenderReceiver parse() {
        InvoiceSenderReceiver section = new InvoiceSenderReceiver();
        section.setNullValues();
        
        NodeList list = getNode().getChildNodes();
        for(int index = 0; index < list.getLength(); index++) {
            Node element = list.item(index);
            if(element.getNodeType() != Node.TEXT_NODE) {
                switch(element.getNodeName()) {
                    case "consignors" : { 
                        ArrayList<InvoiceSenderItem> consignors = new InvoiceConsignorsParser(element).parse();
                        section.setConsingors(consignors);
                        break;
                    }
                    case "consignees" : {
                        ArrayList<InvoiceSenderItem> consignees = new InvoiceConsigneesParser(element).parse();
                        section.setConsignees(consignees);
                        break;
                    }
                }
            }
        }
        
        return section;
    }
    
}
