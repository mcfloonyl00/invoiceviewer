/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.titled;

import com.mcfloonyloo.invoiceviewer.model.Title;
import com.mcfloonyloo.invoiceviewer.model.invoice.DefaultInvoiceSection;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceSenderReceiver;

/**
 *
 * @author avolkov
 */
public class InvoiceSenderReceiverTitled implements DefaultInvoiceSection {
    
    private final Title<InvoiceConsignorsTitled> consignors;
    
    private final Title<InvoiceConsigneesTitled> consignees;
    
    public InvoiceSenderReceiverTitled(InvoiceSenderReceiver senderReceiver) {
        this.consignors = new Title<>("",true); 
        this.consignors.setValue(new InvoiceConsignorsTitled(senderReceiver.getConsignors()));
        this.consignees = new Title<>("",true);
        this.consignees.setValue(new InvoiceConsigneesTitled(senderReceiver.getConsignees()));
    }
    
}
