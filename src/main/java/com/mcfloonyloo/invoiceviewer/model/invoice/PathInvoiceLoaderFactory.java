/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import org.apache.pdfbox.io.IOUtils;
import org.apache.tika.parser.txt.CharsetDetector;
import org.apache.tika.parser.txt.CharsetMatch;

/**
 *
 * @author avolkov
 */
public class PathInvoiceLoaderFactory {
    
    private AbstractPathInvoiceLoader loader;
    
    public Invoice load(Path path) {
        try {            

            switch(getCharset(path.toFile()).getName()) {
                case "UTF-8" : {
                    this.loader = new UTF8PathInvoiceLoader(path);
                    break;
                }
                case "ISO-8859-1" : {
                    this.loader = new Win1251PathInvoiceLoader(path);
                    break;
                }
                default : { 
                    return null;
                }
            }
            
            Invoice invoice = loader.load();
            invoice.setConvertedContent(new FileXmlReader(path.toFile()).read());
            return invoice;
            
        } catch (NullPointerException | IOException ex) {
            return null;
        } 
    }
           
    private CharsetMatch getCharset(File file) throws FileNotFoundException, IOException, IOException {
        CharsetDetector detector = new CharsetDetector();
        try (InputStream stream = new FileInputStream(file)) {
            byte[] bytes = IOUtils.toByteArray(stream);
            detector.setText(bytes);
        }
        return detector.detect();
    }
    
}
