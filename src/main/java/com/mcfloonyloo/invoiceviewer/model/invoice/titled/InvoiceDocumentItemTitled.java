/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.titled;

import com.mcfloonyloo.invoiceviewer.model.Title;
import com.mcfloonyloo.invoiceviewer.model.invoice.DefaultInvoiceSubSectionItem;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceDocumentItem;

/**
 *
 * @author avolkov
 */
public class InvoiceDocumentItemTitled implements DefaultInvoiceSubSectionItem {
    
    private final Title<InvoiceDocumentTypeTitled> docType;
    
    private final Title<String> date;
    
    private final Title<String> blankCode;
    
    private final Title<String> seria;
    
    private final Title<String> number;
    
    public InvoiceDocumentItemTitled(InvoiceDocumentItem documentItem) {
        this.docType = new Title<>("      Вид документа", true);
        this.date = new Title<>("      Дата", true);
        this.blankCode = new Title<>("      Код типа бланка", true);
        this.seria = new Title<>("      Серия", true);
        this.number = new Title<>("      Номер", true);
        
        this.docType.setValue(new InvoiceDocumentTypeTitled(documentItem.getDocType()));
        this.date.setValue(documentItem.getDate());
        this.blankCode.setValue(documentItem.getBlankCode());
        this.seria.setValue(documentItem.getSeria());
        this.number.setValue(documentItem.getNumber());
    }
    
}
