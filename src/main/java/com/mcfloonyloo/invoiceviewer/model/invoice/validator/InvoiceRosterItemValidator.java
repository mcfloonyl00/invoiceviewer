/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.validator;

import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceRosterItem;
import com.mcfloonyloo.invoiceviewer.model.invoice.test.ErrorItem;
import com.mcfloonyloo.invoiceviewer.model.xmldb.XmldbService;
import java.math.BigDecimal;
import java.util.Optional;

/**
 *
 * @author avolkov
 */
public class InvoiceRosterItemValidator extends AbstractSectionValidator {
    
    private final static String SECTION_NAME = "[Данные по товарам].[Список документов].";
    
    private final int index;
    
    private final InvoiceRosterItem rosterItem;
    
    public InvoiceRosterItemValidator(int index, InvoiceRosterItem rosterItem) {
        this.index = index;
        this.rosterItem = rosterItem;
    }
    
    public ErrorItem validateUnit() {
        ErrorItem item = getValidator().validateCompareValues(new BigDecimal(this.rosterItem.getCostVat()), BigDecimal.ZERO, Comparison.MOREEQUAL);
        item.setFieldName(SECTION_NAME+"[Товар №"+this.index+"].[Единица измерения]");
        return item;
    }
    
    public ErrorItem validateUnitDatabase() {
        ErrorItem item = validateUnit();
        if(Optional.ofNullable(item.getMessage()).isPresent()) {
            return item;
        } else {
            if(XmldbService.INSTANCE.getOcei().isRecord(this.rosterItem.getUnits())) {
                return new ErrorItem(null);
            } else {
                ErrorItem unit = new ErrorItem("значение '"+this.rosterItem.getUnits()+"' не найдено в таблице 'OCEI'");
                unit.setFieldName(SECTION_NAME+"[Товар №"+this.index+"].[Единица измерения]");
                return item;
            }
        }
    }
    
    public ErrorItem validateCode() {
        ErrorItem item = getValidator().validateField(this.rosterItem.getCode());
        item.setFieldName(SECTION_NAME+"[Товар №"+this.index+"].[Код ТН ВЭД]");
        return item;
    }
    
    public ErrorItem validateCodeOced() {
        ErrorItem item = getValidator().validateField(this.rosterItem.getCodeOced());
        item.setFieldName(SECTION_NAME+"[Товар №"+this.index+"].[Код ОКЭД]");
        return item;
    }
    
    public ErrorItem validatePrice() {
        ErrorItem item = getValidator().validateCompareValues(new BigDecimal(this.rosterItem.getPrice()), BigDecimal.ZERO, Comparison.MOREEQUAL);
        item.setFieldName(SECTION_NAME+"[Товар №"+this.index+"].[Цена за единицу]");
        return item;
    }
    
    public ErrorItem validateCost() {
        ErrorItem item = getValidator().validateCompareValues(new BigDecimal(this.rosterItem.getCost()), BigDecimal.ZERO, Comparison.MOREEQUAL);
        item.setFieldName(SECTION_NAME+"[Товар №"+this.index+"].[Сумма без НДС]");
        return item;
    }
    
    public ErrorItem validateSummaExcise() {
        ErrorItem item = getValidator().validateCompareValues(new BigDecimal(this.rosterItem.getSummaExcise()), BigDecimal.ZERO, Comparison.MOREEQUAL);
        item.setFieldName(SECTION_NAME+"[Товар №"+this.index+"].[Сумма акциза]");
        return item;
    }
    
    public ErrorItem validateCostVat() {
        ErrorItem item = getValidator().validateCompareValues(new BigDecimal(this.rosterItem.getCostVat()), BigDecimal.ZERO, Comparison.MOREEQUAL);
        item.setFieldName(SECTION_NAME+"[Товар №"+this.index+"].[Суммарная стоимость без НДС]");
        return item;
    }
    
    public ErrorItem validateSummaVat() {
        ErrorItem item = getValidator().validateCompareValues(new BigDecimal(this.rosterItem.getVat().getSummaVat()), BigDecimal.ZERO, Comparison.MOREEQUAL);
        item.setFieldName(SECTION_NAME+"[Товар №"+this.index+"].[Суммарная стоимость без НДС]");
        return item;
    }
    
    public ErrorItem validateVatRate() {
        ErrorItem item = getValidator().validateField(this.rosterItem.getVat().getRateType());
        item.setFieldName(SECTION_NAME+"[Товар №"+this.index+"].[Ставка НДС (тип)]");
        return item;
    }
    
    public ErrorItem validateVatRateDatabase() {
        ErrorItem item = validateVatRate();
        if(Optional.ofNullable(item.getMessage()).isPresent()) {
            return item;
        } else {
            if(XmldbService.INSTANCE.getVatRate().isNameen(this.rosterItem.getVat().getRateType())) {
                switch(this.rosterItem.getVat().getRateType()) {
                    case "DECIMAL" : {
                        if(XmldbService.INSTANCE.getVatRate().isRecord(this.rosterItem.getVat().getRateType(), this.rosterItem.getVat().getRate())) {
                            return new ErrorItem(null);
                        } else {
                            ErrorItem pairRate = new ErrorItem("пара значений '"+this.rosterItem.getVat().getRateType()+"' и '"+this.rosterItem.getVat().getRate()+"' не найдены в таблице 'Vat Rate'");
                            pairRate.setFieldName(SECTION_NAME+"[Товар №"+this.index+"].[Ставка НДС]");
                            return pairRate;
                        }
                    } 
                    default : {
                        return new ErrorItem(null);
                    }
                }
            } else {
                ErrorItem rate = new ErrorItem("значение '"+this.rosterItem.getVat().getRateType()+"' не найдено в таблице 'Vat Rate'");
                rate.setFieldName(SECTION_NAME+"[Товар №"+this.index+"].[Ставка НДС (тип)]");
                return rate;
            }
        }
    }
    
}
