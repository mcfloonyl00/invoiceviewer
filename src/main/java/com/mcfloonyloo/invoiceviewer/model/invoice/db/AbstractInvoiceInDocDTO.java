package com.mcfloonyloo.invoiceviewer.model.invoice.db;

import com.mcfloonyloo.invoiceviewer.model.invoice.db.InvoiceInDTO;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import org.apache.cayenne.BaseDataObject;
import org.apache.cayenne.exp.Property;

/**
 * Class AbstractInvoiceInDocDTO was generated by Cayenne.
 * It is probably a good idea to avoid changing this class manually,
 * since it may be overwritten next time code is regenerated.
 * If you need to make any customizations, please use subclass.
 */
abstract class AbstractInvoiceInDocDTO extends BaseDataObject implements DefaultInvoiceDocDTO{

    private static final long serialVersionUID = 1L; 

    public static final String DATEDOC_PROPERTY = "datedoc";
    public static final String INVOICEIN_PROPERTY = "invoicein";

    public static final String ID_PK_COLUMN = "id";

    public static final Property<LocalDate> DATEDOC = Property.create("datedoc", LocalDate.class);
    public static final Property<InvoiceInDTO> INVOICEIN = Property.create("invoicein", InvoiceInDTO.class);

    protected LocalDate datedoc;

    protected Object invoicein;

    public void setDatedoc(LocalDate datedoc) {
        beforePropertyWrite("datedoc", this.datedoc, datedoc);
        this.datedoc = datedoc;
    }

    public LocalDate getDatedoc() {
        beforePropertyRead("datedoc");
        return this.datedoc;
    }

    public void setInvoicein(InvoiceInDTO invoicein) {
        setToOneTarget("invoicein", invoicein, true);
    }

    public InvoiceInDTO getInvoicein() {
        return (InvoiceInDTO)readProperty("invoicein");
    }

    @Override
    public Object readPropertyDirectly(String propName) {
        if(propName == null) {
            throw new IllegalArgumentException();
        }

        switch(propName) {
            case "datedoc":
                return this.datedoc;
            case "invoicein":
                return this.invoicein;
            default:
                return super.readPropertyDirectly(propName);
        }
    }

    @Override
    public void writePropertyDirectly(String propName, Object val) {
        if(propName == null) {
            throw new IllegalArgumentException();
        }

        switch (propName) {
            case "datedoc":
                this.datedoc = (LocalDate)val;
                break;
            case "invoicein":
                this.invoicein = val;
                break;
            default:
                super.writePropertyDirectly(propName, val);
        }
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        writeSerialized(out);
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        readSerialized(in);
    }

    @Override
    protected void writeState(ObjectOutputStream out) throws IOException {
        super.writeState(out);
        out.writeObject(this.datedoc);
        out.writeObject(this.invoicein);
    }

    @Override
    protected void readState(ObjectInputStream in) throws IOException, ClassNotFoundException {
        super.readState(in);
        this.datedoc = (LocalDate)in.readObject();
        this.invoicein = in.readObject();
    }

}
