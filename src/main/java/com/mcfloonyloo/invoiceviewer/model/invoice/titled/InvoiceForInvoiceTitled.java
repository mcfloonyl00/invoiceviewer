/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.titled;

import com.mcfloonyloo.invoiceviewer.model.Title;
import com.mcfloonyloo.invoiceviewer.model.invoice.DefaultInvoiceSubSection;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceForInvoice;

/**
 *
 * @author avolkov
 */
public class InvoiceForInvoiceTitled implements DefaultInvoiceSubSection {
    
    private final Title<String> number;
    
    private final Title<String> date;
    
    public InvoiceForInvoiceTitled(InvoiceForInvoice forInvoice) {
        this.number = new Title<>("  Номер ЭСЧФ", true);
        this.date = new Title<>("  Дата", true);
        
        this.number.setValue(forInvoice.getNumber());
        this.date.setValue(forInvoice.getDate());
    }
    
}
