/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice;

import java.util.ArrayList;

/**
 *
 * @author avolkov
 */
public class InvoiceRoster implements DefaultInvoiceSection, DefaultInvoiceNullable {
    
    private String totalCostVat;
    
    private String totalExcise;
    
    private String totalVat;
    
    private String totalCost;
    
    private ArrayList<InvoiceRosterItem> rosters;
    
    public String getTotalCostVat() {
        return this.totalCostVat;
    }
    
    public String getTotalExcise() {
        return this.totalExcise;
    }
    
    public String getTotalVat() {
        return this.totalVat;
    }
    
    public String getTotalCost() {
        return this.totalCost;
    }
    
    public ArrayList<InvoiceRosterItem> getRosters() {
        return this.rosters;
    }
    
    public void setTotalCostVat(String totalCostVat) {
        this.totalCostVat = totalCostVat;
    }
    
    public void setTotalExcise(String totalExcise) {
        this.totalExcise = totalExcise;
    }
    
    public void setTotalVat(String totalVat) {
        this.totalVat = totalVat;
    }
    
    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }
    
    public void setRosters(ArrayList<InvoiceRosterItem> rosters) {
        this.rosters.clear();
        this.rosters.addAll(rosters);
    }
    
    public InvoiceRoster() {
        defaultValues();
    }
    
    private void defaultValues() {
        this.totalCostVat = "";
        this.totalExcise = "";
        this.totalVat = "";
        this.totalCost = "";
        this.rosters = new ArrayList<>();
    }

    @Override
    public void setNullValues() {
        this.totalCostVat = null;
        this.totalExcise = null;
        this.totalVat = null;
        this.totalCost = null;
        this.rosters = new ArrayList<>();
    }
    
}
