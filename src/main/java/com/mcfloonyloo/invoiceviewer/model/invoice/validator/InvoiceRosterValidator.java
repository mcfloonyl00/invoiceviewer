/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.validator;

import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceGeneral;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceRoster;
import com.mcfloonyloo.invoiceviewer.model.invoice.test.ErrorItem;
import java.math.BigDecimal;
import java.util.Optional;

/**
 *
 * @author avolkov
 */
public class InvoiceRosterValidator extends AbstractSectionValidator {
    
    private final static String SECTION_NAME = "[Данные по товарам].";
    
    private final InvoiceRoster roster;
    
    private InvoiceGeneralValidator generalValidator;
    
    public InvoiceRosterValidator(InvoiceRoster roster) {
        this.roster = roster;
    }
    
    public void setInvoiceGeneral(InvoiceGeneral general) {
        this.generalValidator = new InvoiceGeneralValidator(general);
    }
    
    public ErrorItem validateTotalCost() {
        ErrorItem compareItem = getValidator().validateCompareValues(new BigDecimal(this.roster.getTotalCost()), BigDecimal.ZERO, Comparison.MOREEQUAL);
        compareItem.setFieldName(SECTION_NAME+"[Общая сумма без НДС]");
        if(Optional.ofNullable(compareItem).isPresent()) {
            return compareItem;
        } else {
            return this.generalValidator.checkTypeInvoice(compareItem);
        }
    }
    
    public ErrorItem validateTotalExcise() {
        ErrorItem compareItem = getValidator().validateCompareValues(new BigDecimal(this.roster.getTotalExcise()), BigDecimal.ZERO, Comparison.MOREEQUAL);
        compareItem.setFieldName(SECTION_NAME+"[Общая сумма акцизов]");
        if(Optional.ofNullable(compareItem).isPresent()) {
            return compareItem;
        } else {
            return this.generalValidator.checkTypeInvoice(compareItem);
        }
    }
    
    public ErrorItem validateTotalVat() {
        ErrorItem compareItem = getValidator().validateCompareValues(new BigDecimal(this.roster.getTotalVat()), BigDecimal.ZERO, Comparison.MOREEQUAL);
        compareItem.setFieldName(SECTION_NAME+"[Общая сумма НДС]");
        if(Optional.ofNullable(compareItem).isPresent()) {
            return compareItem;
        } else {
            return this.generalValidator.checkTypeInvoice(compareItem);
        }
    }
    
    public ErrorItem validateTotalCostVat() {
        ErrorItem compareItem = getValidator().validateCompareValues(new BigDecimal(this.roster.getTotalVat()), BigDecimal.ZERO, Comparison.MOREEQUAL);
        compareItem.setFieldName(SECTION_NAME+"[Общая сумма c НДС]");
        if(Optional.ofNullable(compareItem).isPresent()) {
            return compareItem;
        } else {
            return this.generalValidator.checkTypeInvoice(compareItem);
        }
    }
    
    public ErrorItem validateRosters() {
        ErrorItem item = getValidator().validateList(this.roster.getRosters());
        item.setFieldName(SECTION_NAME+"[Список товаров]");
        return item;
    }
    
}
