/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.titled;

import com.mcfloonyloo.invoiceviewer.model.Title;
import com.mcfloonyloo.invoiceviewer.model.invoice.DefaultInvoiceSection;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceRecipient;
import java.util.Optional;

/**
 *
 * @author avolkov
 */
public class InvoiceRecipientTitled implements DefaultInvoiceSection {
    
    private final Title<String> recipientStatus;
    
    private final Title<String> dependentPerson;
    
    private final Title<String> residentsOfOffshore;
    
    private final Title<String> specialDealGoods;
    
    private final Title<String> bigCompany;
    
    private final Title<String> countryCode;
    
    private final Title<String> unp;
    
    private final Title<String> branchCode;
    
    private final Title<String> name;
    
    private final Title<String> address;
    
    private final Title<String> declaration;
    
    private final Title<InvoiceTaxesTitled> taxes;
    
    private final Title<String> dateImport;
    
    public InvoiceRecipientTitled(InvoiceRecipient recipient) {
        this.recipientStatus = new Title<>("15. Статус получателя", true);
        this.dependentPerson = new Title<>("15.1. Взаимосвязанное лицо", true);
        this.residentsOfOffshore = new Title<>("15.2. Сделка с резидентом оффшорной зоны", true);
        this.specialDealGoods = new Title<>("15.3. Сделка с товарами по перечню", true);
        this.bigCompany = new Title<>("15.4. Организация, включенная в перечень крупных плательщиков", true);
        this.countryCode = new Title<>("16. Код страны получателя", true);
        this.unp = new Title<>("17. УНП", true);
        this.branchCode = new Title<>("17.1. Код филиала (обособленного подразделения", true);
        this.name = new Title<>("18. Получатель", true);
        this.address = new Title<>("19. Юридический адрес", true);
        this.declaration = new Title<>("20. Регистрационный номер выпуска товаров", true);
        this.taxes = new Title<>("21. Реквизиты заявления о ввозе товаров", true);
        this.dateImport = new Title<>("21.1. Дата ввоза товаров", true);
        
        this.recipientStatus.setValue(recipient.getRecipientStatus());
        this.dependentPerson.setValue(recipient.getDependentPerson());
        this.residentsOfOffshore.setValue(recipient.getResidentsOfOffshore());
        this.specialDealGoods.setValue(recipient.getSpecialDealGoods());
        this.bigCompany.setValue(recipient.getBigCompany());
        this.countryCode.setValue(recipient.getCountryCode());
        this.unp.setValue(recipient.getUnp());
        this.branchCode.setValue(recipient.getBranchCode());
        this.name.setValue(recipient.getName());
        this.address.setValue(recipient.getAddress());
        this.declaration.setValue(recipient.getDeclaration());
        if(Optional.ofNullable(recipient.getTaxes()).isPresent()) {
            this.taxes.setValue(new InvoiceTaxesTitled(recipient.getTaxes()));
        }
        this.dateImport.setValue(recipient.getDateImport());
    }
    
}
