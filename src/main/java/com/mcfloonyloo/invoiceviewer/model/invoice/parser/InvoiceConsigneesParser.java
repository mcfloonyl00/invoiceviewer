/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.parser;

import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceSenderItem;
import java.util.ArrayList;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author avolkov
 */
class InvoiceConsigneesParser extends AbstractInvoiceSectionParser<ArrayList<InvoiceSenderItem>> {

    public InvoiceConsigneesParser(Node node) {
        super(node);
    }

    @Override
    public ArrayList<InvoiceSenderItem> parse() {
        ArrayList<InvoiceSenderItem> senderList = new ArrayList<>();
        
        NodeList list = getNode().getChildNodes();
        for(int index = 0; index < list.getLength(); index++) {
            Node element = list.item(index);
            if(element.getNodeType() != Node.TEXT_NODE) {
                switch(element.getNodeName()) {
                    case "consignee" : {
                        senderList.add(new InvoiceSenderItemParser(element).parse());
                        break;
                    }
                }
            }
        }
        
        return senderList;
    }
    
}
