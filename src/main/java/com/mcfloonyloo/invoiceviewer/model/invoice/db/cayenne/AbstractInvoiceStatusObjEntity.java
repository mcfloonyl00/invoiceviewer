/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.db.cayenne;

import com.mcfloonyloo.invoiceviewer.model.cayenne.AbstractObjEntity;
import org.apache.cayenne.map.ObjAttribute;

/**
 *
 * @author avolkov
 */
public class AbstractInvoiceStatusObjEntity extends AbstractObjEntity {
    
    private final String name;
    
    public AbstractInvoiceStatusObjEntity(String name) {
        super(name+"DTO");
        this.name = name;
        init();
    }
    
    private void init() {
        this.getObjEntity().setClassName(PACKAGE+this.name+"DTO");
        this.getObjEntity().setDbEntityName(this.name);
        this.getObjEntity().addAttribute(createDateTimeUpdate());
        this.getObjEntity().addAttribute(createStatusEn());
        this.getObjEntity().addAttribute(createStatusRu());
    }
    
    private ObjAttribute createDateTimeUpdate() {
        ObjAttribute dateTimeUpdate = new ObjAttribute("datetimeupdate");
        dateTimeUpdate.setType("java.time.LocalDateTime");
        dateTimeUpdate.setDbAttributePath("datetimeupdate");
        return dateTimeUpdate;
    }
    
    private ObjAttribute createStatusEn() {
        ObjAttribute dateTimeUpdate = new ObjAttribute("statusen");
        dateTimeUpdate.setType("java.lang.String");
        dateTimeUpdate.setDbAttributePath("statusen");
        return dateTimeUpdate;
    }
    
    private ObjAttribute createStatusRu() {
        ObjAttribute dateTimeUpdate = new ObjAttribute("statusru");
        dateTimeUpdate.setType("java.lang.String");
        dateTimeUpdate.setDbAttributePath("statusru");
        return dateTimeUpdate;
    }
    
}