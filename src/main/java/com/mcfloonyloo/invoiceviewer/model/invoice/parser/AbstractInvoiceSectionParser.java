/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.parser;

import org.w3c.dom.Node;

/**
 *
 * @author avolkov
 */
abstract class AbstractInvoiceSectionParser<DefaultInvoiceSection> {
    
    private final Node node;
    
    public Node getNode() {
        return this.node;
    }
    
    public AbstractInvoiceSectionParser(Node node) {
        this.node = node;
    }
    
    public abstract DefaultInvoiceSection parse();
     
}
