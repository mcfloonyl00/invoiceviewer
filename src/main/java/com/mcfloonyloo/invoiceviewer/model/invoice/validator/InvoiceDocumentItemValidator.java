/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.validator;

import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceDocumentItem;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceGeneral;
import com.mcfloonyloo.invoiceviewer.model.invoice.test.ErrorItem;
import com.mcfloonyloo.invoiceviewer.model.xmldb.XmldbService;
import java.util.Optional;

/**
 *
 * @author avolkov
 */
public class InvoiceDocumentItemValidator extends AbstractSectionValidator {
    
    private final static String SECTION_NAME = "[Условия поставки].[Договор].[Список документов].";
    
    private final int index;
    
    private final InvoiceDocumentItem documentItem;
    
    public InvoiceDocumentItemValidator(int index, InvoiceDocumentItem documentItem) {
        this.index = index;
        this.documentItem = documentItem;
    }
    
    public ErrorItem validateDocumentType() {
        ErrorItem item = getValidator().validateField(this.documentItem.getDocType().getValue());
        item.setFieldName(SECTION_NAME+"[Документ №"+this.index+"].[Код типа документа]");
        return item;
    }
    
    public ErrorItem validateDocumentTypeCode() {
        ErrorItem item = getValidator().validateField(this.documentItem.getDocType().getCode());
        item.setFieldName(SECTION_NAME+"[Документ №"+this.index+"].[Код документа]");
        return item;
    }
    
    public ErrorItem validateDocumentTypeCodeDatabase() {
        ErrorItem item = validateDocumentTypeCode();
        if(Optional.ofNullable(item.getMessage()).isPresent()) {
            return item;
        } else {
            if(XmldbService.INSTANCE.getDocumentType().isRecord(this.documentItem.getDocType().getCode())) {
                return new ErrorItem(null);
            } else {
                ErrorItem type = new ErrorItem("значение '"+this.documentItem.getDocType().getCode()+"' не найдено в таблице 'Document Type'");
                type.setFieldName(SECTION_NAME+"[Документ №"+this.index+"].[Код типа документа]");
                return type;
            }
        }
    }
    
    public ErrorItem validateBlankCode() {
        ErrorItem item = getValidator().validateField(this.documentItem.getBlankCode());
        item.setFieldName(SECTION_NAME+"[Документ №"+this.index+"].[Код бланка документа]");
        return item;
    }
    
    public ErrorItem validateSeria() {
        ErrorItem item = getValidator().validateField(this.documentItem.getSeria());
        item.setFieldName(SECTION_NAME+"[Документ №"+this.index+"].[Серия документа]");
        return item;
    }
    
    public ErrorItem validateSeriaLenght() {
        ErrorItem item = getValidator().validateCompareLenghts(this.documentItem.getSeria(), 2, com.mcfloonyloo.invoiceviewer.model.invoice.validator.Comparison.EQUAL);
        item.setFieldName(SECTION_NAME+"[Документ №"+this.index+"].[Серия документа]");
        return item;
    }
    
    public ErrorItem validateNumber() {
        ErrorItem item = getValidator().validateField(this.documentItem.getNumber());
        item.setFieldName(SECTION_NAME+"[Документ №"+this.index+"].[Номер документа]");
        return item;
    }
    
    public ErrorItem validateDate() {
        ErrorItem item = getValidator().validateField(this.documentItem.getNumber());
        item.setFieldName(SECTION_NAME+"[Документ №"+this.index+"].[Дата документа]");
        return item;
    }
    
    public ErrorItem validateDateEqualDateTransaction(InvoiceGeneral general) {
        ErrorItem item = getValidator().validateFieldValue(this.documentItem.getDate(), general.getDateTransaction());
        item.setFieldName(SECTION_NAME+"[Документ №"+this.index+"].[Дата документа]");
        return item;
    }
    
}
