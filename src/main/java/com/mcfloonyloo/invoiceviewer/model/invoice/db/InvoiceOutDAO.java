/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.db;

import com.mcfloonyloo.invoiceviewer.model.invoice.Invoice;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceDocumentItem;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.query.ObjectSelect;

/**
 *
 * @author avolkov
 */
public class InvoiceOutDAO extends AbstractInvoiceDAO 
        implements DefaultInvoiceDAO<InvoiceOutDTO, InvoiceOutDocDTO, InvoiceOutStatusDTO> {

    public InvoiceOutDAO(String config) {
        super(config);
    }

    @Override
    public void addInvoice(Invoice invoice) {
        ObjectContext context = createContext();
        
        InvoiceOutDTO invoiceOut = context.newObject(InvoiceOutDTO.class);
        invoiceOut.setNumber(invoice.getGeneral().getNumber());
        if(Optional.ofNullable(invoice.getRecipient()).isPresent()) {
            invoiceOut.setUnp(invoice.getRecipient().getUnp());
        }
        invoiceOut.setDatetrans(LocalDate.parse(invoice.getGeneral().getDateTransaction(), DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        if(Optional.ofNullable(invoice.getRoster()).isPresent()) {
            invoiceOut.setTotalcost(new BigDecimal(invoice.getRoster().getTotalCost()));
            invoiceOut.setTotalvat(new BigDecimal(invoice.getRoster().getTotalVat()));
            invoiceOut.setTotalcostvat(new BigDecimal(invoice.getRoster().getTotalCostVat()));
        }
        
        if(Optional.ofNullable(invoice.getDeliveryCondition()).isPresent() 
                && Optional.ofNullable(invoice.getDeliveryCondition().getContract()).isPresent()
                && Optional.ofNullable(invoice.getDeliveryCondition().getContract().getDocuments()).isPresent()) {
            ArrayList<InvoiceDocumentItem> documents = invoice.getDeliveryCondition().getContract().getDocuments();
            for(InvoiceDocumentItem item : documents) {
                InvoiceOutDocDTO docOut = context.newObject(InvoiceOutDocDTO.class);
                docOut.setDatedoc(LocalDate.parse(item.getDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                invoiceOut.addToDoc(docOut);
            };
        }
        
        try {
            context.commitChanges();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        shutdownRuntime();
    }

    @Override
    public boolean isInvoice(String number) {
        boolean is;
        
        ObjectContext context = createContext();
        is = !ObjectSelect.query(InvoiceOutDTO.class).where(InvoiceOutDTO.NUMBER.like(number)).select(context).isEmpty();
        shutdownRuntime();
        
        return is;
    }

    @Override
    public InvoiceOutDTO getInvoice(String number) {
        ObjectContext context = createContext();
        InvoiceOutDTO invoiceOut = ObjectSelect.query(InvoiceOutDTO.class).where(InvoiceOutDTO.NUMBER.like(number)).selectOne(context);
        shutdownRuntime();
        
        return invoiceOut;
    }

    @Override
    public ArrayList<InvoiceOutStatusDTO> getStatuses(String number) {
        return (ArrayList<InvoiceOutStatusDTO>) getInvoice(number).getStatuses();
    }

    @Override
    public ArrayList<InvoiceOutDocDTO> getDocs(String number) {
        return (ArrayList<InvoiceOutDocDTO>) getInvoice(number).getDocs();
    }
    
    public ArrayList<InvoiceOutDTO> getAll() {
        ArrayList<InvoiceOutDTO> invoices = new ArrayList<>();
        
        ObjectContext context = createContext();
        invoices.addAll(ObjectSelect.query(InvoiceOutDTO.class).select(context));
        shutdownRuntime();
        
        return invoices;
    }

    @Override
    public ArrayList<InvoiceOutDTO> getInvoices(LocalDate from, LocalDate to) {
        ArrayList<InvoiceOutDTO> invoices = new ArrayList<>();
        
        ObjectContext context = createContext();
        List<InvoiceOutDTO> invoicesDb = ObjectSelect.query(InvoiceOutDTO.class).where(InvoiceOutDTO.DATETRANS.between(from, to)).select(context);
        if(Optional.ofNullable(invoicesDb).isPresent()) {
            invoices.addAll(invoicesDb);
        }
        shutdownRuntime();
        
        return invoices;
    }
    
}
