/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.db.cayenne;

import com.mcfloonyloo.invoiceviewer.model.cayenne.AbstractDbEntity;
import java.sql.Types;
import org.apache.cayenne.map.DbAttribute;

/**
 *
 * @author avolkov
 */
abstract class AbstractInvoiceDbEntity extends AbstractDbEntity {
    
    public AbstractInvoiceDbEntity(String name) {
        super(name);
        init();
    }
    
    private void init() {
        this.getDbEntity().addAttribute(createDateTrans());
        this.getDbEntity().addAttribute(createId());
        this.getDbEntity().addAttribute(createNumber());
        this.getDbEntity().addAttribute(createTotalCost());
        this.getDbEntity().addAttribute(createTotalCostVat());
        this.getDbEntity().addAttribute(createTotalVat());
        this.getDbEntity().addAttribute(createUnp());
    }
    
    private DbAttribute createDateTrans() {
        DbAttribute dateTrans = new DbAttribute("datetrans");
        dateTrans.setType(Types.DATE);
        dateTrans.setMandatory(true);
        return dateTrans;
    }
    
    private DbAttribute createId() {
        DbAttribute id = new DbAttribute("id");
        id.setType(Types.NUMERIC);
        id.setPrimaryKey(true);
        id.setMandatory(true);
        return id;
    }
    
    private DbAttribute createNumber() {
        DbAttribute number = new DbAttribute("number");
        number.setType(Types.NVARCHAR);
        number.setMandatory(true);
        number.setMaxLength(26);
        return number;
    }
    
    private DbAttribute createTotalCost() {
        DbAttribute totalCost = new DbAttribute("totalcost");
        totalCost.setType(Types.DECIMAL);
        totalCost.setMandatory(true);
        return totalCost;
    }
    
    private DbAttribute createTotalCostVat() {
        DbAttribute totalCostVat = new DbAttribute("totalcostvat");
        totalCostVat.setType(Types.DECIMAL);
        totalCostVat.setMandatory(true);
        return totalCostVat;
    }
    
    private DbAttribute createTotalVat() {
        DbAttribute totalVat = new DbAttribute("totalvat");
        totalVat.setType(Types.DECIMAL);
        totalVat.setMandatory(true);
        return totalVat;
    }
    
    private DbAttribute createUnp() {
        DbAttribute unp = new DbAttribute("unp");
        unp.setType(Types.NVARCHAR);
        unp.setMaxLength(15);
        return unp;
    }
    
}
