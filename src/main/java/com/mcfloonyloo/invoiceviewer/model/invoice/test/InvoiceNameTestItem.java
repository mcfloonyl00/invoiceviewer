/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.test;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author avolkov
 */
public class InvoiceNameTestItem {
    
    private static final String REGEX = "^invoice-([0-9]{9})-([0-9]{4})-([0-9]{10})\\.(\\w*)$";
    
    private final String filename;
    
    public InvoiceNameTestItem(String filename) {
        this.filename = filename;
    }
    
    public ErrorList test() {
        ErrorList errors = new ErrorList();
        
        ErrorItem name = getValidateFilename();
        if(Optional.ofNullable(name).isPresent()) {
            errors.add(name);
        }
        
        return errors;
    }
    
    private ErrorItem getValidateFilename() {
        ErrorItem item;
        Pattern pattern = Pattern.compile(REGEX);
        Matcher matcher = pattern.matcher(this.filename);
        if(matcher.matches()) {
             item = new ErrorItem(null);
        } else {
            item = new ErrorItem("внимание - некорректное имя файла");
            item.setFieldName("Имя файла {"+this.filename+"}");
        }
        
        return item;
    }
    
}
