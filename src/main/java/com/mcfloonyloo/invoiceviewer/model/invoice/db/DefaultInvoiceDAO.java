/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.db;

import com.mcfloonyloo.invoiceviewer.model.invoice.Invoice;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author avolkov
 */
public interface DefaultInvoiceDAO<A extends DefaultInvoiceDTO, B extends DefaultInvoiceDocDTO, C extends DefaultInvoiceStatusDTO> {
    
    public void addInvoice(Invoice invoice);
    
    public String getDatabaseName();
    
    public boolean isInvoice(String number);
    
    A getInvoice(String number);
    
    public ArrayList<B> getDocs(String number);
    
    public ArrayList<C> getStatuses(String number);  
    
    public ArrayList<A> getInvoices(LocalDate from, LocalDate to);
        
}
