/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.test;

import com.mcfloonyloo.invoiceviewer.model.invoice.Invoice;

/**
 *
 * @author avolkov
 */
public class PositiveValuesTest extends AbstractTest {
    
    public PositiveValuesTest(Invoice invoice) {
        super(invoice);
        test();
    }
    
    private void test() {
        PositiveValuesTestItem positiveValuesTestItem = new PositiveValuesTestItem(getInvoice());
        positiveValuesTestItem.test();
        if(positiveValuesTestItem.getErrors().size() > 0) {
            this.getErrors().add(positiveValuesTestItem);
        }
    }
}
