/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.titled;

import com.mcfloonyloo.invoiceviewer.model.Title;
import com.mcfloonyloo.invoiceviewer.model.invoice.DefaultInvoiceSection;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceRoster;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceRosterItem;
import java.util.ArrayList;

/**
 *
 * @author avolkov
 */
public class InvoiceRosterTitled implements DefaultInvoiceSection {
    
    private final Title<String> totalCostVat;
    
    private final Title<String> totalExcise;
    
    private final Title<String> totalVat;
    
    private final Title<String> totalCost;
    
    private final Title<ArrayList<Title<InvoiceRosterItemTitled>>> rosters;
    
    public InvoiceRosterTitled(InvoiceRoster roster) {
        this.totalCostVat = new Title<>("Всего по счету. Стоимость товаров (работ, услуг) с НДС", true);
        this.totalExcise = new Title<>("Всего по счету. Сумма акциза", true);
        this.totalVat = new Title<>("Всего по счету. Сумма НДС", true);
        this.totalCost = new Title<>("Всего по сету. Стоимость товаров (работ, услуг) без НДС", true);
        this.rosters = new Title<>("Список товаров (работ, услуг)", true);
        
        this.totalCostVat.setValue(roster.getTotalCostVat());
        this.totalExcise.setValue(roster.getTotalExcise());
        this.totalVat.setValue(roster.getTotalVat());
        this.totalCost.setValue(roster.getTotalCost());
        
        ArrayList<Title<InvoiceRosterItemTitled>> rosterList = new ArrayList<>();
        int index = 0;
        for(InvoiceRosterItem item : roster.getRosters()) {
            index++;
            Title<InvoiceRosterItemTitled> itemTitle = new Title("  Позиция №"+index, true);
            itemTitle.setValue(new InvoiceRosterItemTitled(item));
            rosterList.add(itemTitle);
        }
        
        this.rosters.setValue(rosterList);
    }
    
}
