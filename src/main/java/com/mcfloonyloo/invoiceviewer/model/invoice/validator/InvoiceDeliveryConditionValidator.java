/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.validator;

import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceDeliveryCondition;
import com.mcfloonyloo.invoiceviewer.model.invoice.test.ErrorItem;

/**
 *
 * @author avolkov
 */
public class InvoiceDeliveryConditionValidator extends AbstractSectionValidator {
    
    private final static String SECTION_NAME = "[Условия поставки].";
    
    private final InvoiceDeliveryCondition deliveryCondition;
    
    public InvoiceDeliveryConditionValidator(InvoiceDeliveryCondition deliveryCondition) {
        this.deliveryCondition = deliveryCondition;
    }
    
    public ErrorItem validateContract() {
        ErrorItem item = getValidator().validateSection(this.deliveryCondition.getContract());
        item.setFieldName(SECTION_NAME+"[Договор]");
        return item;
    }
    
}
