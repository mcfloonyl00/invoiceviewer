/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.test;

import com.mcfloonyloo.invoiceviewer.model.invoice.Invoice;

/**
 *
 * @author avolkov
 */
public abstract class AbstractTestItem {
    
    private final Invoice invoice;
    
    private String title;
    
    private final ErrorList errors;
    
    public Invoice getInvoice() {
        return this.invoice;
    }
        
    public String getTitle() {
        return this.title;
    }
    
    protected final void setTitle(String title) {
        this.title = title;
    }
    
    public ErrorList getErrors() {
        return this.errors;
    }

    public AbstractTestItem(Invoice invoice) {
        this.invoice = invoice;
        this.errors = new ErrorList();
    }
    
    public abstract void test();
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        this.errors.getList().forEach((error) -> {
            builder.append(error.toString());
        });
        return builder.toString();
    }
    
}
