/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.test;

import com.mcfloonyloo.invoiceviewer.model.invoice.Invoice;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceSenderItem;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceProviderValidator;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceRecipientValidator;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceSenderItemValidator;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceSenderReceiverValidator;
import java.util.Optional;

/**
 *
 * @author avolkov
 */
public class RequisiteTestItem extends AbstractTestItem {

    public RequisiteTestItem(Invoice invoice) {
        super(invoice);
        setTitle("[REQUISITES TEST] Реквизиты грузоотправителей и грузополучателей - обнаружены проблемы : ");
    }

    @Override
    public void test() {
        
        validateProvider(this.getErrors());
        validateRecipient(this.getErrors());        
        
        InvoiceSenderReceiverValidator senderReceiverValidator = new InvoiceSenderReceiverValidator(getInvoice().getSenderReceiver());
        validateSenderReceiverConsignors(senderReceiverValidator, this.getErrors());
        validateSenderReceiverConsignees(senderReceiverValidator, this.getErrors());
        
    }
    
    private void validateProvider(ErrorList errors) {
        InvoiceProviderValidator providerValidator = new InvoiceProviderValidator(getInvoice().getProvider());
        
        ErrorItem providerUnp = providerValidator.validateUnp();
        if(Optional.ofNullable(providerUnp).isPresent()) {
            errors.add(providerUnp);
        } else {
            ErrorItem providerCountry = providerValidator.validateCountryCode();
            if(Optional.ofNullable(providerCountry).isPresent()) {
                errors.add(providerCountry);
            }
        }
        ErrorItem providerName = providerValidator.validateName();
        if(Optional.ofNullable(providerName).isPresent()) {
            errors.add(providerName);
        }
        ErrorItem providerAddress = providerValidator.validateAddress();
        if(Optional.ofNullable(providerAddress).isPresent()) {
            errors.add(providerAddress);
        }
    }
    
    private void validateRecipient(ErrorList errors) {
        InvoiceRecipientValidator recipientValidator = new InvoiceRecipientValidator(getInvoice().getRecipient());
        
        ErrorItem recipientUnp = recipientValidator.validateUnp();
        if(Optional.ofNullable(recipientUnp).isPresent()) {
            errors.add(recipientUnp);
        } else {
            ErrorItem recipientCountry = recipientValidator.validateCountryCode();
            if(Optional.ofNullable(recipientCountry).isPresent()) {
                errors.add(recipientCountry);
            }
        }
        ErrorItem recipientName = recipientValidator.validateName();
        if(Optional.ofNullable(recipientName).isPresent()) {
            errors.add(recipientName);
        }
        ErrorItem recipientAddress = recipientValidator.validateAddress();
        if(Optional.ofNullable(recipientAddress).isPresent()) {
            errors.add(recipientAddress);
        }
    }
    
    private void validateSenderReceiverConsignors(InvoiceSenderReceiverValidator senderReceiverValidator, ErrorList errors) {
        ErrorItem consignors = senderReceiverValidator.validateConsignors();
        
        if(Optional.ofNullable(consignors).isPresent()) {
            errors.add(consignors);
        } else {
            int index = 0;
            for(InvoiceSenderItem item : getInvoice().getSenderReceiver().getConsignors()) {
                index++;
                InvoiceSenderItemValidator senderItemValidator = new InvoiceSenderItemValidator(index, item);
                
                ErrorItem consignorUnp = senderItemValidator.validateUnp(consignors);
                if(Optional.ofNullable(consignorUnp).isPresent()) {
                    errors.add(consignorUnp);
                } else {
                    ErrorItem consignorCountry = senderItemValidator.validateCountryCode(consignors);
                    if(Optional.ofNullable(consignorCountry).isPresent()) {
                        errors.add(consignorCountry);
                    } 
                }
                ErrorItem consignorName = senderItemValidator.validateName(consignors);
                if(Optional.ofNullable(consignorName).isPresent()) {
                    errors.add(consignorName);
                }
                ErrorItem consignorAddress = senderItemValidator.validateAddress(consignors);
                if(Optional.ofNullable(consignorAddress).isPresent()) {
                    errors.add(consignorAddress);
                }
            }
        }
    }
    
    private void validateSenderReceiverConsignees(InvoiceSenderReceiverValidator senderReceiverValidator, ErrorList errors) {
        ErrorItem consignees = senderReceiverValidator.validateConsignees();
        
        if(Optional.ofNullable(consignees).isPresent()) {
            errors.add(consignees);
        } else {
            int index = 0;
            for(InvoiceSenderItem item : getInvoice().getSenderReceiver().getConsignees()) {
                index++;
                InvoiceSenderItemValidator senderItemValidator = new InvoiceSenderItemValidator(index, item);
                
                ErrorItem consigneeUnp = senderItemValidator.validateUnp(consignees);
                if(Optional.ofNullable(consigneeUnp).isPresent()) {
                    errors.add(consigneeUnp);
                } else {
                    ErrorItem consigneeCountry = senderItemValidator.validateCountryCode(consignees);
                    if(Optional.ofNullable(consigneeCountry).isPresent()) {
                        errors.add(consigneeCountry);
                    } 
                }
                ErrorItem consigneeName = senderItemValidator.validateName(consignees);
                if(Optional.ofNullable(consigneeName).isPresent()) {
                    errors.add(consigneeName);
                }
                ErrorItem consigneeAddress = senderItemValidator.validateAddress(consignees);
                if(Optional.ofNullable(consigneeAddress).isPresent()) {
                    errors.add(consigneeAddress);
                }
            }
        }
    }
    
}
