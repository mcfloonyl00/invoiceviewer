/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice;

import com.mcfloonyloo.invoiceviewer.model.invoice.parser.InvoiceParser;
import com.mcfloonyloo.invoiceviewer.properties.AppProperties;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import org.xml.sax.SAXException;

/**
 *
 * @author avolkov
 */
class UTF8PathInvoiceLoader extends AbstractPathInvoiceLoader {
    
    public UTF8PathInvoiceLoader(Path path) {
        super(path);
    }

    @Override
    public Invoice load() {
        try {
            Invoice invoice = new InvoiceParser(getDocument("utf-8")).parse();
            
            File file = new File(getPath().toString());
            FileXmlReader reader = new FileXmlReader(file);
            invoice.setConvertedContent(reader.read());
                
            
            XsdInvoiceLoader loader = new XsdInvoiceLoader(AppProperties.INSTANCE.getXsdPath(), invoice.getGeneral());
            invoice.setXsdSchema(loader.getByte());
                        
            return invoice;
        } catch (ParserConfigurationException | SAXException | IOException | XMLStreamException ex) {
            return null;
        } catch (Exception ex) {
            return null;
        }
    }
        
}
