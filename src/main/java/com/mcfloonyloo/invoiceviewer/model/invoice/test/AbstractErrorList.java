/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.test;

import java.util.ArrayList;
import java.util.Optional;

/**
 *
 * @author avolkov
 */
public class AbstractErrorList<T extends DefaultErrorItem> {

    private ArrayList<T> list;
    
    public ArrayList<T> getList() {
        return this.list;
    }
    
    public void setList(ArrayList<T> list) {
        this.list = list;
    }
    
    public AbstractErrorList() {
        this.list = new ArrayList<>();
    }
        
    public int size() {
        return this.list.size();
    }
    
    public void clear() {
        this.list.clear();
    }
    
    public boolean isEmpty() {
        return this.list.isEmpty();
    }
    
    public boolean add(T error) {
        if(Optional.ofNullable(error).isPresent()) {
            if(Optional.ofNullable(error.getMessage()).isPresent()) {
                return this.list.add(error);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    public boolean addAll(ArrayList<T> errors) {
        if(Optional.ofNullable(errors).isPresent()) {
            if(errors.isEmpty()){
                return false;
            }else {
                return errors.stream()
                        .filter((error) -> (Optional.ofNullable(error.getMessage()).isPresent()))
                        .noneMatch((error) -> (!this.list.add(error)));
            }
        } else {
            return false;
        }
    }
}
