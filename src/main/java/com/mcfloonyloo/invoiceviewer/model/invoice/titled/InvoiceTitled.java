/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.titled;

import com.mcfloonyloo.invoiceviewer.model.Title;
import com.mcfloonyloo.invoiceviewer.model.invoice.Invoice;

/**
 *
 * @author avolkov
 */
public class InvoiceTitled {
    
    private final Title<String> sender;
    
    private final Title<InvoiceGeneralTitled> general;
    
    private final Title<InvoiceProviderTitled> provider;
    
    private final Title<InvoiceRecipientTitled> recipient;
    
    private final Title<InvoiceSenderReceiverTitled> senderReceiver;
    
    private final Title<InvoiceDeliveryConditionTitled> deliveryCondition;
    
    private final Title<InvoiceRosterTitled> roster;
    
    public InvoiceTitled(Invoice invoice) {
        this.sender = new Title<>("УНП составителя", true);
        this.general = new Title<>("Общая информация", true);
        this.provider = new Title<>("Реквизиты поставщика", true);
        this.recipient = new Title<>("Реквизиты получателя", true);
        this.senderReceiver = new Title<>("Реквизиты грузоотправителя и грузополучателя", true);
        this.deliveryCondition = new Title<>("Условия поставки", true);
        this.roster = new Title<>("Данные по товарам (работам, услугам)", true);
        
        this.sender.setValue(invoice.getSender());
        this.general.setValue(new InvoiceGeneralTitled(invoice.getGeneral()));
        this.provider.setValue(new InvoiceProviderTitled(invoice.getProvider()));
        this.recipient.setValue(new InvoiceRecipientTitled(invoice.getRecipient()));
        this.senderReceiver.setValue(new InvoiceSenderReceiverTitled(invoice.getSenderReceiver()));
        this.deliveryCondition.setValue(new InvoiceDeliveryConditionTitled(invoice.getDeliveryCondition()));
        this.roster.setValue(new InvoiceRosterTitled(invoice.getRoster()));
    }
    
}
