/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.titled;

import com.mcfloonyloo.invoiceviewer.model.Title;
import com.mcfloonyloo.invoiceviewer.model.invoice.DefaultInvoiceSubSection;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceSenderItem;
import java.util.ArrayList;

/**
 *
 * @author avolkov
 */
public class InvoiceConsignorsTitled implements DefaultInvoiceSubSection {
    
    private final Title<ArrayList<Title<InvoiceSenderItemTitled>>> consignors;
    
    public InvoiceConsignorsTitled(ArrayList<InvoiceSenderItem> consignors) {
        this.consignors = new Title<>("22-25. Реквизиты грузоотправителя", true);
        this.consignors.setValue(new ArrayList<>());
        int index = 0;
        for(InvoiceSenderItem item : consignors) {
            index++;
            Title<InvoiceSenderItemTitled> itemTitle = new Title<>("  Реквизит №"+index, true);
            itemTitle.setValue(new InvoiceSenderItemTitled(item));
            this.consignors.getValue().add(itemTitle);
        }
    }
    
}
