/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.validator;

import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceDescriptionItem;
import com.mcfloonyloo.invoiceviewer.model.invoice.test.ErrorItem;
import com.mcfloonyloo.invoiceviewer.model.xmldb.XmldbService;
import java.util.ArrayList;
import java.util.Optional;

/**
 *
 * @author avolkov
 */
public class InvoiceDescriptionsValidator extends AbstractSectionValidator {
    
    private final static String SECTION_NAME = "[Данные по товарам].[Список документов].";
    
    private final int index;
    
    private final ArrayList<InvoiceDescriptionItem> descriptions;
    
    public InvoiceDescriptionsValidator(int index, ArrayList<InvoiceDescriptionItem> descriptions) {
        this.index = index;
        this.descriptions = descriptions;
    }
    
    public ErrorItem validateDescriptions() {
        ErrorItem item = getValidator().validateList(this.descriptions);
        item.setFieldName(SECTION_NAME+"[Товар №"+this.index+"].[Дополнительные данные]");
        return item;
    }
    
    public ErrorItem validateDescriptionsDatabase() {
        ErrorItem item = validateDescriptions();
        if(Optional.ofNullable(item.getMessage()).isPresent()) {
            return item;
        } else {
            int descriptionIndex = 0;
            if(this.descriptions.isEmpty()) {
                return new ErrorItem(null);
            } else {
                for(InvoiceDescriptionItem descriptionItem : this.descriptions) {
                    descriptionIndex++;
                    ErrorItem description = validateDescriptionItem(descriptionIndex, descriptionItem);
                    if(Optional.ofNullable(item).isPresent()) {
                        return description;
                    } else {
                        ErrorItem descriptionDatabase = validateDescriptionItemDatabase(descriptionIndex, descriptionItem);
                        if(Optional.ofNullable(descriptionDatabase).isPresent()) {
                            return descriptionDatabase;
                        }
                    }
                }
                return new ErrorItem(null);
            }
        }
    }
    
    private ErrorItem validateDescriptionItem(int index, InvoiceDescriptionItem descriptionItem) {
        ErrorItem item = getValidator().validateField(descriptionItem.getDescription());
        item.setFieldName(SECTION_NAME+"[Товар №"+this.index+"].[Дополнительные данные].[Строка №"+index+"]");
        return item;
    }
    
    private ErrorItem validateDescriptionItemDatabase(int index, InvoiceDescriptionItem descriptionItem) {
        ErrorItem item = validateDescriptionItem(index, descriptionItem);
        if(Optional.ofNullable(item.getMessage()).isPresent()) {
            return item;
        } else {
            if(XmldbService.INSTANCE.getVatType().isRecord(descriptionItem.getDescription())) {
                return new ErrorItem(null);
            } else {
                ErrorItem type = new ErrorItem("значение '"+descriptionItem.getDescription()+"' не найдено в таблице 'Vat Type'");
                type.setFieldName(SECTION_NAME+"[Товар №"+this.index+"].[Дополнительные данные].[Строка №"+index+"]");
                return type;
            }
        }
    }
    
}
