/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice;

/**
 *
 * @author avolkov
 */
public class InvoiceSenderItem implements DefaultInvoiceSubSectionItem, DefaultInvoiceNullable {
    
    private String countryCode;
    
    private String unp;
    
    private String name;
    
    private String address;
    
    public String getCountryCode() {
        return this.countryCode;
    }
    
    public String getUnp() {
        return this.unp;
    }
    
    public String getName() { 
        return this.name;
    }
    
    public String getAddress() {
        return this.address;
    }
    
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
    
    public void setUnp(String unp) {
        this.unp = unp;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }
    
    public InvoiceSenderItem() {
        defaultValues();
    }
    
    private void defaultValues() {
        this.countryCode = "";
        this.unp = "";
        this.name = "";
        this.address = "";
    }

    @Override
    public void setNullValues() {
        this.countryCode = null;
        this.unp = null;
        this.name = null;
        this.address = null;
    }
}
