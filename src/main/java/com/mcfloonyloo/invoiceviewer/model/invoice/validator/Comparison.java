/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.validator;

/**
 *
 * @author avolkov
 */
public enum Comparison {
    MORE, // больше
    MOREEQUAL, //больше или равно
    LESS, // меньше
    LESSEQUAL, //меньше или равно
    EQUAL; // равно
}
