/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.parser;

import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceVat;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author avolkov
 */
class InvoiceVatParser extends AbstractInvoiceSectionParser<InvoiceVat> {

    public InvoiceVatParser(Node node) {
        super(node);
    }

    @Override
    public InvoiceVat parse() {
        InvoiceVat section = new InvoiceVat();
        section.setNullValues();
        
        NodeList list = getNode().getChildNodes();
        for(int index = 0; index < list.getLength(); index++) {
            Node element = list.item(index);
            if(element.getNodeType() != Node.TEXT_NODE) {
                switch(element.getNodeName()) {
                    case "rate" : {
                        section.setRate(element.getTextContent());
                        break;
                    }
                    case "rateType" : {
                        section.setRateType(element.getTextContent());
                        break;
                    }
                    case "summaVat" : {
                        section.setSummaVat(element.getTextContent());
                        break;
                    }
                }
            }
        }
        
        return section;
    }
    
}
