/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.parser;

import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceSenderItem;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author avolkov
 */
class InvoiceSenderItemParser extends AbstractInvoiceSectionParser<InvoiceSenderItem>{

    public InvoiceSenderItemParser(Node node) {
        super(node);
    }

    @Override
    public InvoiceSenderItem parse() {
        InvoiceSenderItem section = new InvoiceSenderItem();
        section.setNullValues();
        
        NodeList list = getNode().getChildNodes();
        for(int index = 0; index < list.getLength(); index++) {
            Node element = list.item(index);
            if(element.getNodeType() != Node.TEXT_NODE) {
                switch(element.getNodeName()) {
                    case "countryCode" : {
                        section.setCountryCode(element.getTextContent());
                        break;
                    }
                    case "unp" : {
                        section.setUnp(element.getTextContent());
                        break;
                    }
                    case "name" : {
                        section.setName(element.getTextContent());
                        break;
                    }
                    case "address" : {
                        section.setAddress(element.getTextContent());
                        break;
                    }
                }
            }
        }
        
        return section;
    }
    
}
