/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.test;

import com.mcfloonyloo.invoiceviewer.model.invoice.Invoice;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceRosterItem;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceRosterItemValidator;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceRosterValidator;
import java.util.Optional;

/**
 *
 * @author avolkov
 */
public class PositiveValuesTestItem extends AbstractTestItem {

    public PositiveValuesTestItem(Invoice invoice) {
        super(invoice);
        setTitle("[POSITIVE VALUES TEST] Положительные значения - обнаружены проблемы : ");
    }

    @Override
    public void test() {
        
        InvoiceRosterValidator rosterValidator = new InvoiceRosterValidator(getInvoice().getRoster());
        rosterValidator.setInvoiceGeneral(getInvoice().getGeneral());
        
        ErrorItem totalCost = rosterValidator.validateTotalCost();
        if(Optional.ofNullable(totalCost).isPresent()) {
            this.getErrors().add(totalCost);
        }
        ErrorItem totalExcise = rosterValidator.validateTotalExcise();
        if(Optional.ofNullable(totalExcise).isPresent()) {
            this.getErrors().add(totalExcise);
        }
        ErrorItem totalVat = rosterValidator.validateTotalVat();
        if(Optional.ofNullable(totalVat).isPresent()) {
            this.getErrors().add(totalVat);
        }
        ErrorItem totalCostVat = rosterValidator.validateTotalCostVat();
        if(Optional.ofNullable(totalCostVat).isPresent()) {
            this.getErrors().add(totalCostVat);
        }
        ErrorItem rosters = rosterValidator.validateRosters();
        if(Optional.ofNullable(rosters).isPresent()) {
            this.getErrors().add(rosters);
        } else {
            int index = 0;
            for(InvoiceRosterItem item : getInvoice().getRoster().getRosters()) {
                index++;
                InvoiceRosterItemValidator rosterItemValidator = new InvoiceRosterItemValidator(index, item);
                
                ErrorItem price = rosterItemValidator.validatePrice();
                if(Optional.ofNullable(price).isPresent()) {
                    this.getErrors().add(price);
                }
                ErrorItem cost = rosterItemValidator.validateCost();
                if(Optional.ofNullable(cost).isPresent()) { 
                    this.getErrors().add(cost);
                }
                ErrorItem summaExcise = rosterItemValidator.validateSummaExcise();
                if(Optional.ofNullable(summaExcise).isPresent()) {
                    this.getErrors().add(summaExcise);
                }
                ErrorItem costVat = rosterItemValidator.validateCostVat();
                if(Optional.ofNullable(costVat).isPresent()) {
                    this.getErrors().add(costVat);
                }
                ErrorItem vat = rosterItemValidator.validateSummaVat();
                if(Optional.ofNullable(vat).isPresent()) {
                    this.getErrors().add(vat);
                }
            }
        }
        
    }    
}
