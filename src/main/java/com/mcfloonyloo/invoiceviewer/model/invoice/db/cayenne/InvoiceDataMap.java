/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.db.cayenne;

import com.mcfloonyloo.invoiceviewer.model.cayenne.AbstractDataMap;
import org.apache.cayenne.map.DbJoin;
import org.apache.cayenne.map.DbRelationship;
import org.apache.cayenne.map.DeleteRule;
import org.apache.cayenne.map.ObjRelationship;

/**
 *
 * @author avolkov
 */
public class InvoiceDataMap extends AbstractDataMap {
    
    private InvoiceInDbEntity invoiceInDbEntity;
    
    private InvoiceInDocDbEntity invoiceInDocDbEntity;
    
    private InvoiceInStatusDbEntity invoiceInStatusDbEntity;
    
    private InvoiceOutDbEntity invoiceOutDbEntity;
    
    private InvoiceOutDocDbEntity invoiceOutDocDbEntity;
    
    private InvoiceOutStatusDbEntity invoiceOutStatusDbEntity;
    
    private InvoiceInObjEntity invoiceInObjEntity;
    
    private InvoiceInDocObjEntity invoiceInDocObjEntity;
    
    private InvoiceInStatusObjEntity invoiceInStatusObjEntity;
    
    private InvoiceOutObjEntity invoiceOutObjEntity;
    
    private InvoiceOutDocObjEntity invoiceOutDocObjEntity;
    
    private InvoiceOutStatusObjEntity invoiceOutStatusObjEntity;
    
    public InvoiceDataMap() {
        super("InvoiceMap");
        init();
        
        addInDbEntity();
        addOutDbEntity();
        
        addInObjEntity();
        addOutObjEntity();
    }
    
    private void init() {
        this.getDataMap().setDefaultPackage("com.mcfloonyloo.invoiceviewer.model.invoice.db");
    }
    
    private void addInDbEntity() {
        this.invoiceInDbEntity = new InvoiceInDbEntity();
        this.invoiceInDocDbEntity = new InvoiceInDocDbEntity();
        this.invoiceInStatusDbEntity = new InvoiceInStatusDbEntity();

        this.getDataMap().addDbEntity(this.invoiceInDbEntity.getDbEntity());
        this.getDataMap().addDbEntity(this.invoiceInDocDbEntity.getDbEntity());
        this.getDataMap().addDbEntity(this.invoiceInStatusDbEntity.getDbEntity());

        this.invoiceInDbEntity.getDbEntity().addRelationship(createDbInvoiceInDocs());
        this.invoiceInDbEntity.getDbEntity().addRelationship(createDbInvoiceInStatuses());
        this.invoiceInDocDbEntity.getDbEntity().addRelationship(createDbDocsInvoiceIn());
        this.invoiceInStatusDbEntity.getDbEntity().addRelationship(createDbStatusesInvoiceIn());
    }
    
    private void addOutDbEntity() {
        this.invoiceOutDbEntity = new InvoiceOutDbEntity();
        this.invoiceOutDocDbEntity = new InvoiceOutDocDbEntity();
        this.invoiceOutStatusDbEntity = new InvoiceOutStatusDbEntity();
        
        this.getDataMap().addDbEntity(this.invoiceOutDbEntity.getDbEntity());
        this.getDataMap().addDbEntity(this.invoiceOutDocDbEntity.getDbEntity());
        this.getDataMap().addDbEntity(this.invoiceOutStatusDbEntity.getDbEntity());
        
        this.invoiceOutDbEntity.getDbEntity().addRelationship(createDbInvoiceOutDocs());
        this.invoiceOutDbEntity.getDbEntity().addRelationship(createDbInvoiceOutStatuses());
        this.invoiceOutDocDbEntity.getDbEntity().addRelationship(createDbDocsInvoiceOut());
        this.invoiceOutStatusDbEntity.getDbEntity().addRelationship(createDbStatusesInvoiceOut());
    }
    
    private void addInObjEntity() {
        this.invoiceInObjEntity = new InvoiceInObjEntity();
        this.invoiceInDocObjEntity = new InvoiceInDocObjEntity();
        this.invoiceInStatusObjEntity = new InvoiceInStatusObjEntity();
        
        this.getDataMap().addObjEntity(this.invoiceInObjEntity.getObjEntity());
        this.getDataMap().addObjEntity(this.invoiceInDocObjEntity.getObjEntity());
        this.getDataMap().addObjEntity(this.invoiceInStatusObjEntity.getObjEntity());
        
        this.invoiceInObjEntity.getObjEntity().addRelationship(createObjDocsInvoiceIn());
        this.invoiceInObjEntity.getObjEntity().addRelationship(createObjStatusesInvoiceIn());
        this.invoiceInDocObjEntity.getObjEntity().addRelationship(createObjInvoiceInDocs());
        this.invoiceInStatusObjEntity.getObjEntity().addRelationship(createObjInvoiceInStatuses());
    }
    
    private void addOutObjEntity() {
        this.invoiceOutObjEntity = new InvoiceOutObjEntity();
        this.invoiceOutDocObjEntity = new InvoiceOutDocObjEntity();
        this.invoiceOutStatusObjEntity = new InvoiceOutStatusObjEntity();
                
        this.getDataMap().addObjEntity(this.invoiceOutObjEntity.getObjEntity());
        this.getDataMap().addObjEntity(this.invoiceOutDocObjEntity.getObjEntity());
        this.getDataMap().addObjEntity(this.invoiceOutStatusObjEntity.getObjEntity());
        
        this.invoiceOutObjEntity.getObjEntity().addRelationship(createObjDocsInvoiceOut());
        this.invoiceOutObjEntity.getObjEntity().addRelationship(createObjStatusesInvoiceOut());
        this.invoiceOutDocObjEntity.getObjEntity().addRelationship(createObjInvoiceOutDocs());
        this.invoiceOutStatusObjEntity.getObjEntity().addRelationship(createObjInvoiceOutStatuses());
    }
    
    private DbJoin createDbJoin(DbRelationship relationship, String source, String target) {
        DbJoin join = new DbJoin(relationship);
        join.setSourceName(source);
        join.setTargetName(target);
        return join;
    }
    
    private DbRelationship createDbInvoiceInDocs() {
        DbRelationship relationship = new DbRelationship("docs");
        relationship.setSourceEntity(this.invoiceInDbEntity.getDbEntity());
        relationship.setTargetEntityName(this.invoiceInDocDbEntity.getDbEntity());
        relationship.setToMany(true);
        relationship.addJoin(createDbJoin(relationship, "id", "invoiceid"));
        return relationship;
    }
    
    private DbRelationship createDbInvoiceInStatuses() {
        DbRelationship relationship = new DbRelationship("statuses");
        relationship.setSourceEntity(this.invoiceInDbEntity.getDbEntity());
        relationship.setTargetEntityName(this.invoiceInStatusDbEntity.getDbEntity());
        relationship.setToMany(true);
        relationship.addJoin(createDbJoin(relationship, "id", "invoiceid"));
        return relationship;
    }
    
    private DbRelationship createDbDocsInvoiceIn() {
        DbRelationship relationship = new DbRelationship("invoicein");
        relationship.setSourceEntity(this.invoiceInDocDbEntity.getDbEntity());
        relationship.setTargetEntityName(this.invoiceInDbEntity.getDbEntity());
        relationship.addJoin(createDbJoin(relationship, "invoiceid", "id"));
        return relationship;
    }
    
    private DbRelationship createDbStatusesInvoiceIn() {
        DbRelationship relationship = new DbRelationship("invoicein");
        relationship.setSourceEntity(this.invoiceInStatusDbEntity.getDbEntity());
        relationship.setTargetEntityName(this.invoiceInDbEntity.getDbEntity());
        relationship.addJoin(createDbJoin(relationship, "invoiceid", "id"));
        return relationship;
    }
    
    private DbRelationship createDbInvoiceOutDocs() {
        DbRelationship relationship = new DbRelationship("docs");
        relationship.setSourceEntity(this.invoiceOutDbEntity.getDbEntity());
        relationship.setTargetEntityName(this.invoiceOutDocDbEntity.getDbEntity());
        relationship.setToMany(true);
        relationship.addJoin(createDbJoin(relationship, "id", "invoiceid"));
        return relationship;
    }
    
    private DbRelationship createDbInvoiceOutStatuses() {
        DbRelationship relationship = new DbRelationship("statuses");
        relationship.setSourceEntity(this.invoiceOutDbEntity.getDbEntity());
        relationship.setTargetEntityName(this.invoiceOutStatusDbEntity.getDbEntity());
        relationship.setToMany(true);
        relationship.addJoin(createDbJoin(relationship, "id", "invoiceid"));
        return relationship;
    }
    
    private DbRelationship createDbDocsInvoiceOut() {
        DbRelationship relationship = new DbRelationship("invoiceout");
        relationship.setSourceEntity(this.invoiceOutDocDbEntity.getDbEntity());
        relationship.setTargetEntityName(this.invoiceOutDbEntity.getDbEntity());
        relationship.addJoin(createDbJoin(relationship, "invoiceid", "id"));
        return relationship;
    }
    
    private DbRelationship createDbStatusesInvoiceOut() {
        DbRelationship relationship = new DbRelationship("invoiceout");
        relationship.setSourceEntity(this.invoiceOutStatusDbEntity.getDbEntity());
        relationship.setTargetEntityName(this.invoiceOutDbEntity.getDbEntity());
        relationship.addJoin(createDbJoin(relationship, "invoiceid", "id"));
        return relationship;
    }
    
    private ObjRelationship createObjDocsInvoiceIn() {
        ObjRelationship relationship = new ObjRelationship("docs");
        relationship.setSourceEntity(this.invoiceInObjEntity.getObjEntity());
        relationship.setTargetEntityName(this.invoiceInDocObjEntity.getObjEntity());
        relationship.setDeleteRule(DeleteRule.DENY);
        relationship.setDbRelationshipPath("docs");
        return relationship;
    }
    
    private ObjRelationship createObjStatusesInvoiceIn() {
        ObjRelationship relationship = new ObjRelationship("statuses");
        relationship.setSourceEntity(this.invoiceInObjEntity.getObjEntity());
        relationship.setTargetEntityName(this.invoiceInStatusObjEntity.getObjEntity());
        relationship.setDeleteRule(DeleteRule.DENY);
        relationship.setDbRelationshipPath("statuses");
        return relationship;
    }

    private ObjRelationship createObjInvoiceInDocs() {
        ObjRelationship relationship = new ObjRelationship("invoicein");
        relationship.setSourceEntity(this.invoiceInDocObjEntity.getObjEntity());
        relationship.setTargetEntityName(this.invoiceInObjEntity.getObjEntity());
        relationship.setDeleteRule(DeleteRule.NULLIFY);
        relationship.setDbRelationshipPath("invoicein");
        return relationship;
    }

    private ObjRelationship createObjInvoiceInStatuses() {
        ObjRelationship relationship = new ObjRelationship("invoicein");
        relationship.setSourceEntity(this.invoiceInStatusObjEntity.getObjEntity());
        relationship.setTargetEntityName(this.invoiceInObjEntity.getObjEntity());
        relationship.setDeleteRule(DeleteRule.NULLIFY);
        relationship.setDbRelationshipPath("invoicein");
        return relationship;
    }

    private ObjRelationship createObjDocsInvoiceOut() {
        ObjRelationship relationship = new ObjRelationship("docs");
        relationship.setSourceEntity(this.invoiceOutObjEntity.getObjEntity());
        relationship.setTargetEntityName(this.invoiceOutDocObjEntity.getObjEntity());
        relationship.setDeleteRule(DeleteRule.DENY);
        relationship.setDbRelationshipPath("docs");
        return relationship;
    }

    private ObjRelationship createObjStatusesInvoiceOut() {
        ObjRelationship relationship = new ObjRelationship("statuses");
        relationship.setSourceEntity(this.invoiceOutObjEntity.getObjEntity());
        relationship.setTargetEntityName(this.invoiceOutStatusObjEntity.getObjEntity());
        relationship.setDeleteRule(DeleteRule.DENY);
        relationship.setDbRelationshipPath("statuses");
        return relationship;
    }

    private ObjRelationship createObjInvoiceOutDocs() {
        ObjRelationship relationship = new ObjRelationship("invoiceout");
        relationship.setSourceEntity(this.invoiceOutDocObjEntity.getObjEntity());
        relationship.setTargetEntityName(this.invoiceOutObjEntity.getObjEntity());
        relationship.setDeleteRule(DeleteRule.NULLIFY);
        relationship.setDbRelationshipPath("invoiceout");
        return relationship;
    }

    private ObjRelationship createObjInvoiceOutStatuses() {
        ObjRelationship relationship = new ObjRelationship("invoiceout");
        relationship.setSourceEntity(this.invoiceOutStatusObjEntity.getObjEntity());
        relationship.setTargetEntityName(this.invoiceOutObjEntity.getObjEntity());
        relationship.setDeleteRule(DeleteRule.NULLIFY);
        relationship.setDbRelationshipPath("invoiceout");
        return relationship;
    }

}
