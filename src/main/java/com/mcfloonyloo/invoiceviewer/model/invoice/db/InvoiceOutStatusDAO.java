/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.db;

import by.avest.edoc.client.AvEStatus;
import com.mcfloonyloo.invoiceviewer.model.UpdateInvoiceItem;
import static com.mcfloonyloo.invoiceviewer.model.invoice.db.DefaultInvoiceStatusDAO.getStatusRu;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.query.ObjectSelect;

/**
 *
 * @author avolkov
 */
public class InvoiceOutStatusDAO extends AbstractInvoiceDAO
        implements DefaultInvoiceStatusDAO<InvoiceOutStatusDTO> {

    public InvoiceOutStatusDAO(String config) {
        super(config);
    }

    @Override
    public void addStatus(String number, AvEStatus status) {
        ObjectContext context = createContext();
        InvoiceOutDTO invoiceOut = ObjectSelect.query(InvoiceOutDTO.class).where(InvoiceOutDTO.NUMBER.like(number)).selectOne(context);
        InvoiceOutStatusDTO invoiceOutStatus = context.newObject(InvoiceOutStatusDTO.class);
        invoiceOutStatus.setStatusen(status.getStatus());
        invoiceOutStatus.setStatusru(getStatusRu(status.getStatus()));
        
        String since = LocalDateTime.parse(status.getSince()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));;        
        invoiceOutStatus.setDatetimeupdate(LocalDateTime.parse(since, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        
        invoiceOut.addToStatus(invoiceOutStatus);
        
        context.commitChanges();
        shutdownRuntime();
    }

    public boolean isStatus(String number, AvEStatus status) {
        ObjectContext context = createContext();
        InvoiceOutDTO invoiceOut = ObjectSelect.query(InvoiceOutDTO.class).where(InvoiceOutDTO.NUMBER.like(number)).selectOne(context);
        List<InvoiceOutStatusDTO> list = invoiceOut.getStatuses();        
        
        for(int index = 0; index < list.size(); index++) {
            if(list.get(index).getStatusen().equals(status.getStatus())) {
                shutdownRuntime();
                return true;
            }
        }
        
        shutdownRuntime();
        return false;        
    }

    @Override
    public InvoiceOutStatusDTO getStatus(String number) {
        ObjectContext context = createContext();
        InvoiceOutDTO invoiceOut = ObjectSelect.query(InvoiceOutDTO.class).where(InvoiceOutDTO.NUMBER.like(number)).selectOne(context);
        List<InvoiceOutStatusDTO> list = invoiceOut.getStatuses();
        Collections.sort(list, (InvoiceOutStatusDTO left, InvoiceOutStatusDTO right) -> right.getDatetimeupdate().compareTo(left.getDatetimeupdate()));
        
        shutdownRuntime();
        if(Optional.ofNullable(list).isPresent() && !list.isEmpty()) {
            return list.get(0);
        } else {
            return null;
        }
    }

    @Override
    public ArrayList<UpdateInvoiceItem> getInvoicesAndStatuses(LocalDate from, LocalDate to) {
        ArrayList<UpdateInvoiceItem> updateInvoices = new ArrayList<>();
        
        ObjectContext context = createContext();
        List<InvoiceOutDTO> invoices = ObjectSelect.query(InvoiceOutDTO.class).where(InvoiceOutDTO.DATETRANS.between(from, to)).select(context);
        Collections.sort(invoices, (InvoiceOutDTO left, InvoiceOutDTO right) -> left.getDatetrans().compareTo(right.getDatetrans()));
        
        if(invoices.size() > 0) {
            for(InvoiceOutDTO invoice : invoices) {
                InvoiceOutStatusDTO status = getStatus(invoice.getNumber());
                UpdateInvoiceItem updateItem = new UpdateInvoiceItem();
                if(Optional.ofNullable(status).isPresent()) {
                    updateItem.setNumber(invoice.getNumber());
                    updateItem.setStatusEn(status.getStatusen());
                    updateItem.setStatusRu(status.getStatusru());
                    updateItem.setDate(status.getDatetimeupdate());
                } else {
                    updateItem.setNumber(invoice.getNumber());
                    updateItem.setStatusEn("null");
                    updateItem.setStatusRu("null");
                    updateItem.setDate(LocalDateTime.now());
                }

                updateInvoices.add(updateItem);
            }
        }
        
        return updateInvoices;
    }
    
}
