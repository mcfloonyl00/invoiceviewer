/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.parser;

import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceContract;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author avolkov
 */
class InvoiceContractParser extends AbstractInvoiceSectionParser<InvoiceContract> {

    public InvoiceContractParser(Node node) {
        super(node);
    }

    @Override
    public InvoiceContract parse() {
        InvoiceContract section = new InvoiceContract();
        section.setNullValues();
        
        NodeList list = getNode().getChildNodes();
        for(int index = 0; index < list.getLength(); index++) {
            Node element = list.item(index);
            if(element.getNodeType() != Node.TEXT_NODE) {
                switch(element.getNodeName()) {
                    case "number" : {
                        section.setNumber(element.getTextContent());
                        break;
                    }
                    case "date" : {
                        section.setDate(element.getTextContent());
                        break;
                    }
                    case "documents" : {
                        section.setDocuments(new InvoiceDocumentsParser(element).parse());
                        break;
                    }
                }
            }
        }
        
        return section;
    }
    
}
