/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.titled;

import com.mcfloonyloo.invoiceviewer.model.Title;
import com.mcfloonyloo.invoiceviewer.model.invoice.DefaultInvoiceSection;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceGeneral;

/**
 *
 * @author avolkov
 */
public class InvoiceGeneralTitled implements DefaultInvoiceSection {
    
    private final Title<String> number;
    
    private final Title<String> dateIssuance;
    
    private final Title<String> dateTransaction;
    
    private final Title<String> documentType;
    
    private final Title<String> byInvoice;
    
    private final Title<String> sendToRecipient;
    
    private final Title<String> dateCancelled;
    
    public InvoiceGeneralTitled(InvoiceGeneral general) {
        this.number = new Title<>("1. Номер ЭСЧФ", true);
        this.dateIssuance = new Title<>("2. Дата выгрузки ЭСЧФ", true);
        this.dateTransaction = new Title<>("3. Дата совершения операции ЭСЧФ", true);
        this.documentType = new Title<>("4. Тип ЭСЧФ", true);
        this.byInvoice = new Title<>("5. К ЭСЧФ", true);
        this.sendToRecipient = new Title<>("Отправить получателю", true);
        this.dateCancelled = new Title<>("5.1. Дата аннулирования", true);
        
        this.number.setValue(general.getNumber());
        this.dateIssuance.setValue(general.getDateIssuance());
        this.dateTransaction.setValue(general.getDateTransaction());
        this.documentType.setValue(general.getDocumentType());
        this.byInvoice.setValue(general.getByInvoice());
        this.sendToRecipient.setValue(general.getSendToRecipient());
        this.dateCancelled.setValue(general.getDateCancelled());
    }
    
}
