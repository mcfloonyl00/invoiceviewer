/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice;

import com.mcfloonyloo.invoiceviewer.dialog.ExceptionDialog;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 *
 * @author avolkov
 */
class FileXmlReader {
    
    private final File file;
    
    public FileXmlReader(File file) {
        this.file = file;
    }
    
    public byte[] read() {
        byte[] fileData = new byte[(int) this.file.length()];
        try(DataInputStream stream = new DataInputStream(new FileInputStream(this.file))) {
            stream.readFully(fileData);
            return fileData;
        } catch (IOException ex) {
            String message = "Ошибка загрузки файла "+this.file.getAbsolutePath();
            ExceptionDialog dialog = new ExceptionDialog(message, ex)
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText(ex.getClass().toString());
            dialog.showAndWait();
            return null;
        }
    }
    
}
