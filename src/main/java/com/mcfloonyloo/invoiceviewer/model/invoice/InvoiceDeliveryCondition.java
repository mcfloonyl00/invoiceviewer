/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice;

/**
 *
 * @author avolkov
 */
public class InvoiceDeliveryCondition implements DefaultInvoiceSection, DefaultInvoiceNullable {
    
    private String description;
    
    private InvoiceContract contract;
    
    public String getDescription() {
        return this.description;
    }
    
    public InvoiceContract getContract() {
        return this.contract;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public void setContract(InvoiceContract contract) {
        this.contract = contract;
    }
    
    public InvoiceDeliveryCondition() {
        defaultValues();
    }
    
    private void defaultValues() {
        this.description = "";
        this.contract = new InvoiceContract();
    }

    @Override
    public void setNullValues() {
        this.description = null;
        this.contract = null;
    }
    
}
