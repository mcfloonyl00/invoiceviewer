/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.validator;

import com.mcfloonyloo.invoiceviewer.model.invoice.DefaultInvoiceSection;
import com.mcfloonyloo.invoiceviewer.model.invoice.test.ErrorItem;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author avolkov
 */
public class FieldValidator {
    
    public ErrorItem validateField(String value) {
        if(Optional.ofNullable(value).isPresent()) {
            if(value.trim().length() > 0) {
                return new ErrorItem(null);
            } else {
                return new ErrorItem("внимание - пустое значение поля");
            }
        } else {
            return new ErrorItem("ошибка - отсутствует объект поля");
        }
    }
    
    public ErrorItem validateFieldValue(String field, String value) {
        if(Optional.ofNullable(field).isPresent()) {
            if(field.trim().length() > 0) {
                if(field.trim().equals(value.trim())) {
                    return new ErrorItem(null);
                } else {
                    return new ErrorItem("внимание - значение поля не равно '"+value+"'");
                }
            } else {
                return new ErrorItem("внимание - пустое значение поля");
            }
        } else {
            return new ErrorItem("ошибка - отсутствует объект поля");
        }
    }
    
    public ErrorItem validateSection(DefaultInvoiceSection section) {
        if(Optional.ofNullable(section).isPresent()) {
            return new ErrorItem(null);
        } else {
            return new ErrorItem("ошибка - отсутствует объект секции");
        }
    }
    
    public ErrorItem validateList(List list) {
        if(Optional.ofNullable(list).isPresent()) {
            if(list.isEmpty()) {
                return new ErrorItem("внимание - пустой список");
            } else {
                return new ErrorItem(null);
            }
        } else {
            return new ErrorItem("ошибка - отсутствует объект списка");
        }
    }
    
    public ErrorItem validateCompareValues(BigDecimal field, BigDecimal compareValue, com.mcfloonyloo.invoiceviewer.model.invoice.validator.Comparison sign) {
        if(Optional.ofNullable(field).isPresent()) {
            if(Optional.ofNullable(compareValue).isPresent()) {
                switch(sign) {
                    case MORE : {
                        if(field.doubleValue() > compareValue.doubleValue()) {
                            return new ErrorItem(null);
                        } else {
                            return new ErrorItem("внимание - значение поля не соответствует условию '"+field.doubleValue()+" > "+compareValue.doubleValue()+"'");
                        }
                    }
                    case MOREEQUAL : {
                        if(field.doubleValue() >= compareValue.doubleValue()) {
                            return new ErrorItem(null);
                        } else {
                            return new ErrorItem("внимание - значение поля не соответствует условию '"+field.doubleValue()+" >= "+compareValue.doubleValue()+"'");
                        }
                    }
                    case LESS : {
                        if(field.doubleValue() < compareValue.doubleValue()) {
                            return new ErrorItem(null);
                        } else {
                            return new ErrorItem("внимание - значение поля не соответствует значению '"+field.doubleValue()+" < "+compareValue.doubleValue()+"'");
                        }
                    }
                    case LESSEQUAL : {
                        if(field.doubleValue() <= compareValue.doubleValue()) {
                            return new ErrorItem(null);
                        } else {
                            return new ErrorItem("внимание - значение поля не соответствует условию '"+field.doubleValue()+" <= "+compareValue.doubleValue()+"'");
                        }
                    }
                    case EQUAL : {
                        if(field.doubleValue() == compareValue.doubleValue()) {
                            return new ErrorItem(null);
                        } else {
                            return new ErrorItem("внимание - значение поля не соответствует значению '"+field.doubleValue()+" == "+compareValue.doubleValue()+"'");
                        }
                    }
                    default : {
                        return new ErrorItem("ошибка - используется проверка, отсутствующая в списке");
                    }
                }
            } else {
                return new ErrorItem("ошибка - попытка передать несуществующй объект значения");
            }
        } else {
            return new ErrorItem("ошибка - отсутствует объект поля");
        }
    }
    
    public ErrorItem validateCompareLenghts(String field, int lenght, com.mcfloonyloo.invoiceviewer.model.invoice.validator.Comparison sign) {
        if(Optional.ofNullable(field).isPresent()) {
            if(lenght > 0) {
                switch(sign) {
                    case MORE : {
                        if(field.length() > lenght) {
                            return new ErrorItem(null);
                        } else {
                            return new ErrorItem("внимание - длина поля меньше "+lenght);
                        }
                    }
                    case MOREEQUAL : {
                        if(field.length() >= lenght) {
                            return new ErrorItem(null);
                        } else {
                            return new ErrorItem("внимание - длина поля меньше или равна "+lenght);
                        }
                    }
                    case LESS : {
                        if(field.length() < lenght) {
                            return new ErrorItem(null);
                        } else {
                            return new ErrorItem("внимание - длина поля больше "+lenght);
                        }
                    }
                    case LESSEQUAL : {
                        if(field.length() <= lenght) {
                            return new ErrorItem(null);
                        } else {
                            return new ErrorItem("внимание - длина поля больше или равна "+lenght);
                        }
                    }
                    case EQUAL : {
                        if(field.length() == lenght) {
                            return new ErrorItem(null);
                        } else {
                            return new ErrorItem("внимание - длина поля не равна "+lenght);
                        }
                    }
                    default : {
                        return new ErrorItem("ошибка - используется проверка, отсутствующая в списке");
                    }
                }
            } else {
                return new ErrorItem("внимание - отрицательное или нулевое значение длины поля");
            }
        } else {
            return new ErrorItem("внимание - отсутствует объект поля");
        }
    }
    
    public ErrorItem validateRange(String field, int left, int right) {
        if(left > 0 && right > 0) {
            if(left <= right) {
                if(Optional.ofNullable(field).isPresent()) {
                    if(field.trim().length() >= left && field.trim().length() <= right) {
                        return new ErrorItem(null);
                    } else {
                        return new ErrorItem("внимание - длина поля вне заданного диапазона значений");
                    }
                } else {
                    return new ErrorItem("ошибка - отсутствует объект поля");
                }
            } else {
                return new ErrorItem("ошибка - неверный диапазон значений '"+left+" >= "+right+"'");
            }
        } else {
            return new ErrorItem("ошибка - границы диапазона не могут быть отрицательными или нулевыми");
        }
    }
    
}
