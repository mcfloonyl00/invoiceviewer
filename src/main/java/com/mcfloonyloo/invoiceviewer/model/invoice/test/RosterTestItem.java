/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.test;

import com.mcfloonyloo.invoiceviewer.model.invoice.Invoice;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceDocumentItem;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceRosterItem;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceContractValidator;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceDeliveryConditionValidator;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceDocumentItemValidator;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceRecipientValidator;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceRosterItemValidator;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceRosterValidator;
import java.util.Optional;

/**
 *
 * @author avolkov
 */
public class RosterTestItem extends AbstractTestItem {

    public RosterTestItem(Invoice invoice) {
        super(invoice);
        setTitle("[ROSTERS TEST] Данные по товарам - обнаружены проблемы : ");
    }

    @Override
    public void test() {
        
        InvoiceRosterValidator rosterValidator = new InvoiceRosterValidator(getInvoice().getRoster());
        
        ErrorItem rosters = rosterValidator.validateRosters();
        if(Optional.ofNullable(rosters).isPresent()) {
            this.getErrors().add(rosters);
        } else {
            int index = 0;
            for(InvoiceRosterItem item : getInvoice().getRoster().getRosters()) {
                index++;
                InvoiceRecipientValidator recipientValidator = new InvoiceRecipientValidator(getInvoice().getRecipient());
                InvoiceRosterItemValidator rosterItemValidator = new InvoiceRosterItemValidator(index, item);
                
                ErrorItem countryCode = recipientValidator.validateCountryCode();
                if(Optional.ofNullable(countryCode).isPresent()) {
                    this.getErrors().add(countryCode);
                } else {
                    if(getInvoice().getRecipient().getCountryCode().equals("112")) {//заменить "волшебную строку" на константу
                        ErrorItem units = rosterItemValidator.validateUnit();
                        if(Optional.ofNullable(units).isPresent()) {
                            this.getErrors().add(units);
                        }
                    } else {
                        InvoiceDeliveryConditionValidator deliveryConditionValidator = new InvoiceDeliveryConditionValidator(getInvoice().getDeliveryCondition());
                        
                        ErrorItem contract = deliveryConditionValidator.validateContract();
                        if(Optional.ofNullable(contract).isPresent()) {
                            this.getErrors().add(contract);
                        } else {
                            InvoiceContractValidator contractValidator = new InvoiceContractValidator(getInvoice().getDeliveryCondition().getContract());
                            
                            ErrorItem contractError = contractValidator.validateDocuments();
                            if(Optional.ofNullable(contractError).isPresent()){
                                this.getErrors().add(contractError);
                            } else {
                                int docIndex = 0;
                                for(InvoiceDocumentItem docItem : getInvoice().getDeliveryCondition().getContract().getDocuments()) {
                                    docIndex++;
                                    InvoiceDocumentItemValidator documentItemValidator = new InvoiceDocumentItemValidator(docIndex, docItem);
                                    
                                    ErrorItem docType = documentItemValidator.validateDocumentType();
                                    if(Optional.ofNullable(docType).isPresent()) {
                                        this.getErrors().add(docType);
                                    } else {
                                        ErrorItem switcher = switchesContractDocType(index, item, docItem);
                                        if(Optional.ofNullable(switcher).isPresent()) {
                                            this.getErrors().add(switcher);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
        
    private ErrorItem switchesContractDocType(int rosterItemIndex, InvoiceRosterItem rosterItem, InvoiceDocumentItem documentItem) {
        InvoiceRosterItemValidator rosterItemValidator = new InvoiceRosterItemValidator(rosterItemIndex, rosterItem);
        switch(documentItem.getDocType().getCode()) {
            case "602" : 
            case "603" : 
            case "607" : {
                ErrorItem rosterCode = rosterItemValidator.validateCode();
                if(Optional.ofNullable(rosterCode).isPresent()) {
                    return rosterCode;
                }
                break;
            }
            case "606" : 
            case "612" : {
                ErrorItem rosterCodeOced = rosterItemValidator.validateCodeOced();
                if(Optional.ofNullable(rosterCodeOced).isPresent()) {
                    return rosterCodeOced;
                }
                break;
            }
            default : {
                ErrorItem rosterCode = rosterItemValidator.validateCode();
                ErrorItem rosterCodeOced = rosterItemValidator.validateCodeOced();
                if(Optional.ofNullable(rosterCode).isPresent()) {
                    return rosterCode;
                } else {
                    if(Optional.ofNullable(rosterCodeOced).isPresent()) {
                        return rosterCodeOced;
                    }
                }
            }
        }
        return new ErrorItem(null);
    }
    
}
