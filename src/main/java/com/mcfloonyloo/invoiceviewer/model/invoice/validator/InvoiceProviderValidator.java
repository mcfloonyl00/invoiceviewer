/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.validator;

import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceProvider;
import com.mcfloonyloo.invoiceviewer.model.invoice.test.ErrorItem;
import com.mcfloonyloo.invoiceviewer.model.xmldb.XmldbService;
import java.util.Optional;

/**
 *
 * @author avolkov
 */
public class InvoiceProviderValidator extends AbstractSectionValidator {
    
    private final static String SECTION_NAME = "[Поставщик].";
    
    private final InvoiceProvider provider;
    
    public InvoiceProviderValidator(InvoiceProvider provider) {
        this.provider = provider;
    }
    
    public ErrorItem validateUnp() {
        ErrorItem item = getValidator().validateField(this.provider.getUnp());
        item.setFieldName(SECTION_NAME+"[УНП]");
        return item;
    }
    
    public ErrorItem validateCountryCode() {
        ErrorItem item = getValidator().validateField(this.provider.getCountryCode());
        item.setFieldName(SECTION_NAME+"[Код государства]");
        return item;
    }
    
    public ErrorItem validateCountryCodeDatabase() {
        ErrorItem item = validateCountryCode();
        if(Optional.ofNullable(item.getMessage()).isPresent()) {
            return item;
        } else {
            if(XmldbService.INSTANCE.getCountryCode().isRecord(this.provider.getCountryCode())) {
                return new ErrorItem(null);
            } else {
                ErrorItem country = new ErrorItem("значение '"+this.provider.getCountryCode()+"' не найдено в таблице 'Country'");
                country.setFieldName(SECTION_NAME+"[Код государства]");
                return country;
            }
        }
    }
    
    public ErrorItem validateUnpBranchCodeDatabase() {
        ErrorItem unp = validateUnp();
        if(Optional.ofNullable(unp.getMessage()).isPresent()) {
            return unp;
        }
        ErrorItem branch = validateBranchCode();
        if(Optional.ofNullable(branch.getMessage()).isPresent()) {
            return branch;
        }
        if(XmldbService.INSTANCE.getBranch().isRecord(this.provider.getUnp(), this.provider.getBranchCode())) {
            return new ErrorItem(null);
        } else {
            ErrorItem pair = new ErrorItem("пара значений '"+this.provider.getUnp()+"' и '"+this.provider.getBranchCode()+"' не найдена в таблице 'Branch'");
            pair.setFieldName(SECTION_NAME+"[Код государства]");
            return pair;
        }
    }
    
    public ErrorItem validateBranchCode() {
        ErrorItem item = getValidator().validateField(this.provider.getBranchCode());
        item.setFieldName(SECTION_NAME+"[Код филиала]");
        return item;
    }
    
    public ErrorItem validateName() {
        ErrorItem item = getValidator().validateField(this.provider.getName());
        item.setFieldName(SECTION_NAME+"[Наименование организации]");
        return item;
    }
    
    public ErrorItem validateAddress() {
        ErrorItem item = getValidator().validateField(this.provider.getAddress());
        item.setFieldName(SECTION_NAME+"[Юридический адрес организации]");
        return item;
    }
    
    public ErrorItem validateStatusDatabase() {
        ErrorItem item = validateStatus();
        if(Optional.ofNullable(item.getMessage()).isPresent()) {
            return item;
        } else {
            if(XmldbService.INSTANCE.getProviderStatus().isRecord(this.provider.getProviderStatus())) {
                return new ErrorItem(null);
            } else {
                ErrorItem status = new ErrorItem("значение '"+this.provider.getProviderStatus()+"' не найдено в таблице 'Provider Status'");
                status.setFieldName(SECTION_NAME+"[Статус]");
                return status;
            }
        }
    }
    
    public ErrorItem validateStatus() {
        ErrorItem item = getValidator().validateField(this.provider.getProviderStatus());
        item.setFieldName(SECTION_NAME+"[Статус]");
        return item;
    }
    
}
