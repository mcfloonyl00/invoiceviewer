/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.test;

import com.mcfloonyloo.invoiceviewer.model.invoice.Invoice;
import java.util.ArrayList;

/**
 *
 * @author avolkov
 */
public class AbstractTest {
    
    private final Invoice invoice;
    
    private final ArrayList<AbstractTestItem> errors;
    
    public Invoice getInvoice() {
        return this.invoice;
    }
    
    public ArrayList<AbstractTestItem> getErrors() {
        return this.errors;
    }
    
    public AbstractTest(Invoice invoice) {
        this.invoice = invoice;
        this.errors = new ArrayList<>();
    }
    
    public String getMessage() {
        StringBuilder builder = new StringBuilder("<html>");
        
        this.errors.forEach((AbstractTestItem error) -> {
            builder.append("<b>")
                    .append(error.getTitle())
                    .append("</b>")
                    .append("<br>")
                    .append(error.toString());
        });
        
        return builder.append("<br></html>").toString();
    }
    
}
