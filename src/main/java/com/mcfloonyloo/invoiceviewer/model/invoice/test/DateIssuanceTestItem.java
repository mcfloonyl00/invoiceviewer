    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.test;

import com.mcfloonyloo.invoiceviewer.model.invoice.Invoice;
import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceDocumentItem;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceContractValidator;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceDocumentItemValidator;
import com.mcfloonyloo.invoiceviewer.model.invoice.validator.InvoiceGeneralValidator;
import java.util.Optional;

/**
 *
 * @author avolkov
 */
public class DateIssuanceTestItem extends AbstractTestItem {

    public DateIssuanceTestItem(Invoice invoice) {
        super(invoice);
        setTitle("[DATE TEST] Проверка даты совершения операции - обнаружены проблемы : ");
    }

    @Override
    public void test() {        
        InvoiceGeneralValidator generalValidator = new InvoiceGeneralValidator(getInvoice().getGeneral());
        ErrorItem dateTransaction = generalValidator.validateDateTransaction();
        if(Optional.ofNullable(dateTransaction).isPresent()) {
            this.getErrors().add(dateTransaction);
        } else {
            InvoiceContractValidator contractValidator = new InvoiceContractValidator(getInvoice().getDeliveryCondition().getContract());
            ErrorItem documents = contractValidator.validateDocuments();
            if(Optional.ofNullable(documents).isPresent()) {
                this.getErrors().add(documents);
            } else {
                int index = 0;
                for(InvoiceDocumentItem item : getInvoice().getDeliveryCondition().getContract().getDocuments()) {
                    index++;
                    InvoiceDocumentItemValidator documentItemValidator = new InvoiceDocumentItemValidator(index, item);
                    ErrorItem documentCode = documentItemValidator.validateDocumentTypeCode();
                    if(Optional.ofNullable(documentCode).isPresent()) {
                        this.getErrors().add(documentCode);
                    } else {
                        switch(item.getDocType().getCode()) {
                            case "609" : 
                            case "612" : {
                                break;
                            }
                            default : {
                                ErrorItem documentDateEqualDateTransaction = documentItemValidator.validateDateEqualDateTransaction(getInvoice().getGeneral());
                                if(Optional.ofNullable(documentDateEqualDateTransaction).isPresent()) {
                                    this.getErrors().add(documentDateEqualDateTransaction);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
