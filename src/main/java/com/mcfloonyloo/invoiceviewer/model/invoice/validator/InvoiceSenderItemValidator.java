/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.validator;

import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceSenderItem;
import com.mcfloonyloo.invoiceviewer.model.invoice.test.ErrorItem;
import com.mcfloonyloo.invoiceviewer.model.xmldb.XmldbService;
import java.util.Optional;

/**
 *
 * @author avolkov
 */
public class InvoiceSenderItemValidator extends AbstractSectionValidator {
    
    private final int index;
    
    private final InvoiceSenderItem senderItem;
    
    public InvoiceSenderItemValidator(int index, InvoiceSenderItem senderItem) {
        this.index = index;
        this.senderItem = senderItem;
    }
    
    public ErrorItem validateUnp(ErrorItem senderErrorItem) {
        ErrorItem item = getValidator().validateField(senderItem.getUnp());
        item.setFieldName(senderErrorItem.getFieldName()+".[Реквизит №"+index+"].[УНП]");
        return item;
    }
    
    public ErrorItem validateCountryCode(ErrorItem senderErrorItem) {
        ErrorItem item = getValidator().validateField(senderItem.getCountryCode());
        item.setFieldName(senderErrorItem.getFieldName()+".[Реквизит №"+index+"].[Код государства]");
        return item;
    }
    
    public ErrorItem validateName(ErrorItem senderErrorItem) {
        ErrorItem item = getValidator().validateField(senderItem.getName());
        item.setFieldName(senderErrorItem.getFieldName()+".[Реквизит №"+index+"].[Наименование организации]");
        return item;
    }
    
    public ErrorItem validateAddress(ErrorItem senderErrorItem) {
        ErrorItem item = getValidator().validateField(senderItem.getAddress());
        item.setFieldName(senderErrorItem.getFieldName()+".[Реквизит №"+index+"].[Адрес организации]");
        return item;
    }
    
    public ErrorItem validateCountryCodeDatabase(ErrorItem senderErrorItem) {
        ErrorItem item = validateCountryCode(senderErrorItem);
        if(Optional.ofNullable(item.getMessage()).isPresent()) {
            return item;
        } else {
            if(XmldbService.INSTANCE.getCountryCode().isRecord(this.senderItem.getCountryCode())) {
                return new ErrorItem(null);
            } else {
                ErrorItem country = new ErrorItem("значение '' не найдено в таблице 'Country Code'");
                country.setFieldName(senderErrorItem.getFieldName()+".[Реквизит №"+index+"].[Код государства]");
                return country;
            }
        }
    }
    
}
