/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.parser;

import com.mcfloonyloo.invoiceviewer.model.invoice.InvoiceDocumentType;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author avolkov
 */
class InvoiceDocumentTypeParser extends AbstractInvoiceSectionParser<InvoiceDocumentType> {

    public InvoiceDocumentTypeParser(Node node) {
        super(node);
    }

    @Override
    public InvoiceDocumentType parse() {
        InvoiceDocumentType section = new InvoiceDocumentType();
        section.setNullValues();
        
        NodeList list = getNode().getChildNodes();
        for(int index = 0; index < list.getLength(); index++) {
            Node element = list.item(index);
            if(element.getNodeType() != Node.TEXT_NODE) {
                switch(element.getNodeName()) {
                    case "code" : {
                        section.setCode(element.getTextContent());
                        break;
                    }
                    case "value" : {
                        section.setValue(element.getTextContent());
                        break;
                    }
                }
            }
        }
        
        return section;
    }
    
}
