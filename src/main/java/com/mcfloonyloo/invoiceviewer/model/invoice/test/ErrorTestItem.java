/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.invoice.test;

import java.util.ArrayList;

/**
 * Класс для хранения результатов теста
 * @author avolkov
 */
public class ErrorTestItem implements DefaultErrorItem {
    
    private String message;
    
    private ArrayList<ErrorItem> errors;
    
    @Override
    public String getMessage() {
        return this.message;
    }
    
    public ArrayList<ErrorItem> getErrors() {
        return this.errors;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
    
    public void setErrors(ArrayList<ErrorItem> errors) {
        this.errors = errors;
    }
    
    public String toError() {
        StringBuilder builder = new StringBuilder();
        this.errors.forEach((error) -> {
            builder.append(error.toString())
                    .append("\n");
        });
        return builder.toString();
    }
    
    public ErrorTestItem() {
        defaultValues();
    }
    
    private void defaultValues() {
        this.message = "";
        this.errors = new ArrayList<>();
    }
    
}
