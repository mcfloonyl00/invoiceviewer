/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.preloader.db.cayenne;

import com.mcfloonyloo.invoiceviewer.model.cayenne.AbstractDbEntity;
import java.sql.Types;
import org.apache.cayenne.map.DbAttribute;

/**
 *
 * @author avolkov
 */
class KeyPreloaderDbEntity extends AbstractDbEntity {

    public KeyPreloaderDbEntity() {
        super("KeyPreloader");
        init();
    }
    
    private void init() {
        this.getDbEntity().setPrimaryKeyGenerator(null);
        this.getDbEntity().addAttribute(createAlias());
        this.getDbEntity().addAttribute(createDateAfter());
        this.getDbEntity().addAttribute(createId());
        this.getDbEntity().addAttribute(createOrgId());
    }
    
    private DbAttribute createAlias() {
        DbAttribute alias = new DbAttribute("alias");
        alias.setType(Types.NVARCHAR);
        alias.setMandatory(true);
        alias.setMaxLength(255);
        return alias;
    }
    
    private DbAttribute createDateAfter() {
        DbAttribute dateAfter = new DbAttribute("dateafter");
        dateAfter.setType(Types.NVARCHAR);
        dateAfter.setMandatory(true);
        dateAfter.setMaxLength(10);
        return dateAfter;
    }
    
    private DbAttribute createId() {
        DbAttribute id = new DbAttribute("id");
        id.setType(Types.INTEGER);
        id.setPrimaryKey(true);
        id.setMandatory(true);
        return id;
    }
    
    private DbAttribute createOrgId() {
        DbAttribute orgId = new DbAttribute("orgid");
        orgId.setType(Types.INTEGER);
        orgId.setMandatory(true);
        return orgId;
    }
}
