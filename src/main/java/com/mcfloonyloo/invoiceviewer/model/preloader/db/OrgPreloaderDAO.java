/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.preloader.db;

import com.mcfloonyloo.invoiceviewer.model.AvKeyStoreItem;
import java.util.ArrayList;
import java.util.Optional;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.query.ObjectSelect;

/**
 *
 * @author avolkov
 */
public class OrgPreloaderDAO extends AbstractPreloaderDAO {
    
    private final static String PATH = "";
        
    public OrgPreloaderDAO(String config) {
        super(config);
    }
    
    public OrgPreloaderDTO addOrgKey(OrgPreloaderItem orgItem, AvKeyStoreItem item) {
        ObjectContext context = createContext();
        
        OrgPreloaderDTO org = null;
        if(Optional.ofNullable(orgItem.getUnp()).isPresent() &&
                !orgItem.getUnp().isEmpty() &&
                Optional.ofNullable(orgItem.getOrgname()).isPresent() &&
                !orgItem.getOrgname().isEmpty() &&
                Optional.ofNullable(item).isPresent()) {
            org = context.newObject(OrgPreloaderDTO.class);
            org.setUnp(orgItem.getUnp());
            org.setName(orgItem.getOrgname());
            org.setPathdb(getPathdb(orgItem));
            org.setFlagdelete(false);

            KeyPreloaderDTO key = context.newObject(KeyPreloaderDTO.class);
            key.setOrg(org);
            key.setAlias(item.getAlias());
            key.setDateafter(item.getDateAfter());

            org.addToKeys(key);

            context.commitChanges();
            
        }
        
        shutdownRuntime();
        
        return org;
    }
    
    public OrgPreloaderDTO addKey(String unp, AvKeyStoreItem item) {
        ObjectContext context = createContext();
        
        OrgPreloaderDTO org = ObjectSelect.query(OrgPreloaderDTO.class)
                .where(OrgPreloaderDTO.UNP.like(unp))
                .and(OrgPreloaderDTO.FLAGDELETE.isFalse())
                .selectOne(context);
        if(Optional.ofNullable(org).isPresent()) {
            KeyPreloaderDTO key = context.newObject(KeyPreloaderDTO.class);
            key.setAlias(item.getAlias());
            key.setDateafter(item.getDateAfter());
            
            org.addToKeys(key);
            
            context.commitChanges();
        } else {
            org = null;
        }
        
        shutdownRuntime();
        
        return org;
    }
    
    public boolean isRecord(String unp) {
        boolean is;
        
        ObjectContext context = createContext();
        is = !ObjectSelect.query(OrgPreloaderDTO.class)
                .where(OrgPreloaderDTO.UNP.like(unp))
                .and(OrgPreloaderDTO.FLAGDELETE.isFalse())
                .select(context).isEmpty();
        
        return is;
    }
    
    public AvKeyStoreItem[] getKeys(String unp) {
        ArrayList<AvKeyStoreItem> keys = new ArrayList<>();
        
        ObjectContext context = createContext() ;
        OrgPreloaderDTO org = ObjectSelect.query(OrgPreloaderDTO.class).where(OrgPreloaderDTO.UNP.like(unp)).selectOne(context);
        if(Optional.ofNullable(org).isPresent()) {
            if(org.getKeys().size() > 0) {
                org.getKeys().forEach((key) -> {
                    keys.add(new AvKeyStoreItem(key.getAlias(), key.getDateafter()));
                });
            }
        }
        
        return (AvKeyStoreItem[]) keys.toArray();
    }
    
    public ArrayList<OrgPreloaderDTO> getAll() {
        ArrayList<OrgPreloaderDTO> orgs = new ArrayList<>();
        
        ObjectContext context = createContext();
        orgs.addAll(ObjectSelect.query(OrgPreloaderDTO.class).where(OrgPreloaderDTO.FLAGDELETE.isFalse()).select(context));
        
        return orgs;
    }
    
    public void remove(String unp) {
        ObjectContext context = createContext();
        
        OrgPreloaderDTO org = ObjectSelect.query(OrgPreloaderDTO.class).where(OrgPreloaderDTO.UNP.like(unp)).selectOne(context);
        if(Optional.ofNullable(org).isPresent()) {
            org.setFlagdelete(true);
            org.getKeys().forEach((key) -> {
                org.getKeys().remove(key);
            });
            context.commitChanges();
        }
        
        shutdownRuntime();
    }
    
    private String getPathdb(OrgPreloaderItem orgItem) {
        return new StringBuilder(PATH).append(orgItem.getUnp()).append(".db").toString();
    }
}
