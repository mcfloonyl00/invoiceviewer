/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.preloader.db.cayenne;

import com.mcfloonyloo.invoiceviewer.model.cayenne.AbstractDbEntity;
import java.sql.Types;
import org.apache.cayenne.map.DbAttribute;

/**
 *
 * @author avolkov
 */
class OrgPreloaderDbEntity extends AbstractDbEntity {

    public OrgPreloaderDbEntity() {
        super("OrgPreloader");
        init();
    }
    
    private void init() {
        this.getDbEntity().addAttribute(createFlagdelete());
        this.getDbEntity().addAttribute(createId());
        this.getDbEntity().addAttribute(createName());
        this.getDbEntity().addAttribute(createPathDb());
        this.getDbEntity().addAttribute(createUnp());
    }
    
    private DbAttribute createFlagdelete() {
        DbAttribute flagdelete = new DbAttribute("flagdelete");
        flagdelete.setType(Types.BIT);
        return flagdelete;
    }
    
    private DbAttribute createId() {
        DbAttribute id = new DbAttribute("id");
        id.setType(Types.INTEGER);
        id.setPrimaryKey(true);
        id.setMandatory(true);
        return id;
    }
    
    private DbAttribute createName() {
        DbAttribute name = new DbAttribute("name");
        name.setType(Types.NVARCHAR);
        name.setMandatory(true);
        name.setMaxLength(255);
        return name;
    }
    
    private DbAttribute createPathDb() {
        DbAttribute pathdb = new DbAttribute("pathdb");
        pathdb.setType(Types.NVARCHAR);
        pathdb.setMandatory(true);
        pathdb.setMaxLength(511);
        return pathdb;
    }

    private DbAttribute createUnp() {
        DbAttribute unp = new DbAttribute("unp");
        unp.setType(Types.NVARCHAR);
        unp.setMandatory(true);
        unp.setMaxLength(10);
        return unp;
    }
}
