/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.preloader.db.cayenne;

import com.mcfloonyloo.invoiceviewer.model.cayenne.AbstractDataMap;
import org.apache.cayenne.map.DbJoin;
import org.apache.cayenne.map.DbRelationship;
import org.apache.cayenne.map.DeleteRule;
import org.apache.cayenne.map.ObjRelationship;

/**
 *
 * @author avolkov
 */
public class PreloaderDataMap extends AbstractDataMap {
    
    private KeyPreloaderDbEntity keyPreloaderDbEntity;
    
    private OrgPreloaderDbEntity orgPreloaderDbEntity;
    
    private KeyPreloaderObjEntity keyPreloaderObjEntity;
    
    private OrgPreloaderObjEntity orgPreloaderObjEntity;
    
    public PreloaderDataMap() { 
        super("CertificateMap");
        init();
        
        addDbEntity();
        addObjEntity();
    }
    
    private void init() {
        this.getDataMap().setDefaultPackage("com.mcfloonyloo.invoiceviewer.model.preloader.db");
    }
    
    private void addDbEntity() {
        this.keyPreloaderDbEntity = new KeyPreloaderDbEntity();
        this.orgPreloaderDbEntity = new OrgPreloaderDbEntity();
                
        this.getDataMap().addDbEntity(this.keyPreloaderDbEntity.getDbEntity());
        this.getDataMap().addDbEntity(this.orgPreloaderDbEntity.getDbEntity());
        
        this.keyPreloaderDbEntity.getDbEntity().addRelationship(createDbRelationshipOrg());
        this.orgPreloaderDbEntity.getDbEntity().addRelationship(createDbRelationshipKeys());
    }
    
    private void addObjEntity() {
        this.keyPreloaderObjEntity = new KeyPreloaderObjEntity();
        this.orgPreloaderObjEntity = new OrgPreloaderObjEntity();
        
        this.getDataMap().addObjEntity(this.keyPreloaderObjEntity.getObjEntity());
        this.getDataMap().addObjEntity(this.orgPreloaderObjEntity.getObjEntity());
        
        this.orgPreloaderObjEntity.getObjEntity().addRelationship(createObjRelationshipKeys());
        this.keyPreloaderObjEntity.getObjEntity().addRelationship(createObjRelationshipOrg());
    }
    
    private DbJoin createDbJoin(DbRelationship relationship, String source, String target) {
        DbJoin join = new DbJoin(relationship);
        join.setSourceName(source);
        join.setTargetName(target);
        return join;
    }
    
    private DbRelationship createDbRelationshipOrg() {
        DbRelationship relationship = new DbRelationship("Org");
        relationship.setSourceEntity(this.keyPreloaderDbEntity.getDbEntity());
        relationship.setTargetEntityName(this.orgPreloaderDbEntity.getDbEntity());
        relationship.addJoin(createDbJoin(relationship, "orgid", "id"));
        return relationship;
    }
    
    private DbRelationship createDbRelationshipKeys() {
        DbRelationship relationship = new DbRelationship("Keys");
        relationship.setSourceEntity(this.orgPreloaderDbEntity.getDbEntity());
        relationship.setTargetEntityName(this.keyPreloaderDbEntity.getDbEntity());
        relationship.setToMany(true);
        relationship.addJoin(createDbJoin(relationship, "id", "orgid"));
        return relationship;
    }
    
    private ObjRelationship createObjRelationshipKeys() {
        ObjRelationship relationship = new ObjRelationship("keys");
        relationship.setSourceEntity(this.orgPreloaderObjEntity.getObjEntity());
        relationship.setTargetEntityName(this.keyPreloaderObjEntity.getObjEntity());
        relationship.setDeleteRule(DeleteRule.DENY);
        relationship.setDbRelationshipPath("Keys");
        return relationship;
    }
    
    private ObjRelationship createObjRelationshipOrg() {
        ObjRelationship relationship = new ObjRelationship("org");
        relationship.setSourceEntity(this.keyPreloaderObjEntity.getObjEntity());
        relationship.setTargetEntityName(this.orgPreloaderObjEntity.getObjEntity());
        relationship.setDeleteRule(DeleteRule.NULLIFY);
        relationship.setDbRelationshipPath("Org");
        return relationship;
    }
}
