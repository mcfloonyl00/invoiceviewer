/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.preloader.db;

/**
 *
 * @author avolkov
 */
public class OrgPreloaderItem {
    
    private final String unp;
    
    private final String orgname;
        
    public String getUnp() {
        return this.unp;
    }
    
    public String getOrgname() {
        return this.orgname;
    }
    
    public OrgPreloaderItem(String unp, String name) {
        this.unp = unp;
        this.orgname = name;
    }
    
}
