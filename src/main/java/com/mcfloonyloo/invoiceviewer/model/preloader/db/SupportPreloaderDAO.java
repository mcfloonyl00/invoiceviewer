/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.preloader.db;

import com.mcfloonyloo.invoiceviewer.model.cayenne.SQLiteDataSource;
import com.mcfloonyloo.invoiceviewer.model.preloader.db.cayenne.PreloaderDataMap;
import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Modality;
import org.apache.cayenne.access.DbGenerator;
import org.apache.cayenne.access.dbsync.CreateIfNoSchemaStrategy;
import org.apache.cayenne.configuration.server.ServerRuntime;
import org.apache.cayenne.dba.DbAdapter;
import org.apache.cayenne.log.JdbcEventLogger;

/**
 *
 * @author avolkov
 */
public class SupportPreloaderDAO {
    
    private final static String PATH_DATABASE = ".\\resources\\";
    
    private final String databaseName;
    
    private ServerRuntime runtime;
    
    private final File databaseFile;
    
    private final PreloaderDataMap dataMap;
    
    public SupportPreloaderDAO(String databaseName) {
        this.databaseName = databaseName; 
        this.databaseFile = new File(PATH_DATABASE+databaseName);
        this.dataMap = new PreloaderDataMap();
    }
    
    private boolean isDatabaseFileExists() {
        return this.databaseFile.exists();
    }
    
    public void init() throws FileNotFoundException { 
        if(isDatabaseFileExists()) {
            createRuntime();
        } else {
            throw new FileNotFoundException("Отсутствует файл базы данных "+databaseName);
        }
    }
    
    private void createRuntime() {
        SQLiteDataSource sqliteDataSource = new SQLiteDataSource("jdbc:sqlite:"+PATH_DATABASE+databaseName);
        this.runtime = ServerRuntime.builder().dataSource(sqliteDataSource.getDataSource()).build();

        this.dataMap.getDataMap().setQuotingSQLIdentifiers(true);

        this.runtime.getDataDomain().addDataMap(this.dataMap.getDataMap());

        this.runtime.getDataDomain().getDefaultNode().setSchemaUpdateStrategy(new CreateIfNoSchemaStrategy());
    }
    
    public void validateConnectionDatabase() throws SQLException, NullPointerException {
        if(Optional.ofNullable(this.runtime).isPresent()) {
            Connection connection = this.runtime.getDataSource().getConnection();
            if(Optional.ofNullable(connection).isPresent()) {
                connection.close();
                this.runtime.shutdown();
            } else {
                this.runtime.shutdown();
                throw new SQLException("Ошибка соединения с базой данных "+this.databaseName);
            }
        } else {
            this.runtime.shutdown();
            //напоминание для разработчика класса
            throw new NullPointerException("Не проинициализирован класс Runtime\nдля работы с базой данных");
        }
    }
    
    public void generateDatabase() throws Exception {
        createRuntime();
        
        DbAdapter adapter = this.runtime.getDataDomain().getDefaultNode().getAdapter();
        JdbcEventLogger logger = this.runtime.getDataDomain().getDefaultNode().getJdbcEventLogger();
        DbGenerator generator = new DbGenerator(adapter, this.dataMap.getDataMap(), logger);
        generator.runGenerator(this.runtime.getDataSource());
        
        this.runtime.shutdown();
        
        showInformationDialog();
    }
    
    private void showInformationDialog() {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("АРМ оператора ЭСЧФ");
        alert.setHeaderText("Информация");
        alert.setContentText("Новая база данных организаций создана.\nПолучен полный доступ к возможностям приложения");
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.showAndWait();
    }
}
