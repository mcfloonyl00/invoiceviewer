/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.preloader.db.cayenne;

import com.mcfloonyloo.invoiceviewer.model.cayenne.AbstractObjEntity;
import com.mcfloonyloo.invoiceviewer.model.preloader.db.OrgPreloaderDTO;
import org.apache.cayenne.map.ObjAttribute;

/**
 *
 * @author avolkov
 */
class OrgPreloaderObjEntity extends AbstractObjEntity {
    
    public OrgPreloaderObjEntity() {
        super("OrgPreloaderDTO");
        init();
    }
    
    private void init() {
        this.getObjEntity().setClassName(OrgPreloaderDTO.class.getCanonicalName());
        this.getObjEntity().setDbEntityName("OrgPreloader");
        this.getObjEntity().addAttribute(createFlagDelete());
        this.getObjEntity().addAttribute(createName());
        this.getObjEntity().addAttribute(createPathDb());
        this.getObjEntity().addAttribute(createUnp());
    }
    
    private ObjAttribute createFlagDelete() {
        ObjAttribute flagDelete = new ObjAttribute("flagdelete");
        flagDelete.setType("boolean");
        flagDelete.setDbAttributePath("flagdelete");
        return flagDelete;
    }
 
    
    private ObjAttribute createName() {
        ObjAttribute name = new ObjAttribute("name");
        name.setType("java.lang.String");
        name.setDbAttributePath("name");
        return name;
    }
    
    private ObjAttribute createPathDb() {
        ObjAttribute pathDb = new ObjAttribute("pathdb");
        pathDb.setType("java.lang.String");
        pathDb.setDbAttributePath("pathdb");
        return pathDb;
    }
    
    private ObjAttribute createUnp() {
        ObjAttribute unp = new ObjAttribute("unp");
        unp.setType("java.lang.String");
        unp.setDbAttributePath("unp");
        return unp;
    }
}
