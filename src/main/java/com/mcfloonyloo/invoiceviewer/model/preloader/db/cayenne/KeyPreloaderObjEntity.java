/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.preloader.db.cayenne;

import com.mcfloonyloo.invoiceviewer.model.cayenne.AbstractObjEntity;
import com.mcfloonyloo.invoiceviewer.model.preloader.db.KeyPreloaderDTO;
import org.apache.cayenne.map.ObjAttribute;

/**
 *
 * @author avolkov
 */
class KeyPreloaderObjEntity extends AbstractObjEntity {
    
    public KeyPreloaderObjEntity() {
        super("KeyPreloaderDTO");
        init();
    }
    
    private void init() {
        this.getObjEntity().setClassName(KeyPreloaderDTO.class.getCanonicalName());
        this.getObjEntity().setDbEntityName("KeyPreloader");
        this.getObjEntity().addAttribute(createAlias());
        this.getObjEntity().addAttribute(createDateAfter());
    }
    
    private ObjAttribute createAlias() {
        ObjAttribute alias = new ObjAttribute("alias");
        alias.setType("java.lang.String");
        alias.setDbAttributePath("alias");
        return alias;
    }
    
    private ObjAttribute createDateAfter() {
        ObjAttribute dateAfter = new ObjAttribute("dateafter");
        dateAfter.setType("java.lang.String");
        dateAfter.setDbAttributePath("dateafter");
        return dateAfter;
    }
}
