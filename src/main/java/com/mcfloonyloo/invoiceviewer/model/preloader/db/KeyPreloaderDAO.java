/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.preloader.db;

import com.mcfloonyloo.invoiceviewer.model.AvKeyStoreItem;
import java.util.List;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.query.ObjectSelect;

/**
 *
 * @author avolkov
 */
public class KeyPreloaderDAO extends AbstractPreloaderDAO {

    public KeyPreloaderDAO(String name) {
        super(name);
    }
        
    public boolean isRecord(AvKeyStoreItem item) {
        boolean is;
        
        ObjectContext context = createContext();
        is = !ObjectSelect.query(KeyPreloaderDTO.class)
                .where(KeyPreloaderDTO.ALIAS.like(item.getAlias()))
                .and(KeyPreloaderDTO.DATEAFTER.like(item.getDateAfter()))
                .select(context).isEmpty();
        shutdownRuntime();
        
        return is;
    }
    
    public void remove(KeyPreloaderDTO key) {
        ObjectContext context = createContext();
        
        context.deleteObject(key);
        context.commitChanges();
        
        shutdownRuntime();
    }
    
    public void remove(KeyPreloaderDTO... keys) {
        ObjectContext context = createContext();
        
        for(KeyPreloaderDTO key : keys) {
            context.deleteObject(key);
        }
        
        context.commitChanges();
        
        shutdownRuntime();
    }
    
    public List<KeyPreloaderDTO> getKeys(OrgPreloaderDTO orgValue) {
        
        ObjectContext context = createContext();
        
        OrgPreloaderDTO org = ObjectSelect.query(OrgPreloaderDTO.class)
                .where(OrgPreloaderDTO.UNP.like(orgValue.getUnp()))
                .and(OrgPreloaderDTO.NAME.like(orgValue.getName()))
                .and(OrgPreloaderDTO.FLAGDELETE.isFalse())
                .selectFirst(context);
        
        return org.getKeys();
    }
}
