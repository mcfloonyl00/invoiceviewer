/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.preloader.db;

import com.mcfloonyloo.invoiceviewer.model.cayenne.SQLiteDataSource;
import com.mcfloonyloo.invoiceviewer.model.preloader.db.cayenne.PreloaderDataMap;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.dbsync.CreateIfNoSchemaStrategy;
import org.apache.cayenne.configuration.server.ServerRuntime;

/**
 *
 * @author avolkov
 */
abstract class AbstractPreloaderDAO {
    
    private final String databaseName;
    
    private ServerRuntime runtime;
        
    public AbstractPreloaderDAO(String databaseName) {
        this.databaseName = databaseName;
    }
    
    protected ObjectContext createContext() {
        String url = ".\\resources\\"+this.databaseName;
        SQLiteDataSource sqliteDataSource = new SQLiteDataSource("jdbc:sqlite:"+url);
        this.runtime = ServerRuntime.builder().dataSource(sqliteDataSource.getDataSource()).build();

        PreloaderDataMap map = new PreloaderDataMap();
        this.runtime.getDataDomain().addDataMap(map.getDataMap());

        this.runtime.getDataDomain().getDefaultNode().setSchemaUpdateStrategy(new CreateIfNoSchemaStrategy());
        
        return this.runtime.newContext();

    }
    
    protected void shutdownRuntime() {
        this.runtime.shutdown();
    }
}
