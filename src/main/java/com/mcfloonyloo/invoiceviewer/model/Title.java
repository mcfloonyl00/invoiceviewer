/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model;

/**
 * Класс-обобщение для добавления заголовка поля
 * @author avolkov
 */
public class Title<T> {
    
    private String title;
    
    private final boolean optional;
    
    private T value;
    
    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public boolean isOptional() {
        return this.optional;
    }
    
    public T getValue() {
        return this.value;
    }
    
    public void setValue(T value) {
        this.value = value;
    }
    
    public Title(boolean optional) {
        this.title = "";
        this.optional = optional;
    }
    
    public Title(String title, boolean optional) {
        this.title = title;
        this.optional = optional;
    }
    
}
