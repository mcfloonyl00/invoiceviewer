/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.report.db;

import com.mcfloonyloo.invoiceviewer.model.invoice.db.DefaultInvoiceDAO;
import com.mcfloonyloo.invoiceviewer.model.report.ReportInvoiceItem;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author avolkov
 */
public abstract class AbstractReportInvoiceDAO {
    
    private final DefaultInvoiceDAO invoiceDao;
    
    public DefaultInvoiceDAO getInvoiceDao() {
        return this.invoiceDao;
    }
    
    public AbstractReportInvoiceDAO(DefaultInvoiceDAO invoiceDao) {
        this.invoiceDao = invoiceDao;
    }
    
    public abstract ArrayList<ReportInvoiceItem> getInvoices(LocalDate from, LocalDate to);
    
}
