/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.report;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 *
 * @author avolkov
 */
public class ReportInvoiceItem {
    
    private String unp;
    
    private LocalDate dateTransaction;
    
    private String number;
    
    private String status;
    
    private LocalDateTime dateStatus;
    
    private BigDecimal totalCost;
    
    private BigDecimal totalVat;
    
    private BigDecimal totalCostVat;
    
    public String getUnp() {
        return this.unp;
    }
    
    public LocalDate getDateTransaction() {
        return this.dateTransaction;
    }
    
    public String getNumber() {
        return this.number;
    }
    
    public String getStatus() {
        return this.status;
    }
    
    public LocalDateTime getDateStatus() {
        return this.dateStatus;
    }
    
    public BigDecimal getTotalCost() {
        return this.totalCost;
    }
    
    public BigDecimal getTotalVat() {
        return this.totalVat;
    }
    
    public BigDecimal getTotalCostVat() {
        return this.totalCostVat;
    }
    
    public void setUnp(String unp) {
        this.unp = unp;
    }
    
    public void setDateTransaction(LocalDate dateTransaction) {
        this.dateTransaction = dateTransaction;
    }
    
    public void setNumber(String number) {
        this.number = number;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
    
    public void setDateStatus(LocalDateTime dateStatus) {
        this.dateStatus = dateStatus;
    }
    
    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }
    
    public void setTotalVat(BigDecimal totalVat) {
        this.totalVat = totalVat;
    }
    
    public void setTotalCostVat(BigDecimal totalCostVat) {
        this.totalCostVat = totalCostVat;
    }
    
    public ReportInvoiceItem() {
        defaultValues();
    }
    
    private void defaultValues() {
        this.unp = "";
        this.dateTransaction = LocalDate.now();
        this.number = "";
        this.status = "";
        this.dateStatus = LocalDateTime.now();
        this.totalCost = BigDecimal.ZERO;
        this.totalVat = BigDecimal.ZERO;
        this.totalCostVat = BigDecimal.ZERO;
    }
    
}
