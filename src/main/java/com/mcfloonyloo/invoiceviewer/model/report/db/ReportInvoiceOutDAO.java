/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.model.report.db;

import com.mcfloonyloo.invoiceviewer.model.invoice.db.InvoiceOutDAO;
import com.mcfloonyloo.invoiceviewer.model.invoice.db.InvoiceOutDTO;
import com.mcfloonyloo.invoiceviewer.model.invoice.db.InvoiceOutStatusDAO;
import com.mcfloonyloo.invoiceviewer.model.invoice.db.InvoiceOutStatusDTO;
import com.mcfloonyloo.invoiceviewer.model.report.ReportInvoiceItem;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;

/**
 *
 * @author avolkov
 */
public class ReportInvoiceOutDAO extends AbstractReportInvoiceDAO {
        
    public ReportInvoiceOutDAO(InvoiceOutDAO invoiceDao) {
        super(invoiceDao);
    }
    
    @Override
    public ArrayList<ReportInvoiceItem> getInvoices(LocalDate from, LocalDate to) {        
        ArrayList<ReportInvoiceItem> invoiceReportList = new ArrayList<>();
        
        ArrayList<InvoiceOutDTO> invoices = getInvoiceDao().getInvoices(from, to);
        
        for(InvoiceOutDTO invoice : invoices) {
            InvoiceOutStatusDAO statusDao = new InvoiceOutStatusDAO(getInvoiceDao().getDatabaseName());
            InvoiceOutStatusDTO status = statusDao.getStatus(invoice.getNumber());
            
            ReportInvoiceItem item = new ReportInvoiceItem();
            
            item.setUnp(invoice.getUnp());
            item.setDateTransaction(invoice.getDatetrans());
            item.setNumber(invoice.getNumber());
            if(Optional.ofNullable(status).isPresent()) {
                item.setStatus(status.getStatusru());
                item.setDateStatus(status.getDatetimeupdate());
            } else {
                item.setStatus("null");
                item.setDateStatus(LocalDateTime.now());
            }
            item.setTotalCost(invoice.getTotalcost());
            item.setTotalVat(invoice.getTotalvat());
            item.setTotalCostVat(invoice.getTotalcostvat());
            
            invoiceReportList.add(item);
        }
        
        return invoiceReportList;
    }
    
}
