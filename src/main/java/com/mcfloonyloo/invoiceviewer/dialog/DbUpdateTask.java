/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.dialog;

import by.avest.edoc.client.AvDocException;
import by.avest.edoc.client.AvEStatus;
import com.mcfloonyloo.invoiceviewer.model.UpdateInvoiceItem;
import com.mcfloonyloo.invoiceviewer.model.invoice.db.DefaultInvoiceStatusDAO;
import com.mcfloonyloo.invoiceviewer.service.InvoiceService;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Optional;
import javafx.collections.ObservableList;

/**
 *
 * @author avolkov
 */
public class DbUpdateTask<E extends DefaultInvoiceStatusDAO> extends AbstractUpdateTask<UpdateInvoiceItem>{

    private final E statusDao;
    
    public DbUpdateTask(ObservableList<UpdateInvoiceItem> fileList, E statusDao) {
        super(fileList);
        this.statusDao = statusDao;
    }
    
    public DbUpdateTask(E statusDao) {
        super(null);
        this.statusDao = statusDao;
    }

    @Override
    protected ArrayList<UpdateInvoiceItem> call() throws Exception {
        int index = 0;
        
        if(Optional.ofNullable(this.statusDao).isPresent()) {
            InvoiceService.INSTANCE.getService().connect();
            System.out.println("Service connected");
            for(UpdateInvoiceItem item : getFileList()) {
                if(isCancelled()) {
                    return getReturnFileList();
                }

                index++;
                updateAndReplaceRecord(item);

                this.updateMessage(getMessage(index));
                this.updateProgress(index, getCount());
            }
        
            InvoiceService.INSTANCE.getService().disconnect();
            System.out.println("Service disconnected");
        } else {
            throw new NullPointerException("Не произведена инициализация инструмента для работы с базой данных.\nВозможности работы окна обновления статусов ограничена");
        }
        
        return getReturnFileList();
    }
    
    private void updateAndReplaceRecord(UpdateInvoiceItem item) throws AvDocException, ParseException, IOException {
        if(Optional.ofNullable(item).isPresent()) {
            AvEStatus status = InvoiceService.INSTANCE.getService().getStatus(item.getNumber());
            System.out.println("Status read");
            if(status.verify()) {
                System.out.println("Is status?");
                if(statusDao.isStatus(item.getNumber(), status)) {
                    System.out.println("Status is yes");
                    this.incSkip();
                } else {
                    System.out.println("Status is not");
                    statusDao.addStatus(item.getNumber(), status);
                    System.out.println("Add new status");
                    getReturnFileList().add(item);
                    this.incAccept();
                }
            } else {
                this.incError();
            }
        } else {
            this.incError();
        } 
    }
    
}
