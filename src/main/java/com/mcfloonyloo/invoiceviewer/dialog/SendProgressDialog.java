/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.dialog;

import com.mcfloonyloo.invoiceviewer.model.FileCheckListItem;
import java.util.ArrayList;
import java.util.Optional;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

/**
 *
 * @author avolkov
 */
public class SendProgressDialog extends AbstractProgressDialog<FileCheckListItem> {

    public SendProgressDialog(ObservableList<FileCheckListItem> selectionList) {
        super("Выполняется передача ЭСЧФ...", selectionList, new FileCheckSendTask(selectionList));
    }

    @Override
    public ArrayList<FileCheckListItem> show() {
        new Thread(getTask()).start();
        Optional<ButtonType> sendResult = getAlert().showAndWait();
        if(sendResult.isPresent() && sendResult.get() == getDoneButton()) {
            Optional<ButtonType> okResult = createAndShowAlert("Передача ЭСФЧ завершена", Alert.AlertType.INFORMATION);
            if(okResult.isPresent() && okResult.get() == ButtonType.OK) {
                return getFileList();
            }
        }
        if(sendResult.isPresent() && sendResult.get() == getCancelButton()) {
            getTask().cancel();
            Optional<ButtonType> okResult = createAndShowAlert("Передача ЭСЧФ прервана", Alert.AlertType.WARNING);
            if(okResult.isPresent() && okResult.get() == ButtonType.OK) {
                return getFileList();
            }
        }
        return null;
    }
    
    @Override
    protected String getMessage() {
        StringBuilder builder = new StringBuilder();
        builder.append("Передано ЭСЧФ - ").append(((FileCheckSendTask)getTask()).getAccept());
        builder.append(System.lineSeparator());
        builder.append("Получены ошибки при передаче ЭСЧФ - ").append(((FileCheckSendTask)getTask()).getError());
        builder.append(System.lineSeparator());
        builder.append("Некорректных ЭСЧФ - ").append(((FileCheckSendTask)getTask()).getInvalid());
        return builder.toString();
    }
    
}
