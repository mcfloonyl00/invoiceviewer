/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.dialog;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author avolkov
 */
public class ExceptionDialog {
    
    private final ButtonType ok;
    
    private final Alert alert;
    
    public ButtonType getOk() {
        return this.ok;
    }
    
    public ExceptionDialog setTitle(String title) {
        this.alert.setTitle(title);
        return this;
    }
    
    public ExceptionDialog setHeaderText(String headerText) {
        this.alert.setHeaderText(headerText);
        return this;
    }
    
    public ExceptionDialog setOwner(Stage stage) {
        this.alert.initOwner(stage);
        this.alert.initModality(Modality.APPLICATION_MODAL);
        return this;
    }
    
    public Optional<ButtonType> showAndWait() {
        return this.alert.showAndWait();
    }
    
    public ExceptionDialog(String contentText, Exception ex) {        
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        ex.printStackTrace(printWriter);
        String exceptionText = stringWriter.getBuffer().toString();
        
        Label label = new Label("Содержимое стека исключений: ");
        
        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);
        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setHgrow(textArea, Priority.ALWAYS);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        
        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0,0);
        expContent.add(textArea, 0,1);
        
        this.ok = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
        this.alert = new Alert(AlertType.ERROR, contentText, this.ok);
        this.alert.initModality(Modality.APPLICATION_MODAL);
        this.alert.getDialogPane().setExpandableContent(expContent);
        
    }
}
