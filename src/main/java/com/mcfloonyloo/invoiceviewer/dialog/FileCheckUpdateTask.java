/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.dialog;

import by.avest.edoc.client.AvDocException;
import by.avest.edoc.client.AvEStatus;
import com.mcfloonyloo.invoiceviewer.model.FileCheckListItem;
import com.mcfloonyloo.invoiceviewer.model.invoice.Invoice;
import com.mcfloonyloo.invoiceviewer.model.invoice.PathInvoiceLoaderFactory;
import com.mcfloonyloo.invoiceviewer.model.invoice.db.DefaultInvoiceStatusDAO;
import com.mcfloonyloo.invoiceviewer.service.InvoiceService;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Optional;
import javafx.collections.ObservableList;

/**
 *
 * @author avolkov
 */
public class FileCheckUpdateTask<E extends DefaultInvoiceStatusDAO> extends AbstractUpdateTask<FileCheckListItem> {

    private final E statusDao;
    
    public FileCheckUpdateTask(ObservableList<FileCheckListItem> fileList, E statusDao) {
        super(fileList);
        this.statusDao = statusDao;
    }

    @Override
    protected ArrayList<FileCheckListItem> call() throws Exception {
        int index = 0;

        if(Optional.ofNullable(this.statusDao).isPresent()) {
            InvoiceService.INSTANCE.getService().connect();

            for(FileCheckListItem item : getFileList()) {
                if(isCancelled()) {
                    return getReturnFileList();
                }

                index++;
                updateAndReplaceRecord(item);

                this.updateMessage(getMessage(index));
                this.updateProgress(index, getCount());
            }

            InvoiceService.INSTANCE.getService().disconnect();    
        } else {
            throw new NullPointerException("Не произведена инициализация инструмента для работы с базой данных.\nВозможности работы окна обновления статусов ограничена");
        }
        
        return getReturnFileList();
    }

    private void updateAndReplaceRecord(FileCheckListItem item) throws AvDocException, ParseException, IOException {
        Invoice invoice = new PathInvoiceLoaderFactory().load(item.getPath());
        if(Optional.ofNullable(invoice).isPresent()) {
            AvEStatus status = InvoiceService.INSTANCE.getService().getStatus(invoice.getGeneral().getNumber());
            if(status.verify()) {
                if(statusDao.isStatus(invoice.getGeneral().getNumber(), status)) {
                    this.incSkip();
                } else {
                    statusDao.addStatus(invoice.getGeneral().getNumber(), status);
                    getReturnFileList().add(item);
                    this.incAccept();
                }
            } else {
                this.incError();
            }
        } else {
            this.incError();
        }
    }
    
}
