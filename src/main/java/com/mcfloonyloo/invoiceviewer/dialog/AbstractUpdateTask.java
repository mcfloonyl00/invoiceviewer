/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.dialog;

import java.util.ArrayList;
import javafx.collections.ObservableList;

/**
 *
 * @author avolkov
 */
public abstract class AbstractUpdateTask<T> extends AbstractTask<T> {
    
    private int accept;
    
    private int skip;
    
    private int error;
    
    public int getAccept() {
        return this.accept;
    }
    
    public int getSkip() {
        return this.skip;
    }    
    
    public int getError() {
        return this.error;
    }
    
    public void incAccept() {
        this.accept++;
    }
    
    public void incSkip() {
        this.skip++;
    }
    
    public void incError() {
        this.error++;
    }

    public AbstractUpdateTask(ObservableList<T> fileList) {
        super(fileList);
        defaultValues();
    }

    private void defaultValues() {
        this.accept = 0;
        this.skip = 0;
        this.error = 0;
    }
    
    @Override
    protected abstract ArrayList<T> call() throws Exception;
    
    protected String getMessage(int index) {
        StringBuilder builder = new StringBuilder();
        builder.append("Обновлено ").append(index).append(" из ").append(getCount());
        builder.append(System.lineSeparator());
        builder.append("Корректных - ").append(this.accept).append(" файлов;");
        builder.append(System.lineSeparator());
        builder.append("Некорректных - ").append(this.error).append(" файлов;");
        builder.append(System.lineSeparator());
        builder.append("Пропущено - ").append(this.skip).append(" файлов.");
        return builder.toString();
    }
    
}
