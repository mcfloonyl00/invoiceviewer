/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.dialog;

import by.avest.edoc.client.AvDocException;
import by.avest.edoc.client.AvEStatus;
import com.mcfloonyloo.invoiceviewer.OrgAppService;
import com.mcfloonyloo.invoiceviewer.model.invoice.Invoice;
import com.mcfloonyloo.invoiceviewer.model.invoice.ServiceInvoiceLoader;
import com.mcfloonyloo.invoiceviewer.model.invoice.db.InvoiceInDAO;
import com.mcfloonyloo.invoiceviewer.model.invoice.db.InvoiceInStatusDAO;
import com.mcfloonyloo.invoiceviewer.model.invoice.db.InvoiceOutDAO;
import com.mcfloonyloo.invoiceviewer.model.invoice.db.InvoiceOutStatusDAO;
import com.mcfloonyloo.invoiceviewer.service.InvoiceService;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Optional;
import javafx.collections.ObservableList;

/**
 *
 * @author avolkov
 */
public class GetInvoiceTask extends AbstractTask<String> {

    private int accept;
    
    private int error;
    
    private int skip;
    
    public int getAccept() {
        return this.accept;
    }
    
    public int getError() {
        return this.error;
    }
    
    public int getSkip() {
        return this.skip;
    }
    
    private final InvoiceInDAO invoiceInDao;
    
    private final InvoiceOutDAO invoiceOutDao;
    
    private final InvoiceInStatusDAO invoiceInStatusDao;
    
    private final InvoiceOutStatusDAO invoiceOutStatusDao;
    
    public GetInvoiceTask(ObservableList<String> fileList) {
        super(fileList);
        this.invoiceInDao = new InvoiceInDAO(OrgAppService.INSTANCE.getPathDb());
        this.invoiceOutDao = new InvoiceOutDAO(OrgAppService.INSTANCE.getPathDb());
        
        this.invoiceInStatusDao = new InvoiceInStatusDAO(OrgAppService.INSTANCE.getPathDb());
        this.invoiceOutStatusDao = new InvoiceOutStatusDAO(OrgAppService.INSTANCE.getPathDb());
        defaultValues();
    }
    
    private void defaultValues() {
        this.accept = 0;
        this.error = 0;
        this.skip = 0;
    }


    @Override
    protected ArrayList<String> call() throws Exception {
        int index = 0;
        
        InvoiceService.INSTANCE.getService().connect();
        
        for(String item : getFileList()) {
            if(isCancelled()) {
                return getReturnFileList();
            }
            index++;
            addInvoiceRecord(item);
            
            this.updateMessage(getMessage(index));
            this.updateProgress(index, getCount());
        }
        
        InvoiceService.INSTANCE.getService().disconnect();
        
        return getReturnFileList();
    }
    
    private void addInvoiceRecord(String item) throws AvDocException, IOException, GeneralSecurityException, ParseException {
        Invoice invoice = new ServiceInvoiceLoader(item).load();
        if(Optional.ofNullable(invoice).isPresent()) {
            if(invoice.getSender().trim().equals(OrgAppService.INSTANCE.getUnp().trim())) {
                //УНП соответствуют -> исходящий
                if(invoiceOutDao.isInvoice(item)) {
                    this.skip++;
                } else {
                    invoiceOutDao.addInvoice(invoice);
                    
                    AvEStatus status = InvoiceService.INSTANCE.getService().getStatus(item);
                    if(invoiceOutStatusDao.isStatus(item, status)) {
                        this.skip++;
                    } else {
                        this.invoiceOutStatusDao.addStatus(item, status);
                        this.accept++;
                    }
                }
            } else {
                if(invoice.getRecipient().getUnp().trim().equals(OrgAppService.INSTANCE.getUnp().trim())) {
                     if(invoiceInDao.isInvoice(item)) {
                        this.skip++;
                    } else {
                        invoiceInDao.addInvoice(invoice);
                        
                        AvEStatus status = InvoiceService.INSTANCE.getService().getStatus(item);
                        if(invoiceInStatusDao.isStatus(item, status)) {
                            this.skip++;
                        } else {
                            this.invoiceInStatusDao.addStatus(item, status);
                            this.accept++;
                        }
                    }
                } else {
                    this.skip++;
                }
            }
        } else {
            this.error++;
        }
    }
    
    private String getMessage(int index){
        StringBuilder builder = new StringBuilder();
        builder.append("Загружено ").append(index).append(" ЭСЧФ из ").append(getCount()).append(", причем: ");
        builder.append(System.lineSeparator());
        builder.append("загружено - ").append(this.accept).append(" ЭСЧФ, ");
        builder.append("имеется ошибка - ").append(this.error).append(" ЭСЧФ;");
        builder.append(System.lineSeparator());
        builder.append("пропущено - ").append(this.skip).append(" ЭСЧФ;");
        
        return builder.toString();
    }
}
