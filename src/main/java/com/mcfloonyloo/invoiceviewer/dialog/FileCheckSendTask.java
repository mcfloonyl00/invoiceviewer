/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.dialog;

import by.avest.edoc.client.AvDocException;
import by.avest.edoc.client.AvEDoc;
import by.avest.edoc.client.AvETicket;
import by.avest.edoc.client.AvError;
import com.mcfloonyloo.invoiceviewer.OrgAppService;
import com.mcfloonyloo.invoiceviewer.model.FileCheckListItem;
import com.mcfloonyloo.invoiceviewer.model.invoice.Invoice;
import com.mcfloonyloo.invoiceviewer.model.invoice.PathInvoiceLoaderFactory;
import com.mcfloonyloo.invoiceviewer.model.invoice.db.InvoiceOutDAO;
import com.mcfloonyloo.invoiceviewer.service.InvoiceService;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Optional;
import javafx.collections.ObservableList;

/**
 *
 * @author avolkov
 */
public class FileCheckSendTask extends AbstractTask<FileCheckListItem> {
    
    private int valid;
    
    private int invalid;
    
    private int accept;
    
    private int error;
    
    private int skip;
    
    public int getValid() {
        return this.valid;
    }
    
    public int getInvalid() {
        return this.invalid;
    }
    
    public int getAccept() {
        return this.accept;
    }
    
    public int getError() {
        return this.error;
    }
    
    public int getSkip() {
        return this.skip;
    }
    
    public FileCheckSendTask(ObservableList<FileCheckListItem> fileList) {
        super(fileList);
        defaultValues();
    }
    
    private void defaultValues() {
        this.valid = 0;
        this.invalid = 0;
        this.accept = 0;
        this.error = 0;
        this.skip = 0;
    }

    @Override
    protected ArrayList<FileCheckListItem> call() throws Exception {
        int index = 0;
        
        InvoiceService.INSTANCE.getService().connect();
        System.out.println("Connect open");
        for(FileCheckListItem item : getFileList()) {
            if(isCancelled()) {
                return getReturnFileList();
            }
            index++;
            sendInvoiceAndAddRecord(item);
            //addInvoiceRecord(item);
            System.out.println("Invoice sended");
            this.updateMessage(getMessage(index));
            this.updateProgress(index, getCount());
        }
        
        InvoiceService.INSTANCE.getService().disconnect();
        System.out.println("Connect close");
        return getReturnFileList();
    }
    
    private void sendInvoiceAndAddRecord(FileCheckListItem item) throws AvDocException, IOException, GeneralSecurityException, ParseException {
        Invoice invoice = new PathInvoiceLoaderFactory().load(item.getPath());
        if(Optional.ofNullable(invoice).isPresent()) {
            InvoiceOutDAO inOut = new InvoiceOutDAO(OrgAppService.INSTANCE.getPathDb());
            if(inOut.isInvoice(invoice.getGeneral().getNumber())) {   
                this.skip++;
            } else {   
                AvEDoc doc = InvoiceService.INSTANCE.getService().createEDoc();
                doc.getDocument().load(invoice.getContent());
                if(doc.getDocument().validateXML(invoice.getXsdSchema())) {
                    doc.sign();
                    this.valid++;
                    AvETicket ticket = InvoiceService.INSTANCE.getService().sendEDoc(doc);
                    if(ticket.accepted()) {
                        this.accept++;
                        inOut.addInvoice(invoice);
                        this.getReturnFileList().add(item);
                    } else {
                        AvError err = ticket.getLastError();
                        this.error++;
                    }
                } else {
                    invalid++;
                }
            }
        } else {
            invalid++;
        }
    }
    
    private void addInvoiceRecord(FileCheckListItem item) throws AvDocException, IOException, GeneralSecurityException, ParseException {
        Invoice invoice = new PathInvoiceLoaderFactory().load(item.getPath());
        if(Optional.ofNullable(invoice).isPresent()) {
            InvoiceOutDAO outDao = new InvoiceOutDAO(OrgAppService.INSTANCE.getPathDb());
            if(outDao.isInvoice(invoice.getGeneral().getNumber())) {
                this.skip++;
            } else {   
                this.valid++;
                outDao.addInvoice(invoice);
                this.accept++;
                this.getReturnFileList().add(item);
            }
        } else {
            invalid++;
        }
    }
    
    private String getMessage(int index){
        StringBuilder builder = new StringBuilder();
        builder.append("Передано ").append(index).append(" из ").append(getCount());
        builder.append(System.lineSeparator());
        builder.append("Корректных - ").append(this.valid).append(" файлов, из них: ");
        builder.append("отправлено - ").append(this.accept).append(" файлов, ");
        builder.append("имеется ошибка - ").append(this.error).append(" файлов;");
        builder.append(System.lineSeparator());
        builder.append("Пропущено - ").append(this.skip).append(" файлов;");
        builder.append(System.lineSeparator());
        builder.append("Некорректных - ").append(this.invalid).append(" файлов;");
        return builder.toString();
    }
    
}
