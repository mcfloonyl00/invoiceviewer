/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.dialog;

import java.util.ArrayList;
import java.util.Optional;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

/**
 *
 * @author avolkov
 */
public class UpdateProgressDialog<T> extends AbstractProgressDialog {

    public UpdateProgressDialog(String title, ObservableList<T> selectionList, AbstractTask<T> task) {
        super(title, selectionList, task);
    }

    @Override
    public ArrayList<T> show() {
        new Thread(getTask()).start();
        Optional<ButtonType> updateResult = getAlert().showAndWait();
        if(updateResult.isPresent() && updateResult.get() == getDoneButton()) {
            Optional<ButtonType> okResult = createAndShowAlert("Обновление статусов завершено", Alert.AlertType.INFORMATION);
            if(okResult.isPresent() && okResult.get() == getDoneButton()) {
                return getFileList();
            }
        }
        if(updateResult.isPresent() && updateResult.get() == getCancelButton()) {
            getTask().cancel();
            Optional<ButtonType> okResult = createAndShowAlert("Обновление статусов прервано", Alert.AlertType.WARNING);
            if(okResult.isPresent() && okResult.get() == getCancelButton()) {
                return getFileList();
            }
        }
        
        return null;
    }

    @Override
    protected String getMessage() {
        StringBuilder builder = new StringBuilder();
        builder.append("Обновлено статусов - ").append(((AbstractUpdateTask<T>)getTask()).getAccept()).append(" из ").append(getTask().getCount());
        builder.append(System.lineSeparator());
        builder.append("Ошибки при обновлении статусов - ").append(((AbstractUpdateTask)getTask()).getError()).append(" файлов;");
        builder.append(System.lineSeparator());
        builder.append("Пропущено статусов - ").append(((AbstractUpdateTask)getTask()).getSkip()).append(" файлов");
        return builder.toString();
    }
    
}
