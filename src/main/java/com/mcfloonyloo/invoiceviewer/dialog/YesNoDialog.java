/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.dialog;

import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.stage.Modality;

/**
 *
 * @author avolkov
 */
public class YesNoDialog {
    
    private final ButtonType yes;
    
    private final ButtonType no;
    
    private final Alert alert;
    
    public ButtonType getYes() {
        return this.yes;
    }
    
    public ButtonType getNo() {
        return this.no;
    }
    
    public YesNoDialog setTitle(String title) {
        this.alert.setTitle(title);
        return this;
    }
    
    public YesNoDialog setHeaderText(String headerText) {
        this.alert.setHeaderText(headerText);
        return this;
    }
    
    public Optional<ButtonType> getResult() {
        return this.alert.showAndWait();    
    }
    
    public YesNoDialog(AlertType alertType, String contentText) {
        this.yes = new ButtonType("Да", ButtonBar.ButtonData.YES);
        this.no = new ButtonType("Нет", ButtonBar.ButtonData.NO);
        this.alert = new Alert(alertType, contentText, this.yes, this.no);
        this.alert.initModality(Modality.APPLICATION_MODAL);
    }
    
}
