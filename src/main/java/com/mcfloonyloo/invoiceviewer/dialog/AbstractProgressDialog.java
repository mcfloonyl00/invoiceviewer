/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.dialog;

import java.util.ArrayList;
import java.util.Optional;
import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;

/**
 *
 * @author avolkov
 */
public abstract class AbstractProgressDialog<T> {
    
    private final Alert alert;
    
    private final AbstractTask abstractTask;
    
    private final ButtonType doneButton;
    
    private final ButtonType cancelButton;
    
    private final ArrayList<T> fileList;
    
    public Alert getAlert() {
        return this.alert;
    }
    
    public AbstractTask getTask() {
        return this.abstractTask;
    }
    
    public ButtonType getDoneButton() {
        return this.doneButton;
    }
    
    public ButtonType getCancelButton() {
        return this.cancelButton;
    }
    
    public ArrayList<T> getFileList() {
        return this.fileList;
    }
    
    public AbstractProgressDialog(String title, ObservableList<T> selectionList, AbstractTask<T> abstractTask) {
        this.abstractTask = abstractTask;
        
        this.fileList = new ArrayList<>();
        
        this.abstractTask.setOnSucceeded(getSucceededEventHandler());
        
        this.abstractTask.setOnCancelled(getCancelledEventHandler());
        
        this.doneButton = new ButtonType("ОК", ButtonBar.ButtonData.OK_DONE);
        this.cancelButton = new ButtonType("Отмена", ButtonBar.ButtonData.CANCEL_CLOSE);
        
        this.alert = createPanel(title);   
    }
        
     private EventHandler<WorkerStateEvent> getSucceededEventHandler() {
        return (event) -> {
            this.fileList.addAll(abstractTask.getReturnFileList());
        };
    }
     
     private EventHandler<WorkerStateEvent> getCancelledEventHandler() {
         return (WorkerStateEvent event) -> {
            this.fileList.addAll(abstractTask.getReturnFileList());
        };
     }
    
    private Alert createPanel(String title) {
        Alert innerAlert = new Alert(Alert.AlertType.INFORMATION, title, this.doneButton, this.cancelButton);
        
        innerAlert.setTitle("АРМ оператора ЭСЧФ");
        innerAlert.getDialogPane().lookupButton(this.doneButton).disableProperty().bind(this.abstractTask.runningProperty());
        innerAlert.getDialogPane().lookupButton(this.cancelButton).disableProperty().bind(Bindings.not(this.abstractTask.runningProperty()));
        innerAlert.initModality(Modality.APPLICATION_MODAL);
        innerAlert.getDialogPane().setContent(createPanel());
        
        return innerAlert;
    }
    
    private VBox createPanel() {
        VBox box = new VBox();
        box.setPrefSize(550, 180);
        
        box.getChildren().add(createProgress());
        box.getChildren().add(createMessage());
        
        return box;
    }
    
    private ProgressBar createProgress() {
        ProgressBar progress = new ProgressBar(0);
        progress.setPrefSize(530, 50);
        GridPane.setMargin(progress, new Insets(10));
        
        progress.progressProperty().unbind();
        progress.progressProperty().bind(this.abstractTask.progressProperty());
        
        return progress;
    }
    
    private Label createMessage() {
        Label message = new Label("");
        GridPane.setMargin(message, new Insets(10));
        
        message.textProperty().bind(this.abstractTask.messageProperty());
        
        return message;
    }
    
    public abstract ArrayList<T> show();
    
    protected abstract String getMessage();
    
    protected Optional<ButtonType> createAndShowAlert(String headerText, AlertType alertType) {
        Alert okAlert = new Alert(alertType);
        okAlert.setTitle("АРМ оператора ЭСЧФ");
        okAlert.setHeaderText(headerText);
        okAlert.setContentText(getMessage());
        return okAlert.showAndWait();
    };
}
