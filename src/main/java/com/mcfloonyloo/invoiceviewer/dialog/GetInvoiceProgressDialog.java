/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.dialog;

import java.util.ArrayList;
import java.util.Optional;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

/**
 *
 * @author avolkov
 */
public class GetInvoiceProgressDialog extends AbstractProgressDialog<String> { 

    public GetInvoiceProgressDialog(ObservableList<String> selectionList) {
        super("Выполняется загрузка ЭСЧФ...", selectionList, new GetInvoiceTask(selectionList));
    }

    @Override
    public ArrayList<String> show() {
        new Thread(getTask()).start();
        Optional<ButtonType> getResult = getAlert().showAndWait();
        if(getResult.isPresent() && getResult.get() == getDoneButton()) {
            Optional<ButtonType> okResult = createAndShowAlert("Загрузка ЭСФЧ завершена", Alert.AlertType.INFORMATION);
            if(okResult.isPresent() && okResult.get() == ButtonType.OK) {
                return getFileList();
            }
        }
        if(getResult.isPresent() && getResult.get() == getCancelButton()) {
            getTask().cancel();
            Optional<ButtonType> okResult = createAndShowAlert("Загрузка ЭСЧФ прервана", Alert.AlertType.WARNING);
            if(okResult.isPresent() && okResult.get() == ButtonType.OK) {
                return getFileList();
            }
        }
        return null;
    }

    @Override
    protected String getMessage() {
        StringBuilder builder = new StringBuilder();
        builder.append("Загружено ЭСЧФ - ").append(((GetInvoiceTask)getTask()).getAccept());
        builder.append(System.lineSeparator());
        builder.append("Получены ошибки при загрузке ЭСЧФ - ").append(((GetInvoiceTask)getTask()).getError());
        builder.append(System.lineSeparator());
        builder.append("Пропущено ЭСЧФ - ").append(((GetInvoiceTask)getTask()).getSkip());
        return builder.toString();
    }
    
}
