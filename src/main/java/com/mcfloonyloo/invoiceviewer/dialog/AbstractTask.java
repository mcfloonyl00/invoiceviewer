/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.dialog;

import java.util.ArrayList;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;

/**
 *
 * @author avolkov
 */
public abstract class AbstractTask<T> extends Task<ArrayList<T>> {
    
    private ObservableList<T> fileList;
    
    private int count;
    
    private final ArrayList<T> returnFileList;
    
    public ObservableList<T> getFileList() {
        return this.fileList;
    }
    
    public int getCount() {
        return this.count;
    }
    
    public ArrayList<T> getReturnFileList() {
        return this.returnFileList;
    }
    
    public void setFileList(ObservableList<T> fileList) {
        this.fileList = fileList;
        this.count = this.fileList.size();
    }
    
    public AbstractTask(ObservableList<T> fileList) {
        super();
        this.fileList = FXCollections.observableArrayList();
        if(Optional.ofNullable(fileList).isPresent()) {
            this.fileList.addAll(fileList);
        }
        this.count = this.fileList.size();
        this.returnFileList = new ArrayList<>();
    }
    
}
