/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer;

import com.mcfloonyloo.invoiceviewer.dialog.ExceptionDialog;
import com.mcfloonyloo.invoiceviewer.dialog.YesNoDialog;
import com.mcfloonyloo.invoiceviewer.model.preloader.db.OrgPreloaderDTO;
import com.mcfloonyloo.invoiceviewer.view.MainLayoutController;
import com.mcfloonyloo.invoiceviewer.view.RootLayoutController;
import com.sun.javafx.application.LauncherImpl;
import java.io.IOException;
import java.util.Optional;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author avolkov
 */
public class MainApp extends Application implements MainPreloader.CredentialsConsumer {

    private Stage primaryStage = null;
    
    private OrgPreloaderDTO org;
        
    private ExceptionDialog dialog;
    
    public Stage getPrimaryStage() {
        return this.primaryStage;
    }
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        loginShow();
    }

    @Override
    public void stop() throws Exception {
        if(this.dialog != null){
            Optional<ButtonType> result = this.dialog.showAndWait();
            if(result.isPresent() && result.get() == this.dialog.getOk()){
                //LOGGER.info("Аварийное завершение работы приложения");
                //LOGGER.info("---------------------------");
                System.exit(0);
            }
        }else{
            YesNoDialog yesNoDialog = new YesNoDialog(Alert.AlertType.CONFIRMATION, "Завершить работу приложения?")
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText("Запрос на завершение работы");
            Optional<ButtonType> result = yesNoDialog.getResult();
            if(result.isPresent() && result.get() == yesNoDialog.getYes()){
                //LOGGER.info("Завершение работы приложения");
                //LOGGER.info("---------------------------");
                System.exit(0);
            }
        }
    }
    
    private Scene createScene() {
        try {
            BorderPane borderPane = loadRootLayout();
            borderPane.setCenter(loadMainLayout());
            
            Scene scene = new Scene(borderPane);
            scene.getStylesheets().add(MainApp.class.getResource("view/MainLayout.css").toExternalForm());
            return scene;
        } catch (IOException ex) {
            AnchorPane pane = new AnchorPane();
            Scene scene = new Scene(pane, 800, 600);
            this.dialog = new ExceptionDialog("Ошибка загрузки главного окна приложения", ex)
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setOwner(this.primaryStage)
                    .setHeaderText(ex.getClass().toString());
            this.dialog.showAndWait();
            return scene;
        }
    }
    
    private BorderPane loadRootLayout() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
        BorderPane pane = (BorderPane) loader.load();
        RootLayoutController controller = loader.getController();   
        controller.setMainApp(this);
        return pane;
    }
    
    private AnchorPane loadMainLayout() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("view/MainLayout.fxml"));
        AnchorPane pane = (AnchorPane) loader.load();
        MainLayoutController controller = loader.getController();
        controller.setMainApp(this);
        return pane;
    }
    
    public static void main(String[] args) {
        LauncherImpl.launchApplication(MainApp.class, MainPreloader.class, args);
    }
    
    private void loginShow() {
        if(Optional.ofNullable(this.org).isPresent() && Optional.ofNullable(primaryStage).isPresent()) {
            primaryStage.setScene(createScene());
            primaryStage.setOnCloseRequest((WindowEvent event) -> {
                try {
                    event.consume();
                    stop();
                } catch (Exception ex) {
                    this.dialog = new ExceptionDialog(ex.getLocalizedMessage(), ex)
                            .setTitle("АРМ оператора ЭСЧФ")
                            .setOwner(primaryStage)
                            .setHeaderText(ex.getClass().toString());
                    this.dialog.showAndWait();
                }
            });     
            Platform.runLater(() -> {
                primaryStage.show();
            });
        }
    }

    @Override
    public void setCredential(OrgPreloaderDTO org) {
        this.org = org;
        loginShow();
    }
}
