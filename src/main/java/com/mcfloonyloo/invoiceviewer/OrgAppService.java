/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer;

import com.mcfloonyloo.invoiceviewer.model.preloader.db.OrgPreloaderDTO;
import java.util.Optional;

/**
 *
 * @author avolkov
 */
public enum OrgAppService {
    INSTANCE;
    
    private String unp;
    
    private String name;
    
    private String pathDb;
    
    private boolean present = false;
    
    private OrgAppService() {
        
    }
    
    public void setOrg(OrgPreloaderDTO org) {
        if(Optional.ofNullable(org).isPresent()) {
            this.unp = org.getUnp();
            this.name = org.getName();
            this.pathDb = org.getPathdb();
            this.present = true;
        }
    }
    
    public String getUnp() {
        return this.unp;
    }
    
    public String getName() {
        return this.name;    
    }
    
    public String getPathDb() {
        return this.pathDb;
    }
    
    public boolean isPresent() {
        return this.present;
    }
}
