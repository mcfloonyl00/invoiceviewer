    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer;

import com.mcfloonyloo.invoiceviewer.dialog.ExceptionDialog;
import com.mcfloonyloo.invoiceviewer.dialog.YesNoDialog;
import com.mcfloonyloo.invoiceviewer.model.preloader.db.OrgPreloaderDTO;
import com.mcfloonyloo.invoiceviewer.properties.AppProperties;
import com.mcfloonyloo.invoiceviewer.view.PreloaderLayoutController;
import java.io.IOException;
import java.util.Optional;
import javafx.application.Platform;
import javafx.application.Preloader;
import javafx.beans.Observable;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author avolkov
 */
public class MainPreloader extends Preloader {
    
    public static interface CredentialsConsumer {
        public void setCredential (OrgPreloaderDTO org);
    }
    
    private ExceptionDialog dialog;
    
    private Stage stage = null;
    
    private CredentialsConsumer consumer = null;
    
    private final ObservableValue<OrgPreloaderDTO> org = new SimpleObjectProperty<>();
    
    private Scene createScene() {        
        GridPane layout;
        try {
            layout = loadLayout();
            return new Scene(layout, 620, 335);
        } catch (IOException ex) {
            this.dialog = new ExceptionDialog(ex.getLocalizedMessage(), ex)
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText(ex.getClass().toString());
            layout = new GridPane();
            this.dialog.showAndWait();
            return new Scene(layout, 620, 335);
        } finally {
            this.org.addListener((Observable observable) -> {
                loginHide();
            });
        }
    }
       
    private GridPane loadLayout() throws IOException  {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("view/PreloaderLayout.fxml"));
        GridPane layout = (GridPane) loader.load();
        
        PreloaderLayoutController controller = loader.getController();
        controller.setOrgProperty((SimpleObjectProperty) org);
        
        return layout;
    }
    
    
    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;
        stage.setScene(createScene());
        stage.resizableProperty().setValue(Boolean.FALSE);
        stage.setTitle("АРМ оператора ЭСЧФ v1.0");
        AppProperties properties = AppProperties.INSTANCE;
        properties.setStage(stage);
        
        String libraryPath = System.getProperty("java.library.path");
        System.setProperty("by.avest.loader.shared", "true");
        System.setProperty("java.library.path", properties.getLibraryPath().trim()+";"+libraryPath);
        System.setProperty("classpath", properties.getClassPath().trim());
        
        stage.setOnCloseRequest((WindowEvent event) -> {
            try {
                event.consume();
                stop();
            } catch (Exception ex) {
                this.dialog = new ExceptionDialog(ex.getLocalizedMessage(), ex)
                        .setTitle("АРМ оператора ЭСЧФ")
                        .setOwner(stage)
                        .setHeaderText(ex.getClass().toString());
                dialog.showAndWait();
            }
        });      
        stage.show();
    }

    @Override
    public void stop() throws Exception {
        if(this.dialog != null){
            Optional<ButtonType> result = this.dialog.showAndWait();
            if(result.isPresent() && result.get() == this.dialog.getOk()){
                //LOGGER.info("Аварийное завершение работы приложения");
                //LOGGER.info("---------------------------");
                System.exit(0);
            }
        }else{
            YesNoDialog yesNoDialog = new YesNoDialog(Alert.AlertType.CONFIRMATION, "Завершить работу приложения?")
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText("Запрос на завершение работы");
            Optional<ButtonType> result = yesNoDialog.getResult();
            if(result.isPresent() && result.get() == yesNoDialog.getYes()){
                //LOGGER.info("Завершение работы приложения");
                //LOGGER.info("---------------------------");
                System.exit(0);
            }
        }
    }
    
    private void loginHide() {
        if(stage.isShowing() && Optional.ofNullable(this.org.getValue()).isPresent() && Optional.ofNullable(consumer).isPresent()) {
            consumer.setCredential(this.org.getValue());
            Platform.runLater(() -> {
                stage.hide();
            });
        }
    }

    @Override
    public void handleStateChangeNotification(StateChangeNotification state) {
        if(state.getType() == StateChangeNotification.Type.BEFORE_START) {
            this.consumer = (CredentialsConsumer) state.getApplication();
            loginHide();
        }
    }
}
