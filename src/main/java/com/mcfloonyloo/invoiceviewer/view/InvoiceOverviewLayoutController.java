/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.view;

import com.mcfloonyloo.invoiceviewer.MainApp;
import com.mcfloonyloo.invoiceviewer.dialog.ExceptionDialog;
import com.mcfloonyloo.invoiceviewer.model.Title;
import com.mcfloonyloo.invoiceviewer.model.invoice.DefaultInvoiceSection;
import com.mcfloonyloo.invoiceviewer.model.invoice.Invoice;
import com.mcfloonyloo.invoiceviewer.model.invoice.titled.InvoiceTitled;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Accordion;
import javafx.scene.control.Alert;
import javafx.scene.control.TitledPane;

/**
 * FXML Controller class
 *
 * @author avolkov
 */
public class InvoiceOverviewLayoutController implements Initializable {

    private static final int PRIVATE_FINAL = Modifier.PRIVATE+Modifier.FINAL;
    
    @FXML
    private Accordion accordion;
    
    private Invoice invoice;
    
    public ObservableList<TitledPane> getListPanes() {
        return this.accordion.getPanes();
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    
    
    public void init(Invoice invoice) {
        this.invoice = invoice;
        try {
            fill();
        } catch (IllegalArgumentException | IllegalAccessException ex) {
            String message = ex.getLocalizedMessage();
            ExceptionDialog dialog = new ExceptionDialog(message, ex)
                    .setTitle("АРМ оператора ЭСЧФЭСЧФ")
                    .setHeaderText(ex.getClass().getCanonicalName());
            dialog.showAndWait();
        }
    }
    
    private void fill() throws IllegalArgumentException, IllegalAccessException {
        if(Optional.ofNullable(this.invoice).isPresent()) {
            InvoiceTitled invoiceTitle = new InvoiceTitled(this.invoice);
            if(Optional.ofNullable(invoiceTitle).isPresent()) {
                for(Field field : invoiceTitle.getClass().getDeclaredFields()) {
                    if(field.getType() == Title.class && field.getModifiers() == PRIVATE_FINAL) {
                        field.setAccessible(true);
                        Title internal = (Title) field.get(invoiceTitle);
                        if(internal.getValue() instanceof DefaultInvoiceSection) {
                            this.accordion.getPanes().add(createPaneFxml(internal));
                        }
                    }
                }
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("АРМ оператора ЭСЧФЭСЧФ");
            alert.setHeaderText("Внимание!");
            alert.setContentText("Отсутствует ЭСЧФ для формирования списка секций");
            alert.show();
        }
    }
    
    private TitledPane createPaneFxml(Title section) {
        try {
            return createPane(section);
        } catch (IOException ex) {
            String message = ex.getLocalizedMessage();
            ExceptionDialog dialog = new ExceptionDialog(message, ex)
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText(ex.getClass().toString());
            dialog.showAndWait();
            return null;
        }
    }
    
    private TitledPane createPane(Title section) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("view/SectionInvoiceOverviewLayout.fxml"));
        TitledPane pane = (TitledPane) loader.load();
        
        SectionInvoiceOverviewLayoutController controller = loader.getController();
        controller.setSection(section);
        
        return controller.createPane();
    }
    
}
