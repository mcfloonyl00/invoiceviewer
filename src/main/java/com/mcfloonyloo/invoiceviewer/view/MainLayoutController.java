/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.view;

import com.mcfloonyloo.invoiceviewer.MainApp;
import com.mcfloonyloo.invoiceviewer.OrgAppService;
import com.mcfloonyloo.invoiceviewer.dialog.ExceptionDialog;
import com.mcfloonyloo.invoiceviewer.dialog.FileCheckUpdateTask;
import com.mcfloonyloo.invoiceviewer.dialog.SendProgressDialog;
import com.mcfloonyloo.invoiceviewer.dialog.UpdateProgressDialog;
import com.mcfloonyloo.invoiceviewer.model.DirTreeItem;
import com.mcfloonyloo.invoiceviewer.model.FileCheckListItem;
import com.mcfloonyloo.invoiceviewer.model.StatusFileCheckListItem;
import com.mcfloonyloo.invoiceviewer.model.invoice.Invoice;
import com.mcfloonyloo.invoiceviewer.model.invoice.PathInvoiceLoaderFactory;
import com.mcfloonyloo.invoiceviewer.model.invoice.db.InvoiceOutDAO;
import com.mcfloonyloo.invoiceviewer.model.invoice.db.InvoiceOutStatusDAO;
import com.mcfloonyloo.invoiceviewer.model.invoice.test.BaseTest;
import com.mcfloonyloo.invoiceviewer.model.invoice.test.ErrorList;
import com.mcfloonyloo.invoiceviewer.model.invoice.test.InvoiceNameTestItem;
import com.mcfloonyloo.invoiceviewer.model.invoice.test.PositiveValuesTest;
import com.mcfloonyloo.invoiceviewer.model.invoice.test.XmlStructureTest;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.property.BooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import javax.swing.filechooser.FileSystemView;
import org.controlsfx.control.CheckListView;

public class MainLayoutController implements Initializable{

    @FXML
    private CheckListView<FileCheckListItem> filesCheckListView;

    @FXML
    private WebView errorWebView;
 
    @FXML
    private TreeView<DirTreeItem> directoryTreeView;
    
    @FXML
    private Button sendButton;
    
    @FXML
    private Label sizeLabel;
    
    private MainApp mainApp;
    
    private MenuItem removeItem;
   
    public void setMainApp(MainApp mainApp){
        this.mainApp = mainApp;
        this.mainApp.getPrimaryStage().setTitle(OrgAppService.INSTANCE.getUnp()+" "+OrgAppService.INSTANCE.getName());
    }
     
    public MainLayoutController(){}
    
    @Override
    @SuppressWarnings("Convert2Lambda")
    @FXML
    public void initialize(URL url, ResourceBundle rb){   
        
        this.errorWebView.setFontScale(0.85);
        
        this.directoryTreeView.setShowRoot(true);
        
        this.sendButton.setDisable(true);
                
        this.directoryTreeView.setCellFactory((TreeView<DirTreeItem> treeView) -> new TreeCell<DirTreeItem>() {
            @Override
            protected void updateItem(DirTreeItem item, boolean empty) {        
                super.updateItem(item, empty);
                if(empty){
                    this.setText(null);
                    this.setGraphic(null);
                }else{
                    if(item.isNullable()){
                        this.setText("Компьютер");
                    } else {
                        if(FileSystemView.getFileSystemView().isDrive(item.getFile())){
                            this.setText(item.getFile().getPath() + " ("+ FileSystemView.getFileSystemView().getSystemTypeDescription(item.getFile()) + ")");
                        }else{
                            this.setText(item.getName());
                        }
                    }
                }
            }   
        });
        
        this.directoryTreeView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            this.filesCheckListView.getItems().clear();
            this.filesCheckListView.setItems(getFilesOfDirectory(newValue.getValue()));
        });
        
        this.filesCheckListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(Optional.ofNullable(newValue).isPresent() && Optional.ofNullable(newValue.getMessageError()).isPresent()) {
                this.errorWebView.getEngine().loadContent(newValue.getMessageError());
            }
        });
        
        this.filesCheckListView.setSkin(new CheckListViewSkin(this.filesCheckListView));
        addRoot();
    }

    @FXML
    private void handleSend(){
        SendProgressDialog sendDialog = new SendProgressDialog(this.filesCheckListView.getCheckModel().getCheckedItems());
        ObservableList<FileCheckListItem> sendList = FXCollections.observableList(sendDialog.show());
        
        sendList.forEach((item) -> {
            this.filesCheckListView.getCheckModel().clearCheck(item);
            this.filesCheckListView.getItems().get(this.filesCheckListView.getItems().indexOf(item)).setStatus(StatusFileCheckListItem.IN_DATABASE);
        });
        ((CheckListViewSkin) this.filesCheckListView.getSkin()).refresh();
        InvoiceOutStatusDAO statusDao = new InvoiceOutStatusDAO(OrgAppService.INSTANCE.getPathDb());
        UpdateProgressDialog updateDialog = new UpdateProgressDialog("Выполняется обновление статусов...", sendList, new FileCheckUpdateTask(sendList, statusDao));
        updateDialog.show();
    }
    
    private void addRoot() {
        TreeItem<DirTreeItem> root = new TreeItem<>(new DirTreeItem(null));
        
        root.setExpanded(false);
        root.getChildren().add(new TreeItem<>());
        
        root.expandedProperty().addListener(getExpandedListener(null));
        this.directoryTreeView.setRoot(root);
    }
    
    private void findDir(DirTreeItem dir, TreeItem<DirTreeItem> parent){
        parent.getChildren().clear();
        File[] roots;
        if(dir == null){
            roots = File.listRoots();
        }else{
            roots = dir.getFile().listFiles();
        }
        if(roots != null){
            for(File file: roots){
                if(file.isDirectory() || FileSystemView.getFileSystemView().isDrive(file)){
                    TreeItem<DirTreeItem> item = new TreeItem<>(new DirTreeItem(file.toPath()));
                    item.getChildren().add(new TreeItem<>());
                    item.expandedProperty().addListener(getExpandedListener(item));
                    parent.getChildren().add(item);
                }
            }
        }else{
            parent.getChildren().clear();
        }
    }

    public ObservableList<FileCheckListItem> getFilesOfDirectory(DirTreeItem item){
        this.filesCheckListView.setCellFactory(getCellFactory());
        
        ObservableList<FileCheckListItem> list = FXCollections.observableArrayList();
        list.clear();
        if(item.isNullable()){
            return list;
        }else{
            if(item.getFile().listFiles() == null){
                return list;
            }else{
                for(File file: item.getFile().listFiles()){
                    if(!file.isDirectory()){
                        ErrorList nameErrorList = new InvoiceNameTestItem(file.getName()).test();
                        if(nameErrorList.isEmpty()){
                            Invoice invoice = new PathInvoiceLoaderFactory().load(file.toPath());
                            InvoiceOutDAO outDao = new InvoiceOutDAO(OrgAppService.INSTANCE.getPathDb());
                            if(outDao.isInvoice(invoice.getGeneral().getNumber())) {
                                list.add(new FileCheckListItem(file.toPath(), StatusFileCheckListItem.IN_DATABASE, ""));
                            } else {
                                if(Optional.ofNullable(invoice).isPresent()) {
                                    XmlStructureTest xmlTest = new XmlStructureTest(invoice);
                                    if(xmlTest.getErrors().isEmpty()){
                                        BaseTest baseTest = new BaseTest(invoice);
                                        if(baseTest.getErrors().isEmpty()){
                                            PositiveValuesTest positiveTest = new PositiveValuesTest(invoice);
                                            if(positiveTest.getErrors().isEmpty()){
                                                list.add(new FileCheckListItem(file.toPath(), StatusFileCheckListItem.FOR_SENDING, "Ошибок не обнаружено"));
                                            }else{
                                                list.add(new FileCheckListItem(file.toPath(), StatusFileCheckListItem.ERROR_POSITIVE_TEST, positiveTest.getMessage()));
                                            }
                                        }else{
                                            list.add(new FileCheckListItem(file.toPath(), StatusFileCheckListItem.ERROR_BASE_TEST, baseTest.getMessage()));
                                        }
                                    }else{
                                        list.add(new FileCheckListItem(file.toPath(), StatusFileCheckListItem.ERROR_XML_STRUCTURE, xmlTest.getMessage()));
                                    }
                                } else {
                                    list.add(new FileCheckListItem(file.toPath(), StatusFileCheckListItem.EMPTY, "<html><b>[СHARSET TEST] Ошибка определения кодировки загружаемого файла</b></html>"));
                                }
                            }
                        }else{
                            list.add(new FileCheckListItem(file.toPath(), StatusFileCheckListItem.EMPTY, ""));
                        }
                    }    
                }
                return list;
            }
        }
    }
    
    private Callback<ListView<FileCheckListItem>, ListCell<FileCheckListItem>> getCellFactory(){
        return (ListView<FileCheckListItem> listView) -> {
            CheckBoxListCell<FileCheckListItem> cell = new CheckBoxListCell<FileCheckListItem>(){
                @Override
                public void updateItem(FileCheckListItem item, boolean empty) {
                    super.updateItem(item, empty); //To change body of generated methods, choose Tools | Templates.
                    if(empty){
                        setGraphic(null);
                    }else{
                        for(StatusFileCheckListItem value : StatusFileCheckListItem.values()){
                            getStyleClass().removeAll(value.get());
                            if(value.get().contains(item.getStatus().get())){
                                getStyleClass().add(item.getStatus().get());
                            }
                        }
                        setDisabled(item.getStatus().isDisabled());
                        setText(item.getFileName());
                    }
                }
            };
                        
            this.filesCheckListView.getCheckModel().getCheckedItems().addListener((ListChangeListener.Change<? extends FileCheckListItem> c) -> {
                while(c.next()){
                    switch(c.getList().size()){
                        case 0:{
                            if(Optional.ofNullable(removeItem).isPresent()){
                                removeItem.setDisable(true);
                            }
                            this.sendButton.setDisable(true);
                            this.sizeLabel.setText("");
                            break;
                        }
                        default:{
                            if(Optional.ofNullable(removeItem).isPresent()){
                                removeItem.setDisable(false);
                            }
                            this.sendButton.setDisable(false);
                            this.sizeLabel.setText("Выделено "+c.getList().size()+" файлов");
                            break;
                        }
                    }
                }
            });
            
            cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                if(isNowEmpty){
                    cell.setContextMenu(null);
                }else{
                    String path = this.directoryTreeView.getSelectionModel().getSelectedItem().getValue().getFile().getPath();
                    File file = new File(new StringBuilder(path).append("\\").append(cell.getItem().getFileName()).toString());
                    if(new InvoiceNameTestItem(file.getName()).test().isEmpty()){
                        cell.setContextMenu(getMenu(file));
                    }else{
                        cell.setContextMenu(null);
                    }
                }
            });
            
            cell.setSelectedStateCallback(item -> this.filesCheckListView.getItemBooleanProperty(item));
            return cell;
        };
    }

    private ContextMenu getMenu(File file){
        ContextMenu context = new ContextMenu();
        
        MenuItem openItem = new MenuItem("Открыть...");
        openItem.setOnAction((event) -> {
            try{
                
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(MainLayoutController.class.getResource("InvoiceOverviewLayout.fxml"));
                AnchorPane page = (AnchorPane) loader.load();
                
                Stage stage = new Stage();
                stage.initOwner(this.mainApp.getPrimaryStage());
                stage.initModality(Modality.WINDOW_MODAL);
                stage.initStyle(StageStyle.UTILITY);
                
                InvoiceOverviewLayoutController controller = loader.getController();
                Invoice invoice = new PathInvoiceLoaderFactory().load(file.toPath());
                controller.init(invoice);
                
                Scene scene = new Scene(page);
                stage.setScene(scene);
                
                stage.show();
            } catch(IOException ex){
                String message = ex.getLocalizedMessage();
                ExceptionDialog dialog = new ExceptionDialog(message, ex)
                        .setTitle("АРМ оператора ЭСЧФ")
                        .setHeaderText(ex.getClass().toString());
                dialog.showAndWait();
            }
        });
        context.getItems().add(openItem);
        context.getItems().add(new SeparatorMenuItem());
        
        Menu selectMenu = new Menu("Отметить...");
        context.getItems().add(selectMenu);
        
        MenuItem selectInvoiceAllItem = new MenuItem("Отметить все ЭСЧФ");
        selectInvoiceAllItem.setOnAction((event) -> {
            for(FileCheckListItem item : this.filesCheckListView.getItems()){
                switch(item.getStatus()){
                    case EMPTY : {break;}
                    default : {
                        this.filesCheckListView.getCheckModel().check(item);
                    }
                }
            }
        });
        selectMenu.getItems().add(selectInvoiceAllItem);
        
        MenuItem selectInvoiceSendedItem = new MenuItem("Отметить ЭСЧф для отправления");
        selectInvoiceSendedItem.setOnAction((event) -> {
            for(FileCheckListItem item : this.filesCheckListView.getItems()){
                switch(item.getStatus()){
                    case FOR_SENDING : {
                        this.filesCheckListView.getCheckModel().check(item);
                        break;
                    }
                    default : {
                        break;
                    }
                }
            }
        });
        selectMenu.getItems().add(selectInvoiceSendedItem);
        
        removeItem = new MenuItem("Снять отметки");
        removeItem.setOnAction((event) -> {
            this.filesCheckListView.getCheckModel().clearChecks();
        });
        context.getItems().add(removeItem);
        
        return context;
    }
  
    private ChangeListener<Boolean> getExpandedListener(TreeItem<DirTreeItem> item){
        return (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            BooleanProperty property = (BooleanProperty) observable;
            TreeItem treeItem = (TreeItem) property.getBean();
            if(newValue){
                if(Optional.ofNullable(item).isPresent()){
                    findDir(item.getValue(), treeItem);
                }else{
                    findDir(null, treeItem);
                }
            }else{
                treeItem.getChildren().clear();
                treeItem.getChildren().add(new TreeItem<>());
            }
        };
    }
}
