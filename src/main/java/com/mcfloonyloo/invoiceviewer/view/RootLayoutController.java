/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.view;

import com.mcfloonyloo.invoiceviewer.MainApp;
import com.mcfloonyloo.invoiceviewer.OrgAppService;
import com.mcfloonyloo.invoiceviewer.dialog.DbUpdateTask;
import com.mcfloonyloo.invoiceviewer.dialog.ExceptionDialog;
import com.mcfloonyloo.invoiceviewer.dialog.GetInvoiceProgressDialog;
import com.mcfloonyloo.invoiceviewer.model.invoice.db.InvoiceInDAO;
import com.mcfloonyloo.invoiceviewer.model.invoice.db.InvoiceInStatusDAO;
import com.mcfloonyloo.invoiceviewer.model.invoice.db.InvoiceOutDAO;
import com.mcfloonyloo.invoiceviewer.model.invoice.db.InvoiceOutStatusDAO;
import com.mcfloonyloo.invoiceviewer.model.report.db.ReportInvoiceInDAO;
import com.mcfloonyloo.invoiceviewer.model.report.db.ReportInvoiceOutDAO;
import com.mcfloonyloo.invoiceviewer.properties.AppProperties;
import com.mcfloonyloo.invoiceviewer.service.InvoiceService;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FilenameUtils;

/**
 * Класс контроллера корневого макета
 *
 * @author avolkov
 */
public class RootLayoutController implements Initializable {

    @FXML
    private MenuItem settingsMenuItem;

    @FXML
    private MenuItem exitMenuItem;

    @FXML
    private MenuItem aboutMenuItem;

    private MainApp mainApp;
    
    private MainLayoutController controller;

    public void setMainApp(MainApp mainApp){
        this.mainApp = mainApp;
    }
    
    public RootLayoutController(){}

    @FXML
    @Override
    public void initialize(URL url, ResourceBundle rb) {
 
    }    

    @FXML
    public void handleExit(){
        try {
            if(InvoiceService.INSTANCE.isAuth()){
                InvoiceService.INSTANCE.close();
            }
            this.mainApp.stop();
        } catch (Exception ex) {
            String message = ex.getLocalizedMessage();
            ExceptionDialog dialog = new ExceptionDialog(message, ex)
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText(ex.getClass().toString());
            dialog.showAndWait();
        }
    }

    @FXML
    public void handleAbout(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("АРМ оператора ЭСЧФ");
        alert.setHeaderText("About");
        alert.setContentText("Автор: А.Волков\nОАО \"Гомельагрокомплект\"\"");
        alert.showAndWait();
    }
    
    @FXML
    public void handleUpdateInInvoices() {
        try {
            GridPane layout = createUpdateInInvoicesLayout();    
            Stage stage = new Stage();
            stage.setScene(new Scene(layout, 670, 390));
            stage.setTitle("Обновление входящих ЭСЧФ");
            stage.setResizable(false);
            stage.setFullScreen(false);
            stage.initStyle(StageStyle.UTILITY);
            stage.showAndWait();
        } catch (IOException ex) {
            ExceptionDialog dialog = new ExceptionDialog("Ошибка загрузки окна обновления ЭСЧФ", ex)
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText(ex.getClass().toString());
            dialog.showAndWait();
        }
    }
    
    @FXML
    public void handleUpdateOutInvoices() {
        try {
            GridPane layout = createUpdateOutInvoicesLayout();    
            Stage stage = new Stage();
            stage.setScene(new Scene(layout, 670, 390));
            stage.setTitle("Обновление исходящих ЭСЧФ");
            stage.setResizable(false);
            stage.setFullScreen(false);
            stage.showAndWait();
        } catch (IOException | NullPointerException ex) {
            ExceptionDialog dialog = new ExceptionDialog("Ошибка загрузки окна обновления ЭСЧФ", ex)
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText(ex.getClass().toString());
            dialog.showAndWait();
        }
    }
    
    private GridPane createUpdateInInvoicesLayout() throws NullPointerException, IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("view/UpdateInvoiceLayout.fxml"));
        GridPane layout = (GridPane) loader.load();       
        UpdateInvoiceLayoutController inController = loader.getController();
        InvoiceInStatusDAO statusDao = new InvoiceInStatusDAO(OrgAppService.INSTANCE.getPathDb());
        inController.setStatusDao(statusDao);
        inController.setUpdateTask(new DbUpdateTask(statusDao));
        return layout;
    }
    
    private GridPane createUpdateOutInvoicesLayout() throws NullPointerException, IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("view/UpdateInvoiceLayout.fxml"));
        GridPane layout = (GridPane) loader.load();       
        UpdateInvoiceLayoutController outController = loader.getController();
        InvoiceOutStatusDAO statusDao = new InvoiceOutStatusDAO(OrgAppService.INSTANCE.getPathDb());
        outController.setStatusDao(statusDao);
        outController.setUpdateTask(new DbUpdateTask(statusDao));
        return layout;
    }
    
    @FXML
    public void handleReportInInvoices() {
        try {
            GridPane layout = createReportInInvoicesLayout();    
            Stage stage = new Stage();
            stage.setScene(new Scene(layout, 820, 600));
            stage.setTitle("Формирование отчетов по входящим ЭСЧФ");
            stage.setResizable(false);
            stage.setFullScreen(false);
            stage.initStyle(StageStyle.UTILITY);
            stage.showAndWait();
        } catch (IOException ex) {
            ExceptionDialog dialog = new ExceptionDialog("Ошибка загрузки окна формирования отчетов по входящим ЭСЧФ", ex)
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText(ex.getClass().toString());
            dialog.showAndWait();
        }
    }
    
    @FXML
    public void handleReportOutInvoices() {
        try {
            GridPane layout = createReportOutInvoicesLayout();    
            Stage stage = new Stage();
            stage.setScene(new Scene(layout, 820, 600));
            stage.setTitle("Формирование отчетов по исходящим ЭСЧФ");
            stage.setResizable(false);
            stage.setFullScreen(false);
            stage.initStyle(StageStyle.UTILITY);
            stage.showAndWait();
        } catch (IOException ex) {
            ExceptionDialog dialog = new ExceptionDialog("Ошибка загрузки окна формирования отчетов по исходящим ЭСЧФ", ex)
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText(ex.getClass().toString());
            dialog.showAndWait();
        }
    }
    
    private GridPane createReportInInvoicesLayout() throws NullPointerException, IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("view/ReportInvoiceLayout.fxml"));
        GridPane layout = (GridPane) loader.load();       
        ReportInvoiceLayoutController inController = loader.getController();
        ReportInvoiceInDAO reportDao = new ReportInvoiceInDAO(new InvoiceInDAO(OrgAppService.INSTANCE.getPathDb()));
        inController.setReportDao(reportDao);
        return layout;
    }
    
    private GridPane createReportOutInvoicesLayout() throws NullPointerException, IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("view/ReportInvoiceLayout.fxml"));
        GridPane layout = (GridPane) loader.load();       
        ReportInvoiceLayoutController outController = loader.getController();
        ReportInvoiceOutDAO reportDao = new ReportInvoiceOutDAO(new InvoiceOutDAO(OrgAppService.INSTANCE.getPathDb()));
        outController.setReportDao(reportDao);
        return layout;
    }
    
    @FXML
    public void handleLoadReport() {
        File file = getFileOfFileChooser();
        if(Optional.ofNullable(file).isPresent()) {
            switch(FilenameUtils.getExtension(file.getName()).toUpperCase()) {
                case "ZIP" : {
                    
                    break;
                }
                case "CSV" : {
                    GetInvoiceProgressDialog getInvoiceDialog = new GetInvoiceProgressDialog(FXCollections.observableArrayList(parseNumberInvoice(file.toPath())));
                    getInvoiceDialog.show();
                    break;
                }
                default : {
                    
                }
            } 
        } 
    }
    
    private File getFileOfFileChooser() {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Открыть отчет по ЭСЧФ...");
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Архив (*.zip)", "*.zip"));
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Файл отчета (*.csv)", "*.csv"));
        chooser.setInitialDirectory(new File(AppProperties.INSTANCE.getReportsPath()));
        return chooser.showOpenDialog(null);
    }
    
    private ArrayList<String> parseNumberInvoice(Path path) { 
        ArrayList<String> numbers = new ArrayList<>();
        
        try (InputStreamReader in = new InputStreamReader(new FileInputStream(path.toFile()), "windows-1251")) {
            
            Iterable<CSVRecord> records = CSVFormat.RFC4180
                    .withIgnoreEmptyLines(true)
                    .withDelimiter(';').parse(in);
            
            int index = 0;
            int indexColumn = -1;
            boolean shouldBreak = false;
            for(CSVRecord record : records) {
                switch(index++) {
                    case 0 : { //шапка с типом отчета
                        if(!isTypeInvoiceReport(record)) {
                            shouldBreak = true;
                        }   
                        break;
                    }
                    case 1 : {//шапка отчета, пропускаем
                        break;
                    }
                    case 2 : {//определение индекса столбца с номерами ЭСЧФ
                        indexColumn = getIndexForParsing(record);
                        if(indexColumn == -1) {
                            shouldBreak = true;
                            break;
                        }
                        numbers.add(record.get(indexColumn));
                        break;
                    }
                    default : {
                        if(indexColumn == -1) {
                            shouldBreak = true;
                            break;
                        }
                        numbers.add(record.get(indexColumn));
                        break;
                    }
                }
                
                if(shouldBreak) {
                    numbers.clear();
                    break;
                }
            }
            
        } catch (IOException ex) {
            ExceptionDialog dialog = new ExceptionDialog("Ошибка загрузки окна формирования отчетов по исходящим ЭСЧФ", ex)
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText(ex.getClass().toString());
            numbers.clear();
            dialog.showAndWait();
        }
        
        return numbers;
    }
    
    private boolean isTypeInvoiceReport(CSVRecord record) {
        if(Optional.ofNullable(record).isPresent()) {
            switch(record.get(0).toUpperCase()) {
                case "CAMERAL" : {
                    createAlertWarning("Тип отчета \"Камеральный\".\nЗагрузка отчета прервана").showAndWait();
                    return false;
                }
                case "INVOICE" : {
                    return true;
                }
                default : {
                    createAlertError("Ошибка определения типа отчета.\nЗагрузка отчета прервана").showAndWait();
                    return false;
                }
            }
        } else {
            return false;
        }
    }
    
    private int getIndexForParsing(CSVRecord record) {
        final String REGEX_BEL = "^([0-9]{9})-([0-9]{4})-([0-9]{10})$";
        final String REGEX_OTH = "^([0-9]{10})-([0-9]{4})-([0-9]{10})$";
        Pattern patternBel = Pattern.compile(REGEX_BEL);
        Pattern patternOther = Pattern.compile(REGEX_OTH);
        
        int index = 0;
        for(String value : record) {
            Matcher matcherBel = patternBel.matcher(value);
            Matcher matcherOther = patternOther.matcher(value);
            if(matcherBel.matches() || matcherOther.matches()) {
                return index;
            }
            index++;
        }
        
        return -1;
    }
    
    private Alert createAlertError(String message) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle("АРМ оператора ЭСЧФ");
        dialog.setHeaderText("Ошибка");
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.setContentText(message);
        return dialog;
    }
    
    private Alert createAlertWarning(String message) {
        Alert dialog = new Alert(Alert.AlertType.WARNING);
        dialog.setTitle("АРМ оператора ЭСЧФ");
        dialog.setHeaderText("Внимание");
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.setContentText(message);
        return dialog;
    }
}
