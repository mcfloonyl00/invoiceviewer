/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.view;

import com.sun.javafx.scene.control.skin.ListViewSkin;
import javafx.scene.control.ListView;

/**
 *
 * @author avolkov
 */
public class CheckListViewSkin extends ListViewSkin {
    
    public CheckListViewSkin(ListView listView) {
        super(listView);
    }
    
    public void refresh() {
        super.flow.recreateCells();
    }
    
}
