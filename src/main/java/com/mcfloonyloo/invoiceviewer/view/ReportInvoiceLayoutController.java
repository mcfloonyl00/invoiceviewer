/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.view;

import com.mcfloonyloo.invoiceviewer.dialog.ExceptionDialog;
import com.mcfloonyloo.invoiceviewer.model.invoice.db.InvoiceInDAO;
import com.mcfloonyloo.invoiceviewer.model.invoice.db.InvoiceOutDAO;
import com.mcfloonyloo.invoiceviewer.model.report.ReportInvoiceItem;
import com.mcfloonyloo.invoiceviewer.model.report.db.AbstractReportInvoiceDAO;
import com.mcfloonyloo.invoiceviewer.properties.AppProperties;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

/**
 * FXML Controller class
 *
 * @author avolkov
 */
public class ReportInvoiceLayoutController implements Initializable {

    @FXML
    private DatePicker fromPicker;
    
    @FXML
    private DatePicker toPicker;
    
    @FXML
    private MenuItem loadInvoiceItem;
    
    @FXML
    private MenuItem saveInvoiceItem;
    
    @FXML
    private MenuItem saveAsInvoiceItem;
    
    @FXML
    private TableView<ReportInvoiceItem> tableView;
    
    @FXML
    private TableColumn<ReportInvoiceItem, String> unpColumn;
    
    @FXML
    private TableColumn<ReportInvoiceItem, String> dateTransactionColumn;
    
    @FXML
    private TableColumn<ReportInvoiceItem, String> numberColumn;
    
    @FXML
    private TableColumn<ReportInvoiceItem, String> statusColumn;
    
    @FXML
    private TableColumn<ReportInvoiceItem, String> dateStatusColumn;
    
    @FXML
    private TableColumn<ReportInvoiceItem, String> totalCostColumn;
    
    @FXML
    private TableColumn<ReportInvoiceItem, String> totalVatColumn;
    
    @FXML
    private TableColumn<ReportInvoiceItem, String> totalCostVatColumn;
    
    private final ObservableList<ReportInvoiceItem> invoices = FXCollections.observableArrayList();
    
    public AbstractReportInvoiceDAO reportDao;
    
    public void setReportDao(AbstractReportInvoiceDAO reportDao) {
        this.reportDao = reportDao;
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        this.unpColumn.setCellValueFactory((TableColumn.CellDataFeatures<ReportInvoiceItem, String> param) -> new SimpleStringProperty(param.getValue().getUnp()));
        this.dateTransactionColumn.setCellValueFactory((TableColumn.CellDataFeatures<ReportInvoiceItem, String> param) -> new SimpleStringProperty(param.getValue().getDateTransaction().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))));
        this.numberColumn.setCellValueFactory((TableColumn.CellDataFeatures<ReportInvoiceItem, String> param) -> new SimpleStringProperty(param.getValue().getNumber()));
        this.statusColumn.setCellValueFactory((TableColumn.CellDataFeatures<ReportInvoiceItem, String> param) -> new SimpleStringProperty(param.getValue().getStatus()));
        this.dateStatusColumn.setCellValueFactory((TableColumn.CellDataFeatures<ReportInvoiceItem, String> param) -> new SimpleStringProperty(param.getValue().getDateStatus().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss"))));
        this.totalCostColumn.setCellValueFactory((TableColumn.CellDataFeatures<ReportInvoiceItem, String> param) -> new SimpleStringProperty(String.valueOf(param.getValue().getTotalCost().doubleValue())));
        this.totalVatColumn.setCellValueFactory((TableColumn.CellDataFeatures<ReportInvoiceItem, String> param) -> new SimpleStringProperty(String.valueOf(param.getValue().getTotalVat().doubleValue())));
        this.totalCostVatColumn.setCellValueFactory((TableColumn.CellDataFeatures<ReportInvoiceItem, String> param) -> new SimpleStringProperty(String.valueOf(param.getValue().getTotalCostVat().doubleValue())));
        
        this.tableView.setItems(invoices);
        this.saveInvoiceItem.disableProperty().bind(Bindings.size(invoices).lessThanOrEqualTo(0));        
        this.saveAsInvoiceItem.disableProperty().bind(Bindings.size(invoices).lessThanOrEqualTo(0));        
    }    
    
    @FXML
    public void handleLoadInvoices() {
        if(Optional.ofNullable(this.reportDao).isPresent()) {
            if(!Optional.ofNullable(this.fromPicker.getValue()).isPresent()) {
                Alert alert = createAlertWarning("Не выбрана дата начала диапазона. Загрузка ЭСЧФ отменена");
                alert.showAndWait();
                return;
            }
            if(!Optional.ofNullable(this.toPicker.getValue()).isPresent()) {
                Alert alert = createAlertWarning("Не выбрана дата конца диапазона. Загрузка ЭСЧФ отменена");
                alert.showAndWait();
                return;
            }
            if(this.fromPicker.getValue().isBefore(this.toPicker.getValue()) || this.fromPicker.getValue().isEqual(this.toPicker.getValue())) {
                this.invoices.clear();
                this.invoices.addAll(this.reportDao.getInvoices(this.fromPicker.getValue(), this.toPicker.getValue()));
            } else {
                Alert alert = createAlertWarning("Неверно выбран диапазон дат. Загрузка списка ЭСЧФ отменена");
                alert.showAndWait();
            }
        } else {
            Alert alert = createAlertError("Не произведена инициализация инструмента для работы с базой данных.\nВозможности работы окна формирования отчетов ограничена");
                alert.showAndWait();
        }
    }
    
    @FXML
    public void handleSaveReportByPropertiesPath() {
        try {
            generatedCsvByPropertiesPath();
        } catch (IOException | ClassCastException ex) {
            ExceptionDialog dialog = new ExceptionDialog(ex.getLocalizedMessage(), ex);
            dialog.setTitle("АРМ оператора ЭСЧФ");
            dialog.setHeaderText(ex.getClass().getCanonicalName());
            dialog.showAndWait();
        }
    }
    
    @FXML
    public void handleSaveReportAsFileChooserPath() {
        try {
            generatedCsvByAsFileChooserPath();
        } catch (IOException | ClassCastException ex) {
            ExceptionDialog dialog = new ExceptionDialog(ex.getLocalizedMessage(), ex);
            dialog.setTitle("АРМ оператора ЭСЧФ");
            dialog.setHeaderText(ex.getClass().getCanonicalName());
            dialog.showAndWait();
        }
    }
    
    private void generatedCsvByPropertiesPath() throws IOException, ClassCastException {
        createDirOrSkip();
        Path path = Paths.get(generatePath(AppProperties.INSTANCE.getReportsPath()));
        print(path);
        createAlertInformation("Файл отчета "+path.getFileName()+" успешно сохранен").showAndWait();
    }
    
    private void generatedCsvByAsFileChooserPath() throws IOException, ClassCastException {
        Path path = Paths.get(getPathOfFileChooser());
        print(path);
        createAlertInformation("Файл отчета "+path.getFileName()+" успешно сохранен").showAndWait();
    }
    
    
    private void print(Path path) throws IOException {
        try(BufferedWriter writer = Files.newBufferedWriter(path)) {
            CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.EXCEL.withDelimiter(';'));

            for(ReportInvoiceItem item : this.invoices) {
                Object[] record = {
                    item.getUnp(), 
                    item.getDateTransaction().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")), 
                    item.getNumber(), 
                    item.getStatus(), 
                    item.getDateStatus().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss")), 
                    item.getTotalCost(), 
                    item.getTotalVat(), 
                    item.getTotalCostVat()
                };
                csvPrinter.printRecord(record);
            }

            csvPrinter.flush();
        }
    }
    
    private String generatePath(String path) throws ClassCastException {
        StringBuffer stringBuffer = new StringBuffer(path)
                .append("invoices_");
        
        if(this.reportDao.getInvoiceDao() instanceof InvoiceInDAO) {
            stringBuffer.append("in");
        } else 
            if (this.reportDao.getInvoiceDao() instanceof InvoiceOutDAO) {
                stringBuffer.append("out");
            } else {
                throw new ClassCastException("Указан неверный класс для взаимодействия с базой данных.\nФормирование отчета отменено");
            }
        
        stringBuffer.append("_")
                .append(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")))
                .append(".txt");
        
        return stringBuffer.toString();
    }
    
    private String getPathOfFileChooser() {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Сохранить отчет как...");
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Текстовые файлы (*.txt)", "*.txt"));
        chooser.setInitialDirectory(new File(AppProperties.INSTANCE.getReportsPath()));
        chooser.setInitialFileName(generatePath(""));
        return chooser.showSaveDialog(null).getAbsolutePath();
    }
    
    private void createDirOrSkip() {
        File dir = new File(AppProperties.INSTANCE.getReportsPath());
        if(dir.exists()) {
        } else {
            if (dir.mkdir()) {
                createAlertInformation("Указанная в настройках приложения директория отсутствует.\nПо указанному расположению создана новая директория "+dir.getName()).showAndWait();
            } else {
                createAlertError("Невозможно создать новую директорию\nдля сохранения файлов отчетов").showAndWait();
            }
        }   
    }
    
    private Alert createAlertError(String message) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle("АРМ оператора ЭСЧФ");
        dialog.setHeaderText("Ошибка");
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.setContentText(message);
        return dialog;
    }
    
    private Alert createAlertWarning(String message) {
        Alert dialog = new Alert(Alert.AlertType.WARNING);
        dialog.setTitle("АРМ оператора ЭСЧФ");
        dialog.setHeaderText("Внимание");
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.setContentText(message);
        return dialog;
    }
    
    private Alert createAlertInformation(String message) {
        Alert dialog = new Alert(Alert.AlertType.INFORMATION);
        dialog.setTitle("АРМ оператора ЭСЧФ");
        dialog.setHeaderText("Информация");
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.setContentText(message);
        return dialog;
    }

}
