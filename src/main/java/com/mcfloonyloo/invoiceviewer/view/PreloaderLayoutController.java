/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.view;

import com.mcfloonyloo.invoiceviewer.MainApp;
import com.mcfloonyloo.invoiceviewer.OrgAppService;
import com.mcfloonyloo.invoiceviewer.dialog.ExceptionDialog;
import com.mcfloonyloo.invoiceviewer.dialog.YesNoDialog;
import com.mcfloonyloo.invoiceviewer.model.invoice.db.SupportInvoiceDAO;
import com.mcfloonyloo.invoiceviewer.model.preloader.db.KeyPreloaderDAO;
import com.mcfloonyloo.invoiceviewer.model.preloader.db.OrgPreloaderDAO;
import com.mcfloonyloo.invoiceviewer.model.preloader.db.OrgPreloaderDTO;
import com.mcfloonyloo.invoiceviewer.model.preloader.db.SupportPreloaderDAO;
import com.mcfloonyloo.invoiceviewer.service.InvoiceService;
import com.mcfloonyloo.invoiceviewer.service.PreloaderKeySelectorDialog;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Control;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author avolkov
 */
public class PreloaderLayoutController implements Initializable {

    private final static String DATABASE_FILENAME = "preloaderdb.db";
    
    @FXML
    private Button addButton;
    
    @FXML
    private Button loginButton;
    
    @FXML
    private Button closeButton;
    
    @FXML
    private TableView<OrgPreloaderDTO> tableView;
    
    @FXML
    private TableColumn<OrgPreloaderDTO, String> unpColumn;
    
    @FXML
    private TableColumn<OrgPreloaderDTO, String> orgNameColumn;
    
    private SimpleObjectProperty<OrgPreloaderDTO> org = new SimpleObjectProperty();
    
    private KeyStorePreloaderLayoutController controller;
    
    private final ObservableList<OrgPreloaderDTO> orgs = FXCollections.observableArrayList();
    
    public void setOrgProperty(SimpleObjectProperty org) {
        this.org = org;
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.orgs.clear();
        
        ArrayList<OrgPreloaderDTO> orgList = fillOrgListOrGeneratedDatabase();
        if(Optional.ofNullable(orgList).isPresent()) {
            this.orgs.addAll(orgList);        
        }
        this.tableView.setItems(this.orgs);
        this.tableView.setFixedCellSize(Control.USE_PREF_SIZE);
        
        this.loginButton.disableProperty().bind(this.tableView.getSelectionModel().selectedItemProperty().isNull());
        
        this.unpColumn.setCellValueFactory((param) -> {return new SimpleStringProperty(param.getValue().getUnp());});
        this.orgNameColumn.setCellValueFactory((param) -> {return new SimpleStringProperty(param.getValue().getName());});
    }    
    
    private ArrayList<OrgPreloaderDTO> fillOrgListOrGeneratedDatabase() {
        SupportPreloaderDAO support = new SupportPreloaderDAO(DATABASE_FILENAME);
        try {
            support.init();
            support.validateConnectionDatabase();
            this.addButton.setDisable(false);
            
            return fillOrgList();
        } catch (FileNotFoundException | SQLException | NullPointerException ex) {
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex.getLocalizedMessage(), ex) 
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText(ex.getClass().toString());
            exceptionDialog.showAndWait();
            
            String message = "При отсутствии файла базы данных доступ\nк возможностям приложения ограничен";
            YesNoDialog yesNoDialog = new YesNoDialog(Alert.AlertType.CONFIRMATION, message)
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText("Создать новый файл базы данных?");
            Optional<ButtonType> result = yesNoDialog.getResult();
            if(result.isPresent() && result.get() == yesNoDialog.getYes()) {
                try {
                    support.generateDatabase();
                    return fillOrgListOrGeneratedDatabase();
                } catch (Exception generateEx) {
                    ExceptionDialog generateExceptionDialog = new ExceptionDialog(generateEx.getLocalizedMessage()+"\nДоступ к возможностям приложения ограничен", generateEx) 
                            .setTitle("АРМ оператора ЭСЧФ")
                            .setHeaderText(generateEx.getClass().toString());
                    generateExceptionDialog.showAndWait();
                    return null;
                }
            } else {
                this.addButton.setDisable(true);
                return null;
            }
        }
    }

    @FXML
    private void handleLogin() {
        if(!this.tableView.getSelectionModel().isEmpty()) {
            if(isOrgValuesPresent(this.tableView.getSelectionModel().getSelectedItem())) {
                OrgPreloaderDTO orgPreloaderDTO = this.tableView.getSelectionModel().getSelectedItem();
                KeyPreloaderDAO keyPreloaderDAO = new KeyPreloaderDAO("preloaderdb.db");
                PreloaderKeySelectorDialog dialog = new PreloaderKeySelectorDialog(orgPreloaderDTO.getName(), keyPreloaderDAO.getKeys(orgPreloaderDTO));
                Optional<String> result = dialog.getResult();
                if(result.isPresent() && !result.get().isEmpty()) {
                    if (InvoiceService.INSTANCE.authorize(result.get().trim())) {
                        validateOrGenerateDatabaseFile(this.tableView.getSelectionModel().getSelectedItem());
                    }
                }
            } else {
                Alert alert = createAlertWarning("В записи организации отсутствуют значения некоторых полей.\nДоступ к приложению ограничен");
                alert.setHeaderText("Внимание");
                alert.showAndWait();
            }
        }
    }
    
    @FXML
    private void handleExit() {
        YesNoDialog yesNoDialog = new YesNoDialog(Alert.AlertType.CONFIRMATION, "Завершить работу приложения?")
                .setTitle("АРМ оператора ЭСЧФ")
                .setHeaderText("Запрос на завершение работы");
        Optional<ButtonType> result = yesNoDialog.getResult();
        if(result.isPresent() && result.get() == yesNoDialog.getYes()){
            //LOGGER.info("Завершение работы приложения");
            //LOGGER.info("---------------------------");
            System.exit(0);
        }
    }

    @FXML
    private void handleAddCertificate() {
        try {
            this.controller = getKeyStoreLayout();
            Optional<OrgPreloaderDTO> result = this.controller.showAndWait();
            if(result.isPresent()) {
                if(!contains(result.get())) {
                    this.orgs.add(result.get());
                    new SupportInvoiceDAO(result.get().getPathdb()).generateDatabase();
                } 
            }
        } catch (Exception ex) {
            ExceptionDialog dialog = new ExceptionDialog(ex.getLocalizedMessage(), ex);
            dialog.setTitle("АРМ оператора ЭСЧФ");
            dialog.setHeaderText(ex.getClass().getCanonicalName());
            dialog.showAndWait();
        } 
    }
    
    private boolean isOrgValuesPresent(OrgPreloaderDTO org) {
        return Optional.ofNullable(org.getUnp()).isPresent() &&
                Optional.ofNullable(org.getName()).isPresent() && 
                Optional.ofNullable(org.getPathdb()).isPresent();
    }

    private void validateOrGenerateDatabaseFile(OrgPreloaderDTO record) {
        if(Optional.ofNullable(record).isPresent()) {
            SupportInvoiceDAO support = new SupportInvoiceDAO(record.getPathdb());
            try {
                support.init();
                support.validateConnectionDatabase();

                OrgAppService.INSTANCE.setOrg(record);
                this.org.setValue(record);
            } catch (FileNotFoundException | SQLException | NullPointerException ex) {
                ExceptionDialog exceptionDialog = new ExceptionDialog(ex.getLocalizedMessage(), ex) 
                        .setTitle("АРМ оператора ЭСЧФ")
                        .setHeaderText(ex.getClass().toString());
                exceptionDialog.showAndWait();

                String message = "При отсутствии файла базы данных доступ\nк возможностям приложения ограничен";
                YesNoDialog yesNoDialog = new YesNoDialog(Alert.AlertType.CONFIRMATION, message)
                        .setTitle("АРМ оператора ЭСЧФ")
                        .setHeaderText("Создать новый файл базы данных?");
                Optional<ButtonType> result = yesNoDialog.getResult();
                if(result.isPresent() && result.get() == yesNoDialog.getYes()) {
                    try {
                        support.generateDatabase();
                        validateOrGenerateDatabaseFile(record);
                    } catch (Exception generateEx) {
                        ExceptionDialog generateExceptionDialog = new ExceptionDialog(generateEx.getLocalizedMessage()+"\nДоступ к возможностям приложения ограничен", generateEx) 
                                .setTitle("АРМ оператора ЭСЧФ")
                                .setHeaderText(generateEx.getClass().toString());
                        generateExceptionDialog.showAndWait();
                    }
                }
            }
        } else {
            Alert alert = createAlertError("Отсутствует запись организации.\nДоступ к приложению ограничен");
            alert.setHeaderText("Ошибка");
            alert.showAndWait();
        }
    }
        
    private boolean contains(OrgPreloaderDTO orgDto) {
        return this.orgs.stream().anyMatch((orgDtoFor) -> (
                orgDto.getUnp().equals(orgDtoFor.getUnp()) && orgDto.getName().equals(orgDtoFor.getName()))
        );
    }
    
    private KeyStorePreloaderLayoutController getKeyStoreLayout() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("view/KeyStorePreloaderLayout.fxml"));
        GridPane pane = loader.load();
        
        KeyStorePreloaderLayoutController keyStoreController = loader.getController();
                
        Stage stage = new Stage();
        stage.setScene(new Scene(pane, 700, 400));
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initStyle(StageStyle.UTILITY);
        keyStoreController.setStage(stage);
                
        return keyStoreController;
    }
    
    private Alert createAlertError(String message) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle("АРМ оператора ЭСЧФ");
        dialog.setContentText(message);
        return dialog;
    }
    
    private Alert createAlertWarning(String message) {
        Alert dialog = new Alert(Alert.AlertType.WARNING);
        dialog.setTitle("АРМ оператора ЭСЧФ");
        dialog.setContentText(message);
        return dialog;
    }
    
    private ArrayList<OrgPreloaderDTO> fillOrgList() {
        return new OrgPreloaderDAO(DATABASE_FILENAME).getAll(); 
    }
    
}
