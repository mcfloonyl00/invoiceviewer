/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.view;

import com.mcfloonyloo.invoiceviewer.dialog.AbstractUpdateTask;
import com.mcfloonyloo.invoiceviewer.dialog.UpdateProgressDialog;
import com.mcfloonyloo.invoiceviewer.model.UpdateInvoiceItem;
import com.mcfloonyloo.invoiceviewer.model.invoice.db.DefaultInvoiceStatusDAO;
import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Modality;

/**
 * FXML Controller class
 *
 * @author avolkov
 */
public class UpdateInvoiceLayoutController implements Initializable {

    @FXML
    private DatePicker fromPicker;
    
    @FXML
    private DatePicker toPicker;
    
    @FXML
    private MenuItem loadInvoiceItem;
    
    @FXML
    private MenuItem updateInvoiceItem;
    
    @FXML
    private TableView<UpdateInvoiceItem> tableView;
    
    @FXML
    private TableColumn<UpdateInvoiceItem, String> numberInvoiceColumn;
    
    @FXML
    private TableColumn<UpdateInvoiceItem, String> statusInvoiceColumn;
    
    @FXML
    private TableColumn<UpdateInvoiceItem, String> dateInvoiceColumn;
    
    private final ObservableList<UpdateInvoiceItem> invoices = FXCollections.observableArrayList();
    
    private DefaultInvoiceStatusDAO statusDao;
    
    private AbstractUpdateTask updateTask;

    public void setStatusDao(DefaultInvoiceStatusDAO statusDao) {
        this.statusDao = statusDao;
    }
    
    public void setUpdateTask(AbstractUpdateTask updateTask) {
        this.updateTask = updateTask;
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        this.numberInvoiceColumn.setCellValueFactory((TableColumn.CellDataFeatures<UpdateInvoiceItem, String> param) -> new SimpleStringProperty(param.getValue().getNumber()));
        this.statusInvoiceColumn.setCellValueFactory((TableColumn.CellDataFeatures<UpdateInvoiceItem, String> param) -> new SimpleStringProperty(param.getValue().getStatusRu()));
        this.dateInvoiceColumn.setCellValueFactory((TableColumn.CellDataFeatures<UpdateInvoiceItem, String> param) -> new SimpleStringProperty(param.getValue().getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))));

        this.tableView.setItems(invoices);
        this.updateInvoiceItem.disableProperty().bind(Bindings.size(invoices).lessThanOrEqualTo(0));
    }    
    
    @FXML
    public void handleLoadInvoices() {
        if(Optional.ofNullable(this.statusDao).isPresent()) {
            if(!Optional.ofNullable(this.fromPicker.getValue()).isPresent()) {
                Alert alert = createAlertWarning("Не выбрана дата начала диапазона. Загрузка ЭСЧФ отменена");
                alert.showAndWait();
                return;
            }
            if(!Optional.ofNullable(this.toPicker.getValue()).isPresent()) {
                Alert alert = createAlertWarning("Не выбрана дата конца диапазона. Загрузка ЭСЧФ отменена");
                alert.showAndWait();
                return;
            }
            if(this.fromPicker.getValue().isBefore(this.toPicker.getValue()) || this.fromPicker.getValue().isEqual(this.toPicker.getValue())) {
                this.invoices.clear();
                this.invoices.addAll(this.statusDao.getInvoicesAndStatuses(this.fromPicker.getValue(), this.toPicker.getValue()));
            } else {
                Alert alert = createAlertWarning("Неверно выбран диапазон дат. Загрузка списка ЭСЧФ отменена");
                alert.showAndWait();
            }
        } else {
            Alert alert = createAlertError("Не произведена инициализация инструмента для работы с базой данных.\nВозможности работы окна обновления статусов ограничена");
                alert.showAndWait();
        }
    }
    
    @FXML
    public void handleUpdateInvoices() {
        if(this.invoices.size() > 0) { 
            this.updateTask.setFileList(this.invoices);
            UpdateProgressDialog updateDialog = new UpdateProgressDialog("Выполняется обновление статусов...", this.invoices, this.updateTask);
            updateDialog.show();
        }
    }
    
    private Alert createAlertError(String message) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle("АРМ оператора ЭСЧФ");
        dialog.setHeaderText("Ошибка");
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.setContentText(message);
        return dialog;
    }
    
    private Alert createAlertWarning(String message) {
        Alert dialog = new Alert(Alert.AlertType.WARNING);
        dialog.setTitle("АРМ оператора ЭСЧФ");
        dialog.setHeaderText("Внимание");
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.setContentText(message);
        return dialog;
    }
}
