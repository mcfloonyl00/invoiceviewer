/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.view;

import by.avest.crypto.pkcs11.provider.universal.AvPersonalKeyListKeyStore;
import by.avest.crypto.pkcs11.provider.universal.AvPersonalKeyStore;
import by.avest.crypto.pkcs11.provider.universal.KeyListCallbackHandler;
import by.avest.crypto.pkcs11.provider.universal.KeyListLoadParameter;
import com.mcfloonyloo.invoiceviewer.MainApp;
import com.mcfloonyloo.invoiceviewer.dialog.ExceptionDialog;
import com.mcfloonyloo.invoiceviewer.model.AvKeyStoreItem;
import com.mcfloonyloo.invoiceviewer.model.StatusAvKeyStoreItem;
import com.mcfloonyloo.invoiceviewer.model.preloader.db.KeyPreloaderDAO;
import com.mcfloonyloo.invoiceviewer.model.preloader.db.OrgPreloaderDAO;
import com.mcfloonyloo.invoiceviewer.model.preloader.db.OrgPreloaderDTO;
import com.mcfloonyloo.invoiceviewer.model.preloader.db.OrgPreloaderItem;
import com.mcfloonyloo.invoiceviewer.properties.AppProperties;
import com.mcfloonyloo.invoiceviewer.service.InvoiceServiceAuthorizer;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import javafx.util.Callback;
import sun.security.x509.X509CertImpl;

/**
 * FXML Controller class
 *
 * @author avolkov
 */
public class KeyStorePreloaderLayoutController implements Initializable {

    @FXML
    private TableView<AvKeyStoreItem> tableView;
    
    @FXML
    private TableColumn<AvKeyStoreItem, String> aliasColumn;
    
    @FXML
    private TableColumn<AvKeyStoreItem, String> dateAfterColumn;
    
    @FXML
    private Button ok;
    
    @FXML
    private Button cancel;
    
    private final ObjectProperty<Button> pressButtonProperty = new SimpleObjectProperty<>();
    
    private OrgPreloaderItem orgItem;
    
    private Stage primaryStage;
    
    public Stage getPrimaryStage() {
        return this.primaryStage;
    }
        
    public void setStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }
       
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        setTableViewProperties();
        
        setTableColumnsProperties();
        
        setOkProperties();
        setCancelProperties();
    }    
    
    public Optional<OrgPreloaderDTO> showAndWait() throws FileNotFoundException {
        this.primaryStage.showAndWait();
        if(Optional.ofNullable(this.pressButtonProperty.get()).isPresent()) {
            if(this.pressButtonProperty.get() == this.ok) {
                OrgPreloaderDAO orgDao = new OrgPreloaderDAO("preloaderdb.db");
                if(orgDao.isRecord(this.orgItem.getUnp())) {
                    OrgPreloaderDTO orgDto = orgDao.addKey(this.orgItem.getUnp(), this.tableView.getSelectionModel().getSelectedItem());
                    return Optional.ofNullable(orgDto);
                } else {
                    OrgPreloaderDTO orgDto = orgDao.addOrgKey(this.orgItem, this.tableView.getSelectionModel().getSelectedItem());
                    return Optional.ofNullable(orgDto);
                }
            } else {
                return Optional.empty();
            }
        } else {
            return Optional.empty();
        }  
    }
    
    private ObservableList<AvKeyStoreItem> fill() {
        ObservableList<AvKeyStoreItem> keys = FXCollections.observableArrayList();
        try {
            AvPersonalKeyStore keyStore = createKeyStore();
            Enumeration<String> aliases = keyStore.engineAliases();
            while(aliases.hasMoreElements()) {
                keys.add(createKeyStoreItem(aliases.nextElement(), keyStore));
            }
        } catch (IOException | NoSuchAlgorithmException | CertificateException ex) {
            ExceptionDialog dialog = new ExceptionDialog(ex.getLocalizedMessage(), ex);
            dialog.setTitle("АРМ оператора ЭСЧФ");
            dialog.setHeaderText(ex.getClass().getCanonicalName());
            dialog.showAndWait();
            keys = null;
        }
        return keys;
    }
    
    private void setTableViewProperties() {
        this.tableView.setFixedCellSize(25);
        this.tableView.setItems(fill());
        this.tableView.setRowFactory(getRowFactory());
        this.tableView.getStylesheets().addAll(MainApp.class.getResource("view/KeyStorePreloaderLayout.css").toExternalForm());
        this.tableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            switch(newValue.getStatus()) {
                case ADDED_TO_DB : { 
                    this.ok.setDisable(false);
                    break;
                }
                default : {
                    this.ok.setDisable(true);
                }
            }
        });
    }
    
    private void setTableColumnsProperties() {
        this.aliasColumn.setCellValueFactory((param) -> {
            return new SimpleObjectProperty<>(param.getValue().getAlias()); 
        });
        
        this.dateAfterColumn.setCellValueFactory((param) -> {
            return new SimpleObjectProperty<>(param.getValue().getDateAfter());
        });
    }
    
    private void setOkProperties() {
        this.ok.setDisable(true);
        this.ok.setOnAction((event) -> {
            InvoiceServiceAuthorizer authorizer = new InvoiceServiceAuthorizer(this.tableView.getSelectionModel().getSelectedItem().getAlias());
            this.orgItem = authorizer.authorize(AppProperties.INSTANCE.getServiceUrl());
            if(Optional.ofNullable(this.orgItem).isPresent()) {
                this.primaryStage.close();
            }
        });
        this.ok.pressedProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue) {
                if(Optional.ofNullable(this.pressButtonProperty.get()).isPresent()){ 
                    this.pressButtonProperty.set(null);
                } else {
                    this.pressButtonProperty.set(this.ok);
                }
            }
        });
    }
    
    private void setCancelProperties() {
        this.cancel.setOnAction((event) -> {
            this.primaryStage.close();
        });
        this.cancel.pressedProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue) {
                if(Optional.ofNullable(this.pressButtonProperty.get()).isPresent()){ 
                   this.pressButtonProperty.set(null);
                } else {
                    this.pressButtonProperty.set(this.cancel);
                }
            }
        });
    }
    
    private AvKeyStoreItem createKeyStoreItem(String alias, AvPersonalKeyStore keyStore) throws IOException, NoSuchAlgorithmException, CertificateException {
        String after = new SimpleDateFormat("dd.MM.yyyy").format(((X509CertImpl) keyStore.engineGetCertificate(alias)).getNotAfter());
        AvKeyStoreItem item = new AvKeyStoreItem(alias, after);
        item.setStatus(getStatusKey(keyStore, item));
        return item;
    }
    
    private StatusAvKeyStoreItem getStatusKey(AvPersonalKeyStore keyStore, AvKeyStoreItem item) throws IOException, NoSuchAlgorithmException, CertificateException {
        if(((X509CertImpl) keyStore.engineGetCertificate(item.getAlias())).getNotAfter().before(new Date())) {
            return StatusAvKeyStoreItem.CERTIFICATE_EXPIRED;
        } else {
            AvPersonalKeyListKeyStore keyList = createKeyList();
            Enumeration<String> aliasKeyList = keyList.engineAliases();
            while(aliasKeyList.hasMoreElements()) {
                String aliasKeyItem = aliasKeyList.nextElement();
                if(item.getAlias().equals(aliasKeyItem)) {
                    if(new KeyPreloaderDAO("preloaderdb.db").isRecord(item)) {
                        return StatusAvKeyStoreItem.AVAILABLE_FOR_UPLOAD;
                    } else {
                        return StatusAvKeyStoreItem.ADDED_TO_DB;
                    }
                }
            }
            return StatusAvKeyStoreItem.NO_INFORMATION_CARRIER;
        }
    }
    
    private AvPersonalKeyStore createKeyStore() throws IOException, NoSuchAlgorithmException, CertificateException, CertificateException {
        AvPersonalKeyStore keyStore = new AvPersonalKeyStore();
        keyStore.engineLoad(null);
        return keyStore;
    }
    
    private static AvPersonalKeyListKeyStore createKeyList() throws IOException, NoSuchAlgorithmException, CertificateException, CertificateException {
        AvPersonalKeyListKeyStore keyStore = new AvPersonalKeyListKeyStore();
        keyStore.engineLoad(new KeyListLoadParameter(new KeyStore.CallbackHandlerProtection(new KeyListCallbackHandler())));
        return keyStore;
    }
    
    private Callback<TableView<AvKeyStoreItem>, TableRow<AvKeyStoreItem>> getRowFactory() {
        return (TableView<AvKeyStoreItem> callbackTableView) -> {
            TableRow<AvKeyStoreItem> row = new TableRow<AvKeyStoreItem>(){
                
                @Override
                protected void updateItem(AvKeyStoreItem item, boolean empty) {
                    super.updateItem(item, empty); 
                    if(empty) {
                        setGraphic(null);
                    } else {
                        for(StatusAvKeyStoreItem value : StatusAvKeyStoreItem.values()) {
                            getStyleClass().removeAll(value.get());
                            if(value.get().contains(item.getStatus().get())) {
                                getStyleClass().add(item.getStatus().get());
                            }
                        }
                        setDisabled(item.getStatus().isDisabled());
                    }
                }
                
            };
            return row;
        };
    }
    
}
