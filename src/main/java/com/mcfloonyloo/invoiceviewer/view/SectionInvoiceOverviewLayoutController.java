/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcfloonyloo.invoiceviewer.view;

import com.mcfloonyloo.invoiceviewer.dialog.ExceptionDialog;
import com.mcfloonyloo.invoiceviewer.model.Title;
import com.mcfloonyloo.invoiceviewer.model.invoice.DefaultInvoiceSection;
import com.mcfloonyloo.invoiceviewer.model.invoice.DefaultInvoiceSubSection;
import com.mcfloonyloo.invoiceviewer.model.invoice.DefaultInvoiceSubSectionItem;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Tooltip;
import javafx.scene.text.Text;
import javafx.util.Callback;

/**
 * FXML Controller class
 *
 * @author avolkov
 */
public class SectionInvoiceOverviewLayoutController implements Initializable {

    private static final int PRIVATE_FINAL = Modifier.PRIVATE+Modifier.FINAL;
    
    private Text text;
    
    private Label label;
    
    @FXML
    private TitledPane pane;
    
    @FXML
    private TableView<Title<String>> table;
    
    @FXML
    private TableColumn<Title<String>, String> nameColumn;
    
    @FXML
    private TableColumn<Title<String>, String> valueColumn;
    
    private Title section;
    
    public void setSection(Title section) {
        this.section = section;
    }
    
    public SectionInvoiceOverviewLayoutController() {
        
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
    }
    
    public TitledPane createPane() {
        try { 
            
            this.nameColumn.setCellValueFactory((TableColumn.CellDataFeatures<Title<String>, String> param) -> new SimpleStringProperty(param.getValue().getTitle()));
            this.valueColumn.setCellValueFactory((TableColumn.CellDataFeatures<Title<String>, String> param) -> new SimpleStringProperty(param.getValue().getValue()));
            
            NameTableColumnCallback nameCallback = new NameTableColumnCallback();
            this.nameColumn.setCellFactory(nameCallback);
            ValueTableColumnCallback valueCallback = new ValueTableColumnCallback();
            this.valueColumn.setCellFactory(valueCallback);
            
            ObservableList<Title<String>> list = FXCollections.observableArrayList();
            list.addAll(getListTitlesOfSection(this.section));
            
            this.table.setItems(list);
            this.table.setFixedCellSize(-1);
            
            this.pane.setText(this.section.getTitle());
            
            return this.pane;
        } catch (IllegalArgumentException | IllegalAccessException ex) {
            String message = ex.getLocalizedMessage();
            ExceptionDialog dialog = new ExceptionDialog(message, ex)
                    .setTitle("АРМ оператора ЭСЧФ")
                    .setHeaderText(ex.getClass().toString());
            dialog.showAndWait();
            return null;
        }
    }
    
    private List<Title<String>> getListTitlesOfSection(Title item) throws IllegalArgumentException, IllegalAccessException {
        List<Title<String>> list = new ArrayList<>();
        
        if(Optional.ofNullable(item.getValue()).isPresent()) {
            for(Field field : item.getValue().getClass().getDeclaredFields()) {
                if(field.getType() == Title.class && (field.getModifiers() == Modifier.PRIVATE || field.getModifiers() == PRIVATE_FINAL)) {
                    field.setAccessible(true);
                    Title internalItem = (Title) field.get(item.getValue());
                    list.addAll(getListTitlesOfSubSection(internalItem));
                }
            } 
        }
        
        return list;
    }
    
    private List<Title<String>> getListTitlesOfSubSection(Title item) throws IllegalArgumentException, IllegalAccessException {
        List<Title<String>> list = new ArrayList<>();
         
        if(Optional.ofNullable(item.getValue()).isPresent()) {
            if(item.getValue() instanceof String) {
                list.add(item);
            }
            
            if(item.getValue() instanceof Title ||
                    item.getValue() instanceof DefaultInvoiceSection || 
                    item.getValue() instanceof DefaultInvoiceSubSection || 
                    item.getValue() instanceof DefaultInvoiceSubSectionItem) {
                if(item.getTitle().length() > 0) {
                    list.add(new Title(item.getTitle()+":", true));
                }
                list.addAll(getListTitlesOfSection(item));
            }
            
            if(item.getValue() instanceof ArrayList) {
                if(item.getTitle().length() > 0) {
                    list.add(new Title(item.getTitle()+":", true));
                }
                for(Title internalItem : (ArrayList<Title>) item.getValue()) {
                    list.addAll(getListTitlesOfSubSection(internalItem));
                }
            }
        } else {
            if(item.isOptional()) {
                if(item.getTitle().length() > 0) {
                    list.add(new Title(item.getTitle(), true));
                }
            }
        }
         
        return list;
    }
    
    private class NameTableColumnCallback implements Callback<TableColumn<Title<String>, String>, TableCell<Title<String>, String>> {

        @Override
        public TableCell<Title<String>, String> call(TableColumn<Title<String>, String> param) {
            TableCell<Title<String>, String> cell = new TableCell<Title<String>, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    this.setText("");
                    Tooltip tooltip = new Tooltip(item);
                    tooltip.setWrapText(true);
                    this.setTooltip((empty || item == null) ? null : tooltip);
                }
            };
            
            label = new Label();
            label.setStyle("");
            cell.setGraphic(label);
            cell.setPrefHeight(Control.USE_COMPUTED_SIZE);
            label.textProperty().bind(cell.itemProperty());
            label.setWrapText(true);
                        
            return cell;
        }

    }
    
    private class ValueTableColumnCallback implements Callback<TableColumn<Title<String>, String>, TableCell<Title<String>, String>> {

        @Override
        public TableCell<Title<String>, String> call(TableColumn<Title<String>, String> param) {
            TableCell<Title<String>, String> cell = new TableCell<Title<String>, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    this.setText("");
                    Tooltip tooltip = new Tooltip(item);
                    tooltip.setWrapText(true);
                    this.setTooltip((empty || item == null) ? null : tooltip);
                }
            };
            
            label = new Label();
            label.setStyle("");
            cell.setGraphic(label);
            cell.setPrefHeight(Control.USE_COMPUTED_SIZE);
            label.textProperty().bind(cell.itemProperty());
            label.setWrapText(true);
                        
            return cell;
        }

    }
}
